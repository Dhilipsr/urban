<div class="profile-content">
		<div class="portlet row light">
		<div class="col-md-12 portlet-title tabbable-line clearfix">
		<div class="caption caption-md">
				<span class="caption-subject"><?php  esc_html_e('Dashbord','epfitness');?> </span>
		</div>


		<div style="padding-left:11em; padding-left:10em;  font-weight: 900;font-size:23px">Welcome to Urban Active Fitness</div>
</div>
<div style="display:flex; padding-left:12px";>
    <div style="padding-bottom: 1em;">
			<a class="onelink" href="https://www.a2000demo.com/urbannew/my-account/">
<i class="fas fa-clipboard-check fa-3x"></i>
			<?php esc_html_e('PT Sessions','epfitness'); ?> </a>
		</div>
<div class="one" style="padding-bottom: 1em; padding-left:45px;">
			<a  href="https://www.a2000demo.com/urbannew/my-account/?fitprofile=records">
		<i class="fas fa-clipboard-list fa-3x"></i>
			<?php esc_html_e('Client Progression','epfitness'); ?> </a>
		</div>
    <div class="two" style="padding-bottom: 1em; padding-left:45px;">
			<a href="https://www.a2000demo.com/urbannew/my-account/?fitprofile=workouts">
		<i class="fas fa-dumbbell fa-3x"></i>
			<?php esc_html_e('workout Plans','epfitness'); ?> </a>
		</div>
</div>



<div style="display:flex; padding-left:12px";>
    
    
    <div style="padding-bottom: 1em; padding-top:30px;">
			<a href="https://www.a2000demo.com/urbannew/my-account/?fitprofile=diet-plans">
			<i class="fas fa-calculator fa-3x"></i>
			<?php esc_html_e('Diet Plans','epfitness'); ?> </a>
		</div>
		
		<div style="padding-bottom: 1em; padding-left:53px; padding-top:30px;">
			<a href="https://www.a2000demo.com/urbannew/my-account/?fitprofile=my-report">
			<i class="fas fa-file-alt fa-3x"></i>
			<?php esc_html_e('My Reports','epfitness'); ?> </a>
		</div>
    
 </div>  
    



</div>
</div>
