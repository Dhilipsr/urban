<?php 
class MJ_Gmgt_Login
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}

	public function redirectMethod()
	{
		if($_REQUEST['gmgt_json_api'] == 'user_login')
		{
	       	$json = file_get_contents('php://input');
		   	$data = json_decode($json);
		   	$response=$this->userLogin($data->username,$data->password);
		   	// $response=$this->userLogin($_REQUEST['username'],$_REQUEST['password']);
		   	if(is_array($response))
		   	{
				echo json_encode($response);
		   	}
		   	else
		   	{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}

	public function userLogin($username,$password)
	{
	    if(!empty($username) AND !empty($password) )
	    {		 
			$response=array();
			if (filter_var($username, FILTER_VALIDATE_EMAIL))	
			{ //Invalid Email
	            $user = get_user_by('email', $username);
	        }
			else 
			{
			    $user = get_user_by('login', $username);
			}
			if(get_user_meta($user->ID, 'gmgt_hash', true))
			{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Your account is inactive. Contact your administrator to activate it.",'gym_mgt');
					$response['result']='';
					return $response;
			}
			else
			{
				if($user)
				{
					if(!wp_check_password($password, $user->user_pass, $user->ID))
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("Username/Password incorrect",'gym_mgt');
						$response['result']='';
						return $response;
					}
					else
					{
						$tokan=base64_encode(openssl_random_pseudo_bytes(64));
						$access_tokan = update_user_meta( $user->ID, 'access_token', $tokan );
						$retrived_data=get_userdata($user->ID);
						$user_roles=$retrived_data->roles;
						$user_data['id']=$retrived_data->ID;
						$user_data['access_token']=$tokan;
						$user_data['display_name']=$retrived_data->display_name;
						$user_data['email']=$retrived_data->user_email;
						$user_data['image']=$retrived_data->gmgt_user_avatar;
						$user_data['role_name']=$user_roles[0];
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Successfully login",'gym_mgt');
						$response['result']=$user_data;
						return $response;
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Username/Password incorrect",'gym_mgt');
					$response['result']='';
					return $response;
				}
			}
		}	
	    else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("Authentication credentials were missing",'gym_mgt');
			$response['result']='';
			return $response;
			
		}		 
	}
} ?>