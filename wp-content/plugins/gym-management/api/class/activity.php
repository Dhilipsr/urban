<?php 
class MJ_Gmgt_Activity_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Add Activity
		if($_REQUEST['gmgt_json_api'] == 'add_activity')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='add';
			$response=$this->MJ_gmgt_add_activity_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Activity
		if($_REQUEST['gmgt_json_api'] == 'edit_activity')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='edit';
			$response=$this->MJ_gmgt_add_activity_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Activity list
		if($_REQUEST['gmgt_json_api'] == 'activity_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_activity_list_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Activity
		if($_REQUEST['gmgt_json_api'] == 'single_activity')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_activity_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Activity
		if($_REQUEST['gmgt_json_api'] == 'delete_activity')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_activity_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Add-Edit Activity Function
	function MJ_gmgt_add_activity_data($data,$action)
	{
		$response=array();
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'activity');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		global $wpdb;
		$obj_activity=new MJgmgt_activity;
		$obj_membership=new MJgmgt_membership;
		$table_activity = $wpdb->prefix. 'gmgt_activity';
		$table_gmgt_membership_activities = $wpdb->prefix. 'gmgt_membership_activities';
		$activitydata['activity_cat_id']=$data['activity_category'];
		$activitydata['activity_title']=MJgmgt_strip_tags_and_stripslashes($data['activity_title']);
		$activitydata['activity_assigned_to']=$data['assign_to_staffmember'];
		$activitydata['activity_added_date']=date("Y-m-d");
		$activitydata['activity_added_by']=$data['current_user_id'];
		if ($data['access_token'] == $access_token)
		{
			if($action == 'edit')
			{
				if($menu_access_data['edit'] == '1')
				{
					$activityid['activity_id']=$data['activity_id'];
					$result=$wpdb->update( $table_activity, $activitydata ,$activityid);
					if(!empty($data['membership_id']))
					{
						$obj_activity->MJgmgt_delete_activity_membership($data['activity_id']);
						foreach($data['membership_id'] as $val)
						{					
							$assignactivitydata['activity_id']=$data['activity_id'];
							$assignactivitydata['membership_id']=$val;
							$assignactivitydata['created_date']=date("Y-m-d");
							$assignactivitydata['created_by']=$data['current_user_id'];
							
							$wpdb->insert( $table_gmgt_membership_activities, $assignactivitydata );
							$obj_membership->MJgmgt_update_membership_activity_category($val,$data['activity_category']);					
						}
					}
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully updated","gym_mgt");
					}
					else
					{
						$response['status']=0;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
			else
			{
				if($menu_access_data['add'] == '1')
				{
					$result=$wpdb->insert( $table_activity, $activitydata );
					$activity_id=$wpdb->insert_id;
					if(!empty($data['membership_id']))
					{
						foreach($data['membership_id'] as $val)
						{
							$assignactivitydata['activity_id']=$activity_id;
							$assignactivitydata['membership_id']=$val;
							$assignactivitydata['created_date']=date("Y-m-d");
							$assignactivitydata['created_by']=$data['current_user_id'];
							
							$wpdb->insert( $table_gmgt_membership_activities, $assignactivitydata );
							$obj_membership->MJgmgt_update_membership_activity_category($val,$data['activity_category']);
						}
					}
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully inserted","gym_mgt");
					}
					else
					{
						$response['status']=0;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Activity List Function
	public function MJ_gmgt_activity_list_data($data)
	{
		$obj_activity=new MJgmgt_activity;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'activity');
		$access_token = get_user_meta($user_id , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				$activitydata=$obj_activity->MJ_gmgt_get_all_activity_by_activity_added_by($user_id);
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$activitydata=$obj_activity->MJgmet_all_activity();
			}
			else
			{
				$activitydata="";
			}
			
			$response	=	array();

			if(!empty($activitydata))
			{	
				$i=0;
				foreach ($activitydata as $retrieved_data)
				{		
					$result[$i]['activity_id'] =	$retrieved_data->activity_id;
					$result[$i]['activity_category'] =	get_the_title($retrieved_data->activity_cat_id);
					$result[$i]['activity_title'] = $retrieved_data->activity_title;
					$user=get_userdata($retrieved_data->activity_assigned_to);
					$result[$i]['activity_trainer'] = $user->display_name;
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Single Activity Function
	public function MJ_gmgt_single_activity_data($data)
	{
		$obj_activity=new MJgmgt_activity;
		$result =  $obj_activity->MJgmgt_get_single_activity($data['activity_id']);
		$user_id=$data['current_user_id'];
		$access_token = get_user_meta($user_id , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($result))
			{			
				$activitydata['activity_id'] = $result->activity_id;
				$activitydata['activity_category'] =$result->activity_cat_id;
				$activitydata['activity_title'] = $result->activity_title;
				$activitydata['assign_to_staffmember'] = $result->activity_assigned_to;
				$getmembership_array=$obj_activity->MJgmgt_get_activity_membership($result->activity_id);
				
				$activitydata['membership_id'] = $getmembership_array;

				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$activitydata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Delete Activity Function
	public function MJ_gmgt_delete_activity_data($data)
	{
		$response=array();
		$obj_activity=new MJgmgt_activity;
		$result=$obj_activity->MJgmgt_delete_activity($data['activity_id']);
		$user_id=$data['current_user_id'];
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'activity');
		$access_token = get_user_meta($user_id , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['delete'])
			{
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully deleted",'gym_mgt');
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Record not found",'gym_mgt');
					$response['result']='';
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}	
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>