<?php 
class MJ_Gmgt_Group_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	} 
	public function redirectMethod()
	{
		//error_reporting(0);
		//Add Group 
		if($_REQUEST['gmgt_json_api'] == 'add_group')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='add';
			$response=$this->MJ_gmgt_add_group_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Group
		if($_REQUEST['gmgt_json_api'] == 'edit_group')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='edit';
			$response=$this->MJ_gmgt_add_group_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Group list
		if($_REQUEST['gmgt_json_api'] == 'group_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_group_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Group
		if($_REQUEST['gmgt_json_api'] == 'single_group')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_group_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Group
		if($_REQUEST['gmgt_json_api'] == 'delete_group')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_group_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Group Member
		if($_REQUEST['gmgt_json_api'] == 'view_group_member')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_view_group_member($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Add-Edit Group Function
	function MJ_gmgt_add_group_data($data,$action)
	{
		$response=array();
		$user_id = $data['current_user_id'];
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'group');
		$access_token = get_user_meta($user_id, 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(isset($_FILES['group_image']) && !empty($_FILES['group_image']) && $_FILES['group_image']['size'] !=0)
			{
				if($_FILES['group_image']['size'] > 0)
				{
					 $member_image=MJ_gmgt_load_documets($_FILES['group_image'],'group_image','pimg');
					 $member_image_url=content_url().'/uploads/gym_assets/'.$member_image;
				}
			}
			else
			{		
				if(isset($data['hidden_group_image']))
				{
					$member_image=$data['hidden_group_image'];
					$member_image_url=$member_image;
				}
			}
			global $wpdb;
			$table_group = $wpdb->prefix. 'gmgt_groups';
			$groupdata['group_name']=MJgmgt_strip_tags_and_stripslashes($data['group_name']);
			$groupdata['group_description']=MJgmgt_strip_tags_and_stripslashes($data['group_description']);
			$groupdata['gmgt_groupimage']=$member_image_url;
			$groupdata['created_date']=date("Y-m-d");
			$groupdata['created_by']=$data['current_user_id'];		
		
			if($action == 'edit')
			{
				if($menu_access_data['edit'] == '1')
				{
					$groupid['id']=$data['group_id'];
					$result=$wpdb->update( $table_group, $groupdata ,$groupid);
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully updated","gym_mgt");
					}
					else
					{
						$response['status']=0;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
			else
			{
				if($menu_access_data['add'] == '1')
				{
					$result=$wpdb->insert( $table_group, $groupdata );
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully inserted","gym_mgt");
					}
					else
					{
						$response['status']=0;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Group List Function
	public function MJ_gmgt_group_list_data($data)
	{
		$obj_group=new MJgmgt_group;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		$access_token = get_user_meta($user_id , 'access_token' , true);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'group');
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$groupdata=$obj_group->MJgmgt_get_member_all_groups($user_id);	
				}
				elseif($role == 'staff_member' || $role == 'accountant')
				{	
					$groupdata=$obj_group->MJgmgt_get_all_groups_by_created_by($user_id);	
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$groupdata=$obj_group->MJgmgt_get_all_groups();
			}
			else
			{
				$groupdata="";
			}
			
			$response	=	array();

			if(!empty($groupdata))
			{	
				$i=0;
				foreach ($groupdata as $retrieved_data)
				{		
					$result[$i]['group_id'] =	$retrieved_data->id;
					$result[$i]['group_name'] =	$retrieved_data->group_name;
					$result[$i]['group_description'] = $retrieved_data->group_description;
					$groupimage=$retrieved_data->gmgt_groupimage;
					if(empty($groupimage))
					{
						$image=get_option( 'gmgt_system_logo');
					}
					else
					{
						$image=$groupimage;
					}
					$result[$i]['group_image'] = $image;
					$result[$i]['total_group_member'] =	$obj_group->MJgmgt_count_group_members($retrieved_data->id);
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User",'gym_mgt');
		}
		return $response;
	}
	//Single Group Function
	public function MJ_gmgt_single_group_data($data)
	{
		
		$obj_group=new MJgmgt_group;
		$result =  $obj_group->MJgmgt_get_single_group($data['group_id']);
		$user_id = $data['current_user_id'];
		$access_token = get_user_meta($user_id , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($result))
			{
				$groupdata['group_id'] = $result->id;
				$groupdata['group_name'] =$result->group_name;
				$groupdata['group_description'] = $result->group_description;
				$groupimage=$result->gmgt_groupimage;
				if(empty($groupimage))
				{
					$image=get_option( 'gmgt_system_logo');
				}
				else
				{
					$image=$groupimage;
				}
				$groupdata['group_image'] = $image;
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$groupdata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User",'gym_mgt');
		}
		return $response;
	}
	//Delete Group Function
	public function MJ_gmgt_delete_group_data($data)
	{
		$response=array();
		$obj_group=new MJgmgt_group;
		$result=$obj_group->MJgmgt_delete_group($data['group_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		$user_id = $data['current_user_id'];
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'group');
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['delete'] == '1')
			{
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully deleted",'gym_mgt');
					$response['result']=Null;
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Record not found",'gym_mgt');
					$response['result']=Null;
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User",'gym_mgt');
			$response['result']=Null;
		}
		return $response;
	}
	//Single Group Function
	public function MJ_gmgt_view_group_member($data)
	{
		$response=array();
		$obj_group=new MJgmgt_group;
		$allmembers = MJgmgt_get_groupmember($data['group_id']);
		$user_id = $data['current_user_id'];
		$access_token = get_user_meta($user_id , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($allmembers))
			{
				$i = 0;
				foreach ($allmembers as $retrieved_data)
				{
					// $userid=$retrieved_data->member_id;
					$userimage=get_user_meta($retrieved_data->member_id, 'gmgt_user_avatar', true);
					if(empty($userimage))
					{
						$groupuserdata[$i]['member_image']=get_option( 'gmgt_system_logo');
					}
					else
					{
						$groupuserdata[$i]['member_image']=$userimage;
					}
					$groupuserdata[$i]['member_name'] = MJgmgt_get_display_name($retrieved_data->member_id);
					$i++;
					
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$groupuserdata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User",'gym_mgt');
		}
		return $response;
	}
}
?>