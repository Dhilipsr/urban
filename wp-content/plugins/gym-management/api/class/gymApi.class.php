<?php 
if ( ! class_exists( 'GymApi' ) )
{
	class GymApi
	{
		public function load()
		{
			$this->GymLoginAPI();	
			$this->GymDashboardAPI();	
			$this->GymMemberAPI();		
			$this->GymMembershipAPI();		
			$this->GymAccessRightAPI();		
			$this->GymGroupAPI();		
			$this->GymStafffmemberAPI();		
			$this->GymClassScheduleAPI();		
			$this->GymActivityAPI();		
			$this->GymProductAPI();		
			$this->GymAssignWorkoutAPI();		
			$this->GymWorkoutlogAPI();		
			$this->GymAccountantAPI();		
			$this->GymSalesAPI();		
			$this->GymReservationAPI();		
			$this->GymTaxAPI();		
			$this->GymAttendanceAPI();		
			$this->GymMembershipPaymentAPI();		
			$this->GymPaymentAPI();		
			$this->GymMessageAPI();		
			$this->GymNoticeAPI();
			$this->GymCommanAPI();
			$this->GymNutritionAPI();		
			$this->GymSubscriptionHistoryAPI();		
		}
		function GymLoginAPI()
		{
			$gym_login=new MJ_Gmgt_Login;
		}
		function GymDashboardAPI()
		{
			$gym_dashboard=new MJ_Gmgt_Dashboard_API;
		}
		function GymMemberAPI()
		{
			$gym_member=new MJ_Gmgt_member_API;
		}
		function GymMembershipAPI()
		{
			$gym_membership=new MJ_Gmgt_Membership_API;
		}
		function GymAccessRightAPI()
		{
			$gym_access_right=new MJ_Gmgt_AccessRight_API;
		}
		function GymGroupAPI()
		{
			$gym_group=new MJ_Gmgt_Group_API;
		}
		function GymStafffmemberAPI()
		{
			$gym_staff=new MJ_Gmgt_Stafffmember_API;
		}
		function GymClassScheduleAPI()
		{
			$gym_class_schedule=new MJ_Gmgt_ClassSchedule_API;
		}
		function GymActivityAPI()
		{
			$gym_activity=new MJ_Gmgt_Activity_API;
		}
		function GymProductAPI()
		{
			$gym_product=new MJ_Gmgt_Product_API;
		}
		function GymAssignWorkoutAPI()
		{
			$gym_assign_workout=new MJ_Gmgt_AssignWorkout_API;
		}
		function GymWorkoutlogAPI()
		{
			$gym_workout_log=new MJ_Gmgt_Workoutlog_API;
		}
		function GymAccountantAPI()
		{
			$gym_accountant=new MJ_Gmgt_Accountant_API;
		}
		function GymSalesAPI()
		{
			$gym_sales=new MJ_Gmgt_Sales_API;
		}
		function GymReservationAPI()
		{
			$gym_reservation=new MJ_Gmgt_Reservation_API;
		}
		function GymTaxAPI()
		{
			$gym_tax =new MJ_Gmgt_Tax_API;
		}	
		function GymAttendanceAPI()
		{
			$gym_attendance=new MJ_Gmgt_Attendance_API;
		}
		function GymMembershipPaymentAPI()
		{
			$gym_membership_payment =new MJ_Gmgt_MembershipPayment_API;
		}	
		function GymPaymentAPI()
		{
			$gym_payment =new MJ_Gmgt_Payment_API;
		}	
		function GymMessageAPI()
		{
			$gym_message=new MJ_Gmgt_Message_API;
		}
		function GymNoticeAPI()
		{
			$gym_notice =new MJ_Gmgt_Noticet_API;
		}
		function GymCommanAPI()
		{
			$gym_comman =new MJ_Gmgt_comman_API;
		}
		function GymNutritionAPI()
		{
			$gym_nutrition = new MJgmgt_Nutrition_API;
		}
		function GymSubscriptionHistoryAPI()
		{
			$gym_subscription_history = new MJ_Gmgt_Subscription_History_API;
		}
	}
}
?>