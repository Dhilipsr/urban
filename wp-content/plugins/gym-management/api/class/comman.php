<?php
class MJ_Gmgt_comman_API
{
	function __construct()
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		if($_REQUEST['gmgt_json_api'] == 'get_all_membership')
		{
			$response=$this->MJ_gmgt_get_all_membership();
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}

		if($_REQUEST['gmgt_json_api'] == 'get_class_by_membership')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_get_class_by_membership($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}

		if($_REQUEST['gmgt_json_api'] == 'get_all_staffmember')
		{
			$response=$this->MJ_gmgt_get_all_staffmember();
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		if($_REQUEST['gmgt_json_api'] == 'get_membership_days')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_get_membership_days($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	public function MJ_gmgt_get_all_membership()
	{
		// $currency_symbol = get_option( 'gmgt_currency_code');
		// $a = MJgmgt_get_currency_symbol($currency_symbol);
		$response=array();
		$obj_membership=new MJgmgt_membership;
		$result=$obj_membership->MJgmgt_get_all_membership();
		if (!empty($result))
		{
			$i=0;
			foreach ($result as $membership)
			{						
				$membership_data[$i]['membership_id'] = $membership->membership_id;
				$membership_data[$i]['membership_label'] = $membership->membership_label;
				$membership_data[$i]['membership_length_id'] = $membership->membership_length_id;
				$membership_data[$i]['currency_symbol'] = MJgmgt_get_currency_symbol_api(get_option( 'gmgt_currency_code'));
				$i++;
			}
			$response['status']=1;
			$response['error_code']=200;
			$response['result'] = $membership_data;
			return $response;
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['result']='';
			return $response;
		}
		return $response;
	}
	public function MJ_gmgt_get_class_by_membership($data)
	{
		$response=array();
		$result=$this->MJgmgt_get_class_by_membership($data['membership_id']);
		if (!empty($result))
		{
			$i=0;
			foreach ($result as $key=>$value)
			{						
				$class_data[$i]['class_id'] = $value->class_id;
				$class_data[$i]['class_name'] = MJgmgt_get_class_name($value->class_id);
				$i++;
			}
			$response['status']=1;
			$response['error_code']=200;
			$response['result'] = $class_data;
			return $response;
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['result']='';
			return $response;
		}
		return $response;
	}
	public function MJ_gmgt_get_all_staffmember()
	{
		$response=array();
		$get_staff = array('role' => 'Staff_member');
		$staffdata=get_users($get_staff);
		if (!empty($staffdata))
		{ 
			$i=0;
			foreach ($staffdata as $staff)
			{						
				$staffmember_data[$i]['staff_id'] = $staff->ID;
				$staffmember_data[$i]['staff_name'] = $staff->display_name;
				$i++;
			}
			$response['status']=1;
			$response['error_code']=200;
			$response['result'] = $staffmember_data; 
			return $response;
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['result']='';
			return $response;
		}
		return $response;
	}
	public function MJ_gmgt_get_membership_days($data)
	{
		$response=array();
		$result=$this->MJgmgt_get_membership_days($data['membership_id']);
		if (!empty($result))
		{
			$i=0;
			foreach ($result as $membership)
			{
				$membership_data[$i]['membership_days'] = $membership;
				$i++;
			}
			$response['status']=1;
			$response['error_code']=200;
			$response['result'] = $membership_data;
			return $response;
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['result']='';
			return $response;
		}
		return $response;
	}
	public function MJgmgt_get_class_by_membership($id)
	{
		global $wpdb;	
		$tbl_gmgt_membership_class = $wpdb->prefix."gmgt_membership_class";	
		$result = $wpdb->get_results("SELECT * FROM $tbl_gmgt_membership_class WHERE membership_id=$id");
		return $result;
	}
	//get membership days
	public function MJgmgt_get_membership_days($id)
	{
		global $wpdb;
		$table_membership = $wpdb->prefix. 'gmgt_membershiptype';
		$result = $wpdb->get_row("SELECT membership_length_id FROM $table_membership where membership_id=$id");
		return $result;	
	}
}
?>