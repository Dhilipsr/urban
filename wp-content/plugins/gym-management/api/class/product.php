<?php 
class MJ_Gmgt_Product_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		error_reporting(0);
		//Add Product 
		if($_REQUEST['gmgt_json_api'] == 'add_product')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='add';
			$response=$this->MJ_gmgt_add_product_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Product
		if($_REQUEST['gmgt_json_api'] == 'edit_product')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='edit';
			$response=$this->MJ_gmgt_add_product_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Product list
		if($_REQUEST['gmgt_json_api'] == 'product_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_product_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Product
		if($_REQUEST['gmgt_json_api'] == 'get_single_product')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_product_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Product
		if($_REQUEST['gmgt_json_api'] == 'delete_product')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_product_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Add-Edit Product Function
	function MJ_gmgt_add_product_data($data,$action)
	{
		$response=array();
		$obj_product=new MJgmgt_product;
		global $wpdb;
		$table_product = $wpdb->prefix. 'gmgt_product';
		$productdata['product_name']=MJgmgt_strip_tags_and_stripslashes($data['product_name']);
		$productdata['price']=$data['product_price'];
		$productdata['quentity']=$data['product_quantity'];
		$productdata['created_date']=date("Y-m-d");
		$productdata['created_by']=$data['current_user_id'];	
		$productdata['sku_number']=$data['sku_number'];
		$productdata['product_cat_id']=$data['product_category'];
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'product');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(isset($data['manufacture_company_name']))
			{
				$productdata['manufacture_company_name']=MJgmgt_strip_tags_and_stripslashes($data['manufacture_company_name']);
			}
			if(!empty($data['manufacturer_date']))
			{
				$productdata['manufacture_date']=MJgmgt_get_format_for_db($data['manufacturer_date']);
			}
			else
			{
				$productdata['manufacture_date']=null;
			}	
			$productdata['product_description']=MJgmgt_strip_tags_and_stripslashes($data['product_description']);	

			$productdata['product_specification']=MJgmgt_strip_tags_and_stripslashes($data['product_specification']);	
			if(isset($_FILES['product_image']) && !empty($_FILES['product_image']) && $_FILES['product_image']['size'] !=0)
			{
					
				if($_FILES['product_image']['size'] > 0)
				{
					 $product_image=MJgmgt_load_documets($_FILES['product_image'],'product_image','pimg');
					 $product_image_url=content_url().'/uploads/gym_assets/'.$product_image;
				}
			}
			else
			{			
				if(isset($data['hidden_product_image']))
				{
					$product_image=$data['hidden_product_image'];
					$product_image_url=$product_image;
				}
			}
			$productdata['product_image']=$product_image_url;			
		    $ext=MJgmgt_check_valid_extension($product_image_url);
			if(!$ext == 0)
			{
				if($action == 'edit')
				{
					if($menu_access_data['edit'] == '1')
					{
						$data3=$obj_product->MJgmgt_get_all_product_by_name_count($data['product_name'],$data['product_id']);
						$data1=$obj_product->MJgmgt_get_all_product_by_sku_number_count($data['sku_number'],$data['product_id']);
						$data2=$obj_product->MJgmgt_get_all_product_by_name_and_sku_number_Count($data['product_name'],$data['sku_number'],$data['product_id']);
						if(!empty($data2))
						{
							$response['status']=0;
							$response['error']=__("This product name and SKU Number already Use so please enter another product name and SKU Number.",'gym_mgt');
						}
						else
						{
							if(!empty($data3))
							{
								$response['status']=0;
								$response['error']=__("This product name already store so please enter another product name.",'gym_mgt');
							}				
							elseif(!empty($data1))
							{
								$response['status']=0;
								$response['error']=__("This SKU Number already Use so please enter another SKU Number.",'gym_mgt');
							}				
							else
							{
								$productid['id']=$data['product_id'];
								$result=$wpdb->update( $table_product, $productdata ,$productid);
								if($result)
								{
									$response['status']=1;
									$response['error_code']=200;
									$response['error']=__("Record successfully updated","gym_mgt");
								}
								else
								{
									$response['status']=0;
									$response['error']=__("Please Fill All Fields",'gym_mgt');
								}
							}
						}
					}
					else
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("You Can't Access This Page","gym_mgt");
						$response['result']='';
						return $response;
					}
				}
				else
				{
					if($menu_access_data['add'] == '1')
					{
						$data3=$obj_product->MJgmgt_get_all_product_by_name($data['product_name']);
						$data1=$obj_product->MJgmgt_get_all_product_by_sku_number($data['sku_number']);
						$data2=$obj_product->MJgmgt_get_all_product_by_name_and_sku_number($data['product_name'],$data['sku_number']);
						if(!empty($data2))
						{
							$response['status']=0;
							$response['error']=__("This product name and SKU Number already Use so please enter another product name and SKU Number.",'gym_mgt');
						}
						else
						{
							if(!empty($data3))
							{
								$response['status']=0;
								$response['error']=__("This product name already store so please enter another product name.",'gym_mgt');
							}				
							elseif(!empty($data1))
							{
								$response['status']=0;
								$response['error']=__("This SKU Number already Use so please enter another SKU Number.",'gym_mgt');
							}				
							else
							{
								$result=$wpdb->insert( $table_product, $productdata );
								if($result)
								{
									$response['status']=1;
									$response['error_code']=200;
									$response['error']=__("Record successfully inserted","gym_mgt");
								}
								else
								{
									$response['status']=0;
									$response['error']=__("Please Fill All Fields",'gym_mgt');
								}
							}
						}
					}
					else
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("You Can't Access This Page","gym_mgt");
						$response['result']='';
						return $response;
					}
				}
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Sorry, only JPG, JPEG, PNG & GIF And BMP files are allowed.",'gym_mgt');
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Product List Function
	public function MJ_gmgt_product_list_data($data)
	{
		$obj_product=new MJgmgt_product;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'product');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				$productdata=$obj_product->MJgmgt_get_all_product_by_created_by($user_id);
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$productdata=$obj_product->MJgmgt_get_all_product();
			}
			else
			{
				$productdata="";
			}
			$response = array();
			if(!empty($productdata))
			{	
				$i=0;
				foreach ($productdata as $retrieved_data)
				{		
					if(empty($retrieved_data->product_image))
					{
						$product_image=get_option( 'gmgt_system_logo' );
					}
					else
					{
						$product_image=$retrieved_data->product_image;
					}
					$result[$i]['product_id'] =	$retrieved_data->id;
					$result[$i]['product_image'] =	$product_image;
					$result[$i]['product_name'] =	$retrieved_data->product_name;
					$result[$i]['sku_number'] = $retrieved_data->sku_number;
					$result[$i]['product_category'] = get_the_title($retrieved_data->product_cat_id);
					$result[$i]['product_price'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.$retrieved_data->price;
					$result[$i]['product_quantity'] = $retrieved_data->quentity;
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Single Product Function
	public function MJ_gmgt_single_product_data($data)
	{
		$obj_product=new MJgmgt_product;
		$result =  $obj_product->MJgmgt_get_single_product($data['product_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($result))
			{		
				$productdata['product_id'] = $result->id;
				$productdata['product_image'] =$result->product_image;
				$productdata['product_name'] = $result->product_name;
				$productdata['sku_number'] = $result->sku_number;
				$productdata['product_category'] = $result->product_cat_id;
				$productdata['product_price'] = $result->price;
				$productdata['product_quantity'] = $result->quentity;
				$productdata['manufacture_company_name'] = $result->manufacture_company_name;
				$productdata['manufacturer_date'] = $result->manufacture_date;
				$productdata['product_description'] = $result->product_description;
				$productdata['product_specification'] = $result->product_specification;
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$productdata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Delete Product Function
	public function MJ_gmgt_delete_product_data($data)
	{
		$response=array();
		$obj_product=new MJgmgt_product;
		$result=$obj_product->MJgmgt_delete_product($data['product_id']);
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'product');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['delete'] == '1')
			{
				 if($result)
				 {
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully deleted",'gym_mgt');
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Record not found",'gym_mgt');
					$response['result']='';
					return $response;
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>