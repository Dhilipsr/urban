<?php
require_once(ABSPATH .'wp-admin/includes/user.php');
class MJ_Gmgt_member_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Member Registration
		if($_REQUEST['gmgt_json_api'] == 'member_registration')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			/* header("Access-Control-Allow-Origin: *");
			$h = fopen(GMS_PLUGIN_DIR . '/request.txt', 'r+'); 
			fwrite($h, var_export($_REQUEST['hidden_member_image'], true));
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$h = fopen(GMS_PLUGIN_DIR . '/json_test.txt', 'r+'); 
			fwrite($h, var_export($data, true));*/

			$response=$this->MJ_Gmgt_member_registration($data); 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Member List
		if($_REQUEST['gmgt_json_api'] == 'member_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_Gmgt_member_list($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Add member
		if($_REQUEST['gmgt_json_api'] == 'add_member')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json,true);
		   	$action='add';
			$response=$this->MJ_Gmgt_add_member($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Member 
		if($_REQUEST['gmgt_json_api'] == 'edit_member')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json , true);
			$action='edit';
			$response=$this->MJ_Gmgt_add_member($data,$action);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Member 
		if($_REQUEST['gmgt_json_api'] == 'single_member')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_Gmgt_single_member($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Member 
		if($_REQUEST['gmgt_json_api'] == 'delete_member')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_Gmgt_delete_member($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Update Member Profile
		if($_REQUEST['gmgt_json_api'] == 'update_member_profile')
		{
			$json = file_get_contents('php://input');
			$data = json_decode($json, true);
			$response=$this->MJgmgt_update_member_profile($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Member Registration Function
	function MJ_Gmgt_member_registration($data)
	{
	
		// $fp = fopen(GMS_PLUGIN_DIR . '/test.txt',"wb");
		//$h = fopen(GMS_PLUGIN_DIR . '/test.txt', 'r+'); 
		//fwrite($h, var_export($data, true));
		// fwrite($fp,$data);
		// fclose($fp);
		$response=array();
		global $wpdb;
		$table_members = $wpdb->prefix. 'usermeta';
		$table_gmgt_groupmember = $wpdb->prefix.'gmgt_groupmember';
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		//if (isset($data['first_name']) && isset($data['last_name']) && isset($data['dob']) && isset($data['gender']) && isset($data['address']) && isset($data['city']) && isset($data['zip']) && isset($data['mobile']) && isset($data['email']) && isset($data['username']) && isset($data['password']) && isset($data['staff_member']) && isset($data['membership']) && isset($data['class']) && isset($data['membership_valid_from']) && isset($data['membership_valid_to']))
		if (isset($data['first_name']) && isset($data['last_name'])  && isset($data['gender'])  &&  isset($data['email'])  && isset($data['password']) && isset($data['membership']) && isset($data['class']) && isset($data['membership_valid_from']) && isset($data['membership_valid_to']))
		{
			if( !email_exists( $data['email'] ) ) 
			{
				$userdata = array(
					'user_login'=>MJgmgt_strip_tags_and_stripslashes($data['email']),
					'user_email'=>MJgmgt_strip_tags_and_stripslashes($data['email']),
					'user_url'=> NULL,
					'first_name'=>MJgmgt_strip_tags_and_stripslashes($data['first_name']),
					'last_name'=>MJgmgt_strip_tags_and_stripslashes($data['last_name']),
					'nickname'=>NULL
				);
				if($data['password'] != "")
					$userdata['user_pass']=MJgmgt_password_validation($data['password']);
				
				$user_id = wp_insert_user( $userdata );
				$user = new WP_User($user_id);
				$user->set_role('member');
				$gmgt_avatar = '';
				//$image =$data['hidden_member_image'];
				$image='';
				if(!empty($image))
				{
					//$upload_dir  = wp_upload_dir();
				//	$name = current_time('Y-m-d').'_'.'2e9cfcab-8f6d-4412-9a20-38778334f5f3.png';
				//	$path=$upload_dir['path'].'/'.$name;
				//	$gmgt_avatar=$upload_dir['url'].'/'.$name;
				//	$file=file_put_contents($path,base64_decode($image));
					// $img1 = file_get_contents($image);
			        // $im = imagecreatefromstring($img1);
			        // $width = imagesx($im);
			        // $height = imagesy($im);
			        // $newwidth = '250';
			        // $newheight = '250';
			        // $thumb = imagecreatetruecolor($newwidth, $newheight);
			        // $data = imagecopyresized($thumb, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
			        // $str = time();
			        // $data = str_shuffle($str);
			        // imagejpeg($thumb, $upload_dir['path'].'/'.$data.'.'.'JPG');
					//$gmgt_avatar=$upload_dir['url'].'/'.$name;
			        // $uimgurl = $data.'.'.'JPG';
					// move_uploaded_file($image , $path);
				}
				else 
				{
					if(isset($data['hidden_member_image']))
						$gmgt_avatar_image=$data['hidden_member_image'];
						$gmgt_avatar=$gmgt_avatar_image;
				}
				$lastmember_id=MJgmgt_get_lastmember_id('Member');
				$nodate=substr($lastmember_id,0,-4);
				$memberno=substr($nodate,1);
				$memberno+=1;
				$newmember='M'.$memberno.date("my");
				$usermetadata=array(
					'middle_name'=>MJgmgt_strip_tags_and_stripslashes($data['middle_name']),
					'gender'=>$data['gender'],
					'birth_date'=>MJgmgt_get_format_for_db_api($data['dob']),
					'address'=>MJgmgt_strip_tags_and_stripslashes($data['address']),
					'city_name'=>MJgmgt_strip_tags_and_stripslashes($data['city']),
					'state_name'=>MJgmgt_strip_tags_and_stripslashes($data['state']),
					'zip_code'=>MJgmgt_strip_tags_and_stripslashes($data['zip']),
					'phone'=>MJgmgt_strip_tags_and_stripslashes($data['phone']),
					'mobile'=>MJgmgt_strip_tags_and_stripslashes($data['mobile']),
					'gmgt_user_avatar'=>$gmgt_avatar,
					'member_id'=>MJgmgt_strip_tags_and_stripslashes($newmember),
					'member_type'=>'Member',
					'height'=>MJgmgt_strip_tags_and_stripslashes($data['height']),
					'weight'=>MJgmgt_strip_tags_and_stripslashes($data['weight']),
					'chest'=>MJgmgt_strip_tags_and_stripslashes($data['chest']),
					'waist'=>MJgmgt_strip_tags_and_stripslashes($data['waist']),
					'thigh'=>MJgmgt_strip_tags_and_stripslashes($data['thigh']),
					'arms'=>MJgmgt_strip_tags_and_stripslashes($data['arms']),
					'fat'=>MJgmgt_strip_tags_and_stripslashes($data['fat']),
					'staff_id'=>MJgmgt_strip_tags_and_stripslashes($data['staff_member']),
					'intrest_area'=>MJgmgt_strip_tags_and_stripslashes($data['interest_area']),
					'source'=>MJgmgt_strip_tags_and_stripslashes($data['referral_source']),
					'reference_id'=>MJgmgt_strip_tags_and_stripslashes($data['referred_by']),
					'inqiury_date'=>$data['inquiry_date'],
					'membership_id'=>MJgmgt_strip_tags_and_stripslashes($data['membership']),
					'begin_date'=>MJgmgt_get_format_for_db_api($data['membership_valid_from']),
					'end_date'=>MJgmgt_get_format_for_db_api($data['membership_valid_to']),
					'first_payment_date'=>MJgmgt_get_format_for_db_api($data['first_payment_date'])
				);
				foreach($usermetadata as $key=>$val)
				{	
					$result=update_user_meta( $user_id, $key,$val );
				}
				global $wpdb;
				$table_gmgt_member_class = $wpdb->prefix. 'gmgt_member_class';
				$table_gmgt_groupmember = $wpdb->prefix.'gmgt_groupmember';
				
				$memclss['member_id']=$user_id;
				foreach($data['class'] as $key=>$class)
				{
					$memclss['class_id']=$class;
					$result = $wpdb->insert($table_gmgt_member_class,$memclss);			
				}
				if(!empty($data['group']))
				{
					$obj_member=new MJgmgt_member;
					if($obj_member->MJgmgt_member_exist_ingrouptable($user_id))
						$obj_member->MJ_gmgt_delete_member_from_grouptable($user_id);
					foreach($data['group'] as $id)
					{
						$group_data['group_id']=$id;
						$group_data['member_id']=$user_id;
						$group_data['created_date']=date("Y-m-d");
						$group_data['created_by']=$user_id;
						$wpdb->insert( $table_gmgt_groupmember, $group_data );
					}
				}
				
				$hash = md5( rand(0,1000) );
				$result=update_user_meta($user_id, 'gmgt_hash', $hash);	
				
				$user_info = get_userdata($user_id);
				if(!empty($user_id))
				{
					$gymname=get_option( 'gmgt_system_name' );
					$to = $user_info->user_email;         
					$subject = get_option('registration_title'); 
					$sub_arr['[GMGT_GYM_NAME]']=$gymname;
					$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
					$search=array('[GMGT_MEMBERNAME]','[GMGT_MEMBERID]','[GMGT_STARTDATE]','[GMGT_ENDDATE]','[GMGT_MEMBERSHIP]','[GMGT_GYM_NAME]');
					$membership_name=MJgmgt_get_membership_name($data['membership']);
					$replace = array($user_info->display_name,$user_info->member_id,$data['membership_valid_from'],$data['membership_valid_to'],$membership_name,get_option( 'gmgt_system_name' ));
					$message_replacement = str_replace($search, $replace,get_option('registration_mailtemplate'));
			
					MJgmgt_send_mail($to,$subject,$message_replacement);	
				}
				//------------- SMS SEND -------------//
				$gymname=get_option( 'gmgt_system_name' );		
				$message_content ="You are successfully registered at ".$gymname;
				$mobile_number_new=array(); 
				$mobile_number_new[] = "+".MJgmgt_get_countery_phonecode(get_option( 'gmgt_contry' )).$data['mobile'];
			 
				$current_sms_service 	= 	get_option( 'smgt_sms_service');
				include_once(ABSPATH.'wp-admin/includes/plugin.php');
				if(is_plugin_active('sms-pack/sms-pack.php'))
				{
					$args = array();
					$args['mobile']=$mobile_number_new;
					$args['message_from']="notice";
					$args['message']=$message_content;		
									
					if($current_sms_service=='telerivet' || $current_sms_service ="MSG91" || $current_sms_service=='bulksmsgateway.in' || $current_sms_service=='textlocal.in' || $current_sms_service=='bulksmsnigeria' || $current_sms_service=='africastalking' || $current_sms_service == 'clickatell')
					{				
						$send = send_sms($args);							
					}
				}
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully inserted","gym_mgt");
					$response['result']='';
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Please Fill All Fields",'gym_mgt');
					$response['result']='';
					return $response;
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Username Or Emailid All Ready Exist","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("Fill All Required Field","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Add-Edit Member Function
	public function MJ_Gmgt_add_member($data,$action)	
	{
		$response=array();
		global $wpdb;
		$table_members = $wpdb->prefix. 'usermeta';
		$table_gmgt_groupmember = $wpdb->prefix.'gmgt_groupmember';
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		//-------usersmeta table data--------------
		if ($data['access_token'] == $access_token)
		{
			if (isset($data['first_name']) && isset($data['last_name']) && isset($data['dob']) && isset($data['gender']) && isset($data['address']) && isset($data['city']) && isset($data['zip']) && isset($data['mobile']) && isset($data['email']) && isset($data['username']) && isset($data['password']) && isset($data['member_type']) && isset($data['staff_member']) && isset($data['membership']) && isset($data['class']) && isset($data['membership_valid_from']) && isset($data['membership_valid_to']))
			{
				if(isset($data['middle_name']))
				{
					$usermetadata['middle_name']=MJgmgt_strip_tags_and_stripslashes($data['middle_name']);
				}
				if(isset($data['gender']))
				{
					$usermetadata['gender']=$data['gender'];
				}
				if(isset($data['dob']))
				{
					$usermetadata['birth_date']=MJgmgt_get_format_for_db($data['dob']);
				}
				if(isset($data['address']))
				{
					$usermetadata['address']=MJgmgt_strip_tags_and_stripslashes($data['address']);
				}
				if(isset($data['city']))
				{
					$usermetadata['city_name']=MJgmgt_strip_tags_and_stripslashes($data['city']);
				}
				if(isset($data['state']))
				{
					$usermetadata['state_name']=MJgmgt_strip_tags_and_stripslashes($data['state']);
				}
				if(isset($data['zip']))
				{
					$usermetadata['zip_code']=MJgmgt_strip_tags_and_stripslashes($data['zip']);
				}
				if(isset($data['mobile']))
				{
					$usermetadata['mobile']=MJgmgt_strip_tags_and_stripslashes($data['mobile']);
				}
				if(isset($data['phone']))
				{
					$usermetadata['phone']=MJgmgt_strip_tags_and_stripslashes($data['phone']);
				}
				$gmgt_avatar = '';
				if($_FILES['member_image']['size'] > 0)
				{
					$gmgt_avatar_image = MJgmgt_user_avatar_image_upload('gmgt_user_avatar');
					$gmgt_avatar = content_url().'/uploads/gym_assets/'.$gmgt_avatar_image;
				}
				else 
				{
					if(isset($data['hidden_member_image']))
						$gmgt_avatar_image=$data['hidden_member_image'];
						$gmgt_avatar=$smgt_avatar_image;
				}
				$usermetadata['gmgt_user_avatar']=$gmgt_avatar;
				if(isset($data['member_id']))
				{
					$usermetadata['member_id']=MJgmgt_strip_tags_and_stripslashes($data['member_id']);
				}
				if(isset($data['member_type']))
				{
					$usermetadata['member_type']=MJgmgt_strip_tags_and_stripslashes($data['member_type']);			
				}
				if(isset($data['height']))
				{
					$usermetadata['height']=MJgmgt_strip_tags_and_stripslashes($data['height']);
				}
				if(isset($data['weight']))
				{
					$usermetadata['weight']=MJgmgt_strip_tags_and_stripslashes($data['weight']);
				}
				if(isset($data['chest']))
				{
					$usermetadata['chest']=MJgmgt_strip_tags_and_stripslashes($data['chest']);
				}
				if(isset($data['waist']))
				{
					$usermetadata['waist']=MJgmgt_strip_tags_and_stripslashes($data['waist']);
				}
				if(isset($data['thigh']))
				{
					$usermetadata['thigh']=MJgmgt_strip_tags_and_stripslashes($data['thigh']);
				}
				if(isset($data['arms']))
				{
					$usermetadata['arms']=MJgmgt_strip_tags_and_stripslashes($data['arms']);
				}
				if(isset($data['fat']))
				{
					$usermetadata['fat']=MJgmgt_strip_tags_and_stripslashes($data['fat']);
				}
				if(isset($data['staff_member']))
				{
					$usermetadata['staff_id']=MJgmgt_strip_tags_and_stripslashes($data['staff_member']);
				}
				if(isset($data['staff_member']))
				{
					$usermetadata['intrest_area']=MJgmgt_strip_tags_and_stripslashes($data['interest_area']);
				}
				if(isset($data['referral_source']))
				{
					$usermetadata['source']=MJgmgt_strip_tags_and_stripslashes($data['referral_source']);
				}
				if(isset($data['referred_by']))
				{
					$usermetadata['reference_id']=MJgmgt_strip_tags_and_stripslashes($data['referred_by']);
				}
				if(isset($data['inquiry_date']))
				{
					$usermetadata['inqiury_date']=MJgmgt_get_format_for_db($data['inquiry_date']);
				}
				if(isset($data['trial_enddate']))
				{
					$usermetadata['triel_date']=MJgmgt_get_format_for_db($data['trial_enddate']);
				}
				if(isset($data['membership']))
				{
					$usermetadata['membership_id']=MJgmgt_strip_tags_and_stripslashes($data['membership']);
				}
				if(isset($data['membership_status']))
				{
					$usermetadata['membership_status']=MJgmgt_strip_tags_and_stripslashes($data['membership_status']);
				}
				if(isset($data['auto_renew']))
				{
					$usermetadata['auto_renew']=MJgmgt_strip_tags_and_stripslashes($data['auto_renew']);
				}
				if(isset($data['membership_valid_from']))
				{
					$usermetadata['begin_date']=MJgmgt_get_format_for_db($data['membership_valid_from']);
				}
				if(isset($data['membership_valid_to']))
				{
					$usermetadata['end_date']=MJgmgt_get_format_for_db($data['membership_valid_to']);
				}
				if(isset($data['first_payment_date']))
				{
					$usermetadata['first_payment_date']=MJgmgt_get_format_for_db($data['first_payment_date']);
				}
				if(isset($data['member_convert']))
				{
					$roledata['role']=$data['member_convert'];
				}
				if(isset($data['username']))
				{
					$userdata['user_login']=MJgmgt_strip_tags_and_stripslashes($data['username']);
				}
				if(isset($data['email']))
				{
					$userdata['user_email']=MJgmgt_strip_tags_and_stripslashes($data['email']);
				}
				$userdata['user_nicename']=NULL;
				$userdata['user_url']=NULL;
				if(isset($data['first_name']))
				{
					$userdata['display_name']=MJgmgt_strip_tags_and_stripslashes($data['first_name'])." ".MJgmgt_strip_tags_and_stripslashes($data['last_name']);
				}
				if($data['password'] != "")
				{
					$userdata['user_pass']=MJgmgt_password_validation($data['password']);
				}
				$member_id=0;
				$edit=0;
				if($action == 'edit')
				{
					$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'member');
					if($menu_access_data['edit'] == '1')
					{
						$userdata['ID']=$data['member_id'];
						$memberclass = MJgmgt_get_current_user_classis($data['member_id']);
						$tbl_class = $wpdb->prefix .'gmgt_member_class';
						if(!empty($memberclass))
						{
							$wheredataa['member_id']=$data['member_id'];
							foreach($memberclass as $class_id)
							{
								$where['class_id']=$class_id;
								$wpdb->delete( $tbl_class, $wheredataa ); 
							}
						}
						if(isset($data['class']))
						{
							foreach($data['class'] as $key=>$newclass)
							{
								$wpdb->insert($tbl_class,array('member_id'=>$data['user_id'],'class_id'=>$newclass));
							}
						}
						$user_id =$data['member_id'];
						wp_update_user($userdata);
												
						$returnans=update_user_meta( $user_id, 'first_name', MJgmgt_strip_tags_and_stripslashes($data['first_name']) );
						$returnans=update_user_meta( $user_id, 'last_name', MJgmgt_strip_tags_and_stripslashes($data['last_name']) );
						$gymname=get_option( 'gmgt_system_name' );
						$to[] = MJgmgt_strip_tags_and_stripslashes($data['email']);
						$subject = get_option('registration_title'); 
						$sub_arr['[GMGT_GYM_NAME]']=$gymname;
						$subject1 = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
						$search=array('[GMGT_MEMBERNAME]','[GMGT_MEMBERID]','[GMGT_STARTDATE]','[GMGT_ENDDATE]','[GMGT_MEMBERSHIP]','[GMGT_GYM_NAME]');
						$membership_name=MJgmgt_get_membership_name($data['membership_id']);
						$replace = array($userdata['display_name'],$data['member_id'],$data['membership_valid_from'],$data['membership_valid_to'],$membership_name,get_option( 'gmgt_system_name' ));
						$message_replacement = str_replace($search, $replace,get_option('registration_mailtemplate'));
						MJgmgt_send_mail($to,$subject1,$message_replacement); 
						//under registration mail start
						$role='Member';
						$gymname=get_option( 'gmgt_system_name' );
						$login_link=home_url();
						$arr['[GMGT_USERNAME]']=$userdata['display_name'];	
						$arr['[GMGT_GYM_NAME]']=$gymname;
						$arr['[GMGT_ROLE_NAME]']=$role;
						$arr['[GMGT_Username]']=MJgmgt_strip_tags_and_stripslashes($data['username']);
						$arr['[GMGT_PASSWORD]']=MJgmgt_password_validation($data['password']);
						$arr['[GMGT_LOGIN_LINK]']=$login_link;
						$subject =get_option('Add_Other_User_in_System_Subject');
						$sub_arr['[GMGT_ROLE_NAME]']=$role;
						$sub_arr['[GMGT_GYM_NAME]']=$gymname;
						$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
						$message = get_option('Add_Other_User_in_System_Template');	
						$message_replacement = MJgmgt_string_replacemnet($arr,$message);
						$to[]=MJgmgt_strip_tags_and_stripslashes($data['email']);
						MJgmgt_send_mail($to,$subject,$message_replacement);
						//under registration mail end
						foreach($usermetadata as $key=>$val)
						{
							$returnans=update_user_meta( $user_id, $key,$val );
						}
						if(isset($data['group']))
						{
							if(!empty($data['group']))
							{
								if($this->MJgmgt_member_exist_ingrouptable($user_id))
								{
									$this->MJ_gmgt_delete_member_from_grouptable($user_id);
									foreach($data['group'] as $id)
									{								
										$group_data['group_id']=$id;
										$group_data['member_id']=$user_id;
										$group_data['created_date']=date("Y-m-d");
										$group_data['created_by']=$data['current_user_id'];
										$wpdb->insert( $table_gmgt_groupmember, $group_data );
										$gymname=get_option( 'gmgt_system_name' );
										$obj_group=new MJgmgt_group;
										$groupdata=$obj_group->MJgmgt_get_single_group($id);
										$groupname=$groupdata->group_name;
										$arr['[GMGT_USERNAME]']=$userdata['display_name'];	
										$arr['[GMGT_GROUPNAME]']=$groupname;	
										$arr['[GMGT_GYM_NAME]']=$gymname;
										$subject =get_option('Member_Added_In_Group_subject');
										$sub_arr['[GMGT_GROUPNAME]']=$groupname;
										$sub_arr['[GMGT_GYM_NAME]']=$gymname;
										$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
										$message = get_option('Member_Added_In_Group_Template');	
										$message_replacement = MJgmgt_string_replacemnet($arr,$message);
										$to[]=strip_tags($data['email']);
										MJgmgt_send_mail($to,$subject,$message_replacement);
									}
								}
							}
						}
						if($returnans)
						{
							$response['status']=1;
							$response['error_code']=200;
							$response['error']=__("Record Updated successfully inserted","gym_mgt");
						}
						else
						{
							$response['status']=0;
							$response['error_code']=401;
							$response['error']=__("Something Is Wrong",'gym_mgt');
							$response['result']='';
							return $response;
						}
					}
					else
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("You Can't Access This Page","gym_mgt");
						$response['result']='';
						return $response;
					}
				}		
				else
				{
					$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'member');
					if($menu_access_data['add'] == '1')
					{
						if( !email_exists( $data['email'] ) && !username_exists( $data['username'] ))
						{
							$user_id = wp_insert_user( $userdata );
							$u = new WP_User($user_id);
							$u->add_role( 'member');
							$gymname=get_option( 'gmgt_system_name' );
							$to[] = MJgmgt_strip_tags_and_stripslashes($data['email']);         
							$subject = get_option('registration_title'); 
							$sub_arr['[GMGT_GYM_NAME]']=$gymname;
							$subject1 = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
							$search=array('[GMGT_MEMBERNAME]','[GMGT_MEMBERID]','[GMGT_STARTDATE]','[GMGT_ENDDATE]','[GMGT_MEMBERSHIP]','[GMGT_GYM_NAME]');
							$membership_name=MJgmgt_get_membership_name($data['membership_id']);
							$replace = array($userdata['display_name'],$data['member_id'],$data['membership_valid_from'],$data['membership_valid_to'],$membership_name,get_option( 'gmgt_system_name' ));
							$message_replacement = str_replace($search, $replace,get_option('registration_mailtemplate'));
							MJgmgt_send_mail($to,$subject1,$message_replacement);
							//user registration mail start
							$role='Member';
							$gymname=get_option( 'gmgt_system_name' );
							$login_link=home_url();
							$arr['[GMGT_USERNAME]']=$userdata['display_name'];	
							$arr['[GMGT_GYM_NAME]']=$gymname;
							$arr['[GMGT_ROLE_NAME]']=$role;
							$arr['[GMGT_Username]']=MJgmgt_strip_tags_and_stripslashes($data['username']);
							$arr['[GMGT_PASSWORD]']=MJgmgt_password_validation($data['password']);
							$arr['[GMGT_LOGIN_LINK]']=$login_link;
							$subject =get_option('Add_Other_User_in_System_Subject');
							$sub_arr['[GMGT_ROLE_NAME]']=$role;
							$sub_arr['[GMGT_GYM_NAME]']=$gymname;
							$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
							$message = get_option('Add_Other_User_in_System_Template');	
							$message_replacement = MJgmgt_string_replacemnet($arr,$message);
							$to[]=MJgmgt_strip_tags_and_stripslashes($data['email']);
							MJgmgt_send_mail($to,$subject,$message_replacement);
							//user registration mail end
							$user = new WP_User($user_id);
							$user->set_role(strip_tags($role));
							$usermetadata['membership_status']="Continue";
							if(isset($data['class']))
							{
								$MemberClassData = array();
								$MemberClassData['member_id']=$user_id;
								$tbl_MemberClass = $wpdb->prefix . 'gmgt_member_class';
								foreach($data['class'] as $key=>$class_id)
								{
									$MemberClassData['class_id']=$class_id; 
									$wpdb->insert($tbl_MemberClass,$MemberClassData);
								}
							}
							foreach($usermetadata as $key=>$val)
							{
								$returnans=add_user_meta( $user_id, $key,$val, true );
							}
							if(isset($data['first_name']))
							{
								$returnans=update_user_meta( $user_id, 'first_name', MJgmgt_strip_tags_and_stripslashes($data['first_name']));
							}
							if(isset($data['last_name']))
							{
								$returnans=update_user_meta( $user_id, 'last_name', MJgmgt_strip_tags_and_stripslashes($data['last_name']));
							}
							if(isset($data['group']))
							{
								if(!empty($data['group']))
								{			
									foreach($data['group'] as $id)
									{							
										$group_data['group_id']=$id;
										$group_data['member_id']=$user_id;						
										$group_data['created_date']=date("Y-m-d");
										$group_data['created_by']=$data['current_user_id'];
										$wpdb->insert( $table_gmgt_groupmember, $group_data );
										$gymname=get_option( 'gmgt_system_name' );
										$obj_group=new MJgmgt_group;
										$groupdata=$obj_group->MJgmgt_get_single_group($id);
										$groupname=$groupdata->group_name;
										$arr['[GMGT_USERNAME]']=$userdata['display_name'];	
										$arr['[GMGT_GROUPNAME]']=$groupname;	
										$arr['[GMGT_GYM_NAME]']=$gymname;
										$subject =get_option('Member_Added_In_Group_subject');
										$sub_arr['[GMGT_GROUPNAME]']=$groupname;
										$sub_arr['[GMGT_GYM_NAME]']=$gymname;
										$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
										$message = get_option('Member_Added_In_Group_Template');	
										$message_replacement = MJgmgt_string_replacemnet($arr,$message);
										$to[]=MJgmgt_strip_tags_and_stripslashes($data['email']);
										MJgmgt_send_mail($to,$subject,$message_replacement);
										$to[]= MJgmgt_strip_tags_and_stripslashes($data['email']);
										$login_link=home_url();
										$subject =get_option( 'Member_Approved_Template_Subject' ); 
										$gymname=get_option( 'gmgt_system_name' );
										$sub_arr['[GMGT_GYM_NAME]']=$gymname;
										$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
										$search=array('[GMGT_GYM_NAME]','[GMGT_LOGIN_LINK]');
										$membership_name=MJgmgt_get_membership_name($membership_id);
										$replace = array($gymname,$login_link);
										$message_replacement = str_replace($search, $replace,get_option('Member_Approved_Template'));	
										 MJgmgt_send_mail($to,$subject,$message_replacement);
									}
								}
							}
							//invoice number generate
							$result_invoice_no=$wpdb->get_results("SELECT * FROM $table_income");
							if(empty($result_invoice_no))
							{							
								$invoice_no='00001';
							}
							else
							{							
								$result_no=$wpdb->get_row("SELECT invoice_no FROM $table_income where invoice_id=(SELECT max(invoice_id) FROM $table_income)");
								$last_invoice_number=$result_no->invoice_no;
								$invoice_number_length=strlen($last_invoice_number);
								if($invoice_number_length=='5')
								{
									$invoice_no = str_pad($last_invoice_number+1, 5, 0, STR_PAD_LEFT);
								}
								else	
								{
									$invoice_no='00001';
								}				
							}
							$membership_status = 'continue';
							$payment_data = array();
							$payment_data['invoice_no']=$invoice_no;
							$payment_data['member_id'] = $user_id;
							$payment_data['membership_id'] = $data['membership'];
							$payment_data['membership_fees_amount'] = MJgmgt_get_membership_price($data['membership']);
							$payment_data['membership_signup_amount'] = MJgmgt_get_membership_signup_amount($data['membership']);
							$payment_data['tax_amount'] = MJgmgt_get_membership_tax_amount($data['membership']);
							$membership_amount=$payment_data['membership_fees_amount'] + $payment_data['membership_signup_amount'] + $payment_data['tax_amount'];
							$payment_data['membership_amount'] = 1000;
							$payment_data['start_date'] = MJgmgt_get_format_for_db($data['membership_valid_from']);
							$payment_data['end_date'] = MJgmgt_get_format_for_db($data['membership_valid_to']);
							$payment_data['membership_status'] = $membership_status;
							$payment_data['payment_status']='Unpaid';
							$payment_data['created_date'] = date("Y-m-d");
							$payment_data['created_by'] = $data['current_user_id'];
							$plan_id = $this->MJgmgt_add_membership_payment_detail($payment_data);
							//membership invoice mail send
							$insert_id=$plan_id;
							$paymentlink=home_url().'?dashboard=user&page=membership_payment';
							$gymname=get_option( 'gmgt_system_name' );
							$userdata=get_userdata($data['member_id']);
							$arr['[GMGT_USERNAME]']=$userdata->display_name;	
							$arr['[GMGT_GYM_NAME]']=$gymname;
							$arr['[GMGT_PAYMENT_LINK]']=$paymentlink;
							$subject =get_option('generate_invoice_subject');
							$sub_arr['[GMGT_GYM_NAME]']=$gymname;
							$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
							$message = get_option('generate_invoice_template');	
							$message_replacement = MJgmgt_string_replacemnet($arr,$message);
							$to[]=$userdata->user_email;
							$type='membership_invoice';
							MJgmgt_send_invoice_generate_mail($to,$subject,$message_replacement,$insert_id,$type);
							//save membership payment data into income table			
							$table_income=$wpdb->prefix.'gmgt_income_expense';
							$membership_name=MJgmgt_get_membership_name($data['membership']);
							$entry_array[]=array('entry'=>$membership_name,'amount'=>MJgmgt_get_membership_price($data['membership']));	
							$entry_array1[]=array('entry'=>__("Membership Signup Fee","gym_mgt"),'amount'=>MJgmgt_get_membership_signup_amount($data['membership']));	
							$entry_array_merge=array_merge($entry_array,$entry_array1);
							$incomedata['entry']=json_encode($entry_array_merge);
							$incomedata['invoice_type']='income';
							$incomedata['invoice_label']=__("Fees Payment","gym_mgt");
							$incomedata['supplier_name']=$user_id;
							$incomedata['invoice_date']=date('Y-m-d');
							$incomedata['receiver_id']=$data['current_user_id'];
							$incomedata['amount']=$membership_amount;					
							$incomedata['total_amount']=$membership_amount;
							$incomedata['invoice_no']=$invoice_no;
							$incomedata['tax_id']=MJgmgt_get_membership_tax($data['membership']);
							$incomedata['paid_amount']=0;
							$incomedata['payment_status']='Unpaid';
							$result_income=$wpdb->insert( $table_income,$incomedata);
							if($returnans)
							{
								$response['status']=1;
								$response['error_code']=200;
								$response['error']=__("Record successfully inserted","gym_mgt");
							}
							else
							{
								$response['status']=0;
								$response['error_code']=401;
								$response['error']=__("Please Fill All Fields",'gym_mgt');
								$response['result']='';
								return $response;
							}
						}
						else
						{
							$response['status']=0;
							$response['error_code']=401;
							$response['error']=__("Username Or Emailid All Ready Exist","gym_mgt");
							$response['result']='';
							return $response;
						}
					}
					else
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("You Can't Access This Page","gym_mgt");
						$response['result']='';
						return $response;
					}
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Fill All Required Field","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Member List Function
	public function MJ_Gmgt_member_list($data)
	{
		$obj_membership=new MJgmgt_membership;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		$access_token = get_user_meta($user_id , 'access_token' , true);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'member');
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$membersdata=array();
					$membersdata[] = get_userdata($user_id);				
				}
				elseif($role == 'staff_member')
				{	
					$membersdata = get_users(array('meta_key' => 'staff_id', 'meta_value' =>$user_id ,'role'=>'member'));	
				}
				else
				{
					$membersdata =get_users( array('role' => 'member'));
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				if($role == 'member')
				{	
					$membersdata =get_users( array('role' => 'member'));	
				}
				elseif($role == 'staff_member')
				{
				     $membersdata =get_users( array('role' => 'member'));	
				}
				else
				{
					$membersdata =get_users( array('role' => 'member'));
				}
			}
			else
			{
				$membersdata="";
			}
			
			$response	=	array();

			if(!empty($membersdata))
			{	
				$i=0;
				foreach ($membersdata as $retrieved_data)
				{
					$membership_id = get_user_meta($retrieved_data->ID,'membership_id',true);
					$membershipdata=$obj_membership->MJgmgt_get_single_membership($membership_id);
					if(!empty($membershipdata))
					{
						if($membershipdata->classis_limit=='limited')
						{
								$total_class=$membershipdata->on_of_classis;
								$userd_class=MJ_gmgt_get_user_used_membership_class($membership_id,$retrieved_data->ID);
								$remaining_class_data=$total_class-$userd_class;
								$remaining_class=$remaining_class_data." Out Of ".$total_class;
						}
						else
						{
							$remaining_class='Unlimited';						
						}
					}
					
					$userimagedata=get_avatar_url($retrieved_data->ID);

					if(empty($userimagedata['meta_value']))
					{
						$imageurl=get_option( 'gmgt_member_thumb');
					}
					else
					{
						$imageurl=$userimagedata['meta_value'];
					}
					$result[$i]['member_id'] =	$retrieved_data->ID;

					$result[$i]['image']=$imageurl;

					$result[$i]['member_name']	=	$retrieved_data->display_name;

					$result[$i]['email']=$retrieved_data->user_email;
					$result[$i]['member_type']=$membertype_array[$retrieved_data->member_type];
					if($retrieved_data->member_type!='Prospect')
					{ 
						$joining_date=MJgmgt_getdate_in_input_box($retrieved_data->begin_date); 
						$exp_date=MJgmgt_getdate_in_input_box(MJgmgt_check_membership($retrieved_data->ID)); 
					}
					else
					{ 
						$joining_date="--"; 
						$exp_date="--"; 
					}
					$result[$i]['joining_date']=$joining_date;
					$result[$i]['exp_date']=$exp_date;
					if($retrieved_data->member_type!='Prospect')
					{  
						$membership_status=_e($retrieved_data->membership_status,'gym_mgt'); 					
					}
					else
					{ 
						$membership_status=_e('Prospect','gym_mgt');			
				    }
					$result[$i]['membership_status']=$membership_status;
					$result[$i]['remaining_class']=$remaining_class;
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User",'gym_mgt');
		}
		return $response;
	}
	//Single Member Function
	public function MJ_Gmgt_single_member($data)
	{
		$obj_group=new MJgmgt_group;
		$obj_gym = new MJgmgt_Gym_management();
		$user_info = get_userdata($data['member_id']);
		$access_token = get_user_meta($data['member_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($user_info))
			{
				$ClassArr=MJgmgt_get_current_user_classis($user_info->ID);

				$i=0;
				foreach($ClassArr as $key=>$class_id)
				{
					$class_array_id[$i]=$class_id;
					$class_array_name[$i]=MJgmgt_get_single_class_name($class_id);
					$i=$i+1;
				}
				$groupArr=$obj_group->MJgmgt_get_member_all_groups($user_info->ID);
				$group_array=array();
				$i=0;
				foreach($groupArr as $data)
				{
					$group_array_id[$i]=$data->id;
					$group_array_name[$i]=$data->group_name;
					$i=$i+1;
				}
				$memberdata['member_id'] = $user_info->ID;
				$memberdata['first_name'] =$user_info->first_name;
				$memberdata['middle_name'] = $user_info->middle_name;
				$memberdata['last_name'] = $user_info->last_name;
				$memberdata['gender'] = $user_info->gender;
				$memberdata['dob'] = MJgmgt_getdate_in_input_box($user_info->birth_date);
				$memberdata['class_id'] = implode(',', $class_array_id);
				$memberdata['class_name'] = implode(',', $class_array_name);
				$memberdata['group_id'] = implode(',', $group_array_id);
				$memberdata['group_name'] = implode(',', $group_array_name);
				$memberdata['address'] = $user_info->address;
				$memberdata['city'] = $user_info->city_name;
				$memberdata['state'] = $user_info->state_name;
				$memberdata['zip'] = $user_info->zip_code;
				$memberdata['mobile'] = $user_info->mobile;
				$memberdata['phone'] = $user_info->phone;
				$memberdata['email'] = $user_info->user_email;
				$memberdata['weight'] = $user_info->weight;
				$memberdata['height'] =$user_info->height;
				$memberdata['chest'] = $user_info->chest;
				$memberdata['waist'] = $user_info->waist;
				$memberdata['thigh'] = $user_info->thigh;
				$memberdata['arms'] = $user_info->arms;
				$memberdata['fat'] = $user_info->fat;
				$memberdata['username'] = $user_info->user_login;
				$staff_info = get_userdata($user_info->staff_id);
				$memberdata['staff_member_id'] = $user_info->staff_id;
				$memberdata['staff_member_name'] = $staff_info->display_name;
				$memberdata['interest_area'] = get_the_title($user_info->intrest_area);
				$memberdata['referral_source'] = $user_info->source;
				$memberdata['referred_by'] = $user_info->reference_id;
				$memberdata['inquiry_date'] = MJgmgt_getdate_in_input_box($user_info->inqiury_date);
				$memberdata['trial_enddate'] = MJgmgt_getdate_in_input_box($user_info->triel_date);
				$memberdata['membership_id'] = $user_info->membership_id;
				$memberdata['membership_name'] = MJgmgt_get_membership_name($user_info->membership_id);
				$memberdata['membership_status'] =$user_info->membership_status;
				$memberdata['member_type'] =$user_info->member_type;
				$memberdata['membership_valid_from'] = MJgmgt_getdate_in_input_box($user_info->begin_date);
				$memberdata['membership_valid_to'] = MJgmgt_getdate_in_input_box($user_info->end_date);
				$memberdata['first_payment_date'] = MJgmgt_getdate_in_input_box($user_info->first_payment_date);
				$memberdata['member_image'] = esc_url( $user_info->gmgt_user_avatar );
					   
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$memberdata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error']=__("An Unauthorized User",'gym_mgt');
			$response['result']=Null;
		}
		return $response;
	}
	//Delete Member Function
	public function MJ_Gmgt_delete_member($data)
	{
		$response=array();
		$obj_member=new MJgmgt_member;
		$access_token = get_user_meta($data['member_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			$result=$obj_member->MJgmgt_delete_usedata($data['member_id']);
			if($result)
			{
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record successfully deleted",'gym_mgt');
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error']=__("An Unauthorized User",'gym_mgt');
			$response['result']=Null;
		}
		return $response;
	}
	//Update Member Profile Function
	public function MJgmgt_update_member_profile($data)
	{
		$response=array();
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			$returnans=update_user_meta($data['current_user_id'],'first_name',$data['first_name']);
			$returnans=update_user_meta($data['current_user_id'],'mobile',$data['mobile']);
			$returnans=update_user_meta($data['current_user_id'],'address',$data['address']);
			if(!empty($data['new_password']))
			{
				if ($data['new_password'] == $data['confirm_password'])
				{
					wp_set_password($data['new_password'], $data['current_user_id']);
				}
				else
				{
					$meassage = 1;
				}
			}
			$response['status']=1;
			$response['error_code']=200;
			if(!empty($meassage))
			{
				$response['error']=__("Password Is Incorrect",'gym_mgt');
			}
			else
			{
				$response['error']=__("Profile Updated Successfully",'gym_mgt');
			}
		}
		else
		{
			$response['status']=0;
			$response['error']=__("An Unauthorized User",'gym_mgt');
			$response['result']=Null;
		}
		return $response;
	}
	//add membership payment details
	public function MJgmgt_add_membership_payment_detail($data)
	{
		global $wpdb;
		$table_gmgt_membership_payment = $wpdb->prefix. 'Gmgt_membership_payment';

		$result = $wpdb->insert($table_gmgt_membership_payment,$data);
		 $lastid = $wpdb->insert_id;
		return $lastid;
	}
	public function MJgmgt_member_exist_ingrouptable($member_id)
	{
		global $wpdb;	
		$table_gmgt_groupmember = $wpdb->prefix. 'gmgt_groupmember';
		$result = $wpdb->get_row("SELECT * FROM $table_gmgt_groupmember where member_id=".$member_id);
		if(!empty($result))
			return true;
		return false;
	}
}
?>