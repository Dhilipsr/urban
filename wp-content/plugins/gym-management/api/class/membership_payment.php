<?php 
class MJ_Gmgt_MembershipPayment_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Add Membership Payment 
		if($_REQUEST['gmgt_json_api'] == 'add_membership_payment')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='add';
			$response=$this->MJ_gmgt_add_membership_payment_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Membership Payment
		if($_REQUEST['gmgt_json_api'] == 'edit_membership_payment')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='edit';
			$response=$this->MJ_gmgt_add_membership_payment_data($data,$action);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Membership Payment list
		if($_REQUEST['gmgt_json_api'] == 'membership_payment_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_membership_payment_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Membership Payment
		if($_REQUEST['gmgt_json_api'] == 'get_single_membership_payment')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_membership_payment_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Membership Payment
		if($_REQUEST['gmgt_json_api'] == 'delete_membership_payment')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_membership_payment_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Membership Payment Invoice
		if($_REQUEST['gmgt_json_api'] == 'view_invoice')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_view_membership_payment_invoice_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Pay Membership Payment
		if($_REQUEST['gmgt_json_api'] == 'pay_membership_payment_invoice')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_pay_membership_invoice_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Add-Edit Membership Payment Function
	function MJ_gmgt_add_membership_payment_data($data,$action)
	{
		$response=array();
		global $wpdb;
		$table_gmgt_membership_payment=$wpdb->prefix.'Gmgt_membership_payment';
		$tbl_gmgt_member_class = $wpdb->prefix .'gmgt_member_class';	
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		$payment_data['member_id']=$data['member_id'];
		$payment_data['membership_id']=$data['membership_id'];
		$payment_data['membership_fees_amount'] = MJgmgt_get_membership_price($data['membership_id']);
		$payment_data['membership_signup_amount'] = MJgmgt_get_membership_signup_amount($data['membership_id']);
		$payment_data['tax_amount'] = MJgmgt_get_membership_tax_amount($data['membership_id']);		
		$payment_data['membership_amount']=$data['membership_amount'];				
		$payment_data['start_date']=MJgmgt_get_format_for_db($data['membership_valid_from']);	
		$payment_data['end_date']=MJgmgt_get_format_for_db($data['membership_valid_to']);
		$membershipclass = MJgmgt_get_class_id_by_membership_id($data['membership_id']);
		$DaleteWhere['member_id']=$data['member_id'];
		$wpdb->delete( $tbl_gmgt_member_class, $DaleteWhere);
		$inserClassData['member_id']=$data['member_id'];
		if($membershipclass)
		{
			foreach($membershipclass as $key=>$class_id)
			{
				$inserClassData['class_id']=$class_id;
				$inset = $wpdb->insert($tbl_gmgt_member_class,$inserClassData);
			}
		}
		update_user_meta($data['member_id'],'membership_id',$data['membership_id']);
		update_user_meta( $data['member_id'],'begin_date',MJgmgt_get_format_for_db($data['membership_valid_from']));	
		update_user_meta( $data['member_id'],'end_date',MJgmgt_get_format_for_db($data['membership_valid_to']));
		$payment_data['created_by']=$data['current_user_id'];
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'membership_payment');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($action == 'edit')
			{
				$whereid['mp_id']=$data['mp_id'];
				$paid_amount=$data['paid_amount'];
				$membership_amount=$data['membership_amount'];
				if($paid_amount >= $membership_amount)
				{
					$status='Fully Paid';
				}
				elseif($paid_amount > 0)
				{
					$status='Partially Paid';
				}
				else
				{
					$status= 'Unpaid';	
				}
				$payment_data['payment_status']=$status;
				$result=$wpdb->update( $table_gmgt_membership_payment, $payment_data ,$whereid);
				//save membership payment data into income table			
				$table_income=$wpdb->prefix.'gmgt_income_expense';
				$membership_name=MJgmgt_get_membership_name($data['membership_id']);
				$entry_array[]=array('entry'=>$membership_name,'amount'=>MJgmgt_get_membership_price($data['membership_id']));	
				$entry_array1[]=array('entry'=>__("Membership Signup Fee","gym_mgt"),'amount'=>MJgmgt_get_membership_signup_amount($data['membership_id']));	
				$entry_array_merge=array_merge($entry_array,$entry_array1);
				$incomedata['entry']=json_encode($entry_array_merge);	
				$incomedata['supplier_name']=$data['member_id'];			
				$incomedata['amount']=$data['membership_amount'];
				$incomedata['total_amount']=$data['membership_amount'];
				$incomedata['payment_status']=$status;
				$incomedata['tax_id']=MJgmgt_get_membership_tax($data['membership_id']);
				$invoice_no['invoice_no']=$data['invoice_no'];
				$result=$wpdb->update( $table_income,$incomedata,$invoice_no); 
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully updated","gym_mgt");
					$response['result']='';
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Fill All Required Field","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
			else
			{
				//invoice number generate
				$result_invoice_no=$wpdb->get_results("SELECT * FROM $table_income");
				if(empty($result_invoice_no))
				{							
					$invoice_no='00001';
				}
				else
				{
					$result_no=$wpdb->get_row("SELECT invoice_no FROM $table_income where invoice_id=(SELECT max(invoice_id) FROM $table_income)");
					$last_invoice_number=$result_no->invoice_no;
					$invoice_number_length=strlen($last_invoice_number);
					
					if($invoice_number_length=='5')
					{
						$invoice_no = str_pad($last_invoice_number+1, 5, 0, STR_PAD_LEFT);
					}
					else	
					{
						$invoice_no='00001';
					}				
				}
				$payment_data['invoice_no']=$invoice_no;
				$payment_data['payment_status']='Unpaid';
				$payment_data['created_date']=date('Y-m-d');
				$membership_status = 'continue';		
				$payment_data['membership_status'] = $membership_status;
				$result=$wpdb->insert( $table_gmgt_membership_payment,$payment_data);	
				//------------- SEND MESSAGE --------------------//
				$current_sms_service 	= 	get_option( 'smgt_sms_service');
				if(is_plugin_active('sms-pack/sms-pack.php'))
				{
					$userdata=get_userdata($data['member_id']);
					$mobile_number=array(); 
					$gymname=get_option( 'gmgt_system_name' );
					$mobile_number[] = "+".MJgmgt_get_countery_phonecode(get_option( 'gmgt_contry' )).$userdata->mobile;
					$message_content ="Your membership payment invoice generated at ".$gymname;
					$args = array();
					$args['mobile']=$mobile_number;
					$args['message_from']="Membership Payment";
					$args['message']=$message_content;					
					if($current_sms_service=='telerivet' || $current_sms_service ="MSG91" || $current_sms_service=='bulksmsgateway.in' || $current_sms_service=='textlocal.in' || $current_sms_service=='bulksmsnigeria' || $current_sms_service=='africastalking' || $current_sms_service == 'clickatell')
					{				
						$send = send_sms($args);							
					}
				}
				//membership invoice mail send
				$insert_id=$wpdb->insert_id;
				$paymentlink=home_url().'?dashboard=user&page=membership_payment';
				$gymname=get_option( 'gmgt_system_name' );
				$userdata=get_userdata($data['member_id']);
				$arr['[GMGT_USERNAME]']=$userdata->display_name;	
				$arr['[GMGT_GYM_NAME]']=$gymname;
				$arr['[GMGT_PAYMENT_LINK]']=$paymentlink;
				$subject =get_option('generate_invoice_subject');
				$sub_arr['[GMGT_GYM_NAME]']=$gymname;
				$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
				$message = get_option('generate_invoice_template');	
				$message_replacement = MJgmgt_string_replacemnet($arr,$message);
				$to[]=$userdata->user_email;
				$type='membership_invoice';
				MJgmgt_send_invoice_generate_mail($to,$subject,$message_replacement,$insert_id,$type);
				//save membership payment data into income table			
				$table_income=$wpdb->prefix.'gmgt_income_expense';
				$membership_name=MJgmgt_get_membership_name($data['membership_id']);
				$entry_array[]=array('entry'=>$membership_name,'amount'=>MJgmgt_get_membership_price($data['membership_id']));	
				$entry_array1[]=array('entry'=>__("Membership Signup Fee","gym_mgt"),'amount'=>MJgmgt_get_membership_signup_amount($data['membership_id']));	
				$entry_array_merge=array_merge($entry_array,$entry_array1);
				$incomedata['entry']=json_encode($entry_array_merge);
				$incomedata['invoice_type']='income';
				$incomedata['invoice_label']=__("Fees Payment","gym_mgt");
				$incomedata['supplier_name']=$data['member_id'];
				$incomedata['invoice_date']=date('Y-m-d');
				$incomedata['receiver_id']=$data['current_user_id'];
				$incomedata['amount']=$data['membership_amount'];
				$incomedata['total_amount']=$data['membership_amount'];
				$incomedata['invoice_no']=$invoice_no;
				$incomedata['paid_amount']=0;
				$incomedata['tax_id']=MJgmgt_get_membership_tax($data['membership_id']);
				$incomedata['payment_status']='Unpaid';
				$result=$wpdb->insert( $table_income,$incomedata);
				if($result)
				{
					$user_info=get_userdata($data['member_id']);
					$to = $user_info->user_email;           
					$subject = get_option('subscription_template_title'); 
					$search=array('[GMGT_MEMBERNAME]','[GMGT_MEMBERID]','[GMGT_STARTDATE]','[GMGT_ENDDATE]','[GMGT_MEMBERSHIP]','[GMGT_MEMBERSHIP_AMOUNT]');
					$membership_name=MJgmgt_get_membership_name($data['membership_id']);
					$replace = array($user_info->display_name,$user_info->member_id,$data['membership_valid_from'],$data['membership_valid_to'],$membership_name,$data['membership_amount']);
					$message = str_replace($search, $replace,get_option('subcription_mailcontent'));	
					wp_mail($to, $subject, $message);
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully inserted","gym_mgt");
					$response['result']='';
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Fill All Required Field","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Membership Payment List Function
	public function MJ_gmgt_membership_payment_list_data($data)
	{
		$response = array();
		$obj_membership_payment=new MJgmgt_membership_payment;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'membership_payment');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$paymentdata=$obj_membership_payment->MJgmgt_get_all_membership_payment_byuserid($user_id);
				}
				elseif($role == 'staff_member' || $role == 'accountant')
				{
					$paymentdata=$obj_membership_payment->MJgmgt_get_all_membership_payment();
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$paymentdata=$obj_membership_payment->MJgmgt_get_all_membership_payment();
			}
			else
			{
				$paymentdata=$obj_membership_payment->MJgmgt_get_all_membership_payment();
			}
			if(!empty($paymentdata))
			{
				$i=0;
				foreach ($paymentdata as $retrieved_data)
				{		
					if(!empty($retrieved_data->invoice_no))
					{
						$invoice_no=$retrieved_data->invoice_no;
					}
					else
					{
						$invoice_no='-';
					}	
					 $user=get_userdata($retrieved_data->member_id);
					$memberid=get_user_meta($retrieved_data->member_id,'member_id',true);
					$display_label=$user->display_name;
					if($memberid)
						$display_label.=" (".$memberid.")";
					$result[$i]['membership_id'] =	$retrieved_data->membership_id;
					$result[$i]['invoice_no'] =	$invoice_no;
					$result[$i]['membership_title'] =MJgmgt_get_membership_name($retrieved_data->membership_id);
					$result[$i]['member_name'] = $display_label;
					$result[$i]['amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.$retrieved_data->membership_amount;
					$result[$i]['paid_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.$retrieved_data->paid_amount;
					$result[$i]['due_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.$retrieved_data->membership_amount-$retrieved_data->paid_amount;
					$result[$i]['membership_valid_from'] = MJgmgt_getdate_in_input_box($retrieved_data->start_date);
					$result[$i]['membership_valid_to'] = MJgmgt_getdate_in_input_box($retrieved_data->end_date);
					$result[$i]['payment_status'] = __(MJgmgt_get_membership_paymentstatus($retrieved_data->mp_id), 'gym_mgt' );
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Single Membership Payment Function
	public function MJ_gmgt_single_membership_payment_data($data)
	{
		$obj_membership_payment=new MJgmgt_membership_payment;
		$result = $obj_membership_payment->MJgmgt_get_single_membership_payment($data['mp_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($result))
			{
				$membership_paymentdata['mp_id'] = $result->mp_id;
				$membership_paymentdata['invoice_no'] =$result->invoice_no;
				$membership_paymentdata['member_id'] =$result->member_id;
				$membership_paymentdata['membership_id'] =$result->membership_id;
				$membership_paymentdata['paid_amount'] = $result->paid_amount;
				$membership_paymentdata['membership_amount'] = $result->membership_amount;
				$membership_paymentdata['membership_valid_from'] = MJgmgt_getdate_in_input_box($result->start_date);
				$membership_paymentdata['membership_valid_to'] = MJgmgt_getdate_in_input_box($result->end_date);
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$membership_paymentdata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Delete Membership Payment Function
	public function MJ_gmgt_delete_membership_payment_data($data)
	{
		$response=array();
		$obj_membership_payment=new MJgmgt_membership_payment;
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			$result=$obj_membership_payment->MJgmgt_delete_payment($data['mp_id']);
			if($result)
			{
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record successfully deleted",'gym_mgt');
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//View Membership Payment Invoice Function
	public function MJ_gmgt_view_membership_payment_invoice_data($data)
	{
		$obj_payment= new MJgmgt_payment();
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($data['invoice_type']=='membership_invoice')
			{
				$obj_membership_payment=new MJgmgt_membership_payment;	
				$membership_data=$obj_membership_payment->MJgmgt_get_single_membership_payment($data['invoice_id']);
				$history_detail_result = MJgmgt_get_payment_history_by_mpid($data['invoice_id']);
			}
			if($data['invoice_type']=='income')
			{
				$income_data=$obj_payment->MJgmgt_get_income_data($data['invoice_id']);
				$history_detail_result = MJgmgt_get_income_payment_history_by_mpid($data['invoice_id']);
			}
			if($data['invoice_type']=='expense')
			{
				$expense_data=$obj_payment->MJgmgt_get_income_data($data['invoice_id']);
			}
			if($data['invoice_type']=='sell_invoice')
			{
				$obj_store=new MJgmgt_store;
				$selling_data=$obj_store->MJgmgt_get_single_selling($data['invoice_id']);
				$history_detail_result = MJgmgt_get_sell_payment_history_by_mpid($data['invoice_id']);
			}
						
			$invoicedata['invoice_id'] = $data['invoice_id'];
			$invoicedata['system_name'] =get_option('gmgt_system_name');
			$invoicedata['system_logo'] =get_option( 'gmgt_system_logo' );
			$invoicedata['system_address'] =get_option( 'gmgt_gym_address' );
			$invoicedata['system_email'] = get_option( 'gmgt_email' );
			$invoicedata['system_contact_number'] = get_option( 'gmgt_contact_number' );
			if(!empty($expense_data))
			{
			   $bill_to_name=chunk_split(ucwords($expense_data->supplier_name),30,"<BR>"); 
			}
			else
			{
				if(!empty($income_data))
					$member_id=$income_data->supplier_name;
				 if(!empty($membership_data))
					$member_id=$membership_data->member_id;
				 if(!empty($selling_data))
					$member_id=$selling_data->member_id;
				$patient=get_userdata($member_id);
				$bill_to_name=chunk_split(ucwords($patient->display_name),30,"<BR>"); 
				 $address=get_user_meta( $member_id,'address',true);
				$bill_to_name.=chunk_split($address,30,"<BR>"); 									 
				$bill_to_name.=get_user_meta( $member_id,'city_name',true ).","; 
				$bill_to_name.=get_user_meta( $member_id,'zip_code',true )."<br>"; 
				$bill_to_name.=get_user_meta( $member_id,'mobile',true )."<br>"; 
			}
			$invoicedata['bill_to_name'] = $bill_to_name;
			$issue_date='DD-MM-YYYY';
			$invoice_no='';
			if(!empty($income_data))
			{
				$issue_date=$income_data->invoice_date;
				$payment_status=$income_data->payment_status;
				$invoice_no=$income_data->invoice_no;
			}
			if(!empty($membership_data))
			{
				$issue_date=$membership_data->created_date;
				if($membership_data->payment_status!='0')
				{	
					$payment_status=$membership_data->payment_status;
				}
				else
				{
					$payment_status='Unpaid';
				}		
				$invoice_no=$membership_data->invoice_no;
			}
			if(!empty($expense_data))
			{
				$issue_date=$expense_data->invoice_date;
				$payment_status=$expense_data->payment_status;
				$invoice_no=$expense_data->invoice_no;
			}
			if(!empty($selling_data))
			{
				$issue_date=$selling_data->sell_date;	
				if(!empty($selling_data->payment_status))
				{
					$payment_status=$selling_data->payment_status;
				}	
				else
				{
					$payment_status='Fully Paid';
				}		
				
				$invoice_no=$selling_data->invoice_no;
			}
			$invoicedata['invoice_no'] = $invoice_no;
			$invoicedata['invoice_date'] = MJgmgt_getdate_in_input_box($issue_date);
			$invoicedata['status'] = __($payment_status,'gym_mgt');
			$id=1;
			$i=1;
			$total_amount=0;
			$income_entry=array();
			if(!empty($income_data) || !empty($expense_data))
			{
				if(!empty($expense_data))
					$income_data=$expense_data;							
				$member_income=$obj_payment->MJgmgt_get_oneparty_income_data_incomeid($income_data->invoice_id);
				foreach($member_income as $result_income)
				{
					$income_entries=json_decode($result_income->entry);
					$discount_amount=$result_income->discount;
					$paid_amount=$result_income->paid_amount;
					$total_discount_amount= $result_income->amount - $discount_amount;
					if($result_income->tax_id!='')
					{									
						$total_tax=0;
						$tax_array=explode(',',$result_income->tax_id);
						foreach($tax_array as $tax_id)
						{
							$tax_percentage=MJgmgt_tax_percentage_by_tax_id($tax_id);
							$tax_amount=$total_discount_amount * $tax_percentage / 100;
							$total_tax=$total_tax + $tax_amount;				
						}
					}
					else
					{
						$total_tax=$total_discount_amount * $result_income->tax/100;
					}
					$due_amount=0;
					$due_amount=$result_income->total_amount - $result_income->paid_amount;
					$grand_total=$total_discount_amount + $total_tax;
				   foreach($income_entries as $each_entry)
				   {
						$total_amount+=$each_entry->amount;
						$income_entry[$i]['date']=MJgmgt_getdate_in_input_box($result_income->invoice_date);
						$income_entry[$i]['entry']=$each_entry->entry;
						$income_entry[$i]['amount']=number_format($each_entry->amount,2);
						$id+=1;
						$i+=1;
					}
					if($grand_total=='0')									
					{
						if($income_data->payment_status=='Paid')
						{
							$grand_total=$total_amount;
							$paid_amount=$total_amount;
							$due_amount=0;										
						}
						else
						{
							$grand_total=$total_amount;
							$paid_amount=0;
							$due_amount=$total_amount;															
						}
					}
				}
			}
			$membership_entry=array();	
			$sell_entry=array();
			if(!empty($membership_data))
			{					
				$membership_signup_amounts=$membership_data->membership_signup_amount;
				$membership_entry[0]['date']=MJgmgt_getdate_in_input_box($membership_data->created_date);
				$membership_entry[0]['fees_type']=MJgmgt_get_membership_name($membership_data->membership_id);
				$membership_entry[0]['amount']=number_format($membership_data->membership_fees_amount,2);
				if( $membership_signup_amounts  > 0) 
				{
					$membership_entry[1]['date']=MJgmgt_getdate_in_input_box($membership_data->created_date);
					$membership_entry[1]['fees_type']=_e('Membership Signup Fee','gym_mgt');								
					$membership_entry[1]['amount']=number_format($membership_data->membership_signup_amount,2);
				}
			}
			if(!empty($selling_data))
			{								
				$all_entry=json_decode($selling_data->entry);
				
				if(!empty($all_entry))
				{
					foreach($all_entry as $entry)
					{
						$obj_product=new MJgmgt_product;
						$product = $obj_product->MJgmgt_get_single_product($entry->entry);
							$product_name=$product->product_name;					
							$quentity=$entry->quentity;	
							$price=$product->price;										
						$sell_entry[$i]['date']=MJgmgt_getdate_in_input_box($selling_data->sell_date);$sell_entry[$i]['product_name']=$product_name;
						$sell_entry[$i]['quantity']=$quentity;
						$sell_entry[$i]['price']=$price;
						$sell_entry[$i]['total_price']=number_format($quentity * $price,2);
					$id+=1;
					$i+=1;
					}
				}
				else
				{
					$obj_product=new MJgmgt_product;
					$product = $obj_product->MJgmgt_get_single_product($selling_data->product_id);
					$product_name=$product->product_name;					
					$quentity=$selling_data->quentity;	
					$price=$product->price;	
					$sell_entry[$i]['date']=MJgmgt_getdate_in_input_box($selling_data->sell_date);$sell_entry[$i]['product_name']=$product_name;
					$sell_entry[$i]['quantity']=$quentity;
					$sell_entry[$i]['price']=$price;
					$sell_entry[$i]['total_price']=number_format($quentity * $price,2);
					$id+=1;
					$i+=1;
				}	
			}
			if($data['invoice_type']=='membership_invoice')
			{
				$invoicedata['entries'] = $membership_entry;
			}
			elseif($data['invoice_type']=='sell_invoice')
			{
				$invoicedata['entries'] = $sell_entry;
			}
			else
			{
				$invoicedata['entries'] = $income_entry;
			}
			$invoicedata['bank)name'] = get_option( 'gmgt_bank_name' );
			$invoicedata['account_no'] = get_option( 'gmgt_bank_acount_number' );
			$invoicedata['ifsc_code'] = get_option( 'gmgt_bank_ifsc_code' );
			$invoicedata['paypal_id'] = get_option( 'gmgt_paypal_email' );
			if(!empty($history_detail_result))
			{
				$invoicedata['payment_history'] = $history_detail_result;
			}
			else
			{
				$invoicedata['payment_history'] = $history_detail_result;
			}
			if(!empty($membership_data))
			{
				$total_amount=$membership_data->membership_fees_amount+$membership_data->membership_signup_amount;
				$total_tax=$membership_data->tax_amount;							
				$paid_amount=$membership_data->paid_amount;
				$due_amount=abs($membership_data->membership_amount - $paid_amount);
				$grand_total=$membership_data->membership_amount;
			}
			if(!empty($expense_data))
			{
				$grand_total=$total_amount;
			}
			if(!empty($selling_data))
			{
				$all_entry=json_decode($selling_data->entry);
				if(!empty($all_entry))
				{
					$total_amount=$selling_data->amount;
					$discount_amount=$selling_data->discount;
					$total_discount_amount=$total_amount-$discount_amount;
					if($selling_data->tax_id!='')
					{									
						$total_tax=0;
						$tax_array=explode(',',$selling_data->tax_id);
						foreach($tax_array as $tax_id)
						{
							$tax_percentage=MJgmgt_tax_percentage_by_tax_id($tax_id);
							$tax_amount=$total_discount_amount * $tax_percentage / 100;
							$total_tax=$total_tax + $tax_amount;				
						}
					}
					else
					{
						$tax_per=$selling_data->tax;
						$total_tax=$total_discount_amount * $tax_per/100;
					}
					
					$paid_amount=$selling_data->paid_amount;
					$due_amount=abs($selling_data->total_amount - $paid_amount);
					$grand_total=$selling_data->total_amount;
				}
				else
				{	
					$obj_product=new MJgmgt_product;
					$product = $obj_product->MJgmgt_get_single_product($selling_data->product_id);
					$price=$product->price;
					$total_amount=$price*$selling_data->quentity;
					$discount_amount=$selling_data->discount;
					$total_discount_amount=$total_amount-$discount_amount;
					if($selling_data->tax_id!='')
					{									
						$total_tax=0;
						$tax_array=explode(',',$selling_data->tax_id);
						foreach($tax_array as $tax_id)
						{
							$tax_percentage=MJgmgt_tax_percentage_by_tax_id($tax_id);	
							$tax_amount=$total_discount_amount * $tax_percentage / 100;
							$total_tax=$total_tax + $tax_amount;				
						}
					}
					else
					{
						$tax_per=$selling_data->tax;
						$total_tax=$total_discount_amount * $tax_per/100;
					}
													
					$paid_amount=$total_amount;
					$due_amount='0';
					$grand_total=$total_amount;								
				}		
			}
			$invoicedata['sub_total'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($total_amount,2);
			$invoicedata['discount_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($discount_amount,2);		
			$invoicedata['tax_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($total_tax,2);
			$invoicedata['due_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($due_amount,2);
			$invoicedata['paid_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($paid_amount,2);
			$invoicedata['grand_total'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($grand_total,2);
			$response['status']=1;
			$response['error_code']=200;
			$response['error']=__("Record found successfully",'gym_mgt');
			$response['result']=$invoicedata;
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;	
	}
	//Pay Membership Payment Function
	function MJ_gmgt_pay_membership_invoice_data($data)
	{
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			$response=array();
			$obj_membership_payment=new MJgmgt_membership_payment;
			$feedata['mp_id']=$data['mp_id'];
			$feedata['amount']=$data['amount'];
			$feedata['payment_method']=$data['payment_method'];	
			$feedata['trasaction_id']=$data['trasaction_id'];
			$feedata['created_by']=$data['current_user_id'];	
			$result = $obj_membership_payment->MJgmgt_add_feespayment_history($feedata);
			if($result)
			{
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record successfully inserted","gym_mgt");
				$response['result']='';
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not inserted",'gym_mgt');
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>