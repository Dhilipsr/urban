<?php 
class MJ_Gmgt_Tax_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Tax list
		if($_REQUEST['gmgt_json_api'] == 'tax_list')
		{
			$response=$this->MJ_gmgt_tax_list_data($_REQUEST);	 

			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Tax
		if($_REQUEST['gmgt_json_api'] == 'single_tax')
		{
			$response=$this->MJ_gmgt_single_tax_data($_REQUEST);	 

			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Tax List Function
	public function MJ_gmgt_tax_list_data($data)
	{
		$obj_tax=new MJ_Gmgttax;
		
		$taxdata=$obj_tax->MJ_gmgt_get_all_taxes();
		
		$response	=	array();

		if(!empty($taxdata))
		{	
			$i=0;
			foreach ($taxdata as $retrieved_data)
			{		
				$result[$i]['tax_id'] =	$retrieved_data->tax_id;
				$result[$i]['tax_name'] =	$retrieved_data->tax_title;
				$result[$i]['tax_value'] = $retrieved_data->tax_value;
				$i++;
			}
			$response['status']=1;
			$response['error_code']=200;
			$response['error']=__("Record found successfully",'gym_mgt');
			$response['result']=$result;
			return $response;
		}
		else
		{
			$result=array();
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("Record not found",'gym_mgt');
			$response['result']=$result;
		}
		return $response;
	}
	//Single Tax Function
	public function MJ_gmgt_single_tax_data($data)
	{
		 $obj_tax=new MJ_Gmgttax;
		 $result =  $obj_tax->MJ_gmgt_get_single_tax_data($data['tax_id']);
		 
		 if(!empty($result))
		 {			
			$taxdata['tax_id'] = $result->tax_id;
			$taxdata['tax_name'] =$result->tax_title;
			$taxdata['tax_value'] = $result->tax_value;
			
			$response['status']=1;
			$response['error_code']=200;
			$response['error']=__("Record found successfully",'gym_mgt');
			$response['result']=$taxdata;
			return $response;
		}
		else
		{
			$response['status']=0;
			$response['error']=__("Record not found",'gym_mgt');
			$response['result']=Null;
		}
		return $response;
	}
}
?>