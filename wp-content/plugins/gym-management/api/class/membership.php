<?php 
class MJ_Gmgt_Membership_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Add Membership Type
		if($_REQUEST['gmgt_json_api'] == 'add_membership_type')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='add';
			$response=$this->MJ_gmgt_add_membership_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			} 
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Membership Type
		if($_REQUEST['gmgt_json_api'] == 'edit_membership_type')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='edit';
			$response=$this->MJ_gmgt_add_membership_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Membership Type list
		if($_REQUEST['gmgt_json_api'] == 'membership_type_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_membership_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Membership Type
		if($_REQUEST['gmgt_json_api'] == 'single_membership_type')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_membership_data($data);	
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Membership Type
		if($_REQUEST['gmgt_json_api'] == 'delete_membership_type')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_membership_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Add-Edit Membership Type Function
	function MJ_gmgt_add_membership_data($data,$action)
	{
		$response=array();
		global $wpdb;
		$obj_activity=new MJgmgt_activity;
		$table_membership = $wpdb->prefix. 'gmgt_membershiptype';
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'membership');
		//------- membership table data --------------
		if ($data['access_token'] == $access_token)
		{
			$membershipdata['membership_label']=MJgmgt_strip_tags_and_stripslashes($data['membership_name']);
			$membershipdata['membership_cat_id']=$data['membership_category'];
			$membershipdata['membership_length_id']=$data['membership_period'];		
			$membershipdata['membership_class_limit']=$data['members_limit'];
			if(isset($data['on_of_member']))
			{
				$membershipdata['on_of_member']=$data['no_of_members'];
			}
			else
			{
				$membershipdata['on_of_member']=0;
			}
			$membershipdata['classis_limit']=$data['class_limit'];		
			if(isset($data['on_of_classis']))
			{
				$membershipdata['on_of_classis']=$data['no_of_classis'];
			}
			else
			{
				$membershipdata['on_of_classis']=0;
			}
			$membershipdata['install_plan_id']=$data['installment_plan'];
			$membershipdata['membership_amount']=$data['membership_amount'];
			$membershipdata['installment_amount']=$data['installment_amount'];
			$membershipdata['signup_fee']=$data['signup_fee'];
			$membershipdata['membership_description']=$data['membership_description'];
			if(isset($_FILES['membership_image']) && !empty($_FILES['membership_image']) && $_FILES['membership_image']['size'] !=0)
			{
				if($_FILES['membership_image']['size'] > 0)
				{
					$membership_image=MJ_gmgt_load_documets($_FILES['membership_image'],'membership_image','pimg');
					$membership_image_url=content_url().'/uploads/gym_assets/'.$membership_image;
				}
							
			}
			else
			{	
				if(isset($data['hidden_membership_image']))
				{
					$membership_image=$data['hidden_membership_image'];
					$membership_image_url=$membership_image;
				}
			}
			
			$membershipdata['gmgt_membershipimage']=$membership_image_url;
			$membershipdata['created_date']=date("Y-m-d");
			$membershipdata['created_by_id']=$data['current_user_id'];	
			$membershipdata['activity_cat_status']=1;
			if(isset($data['tax']))
			{
				$membershipdata['tax']=implode(",",$data['tax']);		
			}
			else
			{
				$membershipdata['tax']=null;		
			}
			if(isset($data['activity_cat_id']))
			{
				$membershipdata['activity_cat_id']=implode(",",$data['activity_cat_id']);
			}
			else
			{
				$membershipdata['activity_cat_id']=null;
			}
			if($action == 'edit')
			{
				if($menu_access_data['edit'] == '1')
				{
					$membershipid['membership_id']=$data['membership_id'];
					$result=$wpdb->update( $table_membership, $membershipdata ,$membershipid);
					$obj_activity->MJgmgt_add_membership_activities($data);
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully Updated","gym_mgt");
						$response['result']='';
					}
					else
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
						$response['result']='';
						return $response;
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("An Unauthorized User","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
			else
			{
				if($menu_access_data['add'] == '1')
				{
					$result=$wpdb->insert( $table_membership, $membershipdata );
					if($result)
						$result=$wpdb->insert_id;
					$data['membership_id']=$result;
					$obj_activity->MJgmgt_add_membership_activities($data);
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully inserted","gym_mgt");
						$response['result']='';
					}
					else
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
						$response['result']='';
						return $response;
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("An Unauthorized User","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}	
		return $response;
	}
	//Membership Type List Function
	public function MJ_gmgt_membership_list_data($data)
	{

		$obj_membership=new MJgmgt_membership;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		$access_token = get_user_meta($user_id , 'access_token' , true);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'membership');
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{
					$membership_id = get_user_meta( $user_id,'membership_id', true ); 
					$membershipdata=$obj_membership->MJgmgt_get_member_own_membership($membership_id);					
				}
				elseif($role == 'staff_member' || $role == 'accountant')
				{	
					$membershipdata=$obj_membership->MJ_gmgt_get_membership_by_created_by($user_id);	
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$membershipdata=$obj_membership->MJgmgt_get_all_membership();
			}
			else
			{
				$membershipdata="";
			}
			
			$response	=	array();

			if(!empty($membershipdata))
			{	
				$i=0;
				foreach ($membershipdata as $retrieved_data)
				{
					if($retrieved_data->install_plan_id == 0)
					{
						$plan_id="";
					}
					else
					{
						$plan_id=get_the_title( $retrieved_data->install_plan_id );
					}
					$result[$i]['membership_id'] =	$retrieved_data->membership_id;
					$result[$i]['membership_image'] = $retrieved_data->gmgt_membershipimage;
					$result[$i]['membership_name'] =  $retrieved_data->membership_label;
					$result[$i]['membership_code'] =	"[MembershipCode id=".$retrieved_data->membership_id."]";
					$result[$i]['membership_period'] =	$retrieved_data->membership_length_id;
					$result[$i]['membership_amount'] =	$retrieved_data->membership_amount;
					$result[$i]['installment_plan'] =	$retrieved_data->installment_amount." ".$plan_id;
					$result[$i]['signup_fee'] =	$retrieved_data->signup_fee;
					$result[$i]['currency_symbol'] = MJgmgt_get_currency_symbol_api(get_option( 'gmgt_currency_code'));
					if(!empty($retrieved_data->tax)) 
					{ 
						$tax=MJgmgt_tax_name_by_tax_id_array($retrieved_data->tax); 
					}
					else
					{ 
						$tax='-'; 
					}
					$result[$i]['tax'] = $tax;
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User",'gym_mgt');
		}
		return $response;
	}
	//Single Membership Type Function
	public function MJ_gmgt_single_membership_data($data)
	{
		$obj_membership=new MJgmgt_membership;
		$obj_activity=new MJgmgt_activity;
		$result = $obj_membership->MJgmgt_get_single_membership($data['memebrship_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			$obj_tax = new MJgmgt_tax;
			if(!empty($result))
			{
				$membershipdata['membership_name'] = $result->membership_label;
				$membershipdata['membership_category'] =$result->membership_cat_id;
				$membershipdata['membership_category_name'] =get_the_title($result->membership_cat_id);
				$membershipdata['membership_period'] = $result->membership_length_id;
				$membershipdata['members_limit'] = $result->membership_class_limit;
				$membershipdata['class_limit'] = $result->classis_limit;
				$membershipdata['membership_amount'] =$result->membership_amount;
				$membershipdata['installment_plan'] = $result->installment_amount;
				$membershipdata['signup_fee'] = $result->signup_fee;
				$tax_id=explode(',',$result->tax);
				if(!empty($tax_id))
				{
					$tax = array();
					$taxdata = array();
					$i = 1;
					foreach($tax_id as $data)
					{
						$tax_data = $obj_tax->MJgmgt_get_single_tax_data($data);
						$tax[$i]['tax_id'] = $data;
						$tax[$i]['tax_name'] = $tax_data->tax_title;
						$tax[$i]['tax_value'] = $tax_data->tax_value;
						$i++;
					}
					$taxdata = json_encode($tax);
				}
				$membershipdata['tax'] = $taxdata;			
				$membershipdata['activity_cat_id'] = $result->activity_cat_id;
				// $activity_array = $obj_activity->MJgmgt_get_membership_activity($data['memebrship_id']);
				$membershipdata['membership_description'] = $result->membership_description;
				$membershipdata['membership_image'] = $result->gmgt_membershipimage;
					   
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$membershipdata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User",'gym_mgt');
			$response['result']=Null;
		}
		return $response;
	}
	//Delete Membership Type Function
	public function MJ_gmgt_delete_membership_data($data)
	{
		$response=array();
		$obj_membership=new MJgmgt_membership;
		$result=$obj_membership->MJgmgt_delete_membership($data['membership_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'membership');
		if($menu_access_data['delete'] == '1')
		{
			if ($data['access_token'] == $access_token)
			{
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully deleted",'gym_mgt');
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Record not found",'gym_mgt');
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("An Unauthorized User",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("You Can't Access This Page","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>