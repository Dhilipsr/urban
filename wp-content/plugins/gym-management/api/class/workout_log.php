<?php 
class MJ_Gmgt_Workoutlog_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Add Workout Log 
		if($_REQUEST['gmgt_json_api'] == 'add_workoutlog')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_add_workoutlog_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		} 
		//Add Measurement
		if($_REQUEST['gmgt_json_api'] == 'add_measurement')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_add_measurement_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Workout list
		if($_REQUEST['gmgt_json_api'] == 'workout_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_workout_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Measurement List
		if($_REQUEST['gmgt_json_api'] == 'view_measurement_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_view_measurement_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Workout Log
		if($_REQUEST['gmgt_json_api'] == 'view_workout_log')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_view_workout_log_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Workout Log
		if($_REQUEST['gmgt_json_api'] == 'list_workout_log')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_list_workout_log_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Add Workout Log Function
	function MJ_gmgt_add_workoutlog_data($data)
	{
		$response=array();
		$obj_workout=new MJgmgt_workout;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_daily_workouts';		
		$workoutdata['record_date']=$curr_date=MJgmgt_get_format_for_db($data['record_date']);
		$workoutdata['note']=MJgmgt_strip_tags_and_stripslashes($data['note']);
		$workoutdata['workout_id']=$data['user_workout_id'];
		$workoutdata['created_date']=date("Y-m-d");
		$workoutdata['created_by']=$user_id;
		$workoutdata['member_id']=$data['member_id'];
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'workouts');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['add'] == '1')
			{
				if(!empty($data['workouts_array']))
				{
					$exists_record=MJgmgt_check_user_workouts($data['member_id'],$data['record_date']);
					if($exists_record==0)
					{
						$result=$wpdb->insert( $table_workout, $workoutdata );
						$insertid=$wpdb->insert_id;
						$result=$obj_workout->MJgmgt_add_user_workouts($wpdb->insert_id,$data);
						$abc=$wpdb->insert_id;
						//assign workout SEND MAIL NOTIFICATION
						$asignby=$data['current_user_id'];
						$userdata=get_userdata($asignby);
						$username=$userdata->display_name;
						$useremail=$userdata->user_email;
						$userid=$userdata->ID;
						//$last_workoutid=$data['daily_workout_id'];
						 $recorddate=$workoutdata['record_date'];
						 $gymname=get_option( 'gmgt_system_name' );
						 $day_name = date('l', strtotime($workoutdata['created_date']));
						 $arr['[GMGT_STAFF_MEMBERNAME]']=$username;	
						 $arr['[GMGT_DAY_NAME]']=$day_name;
						 $arr['[GMGT_DATE]']=$workoutdata['created_date'];
						 $arr['[GMGT_GYM_NAME]']=$gymname;
						 $subject =get_option('Submit_Workouts_Subject');
						 $sub_arr['[GMGT_STAFF_MEMBERNAME]']=$gymname;
						 $subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
						 $message = get_option('Submit_Workouts_Template');
						 $message_replacement = MJgmgt_string_replacemnet($arr,$message);
						 $invoice=MJgmgt_submit_workout_html_content($workoutdata['member_id'],$recorddate);
						 $invoic_concat=$message_replacement. $invoice;
						 $to[]=$useremail;
						 
						 $abc=MJgmgt_send_mail_text_html($to,$subject,$invoic_concat);
						if($result)
						{
							$response['status']=1;
							$response['error_code']=200;
							$response['error']=__("Record successfully inserted","gym_mgt");
							$response['result']='';
							return $response;

						}
						else
						{
							$response['status']=0;
							$response['error_code']=401;
							$response['error']=__("Please Fill All Fields",'gym_mgt');
							$response['result']='';
							return $response;
						}
					}
					else
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("Workout is already available for today.",'gym_mgt');
						$response['result']='';
						return $response;
					}		
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Today Can Not Assign Workout.",'gym_mgt');
					$response['result']='';
					return $response;
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Add Measurement Function
	function MJ_gmgt_add_measurement_data($data)
	{
		$response=array();
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		global $wpdb;
		$table_gmgt_measurment = $wpdb->prefix. 'gmgt_measurment';
		$workoutdata['user_id']=$data['member_id'];	
		$workoutdata['result_measurment']=MJgmgt_strip_tags_and_stripslashes($data['result_measurment']);
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'workouts');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['add'] == '1')
			{
				if(isset($_FILES['measurement_image']) && !empty($_FILES['measurement_image']) && $_FILES['measurement_image']['size'] !=0)
				{			
					if($_FILES['measurement_image']['size'] > 0)
						 $measurement_image=MJgmgt_load_documets($_FILES['measurement_image'],'measurement_image','pimg');
						$measurement_image_url=content_url().'/uploads/gym_assets/'.$measurement_image;					
				}
				else
				{		
					if(isset($data['hidden_measurement_image']))
						$measurement_image=$data['hidden_measurement_image'];
						$measurement_image_url=$measurement_image;
				}
				if(!empty($data['result_measurment']))
				{
					foreach ($data['result_measurment'] as $value) 
					{
						$result = 'result_'.$value;
						$workoutdata['result_measurment']=MJgmgt_strip_tags_and_stripslashes($value);
						$workoutdata['result']=MJgmgt_strip_tags_and_stripslashes($data[$result]);
						$workoutdata['result_date']=MJgmgt_get_format_for_db($data['result_date']);
						$workoutdata['created_date']=date("Y-m-d");
						$workoutdata['created_by']=$user_id;
						$result=$wpdb->insert( $table_gmgt_measurment, $workoutdata );
					}
				}
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully inserted","gym_mgt");
					$response['result']='';
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Please Fill All Fields",'gym_mgt');
					$response['result']='';
					return $response;
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Workout List Function
	public function MJ_gmgt_workout_list_data($data)
	{
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'workouts');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$membersdata=array();
					$membersdata[] = get_userdata($user_id);
				}
				elseif($role == 'staff_member' || $role == 'accountant')
				{	
					$get_members = array('role' => 'member');
					$membersdata=get_users($get_members);	
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				if($role == 'member')
				{	
					$membersdata=array();
					$membersdata[] = get_userdata($user_id);
				}
				elseif($role == 'staff_member' || $role == 'accountant')
				{	
					$get_members = array('role' => 'member');
					$membersdata=get_users($get_members);	
				}
			}
			else
			{
				$membersdata="";
			}
			
			$response	=	array();

			if(!empty($membersdata))
			{	
				$i=0;
				foreach ($membersdata as $retrieved_data)
				{	
					$userimage=get_user_meta($retrieved_data->ID, 'gmgt_user_avatar', true);
					if(empty($userimage))
					{
						$userimage=get_option( 'gmgt_system_logo' );
					}
					else
					{
						$userimage=$userimage;
					}
					$user=get_userdata($retrieved_data->ID);
					$display_label=$user->display_name;
					if($memberid)
					{
						$display_label.=" (".$memberid.")";
					}
					if($user->member_type!='Prospect')
					{ 
						$joining_date= MJgmgt_getdate_in_input_box($user->begin_date); 
						$exp_date= MJgmgt_getdate_in_input_box(MJgmgt_check_membership($user->ID)); 
					}
					else
					{
						$joining_date="--"; 
						$exp_date="--"; 
					}
					$memberid=get_user_meta($retrieved_data->ID,'member_id',true);
					$result[$i]['photo'] =	$userimage;
					$result[$i]['member_name'] = $display_label;
					$result[$i]['membership'] = MJgmgt_get_membership_name($user->membership_id);
					$result[$i]['joining_date'] = $joining_date;
					$result[$i]['exp_date'] = $exp_date;
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Measurement List Function
	public function MJ_gmgt_view_measurement_list_data($data)
	{
		$obj_workout = new MJgmgt_workout();
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		$measurement_data = $obj_workout->MJgmgt_get_all_measurement_by_userid($user_id);
		$response	=	array();
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'workouts');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($measurement_data))
			{	
				$i=0;
				foreach ($measurement_data as $retrieved_data)
				{	
					$userimage=$retrieved_data->gmgt_progress_image;
					if(empty($userimage))
					{
						$userimage=get_option( 'gmgt_system_logo' );
					}
					else
					{
						$userimage=$userimage;
					}
					$user=get_userdata($retrieved_data->ID);
					$display_label=$user->display_name;
					
					$memberid=get_user_meta($retrieved_data->ID,'member_id',true);
					$result[$i]['photo'] =	$userimage;
					$result[$i]['member_name'] = $display_label;
					$result[$i]['measurement'] = $retrieved_data->result_measurment;
					$result[$i]['result'] = $retrieved_data->result." ".MJgmgt_measurement_counts_lable_array($retrieved_data->result_measurment);
					$result[$i]['record_date'] = MJgmgt_getdate_in_input_box($retrieved_data->result_date);
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//View Workout Log Function
	public function MJ_gmgt_view_workout_log_data($data)
	{
		$obj_workout = new MJgmgt_workout();
		$tcurrent_date=MJgmgt_get_format_for_db($data['current_date']);
		$today_workouts=$obj_workout->MJgmgt_get_member_today_workouts($data['member_id'],$tcurrent_date);
		$i=0;
		 if(!empty($today_workouts))
		 {	
			foreach($today_workouts as $value)
			{
				$workoutid=$value->user_workout_id;
				$activity_name=$value->workout_name;
				$workflow_category=$obj_workout->MJgmgt_get_user_workouts($workoutid,$activity_name);
				if($workflow_category->sets!='0')
				{
					$sets_progress=$value->sets*100/$workflow_category->sets;
				}
				else
				{
					$sets_progress=100;
				}
				if($workflow_category->reps!='0')
				{							
					$reps_progress=$value->reps*100/$workflow_category->reps;
				}
				else
				{
					$reps_progress=100;
				}
				if($workflow_category->kg!='0')
				{
					$kg_progress=$value->kg*100/$workflow_category->kg;
				}
				else
				{
					$kg_progress=100;
				}
				if($workflow_category->time!='0')
				{
					$rest_time_progress=$value->rest_time*100/$workflow_category->time;
				}
				else
				{
					$rest_time_progress=100;
				}
				$result[$i]['workout_name'] =$value->workout_name;
				$result[$i]['sets'] = $value->sets.' '.__('Out Of','gym_mgt').' '.$workflow_category->sets.' '.__('Sets','gym_mgt');
				$result[$i]['reps'] = $value->reps.' '.__('Out Of','gym_mgt').' '.$workflow_category->reps.' '.__('Reps','gym_mgt');
				$result[$i]['kg'] = $value->kg.' '.__('Out Of','gym_mgt').' '.$workflow_category->kg.' '.__('Kg','gym_mgt');
				$result[$i]['rest_time'] = $value->rest_time.' '.__('Out Of','gym_mgt').' '.$workflow_category->time.' '.__('Rest Time','gym_mgt');
				$i++;
			}
			$response['status']=1;
			$response['error_code']=200;
			$response['error']=__("Record found successfully",'gym_mgt');
			$response['result']=$result;
			return $response;
		 }
		 else
		 {
			$result=array();
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("Record not found",'gym_mgt');
			$response['result']=$result;
		}
		return $response;
	}
	//View Workout Log Function
	public function MJ_gmgt_list_workout_log_data($data)
	{
		$user_id=$data['current_user_id'];
		global $wpdb;
		$table_name = $wpdb->prefix."gmgt_assign_workout";
		$table_gmgt_workout_data = $wpdb->prefix."gmgt_workout_data";
		$date = date('Y-m-d');
		$record_date = $data['record_date'];
		$day_name = date('l', strtotime($date));
		$sql = "Select * From $table_name as workout,$table_gmgt_workout_data as workoutdata where workout.user_id = $user_id AND  workout.workout_id = workoutdata.workout_id AND workoutdata.day_name = '$day_name' AND '".$record_date."' between workout.Start_date and workout.End_date ";
		$result1 = $wpdb->get_results($sql);
		$i=0;
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{		
			if(!empty($result1))
			{
				foreach ($result1 as $retrieved_data)
				{
					$workout_id=$retrieved_data->workout_id;
					$result[$i]['workout_id']= $retrieved_data->id;
					$result[$i]['workout_name']= $retrieved_data->workout_name;
					$result[$i]['sets'] = $retrieved_data->sets;
					$result[$i]['reps'] = $retrieved_data->reps;
					$result[$i]['kg'] = $retrieved_data->kg;
					$result[$i]['rest_time'] = $retrieved_data->time;
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=esc_html__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=esc_html__("No Workout assigned for today",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>