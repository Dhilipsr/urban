<?php 
class MJ_Gmgt_Attendance_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Take Attendance
		if($_REQUEST['gmgt_json_api'] == 'take_attendance')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_take_attendance_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Attendance Class Wise
		if($_REQUEST['gmgt_json_api'] == 'view_attendance')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_view_attendance_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Single Member Attendance List
		if($_REQUEST['gmgt_json_api'] == 'view_member_attendance_list')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_view_member_attendance_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Take Attendance Function
	function MJ_gmgt_take_attendance_data($data)
	{
		$obj_attend=new MJgmgt_attendence;
		$response=array();
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(isset($data['attendence']))
			{			
				$attend_by=$data['current_user_id'];
				$result=$obj_attend->MJgmgt_save_attendence($data['date'],$data['class'],$data['attendence'],$attend_by,$data['status']);
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Attendance successfully saved","gym_mgt");
				}
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Please select at least one member.",'gym_mgt');
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//View Attendance Function
	public function MJ_gmgt_view_attendance_data($data)
	{	
		$response	=	array();
		$obj_attend=new MJgmgt_attendence;
		$class_id =$data['class_id'];
		$date=date("Y-m-d");
		$membersdata= array();
		$MemberClassData = MJgmgt_get_member_by_class_id($data['class_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			foreach($MemberClassData as $key=>$value)
			{
				$members= get_userdata($value->member_id);
				if($members!=false)
				{
					$role= $members->roles;					
					if($role[0]!='staff_member')
					{
						$membersdata[]=$members;						
					}
				}															 
			}
			if(!empty($membersdata))
			{	
				$i=0;
				foreach ($membersdata as $retrieved_data)
				{		
					$check_result=$obj_attend->MJgmgt_check_attendence($retrieved_data->ID,$class_id,$date);
					if($check_result=='true')
					{
						$status=_e('Present','gym_mgt');
					}
					else
					{
						$status=_e('Absent','gym_mgt');
					}
					$result[$i]['date'] = $date;
					$result[$i]['member_name'] =$retrieved_data->first_name.' '.$retrieved_data->last_name.' ('.$retrieved_data->member_id.')';
					$result[$i]['status'] = $status;
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//View Single Member Attendance List Function
	public function MJ_gmgt_view_member_attendance_data($data)
	{
		$response	=	array();
		$obj_attend=new MJgmgt_attendence;
		$curr_user_id =$data['member_id'];
		$start_date =$data['start_date'];
		$end_date =$data['end_date'];
		$attendance = MJgmgt_view_member_attendance($start_date,$end_date,$curr_user_id);
		$current_date =$start_date;
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($attendance))
			{	
				$i=0;
				while ($end_date >= $current_date)
				{		
					$attendance_status = MJgmgt_get_attendence($curr_user_id,$current_date);
					if(!empty($attendance_status))
					{
						$status=MJgmgt_get_attendence($user_id,$curremt_date);
					}
					else 
					{
						$status=__('Absent','gym_mgt');
					}
					$result[$i]['date'] = MJgmgt_getdate_in_input_box($current_date);
					$result[$i]['day'] =date("D", strtotime($current_date));
					$result[$i]['status'] = $status;
					
					$current_date = strtotime("+1 day", strtotime($current_date));
					$current_date = date("Y-m-d", $current_date);
								
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>