<?php 
class MJ_Gmgt_Noticet_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Notice list
		if($_REQUEST['gmgt_json_api'] == 'notice_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_notice_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Notice
		if($_REQUEST['gmgt_json_api'] == 'view_notice')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_view_notice_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Notice List Function
	public function MJ_gmgt_notice_list_data($data)
	{
		$obj_notice=new MJgmgt_notice;
		$obj_class=new MJgmgt_classschedule;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			//Get User Acsess //
			$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'notice');
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				$noticedata =$obj_notice->MJgmgt_get_notice($role);
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$noticedata =$obj_notice->MJgmgt_get_all_notice();
			}
			else
			{
				$noticedata="";
			}
			
			$response	=	array();

			if(!empty($noticedata))
			{	
				$i=0;
				foreach ($noticedata as $retrieved_data)
				{
					$class_id=get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true);
					if($class_id!="")
					{
						$ClassArr=MJgmgt_get_current_user_classis($user_id);
						$staff_classes=$obj_class->MJgmgt_getClassesByStaffmeber($user_id);
						if($role=="member" && in_array($class_id,$ClassArr))
						{		
							$result[$i]['notice_id'] =	$retrieved_data->ID;
							$result[$i]['notice_title'] =	$retrieved_data->post_title;
							$strlength= strlen($retrieved_data->post_content);
							if($strlength > 60)
								$notice_content=substr($retrieved_data->post_content, 0,60).'...';
							else
								$notice_content=$retrieved_data->post_content;
							$result[$i]['notice_content'] = $notice_content;
							$result[$i]['notice_for'] = ucwords(str_replace("_"," ",get_post_meta( $retrieved_data->ID, 'notice_for',true)));
							
							if(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) !="" && get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) =="all")
							{
								 $class=_e('All','gym_mgt');
							 }
							 elseif(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) !="")
							 {
								$class=MJgmgt_get_class_name(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true));
							 }
							 else
							 {
								 $class='-';
							 }
							$result[$i]['class'] = $class;
							$result[$i]['start_date'] = get_post_meta($retrieved_data->ID,'gmgt_start_date',true);
							$result[$i]['end_date'] = get_post_meta($retrieved_data->ID,'gmgt_end_date',true);
						}
						if($role=="staff_member" && !empty($staff_classes) && in_array($class_id,$staff_classes))
						{
							$result[$i]['notice_id'] =	$retrieved_data->ID;
							$result[$i]['notice_title'] =	$retrieved_data->post_title;
							$strlength= strlen($retrieved_data->post_content);
							if($strlength > 60)
								$notice_content=substr($retrieved_data->post_content, 0,60).'...';
							else
								$notice_content=$retrieved_data->post_content;
							$result[$i]['notice_content'] = $notice_content;
							$result[$i]['notice_for'] = ucwords(str_replace("_"," ",get_post_meta( $retrieved_data->ID, 'notice_for',true)));
							
							if(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) !="" && get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) =="all")
							{
								 $class=_e('All','gym_mgt');
							 }
							 elseif(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) !="")
							 {
								$class=MJ_gmgt_get_class_name(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true));
							 }
							 else
							 {
								 $class='-';
							 }
							$result[$i]['class'] = $class;
							$result[$i]['start_date'] = get_post_meta($retrieved_data->ID,'gmgt_start_date',true);
							$result[$i]['end_date'] = get_post_meta($retrieved_data->ID,'gmgt_end_date',true);
						}
					}
					else
					{	
						$result[$i]['notice_id'] =	$retrieved_data->ID;
						$result[$i]['notice_title'] =	$retrieved_data->post_title;
						$strlength= strlen($retrieved_data->post_content);
						if($strlength > 60)
						{
							$notice_content=substr($retrieved_data->post_content, 0,60).'...';
						}
						else
						{
							$notice_content=$retrieved_data->post_content;
						
						$result[$i]['notice_content'] = $notice_content;
						$result[$i]['notice_for'] = ucwords(str_replace("_"," ",get_post_meta( $retrieved_data->ID, 'notice_for',true)));
						}
						if(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) !="" && get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) =="all")
						{
							 $class=_e('All','gym_mgt');
						}
						elseif(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) !="")
						{
							$class=MJ_gmgt_get_class_name(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true));
						}
						else
						{
							$class='-';
						}
						$result[$i]['class'] = $class;
						$result[$i]['start_date'] = get_post_meta($retrieved_data->ID,'gmgt_start_date',true);
						$result[$i]['end_date'] = get_post_meta($retrieved_data->ID,'gmgt_end_date',true);
					}
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//View Notice Function
	public function MJ_gmgt_view_notice_data($data)
	{
		$result = get_post($data['notice_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($result))
			{			
				$noticedata['notice_title'] =$result->post_title;
				$noticedata['notice_content'] =$result->post_content;
				$noticedata['notice_for'] =MJgmgt_GetRoleName(get_post_meta( $result->ID, 'notice_for',true));
				if(get_post_meta( $result->ID, 'notice_for',true) == 'member')
				{
					$class_name='-';
					if(!empty($result->ID)) 
					{ 
						$class_name=MJgmgt_get_class_name(get_post_meta( $result->ID, 'gmgt_class_id',true));
					}
					$noticedata['class_name'] =$class_name;
				}
				$noticedata['start_date'] =MJgmgt_get_format_for_db_api(get_post_meta($result->ID,'gmgt_start_date',true));
				$noticedata['end_date'] =MJgmgt_get_format_for_db_api(get_post_meta( $result->ID, 'gmgt_end_date',true));
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$noticedata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>