<?php 
class MJ_Gmgt_Payment_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Add Invoice 
		if($_REQUEST['gmgt_json_api'] == 'add_invoice')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='add';
			$response=$this->MJ_gmgt_add_invoice_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Invoice
		if($_REQUEST['gmgt_json_api'] == 'edit_invoice')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='edit';
			$response=$this->MJ_gmgt_add_invoice_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Invoice list
		if($_REQUEST['gmgt_json_api'] == 'invoice_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_invoice_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Invoice
		if($_REQUEST['gmgt_json_api'] == 'single_invoice')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_invoice_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Invoice
		if($_REQUEST['gmgt_json_api'] == 'delete_invoice')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_invoice_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//view Invoice
		if($_REQUEST['gmgt_json_api'] == 'view_invoice')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_view_payment_invoice_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Pay Income Invoice
		if($_REQUEST['gmgt_json_api'] == 'pay_invoice')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_pay_income_invoice_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Expense List
		if($_REQUEST['gmgt_json_api'] == 'expense_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_expense_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Add Expense 
		if($_REQUEST['gmgt_json_api'] == 'add_expense')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='add';
			$response=$this->MJ_gmgt_add_expense_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Expense 
		if($_REQUEST['gmgt_json_api'] == 'edit_expense')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='edit';
			$response=$this->MJ_gmgt_add_expense_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Expense
		if($_REQUEST['gmgt_json_api'] == 'single_expense')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_expense_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Expense
		if($_REQUEST['gmgt_json_api'] == 'delete_expense')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_invoice_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Add-Edit Invoice Function
	function MJ_gmgt_add_invoice_data($data,$action)
	{
		$response=array();
		$obj_payment=new MJgmgt_payment;
		$entry_value=$obj_payment->MJgmgt_get_entry_records($data);
		global $wpdb;
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		$incomedata['invoice_type']=$data['invoice_type'];
		$incomedata['invoice_label']=MJgmgt_strip_tags_and_stripslashes($data['invoice_label']);
		$incomedata['supplier_name']=MJgmgt_strip_tags_and_stripslashes($data['memer_id']);	
		$incomedata['entry']=$entry_value;
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'payment');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			//count amount by entry
			$value=json_decode($entry_value);
			if(!empty($value))
			{
				foreach($value as $entry)
				{
					 $total+=$entry->amount;
				}
			}
			$incomedata['amount']=$total;
			$incomedata['discount']=$data['discount_amount'];
			$incomedata['tax']=  0;
			if(isset($data['tax']))
			{
				$incomedata['tax_id']=implode(",",$data['tax']);
			}
			else
			{
				$incomedata['tax_id']=null;	
			}
			$total_after_discount_amount= $total - $data['discount_amount'];
			if(!empty($data['tax']))
			{
				$total_tax=0;
				foreach($data['tax'] as $tax_id)
				{
					$tax_percentage=MJgmgt_tax_percentage_by_tax_id($tax_id);
					$tax_amount=$total_after_discount_amount * $tax_percentage / 100;
					$total_tax=$total_tax + $tax_amount;				
				}
				$total_amount_withtax=$total_after_discount_amount + $total_tax;
			}
			else
			{
				$total_tax=0;			
				$total_amount_withtax=$total_after_discount_amount + $total_tax;
			}
			$incomedata['total_amount']=$total_amount_withtax;
			$incomedata['receiver_id']=$data['current_user_id'];
			if($action == 'edit')
			{
				if($menu_access_data['edit'] == '1' )
				{
					$incomedata['invoice_no']=$data['invoice_no'];
					$income_dataid['invoice_id']=$data['income_id'];
					$incomedata['paid_amount']=$data['paid_amount'];
					$paid_amount=$data['paid_amount'];
					if($paid_amount == 0 || $paid_amount == 0.00)
					{
						$status="Unpaid";
					}
					elseif($paid_amount < $total_amount_withtax)
					{
						$status="Partially Paid";
					}
					elseif($paid_amount >= $total_amount_withtax)
					{
						$status="Fully Paid";
					}
					$incomedata['payment_status']=$status;
					$result=$wpdb->update( $table_income, $incomedata ,$income_dataid);
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully updated","gym_mgt");
						$response['result']='';
						return $response;
					}
					else
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
						$response['result']='';
						return $response;
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
			else
			{
				if($menu_access_data['add'] == '1' )
				{
					//invoice number generate
					$result_invoice_no=$wpdb->get_results("SELECT * FROM $table_income");
					if(empty($result_invoice_no))
					{							
						$invoice_no='00001';
					}
					else
					{							
						$result_no=$wpdb->get_row("SELECT invoice_no FROM $table_income where invoice_id=(SELECT max(invoice_id) FROM $table_income)");
						$last_invoice_number=$result_no->invoice_no;
						$invoice_number_length=strlen($last_invoice_number);
						if($invoice_number_length=='5')
						{
							$invoice_no = str_pad($last_invoice_number+1, 5, 0, STR_PAD_LEFT);
						}
						else	
						{
							$invoice_no='00001';
						}
					}
					$incomedata['invoice_date']=MJgmgt_get_format_for_db($data['invoice_date']);
					$incomedata['invoice_no']=$invoice_no;
					$incomedata['paid_amount']=0;
					$incomedata['payment_status']='Unpaid';
					$incomedata['create_by']=$data['current_user_id'];
					$result=$wpdb->insert( $table_income,$incomedata);
					//income invoice mail send
					$insert_id=$wpdb->insert_id;
					//------------- SEND MESSAGE --------------------//
					$current_sms_service = get_option( 'smgt_sms_service');
					if(is_plugin_active('sms-pack/sms-pack.php'))
					{
						$userdata=get_userdata(MJgmgt_strip_tags_and_stripslashes($data['memer_id']));
						$mobile_number=array(); 
						$gymname=get_option( 'gmgt_system_name' );
						$mobile_number[] = "+".MJgmgt_get_countery_phonecode(get_option( 'gmgt_contry' )).$userdata->mobile;
						$message_content ="Your payment invoice generated at ".$gymname;
						$args = array();
						$args['mobile']=$mobile_number;
						$args['message_from']="Invoice Payment";
						$args['message']=$message_content;					
						if($current_sms_service=='telerivet' || $current_sms_service ="MSG91" || $current_sms_service=='bulksmsgateway.in' || $current_sms_service=='textlocal.in' || $current_sms_service=='bulksmsnigeria' || $current_sms_service=='africastalking' || $current_sms_service == 'clickatell')
						{			
							$send = send_sms($args);							
						}
					}
					$gymname=get_option( 'gmgt_system_name' );
					$userdata=get_userdata(MJgmgt_strip_tags_and_stripslashes($data['memer_id']));
					$arr['[GMGT_USERNAME]']=$userdata->display_name;	
					$arr['[GMGT_GYM_NAME]']=$gymname;
					$subject =get_option('add_income_subject');
					$sub_arr['[GMGT_ROLE_NAME]']=implode(',', $userdata->roles);
					$sub_arr['[GMGT_GYM_NAME]']=$gymname;
					$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
					$message = get_option('add_income_template');	
					$message_replacement = MJgmgt_string_replacemnet($arr,$message);
					$to[]=$userdata->user_email;
					$type='income';
					MJgmgt_send_invoice_generate_mail($to,$subject,$message_replacement,$insert_id,$type);
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully inserted","gym_mgt");
					}
					else
					{
						$response['status']=0;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Invoice List Function
	public function MJ_gmgt_invoice_list_data($data)
	{
		$obj_payment=new MJgmgt_payment;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'payment');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$paymentdata=$obj_payment->MJgmgt_get_all_income_data_by_member();
				}
				elseif($role == 'staff_member' || $role == 'accountant')
				{	
					$paymentdata=$obj_payment->MJgmgt_get_all_income_data_by_created_by($user_id);		
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$paymentdata=$obj_payment->MJgmgt_get_all_income_data();
			}
			else
			{
				$paymentdata="";
			}
			$response	=	array();

			if(!empty($paymentdata))
			{	
				$i=0;
				foreach ($paymentdata as $retrieved_data)
				{	
					if(empty($retrieved_data->invoice_no))
					{
						$invoice_no='-';
						if($retrieved_data->invoice_label=='Sell Product')
						{	
							$entry=json_decode($retrieved_data->entry);
							if(!empty($entry))
							{
								foreach($entry as $data)
								{
									 $amount=$data->amount;
								}
							}
							$total_amount=$amount;
							$paid_amount=$amount;
							$due_amount='0';
						}
						else
						{
							$entry=json_decode($retrieved_data->entry);
							$amount_value='0';
							if(!empty($entry))
							{
								foreach($entry as $data)
								{
									 $amount_value+=$data->amount;
								}
							}
							if($retrieved_data->payment_status=='Paid')
							{
								$total_amount=$amount_value;
								$paid_amount=$amount_value;
								$due_amount='0';
							}
							else
							{
								$total_amount=$amount_value;
								$paid_amount='0';
								$due_amount=$amount_value;
							}
						}	
					}
					else
					{								
						$invoice_no=$retrieved_data->invoice_no;
						$total_amount=$retrieved_data->total_amount;
						$paid_amount=$retrieved_data->paid_amount;
						$due_amount=abs($total_amount-$paid_amount);
					}	
					if($retrieved_data->total_amount == '0')
					{
						$status='Fully Paid';
					}
					else
					{
						$status=$retrieved_data->payment_status;
					}
					$user=get_userdata($retrieved_data->supplier_name);
					$memberid=get_user_meta($retrieved_data->supplier_name,'member_id',true);
					$display_label=$user->display_name;
					if($memberid)
						$display_label.=" (".$memberid.")";		
					$result[$i]['invoice_id'] =	$retrieved_data->id;
					$result[$i]['member_name'] = $display_label;
					$result[$i]['income_name'] = $retrieved_data->invoice_label;
					$result[$i]['invoice_no'] = $invoice_no;
					$result[$i]['amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($total_amount,2);
					$result[$i]['paid_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($paid_amount,2);
					$result[$i]['due_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($due_amount,2);
					$result[$i]['date'] = MJgmgt_getdate_in_input_box($retrieved_data->invoice_date);
					$result[$i]['payment_status'] = __("$status","gym_mgt");
					if($retrieved_data->invoice_label=='Sell Product')
					{
						$invoice_type='sell_invoice';
					}
					elseif($retrieved_data->invoice_label=='Fees Payment')
					{
						$invoice_type='membership_invoice';
					}
					else
					{
						$invoice_type='income';
					}	
					$result[$i]['invoice_type'] =$invoice_type;
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Single Invoice Function
	public function MJ_gmgt_single_invoice_data($data)
	{
		$obj_payment=new MJgmgt_payment;
		$result = $obj_payment->MJgmgt_get_income_data($data['invoice_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($result))
			{			
				$incomedata['invoice_no'] = $result->paid_amount;
				$incomedata['paid_amount'] =$result->paid_amount;
				$incomedata['member_id'] = $result->supplier_name;
				$incomedata['invoice_date'] = MJgmgt_getdate_in_input_box($result->invoice_date);
				$incomedata['entry'] = $result->entry;
				$incomedata['tax'] = $result->tax_id;
				$incomedata['discount'] = $result->discount;
				$incomedata['invoice_type'] = 'income';
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$incomedata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Delete Invoice Function
	public function MJ_gmgt_delete_invoice_data($data)
	{
		$response=array();
		$obj_payment=new MJgmgt_payment;
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'payment');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['delete'] == '1' )
			{
				if(isset($data['payment_id']))
				{
					$result=$obj_payment->MJgmgt_delete_payment($data['payment_id']);
				}
				if(isset($data['income_id']))
				{
					$result=$obj_payment->MJgmgt_delete_income($data['income_id']);
				}
				if(isset($data['expense_id']))
				{
					$result=$obj_payment->MJgmgt_delete_expense($data['expense_id']);
				} 
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=esc_html__("Record successfully deleted",'gym_mgt');
					$response['result']='';
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=esc_html__("Record not found",'gym_mgt');
					$response['result']='';
					return $response;
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=esc_html__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Pay Income Payment Function
	function MJ_gmgt_pay_income_invoice_data($data)
	{
		$response=array();
		$obj_payment=new MJgmgt_payment;
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			$incomedata['member_id']=$data['member_id'];
			$incomedata['income_id']=$data['mp_id'];
			$incomedata['amount']=$data['amount'];
			$incomedata['trasaction_id']=$data['trasaction_id'];
			$incomedata['payment_method']=$data['payment_method'];
			$incomedata['created_by']=$data['current_user_id'];
			$result = $obj_payment->MJgmgt_add_income_payment_history($incomedata);
			if($result)
			{
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record successfully inserted","gym_mgt");
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not inserted",'gym_mgt');
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Expense List Function
	public function MJ_gmgt_expense_list_data($data)
	{
		$obj_payment=new MJgmgt_payment;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'payment');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				$expensedata=$obj_payment->MJgmgt_get_all_expense_data_by_created_by($user_id);
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$expensedata=$obj_payment->MJgmgt_get_all_expense_data();
			}
			else
			{
				$expensedata="";
			}
			$response = array();
			if(!empty($expensedata))
			{	
				$i=0;
				foreach ($expensedata as $retrieved_data)
				{	
					$all_entry=json_decode($retrieved_data->entry);
					$total_amount=0;
					foreach($all_entry as $entry)
					{
						$total_amount+=$entry->amount;
					}
					$result[$i]['invoice_id'] =	$retrieved_data->invoice_id;
					$result[$i]['supplier_name'] =	$retrieved_data->supplier_name;
					$result[$i]['amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($total_amount,2);
					$result[$i]['invoice_date'] = MJgmgt_getdate_in_input_box($retrieved_data->invoice_date);
					$result[$i]['invoice_type'] = 'expense';
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Add-Edit Expense Function
	function MJ_gmgt_add_expense_data($data,$action)
	{
		$response=array();
		$obj_payment=new MJgmgt_payment;
		$entry_value=$obj_payment->MJgmgt_get_entry_records($data);
		global $wpdb;
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		//invoice number generate
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'payment');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			$result_invoice_no=$wpdb->get_results("SELECT * FROM $table_income");
			if(empty($result_invoice_no))
			{							
				$invoice_no='00001';
			}
			else
			{							
				$result_no=$wpdb->get_row("SELECT invoice_no FROM $table_income where invoice_id=(SELECT max(invoice_id) FROM $table_income)");
				$last_invoice_number=$result_no->invoice_no;
				$invoice_number_length=strlen($last_invoice_number);
				
				if($invoice_number_length=='5')
				{
					$invoice_no = str_pad($last_invoice_number+1, 5, 0, STR_PAD_LEFT);
				}
				else	
				{
					$invoice_no='00001';
				}
				
			}
			$incomedata['invoice_type']=$data['invoice_type'];
			$incomedata['supplier_name']=$data['supplier_name'];
			$incomedata['invoice_no']=$invoice_no;
			$incomedata['invoice_date']=MJgmgt_get_format_for_db($data['date']);
			$incomedata['payment_status']=$data['status'];
			$incomedata['entry']=$entry_value;
			$incomedata['receiver_id']=$data['current_user_id'];
			if($action == 'edit')
			{
				if($menu_access_data['edit'] == '1')
				{
					$expense_dataid['invoice_id']=$data['expense_id'];
					$result=$wpdb->update( $table_income, $incomedata ,$expense_dataid);
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully updated","gym_mgt");
						$response['result']='';
						return $response;
					}
					else
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
						$response['result']='';
						return $response;
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=esc_html__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
			else
			{
				if($menu_access_data['add'] == '1')
				{
					$incomedata['create_by']=$data['current_user_id'];
					$result=$wpdb->insert( $table_income,$incomedata);
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully inserted","gym_mgt");
					}
					else
					{
						$response['status']=0;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=esc_html__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}

		return $response;
	}
	//Single Expense Function
	public function MJ_gmgt_single_expense_data($data)
	{
		 $obj_payment=new MJgmgt_payment;
		 $result =  $obj_payment->MJgmgt_get_income_data($data['expense_id']);
		 $access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($result))
			{			
				$incomedata['expense_id'] = $result->expense_id;
				$incomedata['supplier_name'] = $result->supplier_name;
				$incomedata['status'] = $result->payment_status;
				$incomedata['date'] =MJgmgt_getdate_in_input_box($result->invoice_date);
				$incomedata['entry'] = $result->entry;
				$incomedata['invoice_type'] = 'expense';
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$incomedata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//View Payment Invoice Function
	public function MJ_gmgt_view_payment_invoice_data($data)
	{
		$obj_payment= new MJgmgt_payment();
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($data['invoice_type']=='membership_invoice')
			{
				$obj_membership_payment=new MJgmgt_membership_payment;	
				$membership_data=$obj_membership_payment->MJgmgt_get_single_membership_payment($data['invoice_id']);
				$history_detail_result = MJgmgt_get_payment_history_by_mpid($data['invoice_id']);
			}
			if($data['invoice_type']=='income')
			{
				$income_data=$obj_payment->MJgmgt_get_income_data($data['invoice_id']);
				$history_detail_result = MJgmgt_get_income_payment_history_by_mpid($data['invoice_id']);
			}
			if($data['invoice_type']=='expense')
			{
				$expense_data=$obj_payment->MJgmgt_get_income_data($data['invoice_id']);
			}
			if($data['invoice_type']=='sell_invoice')
			{
				$obj_store=new MJgmgt_store;
				$selling_data=$obj_store->MJgmgt_get_single_selling($data['invoice_id']);
				$history_detail_result = MJgmgt_get_sell_payment_history_by_mpid($data['invoice_id']);
			}
						
			$invoicedata['invoice_id'] = $data['invoice_id'];
			$invoicedata['system_name'] =get_option('gmgt_system_name');
			$invoicedata['system_logo'] =get_option( 'gmgt_system_logo' );
			$invoicedata['system_address'] =get_option( 'gmgt_gym_address' );
			$invoicedata['system_email'] = get_option( 'gmgt_email' );
			$invoicedata['system_contact_number'] = get_option( 'gmgt_contact_number' );
			if(!empty($expense_data))
			{
			   $bill_to_name=chunk_split(ucwords($expense_data->supplier_name),30,"<BR>"); 
			}
			else
			{
				if(!empty($income_data))
					$member_id=$income_data->supplier_name;
				 if(!empty($membership_data))
					$member_id=$membership_data->member_id;
				 if(!empty($selling_data))
					$member_id=$selling_data->member_id;
				$patient=get_userdata($member_id);
				$bill_to_name=chunk_split(ucwords($patient->display_name),30,"<BR>"); 
				 $address=get_user_meta( $member_id,'address',true);
				$bill_to_name.=chunk_split($address,30,"<BR>"); 									 
				$bill_to_name.=get_user_meta( $member_id,'city_name',true ).","; 
				$bill_to_name.=get_user_meta( $member_id,'zip_code',true )."<br>"; 
				$bill_to_name.=get_user_meta( $member_id,'mobile',true )."<br>"; 
			}
			$invoicedata['bill_to_name'] = $bill_to_name;
			$issue_date='DD-MM-YYYY';
			$invoice_no='';
			if(!empty($income_data))
			{
				$issue_date=$income_data->invoice_date;
				$payment_status=$income_data->payment_status;
				$invoice_no=$income_data->invoice_no;
			}
			if(!empty($membership_data))
			{
				$issue_date=$membership_data->created_date;
				if($membership_data->payment_status!='0')
				{	
					$payment_status=$membership_data->payment_status;
				}
				else
				{
					$payment_status='Unpaid';
				}		
				$invoice_no=$membership_data->invoice_no;
			}
			if(!empty($expense_data))
			{
				$issue_date=$expense_data->invoice_date;
				$payment_status=$expense_data->payment_status;
				$invoice_no=$expense_data->invoice_no;
			}
			if(!empty($selling_data))
			{
				$issue_date=$selling_data->sell_date;	
				if(!empty($selling_data->payment_status))
				{
					$payment_status=$selling_data->payment_status;
				}	
				else
				{
					$payment_status='Fully Paid';
				}		
				
				$invoice_no=$selling_data->invoice_no;
			}
			$invoicedata['invoice_no'] = $invoice_no;
			$invoicedata['invoice_date'] = MJgmgt_getdate_in_input_box($issue_date);
			$invoicedata['status'] = __($payment_status,'gym_mgt');
			$id=1;
			$i=1;
			$total_amount=0;
			$income_entry=array();
			if(!empty($income_data) || !empty($expense_data))
			{
				if(!empty($expense_data))
					$income_data=$expense_data;							
				$member_income=$obj_payment->MJgmgt_get_oneparty_income_data_incomeid($income_data->invoice_id);
				foreach($member_income as $result_income)
				{
					$income_entries=json_decode($result_income->entry);
					$discount_amount=$result_income->discount;
					$paid_amount=$result_income->paid_amount;
					$total_discount_amount= $result_income->amount - $discount_amount;
					if($result_income->tax_id!='')
					{									
						$total_tax=0;
						$tax_array=explode(',',$result_income->tax_id);
						foreach($tax_array as $tax_id)
						{
							$tax_percentage=MJgmgt_tax_percentage_by_tax_id($tax_id);
							$tax_amount=$total_discount_amount * $tax_percentage / 100;
							$total_tax=$total_tax + $tax_amount;				
						}
					}
					else
					{
						$total_tax=$total_discount_amount * $result_income->tax/100;
					}
					$due_amount=0;
					$due_amount=$result_income->total_amount - $result_income->paid_amount;
					$grand_total=$total_discount_amount + $total_tax;
				   foreach($income_entries as $each_entry)
				   {
						$total_amount+=$each_entry->amount;
						$income_entry[$i]['date']=MJgmgt_getdate_in_input_box($result_income->invoice_date);
						$income_entry[$i]['entry']=$each_entry->entry;
						$income_entry[$i]['amount']=number_format($each_entry->amount,2);
						$id+=1;
						$i+=1;
					}
					if($grand_total=='0')									
					{
						if($income_data->payment_status=='Paid')
						{
							$grand_total=$total_amount;
							$paid_amount=$total_amount;
							$due_amount=0;										
						}
						else
						{
							$grand_total=$total_amount;
							$paid_amount=0;
							$due_amount=$total_amount;															
						}
					}
				}
			}
			$membership_entry=array();	
			$sell_entry=array();
			if(!empty($membership_data))
			{					
				$membership_signup_amounts=$membership_data->membership_signup_amount;
				$membership_entry[0]['date']=MJgmgt_getdate_in_input_box($membership_data->created_date);
				$membership_entry[0]['fees_type']=MJgmgt_get_membership_name($membership_data->membership_id);
				$membership_entry[0]['amount']=number_format($membership_data->membership_fees_amount,2);
				if( $membership_signup_amounts  > 0) 
				{
					$membership_entry[1]['date']=MJgmgt_getdate_in_input_box($membership_data->created_date);
					$membership_entry[1]['fees_type']=_e('Membership Signup Fee','gym_mgt');								
					$membership_entry[1]['amount']=number_format($membership_data->membership_signup_amount,2);
				}
			}
			if(!empty($selling_data))
			{								
				$all_entry=json_decode($selling_data->entry);
				
				if(!empty($all_entry))
				{
					foreach($all_entry as $entry)
					{
						$obj_product=new MJgmgt_product;
						$product = $obj_product->MJgmgt_get_single_product($entry->entry);
							$product_name=$product->product_name;					
							$quentity=$entry->quentity;	
							$price=$product->price;										
						$sell_entry[$i]['date']=MJgmgt_getdate_in_input_box($selling_data->sell_date);$sell_entry[$i]['product_name']=$product_name;
						$sell_entry[$i]['quantity']=$quentity;
						$sell_entry[$i]['price']=$price;
						$sell_entry[$i]['total_price']=number_format($quentity * $price,2);
					$id+=1;
					$i+=1;
					}
				}
				else
				{
					$obj_product=new MJgmgt_product;
					$product = $obj_product->MJgmgt_get_single_product($selling_data->product_id);
					$product_name=$product->product_name;					
					$quentity=$selling_data->quentity;	
					$price=$product->price;	
					$sell_entry[$i]['date']=MJgmgt_getdate_in_input_box($selling_data->sell_date);$sell_entry[$i]['product_name']=$product_name;
					$sell_entry[$i]['quantity']=$quentity;
					$sell_entry[$i]['price']=$price;
					$sell_entry[$i]['total_price']=number_format($quentity * $price,2);
					$id+=1;
					$i+=1;
				}	
			}
			if($data['invoice_type']=='membership_invoice')
			{
				$invoicedata['entries'] = $membership_entry;
			}
			elseif($data['invoice_type']=='sell_invoice')
			{
				$invoicedata['entries'] = $sell_entry;
			}
			else
			{
				$invoicedata['entries'] = $income_entry;
			}
			$invoicedata['bank)name'] = get_option( 'gmgt_bank_name' );
			$invoicedata['account_no'] = get_option( 'gmgt_bank_acount_number' );
			$invoicedata['ifsc_code'] = get_option( 'gmgt_bank_ifsc_code' );
			$invoicedata['paypal_id'] = get_option( 'gmgt_paypal_email' );
			if(!empty($history_detail_result))
			{
				$invoicedata['payment_history'] = $history_detail_result;
			}
			else
			{
				$invoicedata['payment_history'] = $history_detail_result;
			}
			if(!empty($membership_data))
			{
				$total_amount=$membership_data->membership_fees_amount+$membership_data->membership_signup_amount;
				$total_tax=$membership_data->tax_amount;							
				$paid_amount=$membership_data->paid_amount;
				$due_amount=abs($membership_data->membership_amount - $paid_amount);
				$grand_total=$membership_data->membership_amount;
			}
			if(!empty($expense_data))
			{
				$grand_total=$total_amount;
			}
			if(!empty($selling_data))
			{
				$all_entry=json_decode($selling_data->entry);
				if(!empty($all_entry))
				{
					$total_amount=$selling_data->amount;
					$discount_amount=$selling_data->discount;
					$total_discount_amount=$total_amount-$discount_amount;
					if($selling_data->tax_id!='')
					{									
						$total_tax=0;
						$tax_array=explode(',',$selling_data->tax_id);
						foreach($tax_array as $tax_id)
						{
							$tax_percentage=MJgmgt_tax_percentage_by_tax_id($tax_id);
							$tax_amount=$total_discount_amount * $tax_percentage / 100;
							$total_tax=$total_tax + $tax_amount;				
						}
					}
					else
					{
						$tax_per=$selling_data->tax;
						$total_tax=$total_discount_amount * $tax_per/100;
					}
					
					$paid_amount=$selling_data->paid_amount;
					$due_amount=abs($selling_data->total_amount - $paid_amount);
					$grand_total=$selling_data->total_amount;
				}
				else
				{	
					$obj_product=new MJgmgt_product;
					$product = $obj_product->MJgmgt_get_single_product($selling_data->product_id);
					$price=$product->price;
					$total_amount=$price*$selling_data->quentity;
					$discount_amount=$selling_data->discount;
					$total_discount_amount=$total_amount-$discount_amount;
					if($selling_data->tax_id!='')
					{									
						$total_tax=0;
						$tax_array=explode(',',$selling_data->tax_id);
						foreach($tax_array as $tax_id)
						{
							$tax_percentage=MJgmgt_tax_percentage_by_tax_id($tax_id);	
							$tax_amount=$total_discount_amount * $tax_percentage / 100;
							$total_tax=$total_tax + $tax_amount;				
						}
					}
					else
					{
						$tax_per=$selling_data->tax;
						$total_tax=$total_discount_amount * $tax_per/100;
					}
													
					$paid_amount=$total_amount;
					$due_amount='0';
					$grand_total=$total_amount;								
				}		
			}
			$invoicedata['sub_total'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($total_amount,2);
			$invoicedata['discount_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($discount_amount,2);		
			$invoicedata['tax_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($total_tax,2);
			$invoicedata['due_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($due_amount,2);
			$invoicedata['paid_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($paid_amount,2);
			$invoicedata['grand_total'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($grand_total,2);
			$response['status']=1;
			$response['error_code']=200;
			$response['error']=__("Record found successfully",'gym_mgt');
			$response['result']=$invoicedata;
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;	
	}
}
?>