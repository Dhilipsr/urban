<?php 
class MJ_Gmgt_Message_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Compose Message
		if($_REQUEST['gmgt_json_api'] == 'compose_message')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_compose_message_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Inbox Message list
		if($_REQUEST['gmgt_json_api'] == 'inbox_message_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_inbox_message_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Sentbox Message list
		if($_REQUEST['gmgt_json_api'] == 'sentbox_message_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_sentbox_message_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Message
		if($_REQUEST['gmgt_json_api'] == 'view_message')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_view_message_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Message
		if($_REQUEST['gmgt_json_api'] == 'delete_message')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_message_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Message Reply
		if($_REQUEST['gmgt_json_api'] == 'delete_reply_message')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_reply_message_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Send Reply Message
		if($_REQUEST['gmgt_json_api'] == 'send_reply_message')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_send_reply_message_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Message
		if($_REQUEST['gmgt_json_api'] == 'view_message')
		{
			$response=$this->MJ_gmgt_view_message_data($_REQUEST);	 

			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Reply Message List
		if($_REQUEST['gmgt_json_api'] == 'reply_message_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_reply_message_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Compose Message Function
	function MJ_gmgt_compose_message_data($data)
	{
		$response=array();
		$created_date = date("Y-m-d H:i:s");
		$subject = MJgmgt_strip_tags_and_stripslashes($data['subject']);
		$message_body = MJgmgt_strip_tags_and_stripslashes($data['message_body']);
		$created_date = date("Y-m-d H:i:s");
		$tablename="Gmgt_message";
		$role=$data['receiver'];
		if(isset($data['class_id']))
		$class_id = $data['class_id'];
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($role == 'member' || $role == 'staff_member' || $role == 'accountant' || $role == 'administrator')
			{
				$userdata=MJgmgt_get_user_notice($role,$data['class_id']);
				if(!empty($userdata))
				{
					$mail_id = array();
					$i = 0;
						foreach($userdata as $user)
						{
							if($role == 'parent' && $class_id != 'all')
							$mail_id[]=$user['ID'];
							else 
								$mail_id[]=$user->ID;
							$i++;
						}
					$post_id = wp_insert_post( array(
							'post_status' => 'publish',
							'post_type' => 'message',
							'post_title' => $subject,
							'post_content' =>$message_body
					) );
					foreach($mail_id as $user_id)
					{
						$reciever_id = $user_id;
						$message_data=array('sender'=>$data['current_user_id'],
								'receiver'=>$user_id,
								'subject'=>$subject,
								'message_body'=>$message_body,
								'date'=>$created_date,
								'status' =>0,
								'post_id' =>$post_id
						);
						MJgmgt_insert_record($tablename,$message_data);
						//-----MESSAGE SEND NOTIFICATION TEMPLATE-------
						 $userdata = get_userdata($user_id);
						 $role=$userdata->roles;
						 $reciverrole=$role[0];
						 if($reciverrole == 'administrator' ) 
						 {
							$page_link=admin_url().'admin.php?page=Gmgt_message&tab=inbox';
						 }
						 else
						 {
							$page_link=home_url().'/?dashboard=user&page=message&tab=inbox';
						 } 
						$gymname=get_option( 'gmgt_system_name' );
						$userdata = get_userdata($user_id);
						$senderuserdata = get_userdata($data['current_user_id']);
						$arr['[GMGT_RECEIVER_NAME]']=$userdata->display_name;	
						$arr['[GMGT_GYM_NAME]']=$gymname;
						$arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;
						$arr['[GMGT_MESSAGE_CONTENT]']=$message_body;
						$arr['[GMGT_MESSAGE_LINK]']=$page_link;
						$subject =get_option('message_received_subject');
						$sub_arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;;
						$sub_arr['[GMGT_GYM_NAME]']=$gymname;
						$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
						$message_template = get_option('message_received_template');	
						$message_replacement = MJgmgt_string_replacemnet($arr,$message_template);
							$to=$userdata->user_email;
								MJgmgt_send_mail($to,$subject,$message_replacement);	
					}
					$result=add_post_meta($post_id, 'message_for',$role);
					$result=add_post_meta($post_id, 'gmgt_class_id',$data['class_id']);
				}
				else
				{
					$post_id = wp_insert_post( array(
						'post_status' => 'publish',
						'post_type' => 'message',
						'post_title' => $subject,
						'post_content' =>$message_body
				
					) );
					$user_id =$data['receiver'];
					$message_data=array('sender'=>$data['current_user_id'],
							'receiver'=>$user_id,
							'subject'=>$subject,
							'message_body'=>$message_body,
							'date'=>$created_date,
							'status' =>0,
							'post_id' =>$post_id
					);
					MJgmgt_insert_record($tablename,$message_data);
					//-----MESSAGE SEND NOTIFICATION TEMPLATE-------
						$userdata = get_userdata($user_id);
						$role=$userdata->roles;
						$reciverrole=$role[0];
						if($reciverrole == 'administrator' ) 
						{
							$page_link=admin_url().'admin.php?page=Gmgt_message&tab=inbox';
						}
						else
						{
							$page_link=home_url().'/?dashboard=user&page=message&tab=inbox';
						} 
						$gymname=get_option( 'gmgt_system_name' );
						$userdata = get_userdata($user_id);
						$senderuserdata = get_userdata($data['current_user_id']);
						$arr['[GMGT_RECEIVER_NAME]']=$userdata->display_name;	
						$arr['[GMGT_GYM_NAME]']=$gymname;
						$arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;
						$arr['[GMGT_MESSAGE_CONTENT]']=$message_body;
						$arr['[GMGT_MESSAGE_LINK]']=$page_link;
						$subject =get_option('message_received_subject');
						$sub_arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;;
						$sub_arr['[GMGT_GYM_NAME]']=$gymname;
						$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
						$message_template = get_option('message_received_template');	
						$message_replacement = MJgmgt_string_replacemnet($arr,$message_template);
							$to=$userdata->user_email;
								MJgmgt_send_mail($to,$subject,$message_replacement);	
				
						$result=add_post_meta($post_id, 'message_for','user');
						$result=add_post_meta($post_id, 'message_gmgt_user_id',$user_id);
				}
			}
			else
			{
			$post_id = wp_insert_post( array(
						'post_status' => 'publish',
						'post_type' => 'message',
						'post_title' => $subject,
						'post_content' =>$message_body
				) );
				$user_id =$data['receiver'];
				$message_data=array('sender'=>$data['current_user_id'],
						'receiver'=>$user_id,
						'subject'=>$subject,
						'message_body'=>$message_body,
						'date'=>$created_date,
						'status' =>0,
						'post_id' =>$post_id
				);
				MJgmgt_insert_record($tablename,$message_data);
				//-----MESSAGE SEND NOTIFICATION-------
					$userdata = get_userdata($user_id);
					$role=$userdata->roles;
					$reciverrole=$role[0];
					if($reciverrole == 'administrator' ) 
					{
						$page_link=admin_url().'admin.php?page=Gmgt_message&tab=inbox';
					}
					else
					{
						$page_link=home_url().'/?dashboard=user&page=message&tab=inbox';
					} 
					$gymname=get_option( 'gmgt_system_name' );
					$userdata = get_userdata($user_id);
					$senderuserdata = get_userdata($data['current_user_id']);
					$arr['[GMGT_RECEIVER_NAME]']=$userdata->display_name;	
					$arr['[GMGT_GYM_NAME]']=$gymname;
					$arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;
					$arr['[GMGT_MESSAGE_CONTENT]']=$message_body;
					$arr['[GMGT_MESSAGE_LINK]']=$page_link;
					$subject =get_option('message_received_subject');
					$sub_arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;;
					$sub_arr['[GMGT_GYM_NAME]']=$gymname;
					$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
					$message_template = get_option('message_received_template');	
					$message_replacement = MJgmgt_string_replacemnet($arr,$message_template);
						$to=$userdata->user_email;
							MJgmgt_send_mail($to,$subject,$message_replacement);
					$result=add_post_meta($post_id, 'message_for','user');
					$result=add_post_meta($post_id, 'message_gmgt_user_id',$user_id);
			}
			if($result)
			{
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Message successfully send","gym_mgt");
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Please Fill All Fields",'gym_mgt');
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Inbox MESSAGE List Function
	public function MJ_gmgt_inbox_message_list_data($data)
	{
		$user_id=$data['current_user_id'];
		$obj_message= new MJgmgt_message;
		$response	=	array();
		global $wpdb;
		$tbl_name_message = $wpdb->prefix .'Gmgt_message';
		$tbl_name_message_replies = $wpdb->prefix .'gmgt_message_replies';
		$inbox_data = $wpdb->get_results("SELECT DISTINCT b.message_id, a.* FROM $tbl_name_message a LEFT JOIN $tbl_name_message_replies b ON a.message_id = b.message_id WHERE ( a.receiver = $user_id OR b.receiver_id =$user_id) ORDER BY date DESC");
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($inbox_data))
			{
				$i=0;
				foreach ($inbox_data as $retrieved_data)
				{
					$result[$i]['message_for'] = MJgmgt_get_display_name($retrieved_data->sender);
					$result[$i]['subject'] =	$retrieved_data->subject;
					if($obj_message->MJgmgt_count_reply_item($retrieved_data->post_id) >= 1)
					{
						$result[$i]['count_reply_item'] =$obj_message->MJgmgt_count_reply_item($retrieved_data->post_id);
					}
					else
					{
						$result[$i]['count_reply_item']=0;
					}
					$result[$i]['description'] = wp_trim_words($retrieved_data->message_body,5);
					$result[$i]['date'] = MJgmgt_getdate_in_input_box($retrieved_data->date);
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error']=__("No message available",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Sentbox MESSAGE List Function
	public function MJ_gmgt_sentbox_message_list_data($data)
	{
		$user_id=$data['current_user_id'];
		$obj_message= new MJgmgt_message;
		$response = array();
		global $wpdb;
		$tbl_name = $wpdb->prefix .'Gmgt_message';
		$role=MJgmgt_get_roles($data['current_user_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($role=='staff_member' || $role=='accountant' || $role == 'member')
			{			
				$args['post_type'] = 'message';
				$args['post_status'] = 'public';
				$args['author'] = $user_id;
				$q = new WP_Query();
				$sent_message = $q->query( $args );
			}
			else 
			{
				$sent_message =$wpdb->get_results("SELECT *  FROM $tbl_name where sender = $user_id ");
			}
			if(!empty($sent_message))
			{	
				$i=0;
				foreach ($sent_message as $retrieved_data)
				{		
					if($retrieved_data->post_author == $data['current_user_id'])
					{
						$result[$i]['message_id'] =	$retrieved_data->message_id;
						if(get_post_meta( $retrieved_data->ID, 'message_for',true) == 'user')
						{
							if(get_post_meta( $retrieved_data->ID, 'message_gmgt_user_id',true) == 'employee')
							{
								$message_for="employee";
							}
							else
							{									
							   $message_for=MJgmgt_get_display_name(get_post_meta( $retrieved_data->ID, 'message_gmgt_user_id',true));
							}
						}
						else 
						{
							$rolename=get_post_meta( $retrieved_data->ID, 'message_for',true);
							$message_for=MJgmgt_GetRoleName_front($rolename);
						}
						$result[$i]['message_for'] =$message_for;
						$result[$i]['subject'] =	$retrieved_data->post_title;
						if(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) != "" && get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) != "all" ) 
						{
							$class=MJgmgt_get_class_name(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true));
						}
						elseif(get_post_meta( $retrieved_data->ID, 'gmgt_class_id',true) == "all")
						{
							$class="All";
						}
						$result[$i]['class'] = $class;
						$result[$i]['description'] =$retrieved_data->post_content;
						$i++;
					}				
				}
				if($i == 0)
				{
					$result=array();
					$response['status']=0;
					$response['error']=__("No message available",'gym_mgt');
					$response['result']=$result;
				}
				else
				{	
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record found successfully",'gym_mgt');
					$response['result']=$result;
				}
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error']=__("No message available",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Delete Message Function
	public function MJ_gmgt_delete_message_data($data)
	{
		$response=array();
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			//Delete Sentbox Data
			if($data['from']=='sendbox')
			{
				$message = get_post($data['message_id']);
				$box='sendbox';
				if(isset($data['delete']))
				{
					wp_delete_post($data['message_id']);
				}
			}
			//Delete INBOX DATA
			if($data['from']=='inbox')
			{
				$message = MJgmgt_get_message_by_id($data['message_id']);
				MJgmgt_change_read_status($data['message_id']);
				$box='inbox';
				if(isset($data['delete']))
				{
					MJgmgt_delete_message('smgt_message',$data['message_id']);
				}
			}
			$response['status']=1;
			$response['error_code']=200;
			$response['error']=__("Record successfully deleted",'gym_mgt');
			$response['result']='';
			return $response;
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Delete Reply Message Function
	public function MJ_gmgt_delete_reply_message_data($data)
	{
		$response=array();
		$obj_message= new MJgmgt_message;
		$result=$obj_message->MJgmgt_delete_reply($data['message_reply_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($result)
			{
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record successfully deleted",'gym_mgt');
				$response['result']='';
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not deleted",'gym_mgt');
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Send Reply Message Function
	function MJ_gmgt_send_reply_message_data($data)
	{
		$response=array();
		global $wpdb;
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			$table_name = $wpdb->prefix . "gmgt_message_replies";
			$messagedata['message_id'] = $data['message_id'];
			$messagedata['sender_id'] = $data['current_user_id'];
			$messagedata['receiver_id'] = $data['receiver_id'];
			$messagedata['message_comment'] = MJgmgt_strip_tags_and_stripslashes($data['reply_message_content']);
			$messagedata['created_date'] = date("Y-m-d h:i:s");
			$result=$wpdb->insert( $table_name, $messagedata );
			   	$gymname=get_option( 'gmgt_system_name' );
				$userdata = get_userdata($data['receiver_id']);
				$senderuserdata = get_userdata($data['current_user_id']);
				$role=$userdata->roles;
				$reciverrole=$role[0];
				if($reciverrole == 'administrator' ) 
				{
					$page_link=admin_url().'admin.php?page=Gmgt_message&tab=inbox';
				}
				else
				{
					$page_link=home_url().'/?dashboard=user&page=message&tab=inbox';
				} 
			
				$arr['[GMGT_RECEIVER_NAME]']=$userdata->display_name;	
				$arr['[GMGT_GYM_NAME]']=$gymname;
				$arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;
				$arr['[GMGT_MESSAGE_CONTENT]']=MJgmgt_strip_tags_and_stripslashes($data['reply_message_content']);
				$arr['[GMGT_MESSAGE_LINK]']=$page_link;
				$subject =get_option('message_received_subject');
				$sub_arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;;
				$sub_arr['[GMGT_GYM_NAME]']=$gymname;
				$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
				$message = get_option('message_received_template');	
				$message_replacement = MJgmgt_string_replacemnet($arr,$message);
				$to[]=$userdata->user_email;
				MJgmgt_send_mail($to,$subject,$message_replacement);
			if($result)
			{
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Message successfully send",'gym_mgt');
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Message not send",'gym_mgt');
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//View Message Function
	public function MJ_gmgt_view_message_data($data)
	{
		$obj_message= new MJgmgt_message;
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($data['from']=='sendbox')
			{
				$box='sendbox';
				$message = get_post($data['message_id']);
			}
			if($data['from']=='inbox')
			{
				$box='inbox';
				$message = MJgmgt_get_message_by_id($data['message_id']);
				MJgmgt_change_read_status($data['message_id']);
			}
			if(!empty($message))
			{			
				$messagedata['message_id'] = $data['message_id'];
				$messagedata['subject'] =$message->subject;
				$messagedata['date'] = MJgmgt_getdate_in_input_box($message->date);
				if($box=='sendbox')
				{ 
					$sender=MJgmgt_get_display_name($message->post_author);
					$sender_email=MJgmgt_get_emailid_byuser_id($message->post_author);					
				} 
				else
				{
					$sender=MJgmgt_get_display_name($message->sender); 
					$sender_email=MJgmgt_get_emailid_byuser_id($message->sender);
				}  
				$messagedata['sender'] = $sender;
				$messagedata['sender_email'] = $sender_email;
				$receiver_id=0;
				if($box=='sendbox')
				{ 
					$message_content=$message->post_content; 
					$receiver_id=(get_post_meta($data['message_id'],'message_for_userid',true));
				}
				else
				{ 
					$message_content=$message->message_body;
					$receiver_id=$message->sender;
				}
				$messagedata['message_content'] = $message_content;
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$messagedata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Reply Message List Function
	public function MJ_gmgt_reply_message_list_data($data)
	{
		$obj_message= new MJgmgt_message;
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(isset($data['from']) && $data['from']=='inbox')
			{
				$allreply_data=$obj_message->MJgmgt_get_all_replies($data['message_id']);
			}
			else
			{
				$allreply_data=$obj_message->MJgmgt_get_all_replies($data['message_id']);
			}
			if(!empty($allreply_data))
			{			
				$i=0;
				foreach($allreply_data as $reply)
				{		
					$result[$i]['message_id'] =$data['message_id'];
					$result[$i]['message_reply_id'] =	$reply->id;
					$result[$i]['message_comment'] =	$reply->message_comment;
					$result[$i]['sender'] = MJgmgt_get_display_name($reply->sender_id);
					$result[$i]['created_date'] = $reply->created_date;
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=esc_html__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>