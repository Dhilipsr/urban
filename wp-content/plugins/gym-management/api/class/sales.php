<?php 
class MJ_Gmgt_Sales_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Add Sales 
		if($_REQUEST['gmgt_json_api'] == 'add_sales_product')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='add';
			$response=$this->MJ_gmgt_add_sales_product_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Sales
		if($_REQUEST['gmgt_json_api'] == 'edit_sales_product')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='edit';
			$response=$this->MJ_gmgt_add_sales_product_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Sales list
		if($_REQUEST['gmgt_json_api'] == 'sales_product_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_sales_product_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Sales
		if($_REQUEST['gmgt_json_api'] == 'single_sales_product')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_sales_product_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Sales
		if($_REQUEST['gmgt_json_api'] == 'delete_sales_product')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_sales_product_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Add-Edit Sales Function
	function MJ_gmgt_add_sales_product_data($data,$action)
	{
		$response=array();
		$obj_class=new MJgmgt_classschedule;
		$obj_product=new MJgmgt_product;
		$obj_store=new MJgmgt_store;
		global $wpdb;
		$table_sell = $wpdb->prefix. 'gmgt_store';
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		$table_product = $wpdb->prefix. 'gmgt_product';
		$entry_value=$obj_store->MJgmgt_get_entry_records($data);
		$storedata['member_id']=$data['member_id'];		
		$storedata['entry']=$entry_value;
		$storedata['sell_date']=MJgmgt_get_format_for_db($data['sell_date']);
		$storedata['tax']=0;
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'store');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(isset($data['tax']))
			{
				$storedata['tax_id']=implode(",",$data['tax']);
			}
			else
			{
				$storedata['tax_id']=null;	
			}	
			$storedata['sell_by']=$data['current_user_id'];		
		
			if($action == 'edit')
			{
				if($menu_access_data['edit'] == '1' )
				{
					//add old quentiy
					$all_income_entry=$data['old_product_id'];
					$all_income_quentity=$data['old_quentity'];
					if(!empty($all_income_entry))
					{
						$i=0;
						foreach($all_income_entry as $oldentry)
						{
							$product_id=$oldentry;
							$quentity=$all_income_quentity[$i];
							$obj_product=new MJgmgt_product;
							$product = $obj_product->MJgmgt_get_single_product($product_id);
							//	remainig_quentiy					
							$before_quentity=$product->quentity;
							$remainig_quentiy=$before_quentity+$quentity;
							$productdata['quentity']=$remainig_quentiy;
							$productid['id']=$product->id;
							$wpdb->update( $table_product, $productdata ,$productid); 
							$i++;
						}
					}
					
					$entry_valuea=json_decode($entry_value);
					$amount=0;
					foreach($entry_valuea as $entry_valueb)
					{
						$product_id=$entry_valueb->entry;
						$quentity=$entry_valueb->quentity;
						$obj_product=new MJgmgt_product;
						$product = $obj_product->MJgmgt_get_single_product($product_id);
						$price=$product->price;
						$amount+= $quentity * $price;				
						//	remainig_quentiy					
						$before_quentity=$product->quentity;
						$remainig_quentiy=$before_quentity-$quentity;
						$productdata['quentity']=$remainig_quentiy;
						$productid['id']=$product->id;
						$wpdb->update( $table_product, $productdata ,$productid);
					}			
					$discount=$data['discount'];
					if($discount>$amount)
					{
						$alert_amount='3';
						return $alert_amount;
					}
					$total_after_discount_amount= $amount - $discount;
					if(!empty($data['tax']))
					{
						$total_tax=0;
						foreach($data['tax'] as $tax_id)
						{
							$tax_percentage=MJgmgt_tax_percentage_by_tax_id($tax_id);
							$tax_amount=$total_after_discount_amount * $tax_percentage / 100;
							$total_tax=$total_tax + $tax_amount;				
						}
						$total_amount_withtax=$total_after_discount_amount + $total_tax;
					}
					else
					{
						$total_tax=0;			
						$total_amount_withtax=$total_after_discount_amount + $total_tax;
					}			
					
					$storedata['discount']=$data['discount'];
					$storedata['amount']=$amount;
					$storedata['total_amount']=$total_amount_withtax;
					$paid_amount=$data['paid_amount'];
					if($paid_amount >= $total_amount_withtax)
					{
						$status='Fully Paid';
					}
					elseif($paid_amount > 0)
					{
						$status='Partially Paid';
					}
					else
					{
						$status= 'Unpaid';	
					}
					$storedata['payment_status']=$status;
					$sellid['id']=$data['sell_id'];
					$result1=$wpdb->update( $table_sell, $storedata ,$sellid);
				  //---------edit Entry into income invoice------------
					$table_income=$wpdb->prefix.'gmgt_income_expense';
					$incomedata['entry']=$entry_value;
					$incomedata['supplier_name']=$data['member_id'];
					$incomedata['invoice_date']=MJgmgt_get_format_for_db($data['sell_date']);
					$incomedata['receiver_id']=$data['current_user_id'];
					$incomedata['amount']=$amount;
					$incomedata['discount']=$data['discount'];
					$incomedata['tax']=0;
					$incomedata['tax_id']=implode(",",$data['tax']);
					$incomedata['total_amount']=$total_amount_withtax;
					$incomedata['payment_status']=$status;
					$invoice_no['invoice_no']=$data['invoice_number'];
					$result=$wpdb->update( $table_income, $incomedata ,$invoice_no);
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully updated","gym_mgt");
					}
					else
					{
						$response['status']=0;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
			else
			{
				if($menu_access_data['add'] == '1' )
				{
					//invoice number generate
					$result_invoice_no=$wpdb->get_results("SELECT * FROM $table_income");
					if(empty($result_invoice_no))
					{							
						$invoice_no='00001';
					}
					else
					{							
						$result_no=$wpdb->get_row("SELECT invoice_no FROM $table_income where invoice_id=(SELECT max(invoice_id) FROM $table_income)");
						$last_invoice_number=$result_no->invoice_no;
						$invoice_number_length=strlen($last_invoice_number);
						
						if($invoice_number_length=='5')
						{
							$invoice_no = str_pad($last_invoice_number+1, 5, 0, STR_PAD_LEFT);
						}
						else	
						{
							$invoice_no='00001';
						}				
					}
					$entry_valuea=json_decode($entry_value);
					$amount=0;
					foreach($entry_valuea as $entry_valueb)
					{
						$product_id=$entry_valueb->entry;
						$quentity=$entry_valueb->quentity;
						$obj_product=new MJgmgt_product;
						$product = $obj_product->MJgmgt_get_single_product($product_id);
						$price=$product->price;
						$amount+= $quentity * $price;
						//	remainig_quentiy					
						$before_quentity=$product->quentity;
						$remainig_quentiy=$before_quentity-$quentity;
						$productdata['quentity']=$remainig_quentiy;
						$productid['id']=$product->id;
						$wpdb->update( $table_product, $productdata ,$productid);
					}
					$discount=$data['discount'];
					if($discount>$amount)
					{
						$alert_amount='3';
						return $alert_amount;
					}
					$total_after_discount_amount= $amount - $discount;
					if(!empty($data['tax']))
					{
						$total_tax=0;
						foreach($data['tax'] as $tax_id)
						{
							$tax_percentage=MJgmgt_tax_percentage_by_tax_id($tax_id);
							$tax_amount=$total_after_discount_amount * $tax_percentage / 100;
							$total_tax=$total_tax + $tax_amount;				
						}
						$total_amount_withtax=$total_after_discount_amount + $total_tax;
					}
					else
					{
						$total_tax=0;			
						$total_amount_withtax=$total_after_discount_amount + $total_tax;
					}
					$storedata['invoice_no']=$invoice_no;
					$storedata['discount']=$data['discount'];
					$storedata['amount']=$amount;
					$storedata['total_amount']=$total_amount_withtax;
					$storedata['paid_amount']=0;
					$storedata['payment_status']='Unpaid';
					$storedata['created_date']=date('Y-m-d');
					$result=$wpdb->insert( $table_sell, $storedata );
					$insert_id=$wpdb->insert_id;
					//---------Add Entry into income invoice------------
					$table_income=$wpdb->prefix.'gmgt_income_expense';
					$incomedata['entry']=$entry_value;	
					$incomedata['invoice_type']='income';
					$incomedata['invoice_label']=__("Sell Product","gym_mgt");
					$incomedata['supplier_name']=$data['member_id'];
					$incomedata['invoice_date']=MJgmgt_get_format_for_db($data['sell_date']);
					$incomedata['receiver_id']=$data['current_user_id'];
					$incomedata['amount']=$amount;
					$incomedata['discount']=$data['discount'];
					$incomedata['tax']=0;
					$incomedata['tax_id']=implode(",",$data['tax']);
					$incomedata['total_amount']=$total_amount_withtax;
					$incomedata['invoice_no']=$invoice_no;
					$incomedata['paid_amount']=0;
					$incomedata['payment_status']='Unpaid';
					$result_income=$wpdb->insert( $table_income,$incomedata); 
					//------------- SEND MESSAGE --------------------//
					$current_sms_service 	= 	get_option( 'smgt_sms_service');
					if(is_plugin_active('sms-pack/sms-pack.php'))
					{
						$userdata=get_userdata($data['member_id']);
						$mobile_number=array(); 
						$gymname=get_option( 'gmgt_system_name' );
						$mobile_number[] = "+".MJgmgt_get_countery_phonecode(get_option( 'gmgt_contry' )).$userdata->mobile;
						$message_content ="You have been purchased new Product at ".$gymname;
						$args = array();
						$args['mobile']=$mobile_number;
						$args['message_from']="Sell Product";
						$args['message']=$message_content;					
						if($current_sms_service=='telerivet' || $current_sms_service ="MSG91" || $current_sms_service=='bulksmsgateway.in' || $current_sms_service=='textlocal.in' || $current_sms_service=='bulksmsnigeria' || $current_sms_service=='africastalking' || $current_sms_service == 'clickatell')
						{				
							$send = send_sms($args);							
						}
					}
					//sell product invoice invoice mail send				
					$gymname=get_option( 'gmgt_system_name' );
					$userdata=get_userdata($data['member_id']);
					$arr['[GMGT_USERNAME]']=$userdata->display_name;	
					$arr['[GMGT_GYM_NAME]']=$gymname;				
					$subject =get_option('sell_product_subject');
					$sub_arr['[GMGT_GYM_NAME]']=$gymname;
					$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
					$message = get_option('sell_product_template');	
					$message_replacement = MJgmgt_string_replacemnet($arr,$message);
					$to[]=$userdata->user_email;
					$type='sell_invoice';
					MJgmgt_send_invoice_generate_mail($to,$subject,$message_replacement,$insert_id,$type);
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully inserted","gym_mgt");
					}
					else
					{
						$response['status']=0;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Sell List Function
	public function MJ_gmgt_sales_product_list_data($data)
	{
		$obj_product=new MJgmgt_product;
		$obj_store=new MJgmgt_store;
		$obj_class=new MJgmgt_classschedule;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'store');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$storedata=$obj_store->MJgmgt_get_all_selling_by_member($user_id);
				}
				elseif($role == 'staff_member' || $role == 'accountant')
				{	
					$storedata=$obj_store->MJgmgt_get_all_selling_by_sell_by($user_id);	
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$storedata=$obj_store->MJgmgt_get_all_selling();
			}
			else
			{
				$storedata="";
			}
			$response	=	array();
			if(!empty($storedata))
			{	
				$i=0;
				foreach ($storedata as $retrieved_data)
				{	
					if(empty($retrieved_data->invoice_no))
					{
						$obj_product=new MJgmgt_product;
						$product = $obj_product->MJgmgt_get_single_product($retrieved_data->product_id); 				
						$price=$product->price;	
						$quentity=$retrieved_data->quentity;
						$invoice_no='-';					
						$total_amount=$price*$quentity;
						$paid_amount=$price*$quentity;
						$due_amount='0';
					}
					else
					{
						$invoice_no=$retrieved_data->invoice_no;
						$total_amount=$retrieved_data->total_amount;
						$paid_amount=$retrieved_data->paid_amount;
						$due_amount=$total_amount-$paid_amount;
					}	
					$userdata=get_userdata($retrieved_data->member_id);
					$result[$i]['sell_id'] =$retrieved_data->id;
					$result[$i]['invoice_no'] =	$invoice_no;
					$result[$i]['member_name'] = $userdata->display_name;
					$entry_valuea=json_decode($retrieved_data->entry);
					if(!empty($entry_valuea))
					{
						foreach($entry_valuea as $entry_valueb)
						{
							$product = $obj_product->MJgmgt_get_single_product($entry_valueb->entry);
							$product_name=$product->product_name;					
							$quentity=$entry_valueb->quentity;
							$product_quantity=$product_name . " => " . $quentity . ",";
							$product_name_and_quantity[]=rtrim($product_quantity,',').'<br>';
						}
					}
					else
					{
						$obj_product=new MJgmgt_product;
						$product = $obj_product->MJgmgt_get_single_product($retrieved_data->product_id); 
						$product_name=$product->product_name;					
						$quentity=$retrieved_data->quentity;	
						$product_name_and_quantity[]=$product_name . " => " . $quentity;
					}						
					$result[$i]['product_name_and_quantity'] = $product_name_and_quantity;
					$result[$i]['total_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($total_amount,2);
					$result[$i]['paid_amount'] =MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($paid_amount,2);
					$result[$i]['due_amount'] = MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($due_amount,2);
					if(!empty($retrieved_data->payment_status))
					{								
						$payment_status=__("$retrieved_data->payment_status","gym_mgt");
					}								
					else
					{				
						$payment_status=__("Fully Paid","gym_mgt");
					}	
					$result[$i]['payment_status'] =	$payment_status;
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Single Sales Function
	public function MJ_gmgt_single_sales_product_data($data)
	{
		$obj_product=new MJgmgt_product;
		$obj_store=new MJgmgt_store;
		$obj_class=new MJgmgt_classschedule;
		$result =  $obj_store->MJgmgt_get_single_selling($data['sell_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($result))
			{			
				$storedata['sell_id'] = $result->id;
				$storedata['member_id'] =$result->member_id;
				$storedata['sell_date'] = MJgmgt_getdate_in_input_box($result->sell_date);
				$storedata['discount'] = $result->discount;
				$storedata['product_entry'] = $result->entry;
				$storedata['tax'] = $result->tax_id;
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$storedata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Delete Sell Product Function
	public function MJ_gmgt_delete_sales_product_data($data)
	{
		$response=array();
		$obj_store=new MJgmgt_store;
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'store');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['delete'] == '1' )
			{
				$result=$obj_store->MJgmgt_delete_selling($data['sell_id']);
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully deleted",'gym_mgt');
					$response['result']='';
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Record not found",'gym_mgt');
					$response['result']='';
					return $response;
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>