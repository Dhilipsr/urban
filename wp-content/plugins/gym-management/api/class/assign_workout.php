<?php 
class MJ_Gmgt_AssignWorkout_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Assign Workout 
		if($_REQUEST['gmgt_json_api'] == 'assign_workout')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode(stripslashes($json), true);
			$response=$this->MJ_gmgt_assign_workout_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//View Assign Workout 
		if($_REQUEST['gmgt_json_api'] == 'view_assign_workout')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_view_assign_workout_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Workout
		if($_REQUEST['gmgt_json_api'] == 'delete_workout')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode(stripslashes($json), true);
			$response=$this->MJ_gmgt_delete_workout_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Assign Workout Function
	function MJ_gmgt_assign_workout_data($data)
	{
		$response=array();
		$obj_workouttype=new MJgmgt_workouttype;
		global $wpdb;
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'assign-workout');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		$table_workout = $wpdb->prefix. 'gmgt_assign_workout';
		$workoutdata['user_id']=$data['member_id'];
		$workoutdata['level_id']=$data['level'];
		$workoutdata['description']=MJgmgt_strip_tags_and_stripslashes($data['description']);		
		$workoutdata['start_date']=MJgmgt_get_format_for_db($data['start_date']);	
		$workoutdata['end_date']= MJgmgt_get_format_for_db($data['end_date']);
		$workoutdata['created_date']=date("Y-m-d");
		$workoutdata['created_by']=$data['current_user_id'];
		$new_array = array();
		$i = 0;
		$phpobj = array();
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['add'] == '1')
			{
				if(!empty($data['activity_list']))
				{
					foreach($data['activity_list'] as $val)
					{
						$data_value = $val;
						$phpobj[] = $val;
					}
				}
				$j=0;
				$final_array = array();
				$resultarray =array();
				foreach($phpobj as $key => $val)
				{
					$day = array();
					$activity = array();
					foreach($val as $key=>$key_val)
					{				
						if($key == "days")
						foreach($key_val as $val1)
						{
							$day['day'][] =$val1['day_name'] ;
						}
						if($key == "activity")
							foreach($key_val as $val2)
							{						
								echo $val2['activity']['activity'];
								$activity['activity'][] =array('activity'=>$val2['activity']['activity'],
															'reps'=>$val2['activity']['reps'],
															'sets'=>$val2['activity']['sets'],
															'kg'=>$val2['activity']['kg'],
															'time'=>$val2['activity']['time'],
								) ;
							}
					}
					$resultarray[] = array_merge($day, $activity);
				}
				$result=0;
				if(!empty($phpobj))
				{
					$result=$wpdb->insert( $table_workout, $workoutdata );
					$assign_workout_id = $wpdb->insert_id;
					$obj_workouttype->MJgmgt_assign_workout_detail($assign_workout_id,$resultarray);
					//------------- SEND MESSAGE --------------------//
					$current_sms_service 	= 	get_option( 'smgt_sms_service');
					if(is_plugin_active('sms-pack/sms-pack.php'))
					{
						$userdata=get_userdata($data['member_id']);
						$mobile_number=array(); 
						$gymname=get_option( 'gmgt_system_name' );
						$mobile_number[] = "+".MJgmgt_get_countery_phonecode(get_option( 'gmgt_contry' )).$userdata->mobile;
						$message_content ="You have assigned new workouts for ".$workoutdata['start_date']." To ". $workoutdata['end_date']." At ".$gymname;
						$args = array();
						$args['mobile']=$mobile_number;
						$args['message_from']="Assign Workout";
						$args['message']=$message_content;					
						if($current_sms_service=='telerivet' || $current_sms_service ="MSG91" || $current_sms_service=='bulksmsgateway.in' || $current_sms_service=='textlocal.in' || $current_sms_service=='bulksmsnigeria' || $current_sms_service=='africastalking' || $current_sms_service == 'clickatell')
						{				
							$send = send_sms($args);							
						}
					}
						
					//SEND WORKOUT MAIL NOTIFICATION
					$userdata=get_userdata($data['member_id']);
					$username=$userdata->display_name;
					$useremail=$userdata->user_email;
					
					$gymname=get_option( 'gmgt_system_name' );
					$page_link='<a href='.home_url().'?dashboard=user&page=assign-workout>View Workout</a>';
					$arr['[GMGT_MEMBERNAME]']=$username;	
					$arr['[GMGT_GYM_NAME]']=$gymname;
					$arr['[GMGT_STARTDATE]']=$workoutdata['start_date'];
					$arr['[GMGT_ENDDATE]']=$workoutdata['end_date'];
					$arr['[GMGT_PAGE_LINK]']=$page_link;
					$subject =get_option('Assign_Workouts_Subject');			
					$sub_arr['[GMGT_GYM_NAME]']=$gymname;
					$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
					$message = get_option('Assign_Workouts_Template');
					$message_replacement = MJgmgt_string_replacemnet($arr,$message);
					$invoice=MJgmgt_Assign_Workouts_Add_Html_Content($assign_workout_id);		
					$invoic_concat=$message_replacement. $invoice;			
					$to[]=$useremail;
					MJgmgt_send_mail_text_html($to,$subject,$invoic_concat);
					//SEND WORKOUT MAIL NOTIFICATION END
				}
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully inserted","gym_mgt");
					$response['result']='';
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Please Fill All Fields",'gym_mgt');
					$response['result']='';
					return $response;
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//View Assign Workout Function
	public function MJ_gmgt_view_assign_workout_data($data)
	{
		$obj_workouttype=new MJgmgt_workouttype;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'assign-workout');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		$workout_logdata=MJgmgt_get_userworkout($user_id);
		$response =	array();
		if ($data['access_token'] == $access_token)
		{
			if(!empty($workout_logdata))
			{
				$result = array();
				$i=0;
				foreach($workout_logdata as $row)
				{
					
					$all_logdata=MJgmgt_get_workoutdata($row->workout_id); 
					// $arranged_workout=MJgmgt_set_workoutarray($all_logdata);
					$result[$i]['workout_id'] =	$row->workout_id;
					$result[$i]['start_date'] =	MJgmgt_getdate_in_input_box($row->start_date);
					$result[$i]['end_date'] =	MJgmgt_getdate_in_input_box($row->end_date);
					$result[$i]['description'] =$row->description;
					$arranged_workout_array=array();
					// $v=0;
					if(!empty($all_logdata))
					{
						foreach($all_logdata as $data)
						{
							// var_dump($data);
							// die();
							$arranged_workout_array['day']=$data->day_name;
							$arranged_workout_array['workout_name']=$data->workout_name;
							$arranged_workout_array['sets']=$data->sets;
							$arranged_workout_array['reps']=$data->reps;
							$arranged_workout_array['kg']=$data->kg;
							$arranged_workout_array['rest_time']=$data->time;
							$result[$i]['arranged_workout'][$v] =$arranged_workout_array;
							$v++;
						}
					}
					// if(!empty($arranged_workout))
					// {
					// 	foreach($arranged_workout as $key=>$rowdata)
					// 	{
					// 		$arranged_workout_array['day'][$v]=$key;
					// 		$arranged_workout_array['activity'][$v]=$rowdata[0];
					// 		$arranged_workout_array['sets'][$v]=$rowdata[1];
					// 		$arranged_workout_array['reps'][$v]=$rowdata[2];
					// 		$arranged_workout_array['kg'][$v]=$rowdata[3];
					// 		$arranged_workout_array['rest_time'][$v]=$rowdata[4];
					// 		$v++;
					// 	}
						
					// 	$result[$i]['arranged_workout'] =$arranged_workout_array;
					// }
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				// return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Delete Group Function
	public function MJ_gmgt_delete_workout_data($data)
	{
		$response=array();
		$work_out_id = $data['workout_id'];
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_assign_workout';
		$table_workout_data = $wpdb->prefix. 'gmgt_workout_data';
		$result = $wpdb->query("DELETE FROM $table_workout_data where workout_id= ".$work_out_id);
		$result = $wpdb->query("DELETE FROM $table_workout where workout_id= ".$work_out_id);
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'assign-workout');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['delete'] == '1')
			{
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully deleted",'gym_mgt');
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error']=__("Record not found",'gym_mgt');
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>