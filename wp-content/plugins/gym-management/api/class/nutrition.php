<?php 
class MJgmgt_Nutrition_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		// NUTRITION LIST API
		if($_REQUEST['gmgt_json_api'] == 'nutrition_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);

			$response=$this->MJ_gmgt_nutrition_list($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		// ADD NUTRITION API
		if($_REQUEST['gmgt_json_api'] == 'add_nutrition')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
		   	
			$response=$this->MJ_gmgt_nutrition_list($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	// NUTRITION LIST API FUNCTION
	function MJ_gmgt_nutrition_list($data)
	{
		$nutrition_logdata=MJgmgt_get_user_nutrition($data['current_user_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if (!empty($nutrition_logdata))
			{
				foreach($nutrition_logdata as $row)
				{
					$all_logdata=MJgmgt_get_nutritiondata($row->id);
					$workout_array=array(); 
					$result['start_date'] = $row->start_date;
					$result['end_date'] = $row->expire_date;
					if (!empty($all_logdata))
					{
						foreach ($all_logdata as $value) 
						{
							$workout_array[$value->day_name][] = $value->nutrition_time.' '.$value->nutrition_value;
						}
						$result['nutrition_data'] = $workout_array;
					}
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record found successfully",'gym_mgt');
					$response['result']=$result;
					return $response;
				}
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
	}
	// ADD NUTRITION API FUNCTION
	function MJ_gmgt_add_nutrition($data)
	{
		
	}
}
?>