<?php 
class MJ_Gmgt_ClassSchedule_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Add Class 
		if($_REQUEST['gmgt_json_api'] == 'add_class')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='add';
			$response=$this->MJ_gmgt_add_class_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Class
		if($_REQUEST['gmgt_json_api'] == 'edit_class')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='edit';
			$response=$this->MJ_gmgt_add_class_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Class list
		if($_REQUEST['gmgt_json_api'] == 'class_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_class_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Class
		if($_REQUEST['gmgt_json_api'] == 'single_class')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_class_data($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Class
		if($_REQUEST['gmgt_json_api'] == 'delete_class')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_class_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Class Schedule List
		if($_REQUEST['gmgt_json_api'] == 'class_schedult_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_class_schedule_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Class Booking List
		if($_REQUEST['gmgt_json_api'] == 'class_booking_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_class_booking_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Add-Edit Class Function
	function MJ_gmgt_add_class_data($data,$action)
	{
		$response=array();
		$obj_class=new MJgmgt_classschedule;
		global $wpdb;
		$table_class = $wpdb->prefix. 'gmgt_class_schedule';
		$tbl_membership_class = $wpdb->prefix. 'gmgt_membership_class';
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'class-schedule');
		if ($data['access_token'] == $access_token)
		{
			$classdata['class_name']=MJgmgt_remove_tags_and_special_characters($data['class_name']);
			$classdata['staff_id']=$data['staff_member'];
			$classdata['asst_staff_id']=$data['assistant_staff_member'];
			$classdata['day']=json_encode($data['day']);
			$classdata['class_creat_date']=date("Y-m-d");
			$classdata['class_created_id']=$data['current_user_id'];
	        $classdata['start_date']=MJgmgt_get_format_for_db($data['start_date']);
			$classdata['end_date']=MJgmgt_get_format_for_db($data['end_date']);
			$classdata['color']=$data['class_color'];
			$classdata['member_limit']=$data['member_limit'];
			if($action == 'edit')
			{
				if($menu_access_data['edit'] == '1')
				{
					$classdata['start_time']=$data['start_time'].':'.$data['start_min'].':'.$data['start_ampm'];
					$classdata['end_time']=$data['end_time'].':'.$data['end_min'].':'.$data['end_ampm'];
					$classid['class_id']=$data['class_id'];
					$result=$wpdb->update( $table_class, $classdata ,$classid);
					$new_membership =isset($data['membership_id'])?$data['membership_id']:array();
					$old_membership = $obj_class->MJgmgt_old_membership($data['class_id']);
					$different_insert = array_diff($new_membership,$old_membership);
					$different_delete = array_diff($old_membership,$new_membership);
					if(!empty($different_insert))	
					{
						$membershipdata['class_id']=$data['class_id'];
						foreach($different_insert as $membership_id)
						{
							$membershipdata['membership_id']=$membership_id;
							$wpdb->insert($tbl_membership_class,$membershipdata);
						}
					}	
					if(!empty($different_delete))
					{
						foreach($different_delete as $membership_id)
						{	
							$wpdb->delete( $tbl_membership_class, array( 'membership_id' => $membership_id ) );
						}
					}
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully updated","gym_mgt");
						$response['result']='';
					}
					else
					{
						$response['status']=0;
						$response['error_code']=401;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
						$response['result']='';
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
			else
			{
				if($menu_access_data['add'] == '1')
				{
					if(!empty($data['start_time']))
					{
						foreach($data['start_time'] as $key=>$start_time)
						{
							$classdata['start_time']=$start_time.':'.$data['start_min'][$key].':'.$data['start_ampm'][$key];
							$classdata['end_time']=$data['end_time'][$key].':'.$data['end_min'][$key].':'.$data['end_ampm'][$key];
							$result=$wpdb->insert($table_class,$classdata);	
							$classmeta=array();
							$classmeta['class_id'] = $wpdb->insert_id;
							if(!empty($data['membership_id']))
							{
								foreach($data['membership_id'] as $membership_id)
								{
									$classmeta['membership_id']=$membership_id;
									$result=$wpdb->insert( $tbl_membership_class, $classmeta );
								}
							}	
						}
					}
					if($result)
					{
						$response['status']=1;
						$response['error_code']=200;
						$response['error']=__("Record successfully inserted","gym_mgt");
					}
					else
					{
						$response['status']=0;
						$response['error']=__("Please Fill All Fields",'gym_mgt');
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Class List Function
	public function MJ_gmgt_class_list_data($data)
	{
		$obj_class=new MJgmgt_classschedule;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		$access_token = get_user_meta($user_id, 'access_token' , true);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'class-schedule');
		$cur_user_class_id = MJgmgt_get_current_user_classis($user_id);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$classdata=$obj_class->MJgmgt_get_all_classes_by_member($cur_user_class_id);	
				}
				elseif($role == 'staff_member')
				{	
					$classdata=$obj_class->MJgmgt_get_all_classes_by_staffmember($user_id);	
				}
				elseif($role == 'accountant')
				{
					$classdata=$obj_class->MJgmgt_get_all_classes_by_class_created_id($user_id);	
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$classdata=$obj_class->MJgmgt_get_all_classes();
			}
			else
			{
				$classdata="";
			}
			
			$response	=	array();

			if(!empty($classdata))
			{	
				$i=0;
				foreach ($classdata as $retrieved_data)
				{		
					$userdata=get_userdata( $retrieved_data->staff_id );
					$result[$i]['class_id'] = $retrieved_data->class_id;
					$result[$i]['class_name'] =	$retrieved_data->class_name;
					$result[$i]['staff_member'] = $userdata->display_name;
					$result[$i]['start_time'] = MJgmgt_timeremovecolonbefoream_pm($retrieved_data->start_time);
					$result[$i]['end_time'] = MJgmgt_timeremovecolonbefoream_pm($retrieved_data->end_time);
					$days_array=json_decode($retrieved_data->day); 
					$days_string=array();
					if(!empty($days_array))
					{
						foreach($days_array as $day)
						{
							$days_string[]=substr($day,0,3);
						}
					}
					$result[$i]['day'] = implode(", ",$days_string);
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Single Class Function
	public function MJ_gmgt_single_class_data($data)
	{
		$obj_class=new MJgmgt_classschedule;
		$result = $obj_class->MJgmgt_get_single_class($data['class_id']);
		$access_token = get_user_meta($data['current_user_id'],'access_token',true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($result))
			{
				$classdata['class_id'] = $result->class_id;
				$classdata['class_name'] =$result->class_name;
				$classdata['staff_member'] = $result->staff_id;
				$classdata['assistant_staff_member'] = $result->asst_staff_id;
				$classdata['start_date'] = $result->start_date;
				$classdata['end_date'] = $result->end_date;
				$classdata['day'] = $result->day;
				$membershipdata = $obj_class->MJgmgt_get_class_members($result->class_id);
				foreach($membershipdata as $key=>$val)
				{
					$data[]= $val->membership_id;
				}
				$classdata['membership'] = $data;
				$classdata['member_limit'] = $result->member_limit;
				$classdata['start_time'] = $result->start_time;
				$classdata['end_time'] = $result->end_time;
				$classdata['class_color'] = $result->color;
				
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$classdata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']='';
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Delete Class Function
	public function MJ_gmgt_delete_class_data($data)
	{
		 $response=array();
		 $user_id = $data['current_user_id'];
		 $obj_class=new MJgmgt_classschedule;
		 $result=$obj_class->MJgmgt_delete_class($data['class_id']);
		 $access_token = get_user_meta($user_id, 'access_token' , true);
		 $menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'class-schedule');
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['delete'] == '1')
			{
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully deleted",'gym_mgt');
					$response['result']='';
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Record not found",'gym_mgt');
					$response['result']='';
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Class Schedule List Function
	public function MJ_gmgt_class_schedule_list_data($data)
	{
		$obj_class=new MJgmgt_classschedule;
		$curr_user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$user_access=MJgmgt_get_userrole_wise_access_right_array_in_api($curr_user_id,'class-schedule');
		$cur_user_class_id = MJgmgt_get_current_user_classis($curr_user_id);
		$response = array();
		$class_schedule_array = array();
		$access_token = get_user_meta($data['current_user_id'],'access_token',true);
		if ($data['access_token'] == $access_token)
		{
			foreach(MJgmgt_days_array() as $daykey => $dayname)
			{
				$period = $obj_class->MJgmgt_get_schedule_byday($daykey);
				if(!empty($period))
				{
					$i=0;
					$class_schedule	= array();
					foreach($period as $period_data)
					{
						if(!empty($period_data))
						{						
							if($role=='staff_member')
							{
								if($user_access['own_data']=='1')
								{
									if($period_data['staff_id']==$curr_user_id || $period_data['asst_staff_id']==$curr_user_id)
									{
										$class_schedule[$i]['class_name']=MJgmgt_get_single_class_name($period_data['class_id']);
										$class_schedule[$i]['start_time']=MJgmgt_timeremovecolonbefoream_pm($period_data['start_time']);
										$class_schedule[$i]['end_time']=MJgmgt_timeremovecolonbefoream_pm($period_data['end_time']);
									}								
								}
								else
								{
									$class_schedule[$i]['class_name']=MJgmgt_get_single_class_name($period_data['class_id']);
									$class_schedule[$i]['start_time']=MJgmgt_timeremovecolonbefoream_pm($period_data['start_time']);
									$class_schedule[$i]['end_time']=MJgmgt_timeremovecolonbefoream_pm($period_data['end_time']);	
								}		
							}
							elseif($role == 'member')
							{												
								if(in_array($period_data['class_id'],$cur_user_class_id))
								{
									$class_schedule[$i]['class_name']=MJgmgt_get_single_class_name($period_data['class_id']);
									$class_schedule[$i]['start_time']=MJgmgt_timeremovecolonbefoream_pm($period_data['start_time']);
									$class_schedule[$i]['end_time']=MJgmgt_timeremovecolonbefoream_pm($period_data['end_time']);
								}												
							}
							else
							{		
								if($user_access['own_data']=='1')
								{													
									if($period_data['class_created_id'] == $curr_user_id)		
									{													
										$class_schedule[$i]['class_name']=MJgmgt_get_single_class_name($period_data['class_id']);
										$class_schedule[$i]['start_time']=MJgmgt_timeremovecolonbefoream_pm($period_data['start_time']);
										$class_schedule[$i]['end_time']=MJgmgt_timeremovecolonbefoream_pm($period_data['end_time']);								
									}												
								}
								else
								{
									$class_schedule[$i]['class_name']=MJgmgt_get_single_class_name($period_data['class_id']);
									$class_schedule[$i]['start_time'] = MJgmgt_timeremovecolonbefoream_pm($period_data['start_time']);
									$class_schedule[$i]['end_time'] = MJgmgt_timeremovecolonbefoream_pm($period_data['end_time']);
								}
							}		
						}
						$i++;
					}	
					$class_schedule_array[$daykey]=$class_schedule;
				}
			}
			
			if(!empty($class_schedule_array))
			{			
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$class_schedule_array;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Class Booking List Function
	public function MJ_gmgt_class_booking_list_data($data)
	{
		$obj_class=new MJgmgt_classschedule;
		$user_id=$data['current_user_id'];
		$bookingdata=$obj_class->MJgmgt_get_member_book_class($user_id);
		$response = array();
		$access_token = get_user_meta($user_id,'access_token',true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($bookingdata))
			{
				$i=0;
				foreach ($bookingdata as $retrieved_data)
				{		
					$result[$i]['member_name'] =MJgmgt_get_display_name($retrieved_data->member_id);
					$result[$i]['class_name'] = $obj_class->MJgmgt_get_class_name($retrieved_data->class_id);
					$result[$i]['class_date'] = str_replace('00:00:00',"",$retrieved_data->class_booking_date);
					$result[$i]['booking_date'] = str_replace('00:00:00',"",$retrieved_data->booking_date);
					$result[$i]['day'] = $retrieved_data->booking_day;
					$class_data = $obj_class->MJgmgt_get_single_class($retrieved_data->class_id);
					$result[$i]['start_time'] = MJgmgt_timeremovecolonbefoream_pm($class_data->start_time);
					$result[$i]['end_time'] = MJgmgt_timeremovecolonbefoream_pm($class_data->end_time);
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>