<?php 
class MJ_Gmgt_Dashboard_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Module Wise Total Count
		if($_REQUEST['gmgt_json_api'] == 'module_wise_total_count')
		{	
			$response=$this->MJ_gmgt_module_wise_total_count_data($_REQUEST);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Module Wise Data List
		if($_REQUEST['gmgt_json_api'] == 'module_wise_data_list')
		{	
			$response=$this->MJ_gmgt_module_wise_data_list_data($_REQUEST);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		if($_REQUEST['gmgt_json_api'] == 'dashboard_report')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode(stripslashes($json), true);
			$response=$this->MJgmgt_dashboard_report($data);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Module Wise Total Count Function
	function MJ_gmgt_module_wise_total_count_data($data)
	{
		$response=array();
		$obj_dashboard= new MJ_Gmgtdashboard;
		 
		$module_wise_data['member'] = MJ_gmgt_count_total_member_dashboard_by_access_right('member');;
		$module_wise_data['staff_member'] =MJ_gmgt_count_total_staff_member_dashboard_by_access_right('staff_member');
		$module_wise_data['accountant'] = count(get_users(array('role'=>'accountant')));
		$module_wise_data['group'] = MJ_gmgt_count_total_group_dashboard_by_access_right('group');
		$module_wise_data['message'] = count(MJ_gmgt_count_inbox_item($data['current_user_id']));
		$module_wise_data['membership'] = MJ_gmgt_count_total_membership_dashboard_by_access_right('membership');
		$module_wise_data['notice'] = MJ_gmgt_count_total_notice_dashboard_by_access_right('notice');
		$module_wise_data['class'] = MJ_gmgt_count_total_class_dashboard_by_access_right('class-schedule');
		$module_wise_data['reservation'] = MJ_gmgt_count_total_reservation_dashboard_by_access_right('reservation');
		$module_wise_data['product'] = MJ_gmgt_count_total_product_dashboard_by_access_right('product');
		$module_wise_data['attendence'] = $obj_dashboard->today_presents();
	
		$response['status']=1;
		$response['error_code']=200;
		$response['error']=__("Record found successfully",'gym_mgt');
		$response['result']=$module_wise_data;
			
		return $response;
	}
	//Module Wise Data List Function
	public function MJ_gmgt_module_wise_data_list_data($data)
	{
		$user_id=$data['current_user_id'];
		$role=MJ_gmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$module_name=$data['module_name'];
		$menu_access_data=MJ_gmgt_get_userrole_wise_access_right_array_in_api($user_id,$module_name);
		
		//Activities List
		$obj_activity=new MJ_Gmgtactivity;		
		if($module_name == 'activity')
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				$activitydata=$obj_activity->MJ_gmgt_get_all_activity_by_activity_added_by_dashboard($user_id);
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$activitydata=$obj_activity->MJ_get_all_activity_dashboard();
			}
			else
			{
				$activitydata="";
			}
			
			$response	=	array();

			if(!empty($activitydata))
			{	
				$i=0;
				foreach ($activitydata as $retrieved_data)
				{		
					$user=get_userdata($retrieved_data->activity_assigned_to);
					$result[$i]['activity_name'] =	$retrieved_data->activity_title;
					$result[$i]['activity_category'] =	get_the_title($retrieved_data->activity_cat_id);
					$result[$i]['activity_trainer'] = $user->display_name;
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		//Group List
		$obj_group=new MJ_Gmgtgroup;		
		if($module_name == 'group')
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$groupdata=$obj_group->MJ_gmgt_get_member_all_groups_dashboard($user_id);			
				}
				elseif($role == 'staff_member' || $role == 'accountant')
				{	
					$groupdata=$obj_group->MJ_gmgt_get_all_groups_by_created_by_dashboard($user_id);
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$groupdata=$obj_group->MJ_gmgt_get_all_groups_dashboard();
			}
			else
			{
				$groupdata="";
			}
			
			$response	=	array();

			if(!empty($groupdata))
			{	
				$i=0;
				foreach ($groupdata as $retrieved_data)
				{		
					$group_count=$obj_group->MJ_gmgt_count_group_members($retrieved_data->id);
					$user=get_userdata($retrieved_data->activity_assigned_to);
					$result[$i]['id'] =	$retrieved_data->id;
					$result[$i]['group_name'] =	$retrieved_data->group_name;
					$result[$i]['group_count'] =	$group_count;
					$result[$i]['group_description'] = $user->group_description;
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		//Membership List
		$obj_membership=new MJ_Gmgtmembership;		
		if($module_name == 'membership')
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$membership_id = get_user_meta( $user_id,'membership_id', true ); 
					$membershipdata=$obj_membership->MJ_gmgt_get_member_own_membership_dashboard($membership_id);			
				}
				elseif($role == 'staff_member' || $role == 'accountant')
				{	
					$membershipdata=$obj_membership->MJ_gmgt_get_membership_by_created_by_dashboard($user_id);
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$membershipdata=$obj_membership->MJ_gmgt_get_all_membership_dashboard();
			}
			else
			{
				$membershipdata="";
			}
			
			$response	=	array();

			if(!empty($membershipdata))
			{	
				$i=0;
				foreach ($membershipdata as $retrieved_data)
				{		
					
					$result[$i]['membership_id'] = $retrieved_data->membership_id;
					$result[$i]['membership_name'] = $retrieved_data->membership_label;
					$result[$i]['period'] =	$retrieved_data->membership_length_id;
					$result[$i]['amount'] = $retrieved_data->membership_amount;
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		//Payment List
		$obj_payment=new MJ_Gmgtpayment;		
		if($module_name == 'payment')
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$paymentdata=$obj_payment->MJ_gmgt_get_all_income_data_by_member_dashboard();			
				}
				elseif($role == 'staff_member' || $role == 'accountant')
				{	
					$paymentdata=$obj_payment->MJ_gmgt_get_all_income_data_by_created_by_dashboard($user_id);
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$paymentdata=$obj_payment->MJ_gmgt_get_all_income_data_dashboard();
			}
			else
			{
				$paymentdata="";
			}
			
			$response	=	array();

			if(!empty($paymentdata))
			{	
				$i=0;
				foreach ($paymentdata as $retrieved_data)
				{		
					$user=get_userdata($retrieved_data->supplier_name);
					$memberid=get_user_meta($retrieved_data->supplier_name,'member_id',true);
					$display_label=$user->display_name;
					if($memberid)
					$display_label.=" (".$memberid.")";
					
					$result[$i]['invoice_id'] = $retrieved_data->invoice_id;
					$result[$i]['invoice_no'] = $retrieved_data->invoice_no;
					$result[$i]['member_name'] = $display_label;
					$result[$i]['amount'] = MJ_gmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )).''.number_format($retrieved_data->total_amount,2);
					$result[$i]['status'] = $retrieved_data->payment_status;
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		//Reservation List
		$obj_reservation=new MJ_Gmgtreservation;		
		if($module_name == 'reservation')
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				$reservationdata=$obj_reservation->MJ_gmgt_get_reservation_by_created_by_dashboard();
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$reservationdata=$obj_reservation->MJ_gmgt_get_all_reservation_dashboard();
			}
			else
			{
				$reservationdata="";
			}
			
			$response	=	array();

			if(!empty($reservationdata))
			{	
				$i=0;
				foreach ($reservationdata as $retrieved_data)
				{		
					$result[$i]['id'] = $retrieved_data->id;
					$result[$i]['event_name'] = $retrieved_data->event_name;
					$result[$i]['date'] =MJ_gmgt_getdate_in_input_box($retrieved_data->event_date);
					$result[$i]['event_place'] = get_the_title( $retrieved_data->place_id );
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		//Notice List
		$obj_notice=new MJ_Gmgtnotice;		
		if($module_name == 'notice')
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				$noticedata =$obj_notice->MJ_gmgt_get_notice_dashboard($role);
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$noticedata =$obj_notice->MJ_gmgt_get_all_notice_dashboard();
			}
			else
			{
				$noticedata="";
			}
			
			$response	=	array();

			if(!empty($noticedata))
			{	
				$i=0;
				foreach ($noticedata as $retrieved_data)
				{		
					$result[$i]['id'] = $retrieved_data->ID;
					$result[$i]['notice_title'] = $retrieved_data->post_title;
					$result[$i]['notice_description'] =$retrieved_data->post_content;
					$result[$i]['start_date'] = MJ_gmgt_getdate_in_input_box(get_post_meta($retrieved_data->ID,'gmgt_start_date',true));
					$result[$i]['end_date'] = MJ_gmgt_getdate_in_input_box(get_post_meta($retrieved_data->ID,'gmgt_end_date',true));
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		//Class List
		$obj_class=new MJ_Gmgtclassschedule;		
		if($module_name == 'class-schedule')
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$cur_user_class_id = array();	
					
					$cur_user_class_id = MJ_gmgt_get_current_user_classis($user_id);				
					$classdata_list=$obj_class->MJ_gmgt_get_all_classes_by_member_dashboard($cur_user_class_id);	
				}
				elseif($role == 'staff_member')
				{	
					$classdata_list=$obj_class->MJ_gmgt_get_all_classes_by_staffmember_dashboard($user_id);	
				}
				elseif($role == 'accountant')
				{	
					$classdata_list=$obj_class->MJ_gmgt_get_all_classes_by_class_created_id_dashboard($user_id);	
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$classdata_list=$obj_class->MJ_gmgt_get_all_classes_dashboard();
			}
			else
			{
				$classdata_list="";
			}
			
			$response	=	array();

			if(!empty($classdata_list))
			{	
				$i=0;
				foreach ($classdata_list as $retrieved_data)
				{		
					$userdata=get_userdata( $retrieved_data->staff_id );
					$days_array=json_decode($retrieved_data->day); 
					$days_string=array();
					if(!empty($days_array))
					{
						foreach($days_array as $day)
						{
							$days_string[]=substr($day,0,3);
						}
					}		
					$result[$i]['class_id'] = $retrieved_data->class_id;
					$result[$i]['class_name'] = $retrieved_data->class_name;
					$result[$i]['staff_name'] =$userdata->display_name;
					$result[$i]['day'] = implode(", ",$days_string);
					$result[$i]['start_time'] = MJ_gmgt_timeremovecolonbefoream_pm($retrieved_data->start_time);
					$result[$i]['end_time'] = MJ_gmgt_timeremovecolonbefoream_pm($retrieved_data->end_time);
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		//Booking List
		$obj_class=new MJ_Gmgtclassschedule;		
		if($module_name == 'booking')
		{
			$booking_data=$obj_class->MJ_gmgt_get_member_book_class_dashboard($data['current_user_id']);
			
			$response	=	array();

			if(!empty($booking_data))
			{	
				$i=0;
				foreach ($booking_data as $retrieved_data)
				{		
						
					$result[$i]['id'] = $retrieved_data->id;
					$result[$i]['member_name'] = MJ_gmgt_get_display_name($retrieved_data->member_id);
					$result[$i]['class_name'] =$obj_class->MJ_gmgt_get_class_name($retrieved_data->class_id);
					$result[$i]['class_date'] = str_replace('00:00:00',"",$retrieved_data->class_booking_date);
					$result[$i]['booking_date'] = str_replace('00:00:00',"",$retrieved_data->booking_date);
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		return $response;
	}
	public function MJgmgt_dashboard_report($data)
	{
		$response =	array();
		$obj_gyme = new MJgmgt_Gym_management();
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			$report_data = $obj_gyme->MJgmgt_get_dashboard_report_api($data['report_type'],esc_attr($data['current_user_id']));
			$response['status']=1;
			$response['error_code']=200;
			$response['error']=__("Record found successfully","gym_mgt");
			$response['result']=$report_data;
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';

		}
		return $response;
	}
}
?>