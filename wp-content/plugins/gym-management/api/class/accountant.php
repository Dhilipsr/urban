<?php 
class MJ_Gmgt_Accountant_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Accountant list
		if($_REQUEST['gmgt_json_api'] == 'accountant_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_accountant_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Accountant List Function
	public function MJ_gmgt_accountant_list_data($data)
	{
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'accountant');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1')
			{
				$get_accountant = array('role' => 'accountant');
				$accountantdata=get_users($get_accountant);
			}
			else
			{
				$accountantdata="";
			}
			
			$response	=	array();

			if(!empty($accountantdata))
			{	
				$i=0;
				foreach ($accountantdata as $retrieved_data)
				{
					$uid=$retrieved_data->ID;
					$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);
					if(empty($userimage))
					{
						$userimage=get_option( 'gmgt_system_logo' );
					}
					else
					{
						$userimage=$userimage;
					}
					$result[$i]['accountant_name'] = $retrieved_data->display_name;
					$result[$i]['accountant_image'] = $userimage;
					$result[$i]['email'] = $retrieved_data->user_email;
					$result[$i]['mobile'] =	$retrieved_data->mobile;
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>