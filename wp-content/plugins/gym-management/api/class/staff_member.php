<?php 
class MJ_Gmgt_Stafffmember_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Staff list
		if($_REQUEST['gmgt_json_api'] == 'staffmember_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_staff_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		// SINGAL STAFF MEMBER 
		if($_REQUEST['gmgt_json_api'] == 'get_singal_staff_member')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_staffmember($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}

	}
	//Staff List Function
	public function MJ_gmgt_staff_list_data($data)
	{
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'staff_member');
		$access_token = get_user_meta($user_id , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				if($role == 'member')
				{	
					$staff_id = get_user_meta( $user_id,'staff_id', true ); 
					$staffdata=array();
					$staffdata[] = get_userdata($staff_id);				
				}
				elseif($role == 'staff_member')
				{			
					$staffdata=array();
					$staffdata[] = get_userdata($staff_id);
				}
				else
				{
					$get_staff = array('role' => 'Staff_member');
					$staffdata=get_users($get_staff);
				}
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$get_staff = array('role' => 'Staff_member');
				$staffdata=get_users($get_staff);
			}
			else
			{
				$staffdata="";
			}
			$response = array();
			if(!empty($staffdata))
			{
				$i=0;
				foreach ($staffdata as $retrieved_data)
				{
					$uid=$retrieved_data->ID;
					$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);
					if(empty($userimage))
					{
						$userimage=get_option( 'gmgt_system_logo' );
					}
					else
					{
						$userimage=$userimage;
					}
					$result[$i]['staff_id'] = $retrieved_data->id;
					$result[$i]['staff_name'] =	$retrieved_data->display_name;
					$result[$i]['staff_image'] = $userimage;
					$postdata=array();
					if($retrieved_data->role_type!="")
						$postdata=get_post($retrieved_data->role_type);
					if(!empty($postdata))
					{
						$role_type=$postdata->post_title;
					}
					else
					{
						$role_type='-';
					}
					$result[$i]['role'] =  $role_type;
					$result[$i]['email'] =	$retrieved_data->user_email;
					$result[$i]['mobile'] =	$retrieved_data->mobile;
					$specilization_array=explode(',',$retrieved_data->activity_category);
					$specilization_name_array=array();
					if(!empty($specilization_array))
					{
						foreach ($specilization_array as $data)
						{
							$specilization_name_array[]=get_the_title($data);
						}	
					}
					$result[$i]['specilization'] =	implode(',',$specilization_name_array);
					
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User",'gym_mgt');
		}
		return $response;
	}
	//Single Staff Function
	public function MJ_gmgt_single_staffmember($data)
	{
		$obj_activity = new MJgmgt_activity(); 
		$user_info = get_userdata($data['staff_member_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($user_info))
			{
				$activity_array=$obj_activity->MJgmgt_get_activity_staffmemberwise($user_info->ID);
				$class_name="-";
				if($activity_array)
				{												
					foreach($activity_array as $key=>$activity_id)
					{
					  $activity = esc_html($activity_id);
					}
				}						
				else
				{
					$activity = esc_html($class_name);
				}
				$staffmemberdata['staff_member_id'] = $user_info->ID;
				$staffmemberdata['first_name'] =$user_info->first_name;
				$staffmemberdata['middle_name'] = $user_info->middle_name;
				$staffmemberdata['last_name'] = $user_info->last_name;
				$staffmemberdata['gender'] = $user_info->gender;
				$staffmemberdata['dob'] = MJgmgt_getdate_in_input_box($user_info->birth_date);
				$staffmemberdata['email'] = $user_info->user_email;
				$staffmemberdata['address'] = $user_info->address;
				$staffmemberdata['city'] = $user_info->city_name;
				$staffmemberdata['state'] = $user_info->state_name;
				$staffmemberdata['zip'] = $user_info->zip_code;
				$staffmemberdata['mobile'] = $user_info->mobile;
				$staffmemberdata['phone'] = $user_info->phone;
				$staffmemberdata['username'] = $user_info->user_login;
				$staffmemberdata['member_type'] =$user_info->member_type;
				$staffmemberdata['activity'] =$activity;
				$staffmemberdata['assign_role'] =get_the_title(esc_html($user_info->role_type));
				$staffmemberdata['member_image'] = esc_url( $user_info->gmgt_user_avatar );
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$staffmemberdata;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error']=__("An Unauthorized User",'gym_mgt');
			$response['result']=Null;
		}
		return $response;
	}
}
?>