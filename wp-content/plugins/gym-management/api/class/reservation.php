<?php 
class MJ_Gmgt_Reservation_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Add Reservation 
		if($_REQUEST['gmgt_json_api'] == 'add_reservation')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='add';
			$response=$this->MJ_gmgt_add_reservation_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Edit Reservation
		if($_REQUEST['gmgt_json_api'] == 'edit_reservation')
		{	
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$action='edit';
			$response=$this->MJ_gmgt_add_reservation_data($data,$action);	 
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Reservation list
		if($_REQUEST['gmgt_json_api'] == 'reservation_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_reservation_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Single Reservation
		if($_REQUEST['gmgt_json_api'] == 'single_reservation')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_single_reservation_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
		//Delete Reservation
		if($_REQUEST['gmgt_json_api'] == 'delete_reservation')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_delete_reservation_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Add-Edit Reservation Function
	function MJ_gmgt_add_reservation_data($data,$action)
	{
		$response=array();
		global $wpdb;
		$table_reservation = $wpdb->prefix. 'gmgt_reservation';
		$reservationdata['event_name']=MJgmgt_strip_tags_and_stripslashes($data['event_name']);
		$reservationdata['event_date']=MJgmgt_get_format_for_db($data['event_date']);
		$reservationdata['start_time']=$data['start_time'].':'.$data['start_min'].':'.$data['start_ampm'];
		$reservationdata['end_time']=$data['end_time'].':'.$data['end_min'].':'.$data['end_ampm'];
		$reservationdata['place_id']=$data['event_place'];
		$reservationdata['staff_id']=$data['staff_id'];
		$reservationdata['created_date']=date("Y-m-d");
		$reservationdata['created_by']=$data['current_user_id'];
		$start_time_24_hours=MJgmgt_get_reservation_time_in_24_hours($reservationdata['start_time']);
		$end_time_24_hours=MJgmgt_get_reservation_time_in_24_hours($reservationdata['end_time']);
		$reserved_class="";
		$reserv_datedata = $wpdb->get_results("SELECT * FROM $table_reservation where event_date ='".$reservationdata['event_date']."' and place_id ='".$reservationdata['place_id']."' and id!=".$data['reservation_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'reservation');
		if ($data['access_token'] == $access_token)
		{
			if(!empty($reserv_datedata))
			{	
				foreach($reserv_datedata as $retrieved_data)
				{
					$reserved_start_time_24_hours=MJgmgt_get_reservation_time_in_24_hours($retrieved_data->start_time);
					$reserved_end_time_24_hours=MJgmgt_get_reservation_time_in_24_hours($retrieved_data->end_time);
					if(($reserved_start_time_24_hours > $start_time_24_hours && $reserved_start_time_24_hours < $end_time_24_hours) || ($reserved_end_time_24_hours > $start_time_24_hours && $reserved_end_time_24_hours < $end_time_24_hours) || ($reserved_start_time_24_hours < $start_time_24_hours && $reserved_end_time_24_hours > $end_time_24_hours) || ($reserved_start_time_24_hours > $start_time_24_hours && $reserved_end_time_24_hours < $end_time_24_hours))	
					{
						$reserved_class="reserved";
					}
				}
			}
			if($action == 'edit')
			{
				if($menu_access_data['edit'] == '1' )
				{
					if($data['start_ampm'] == $data['end_ampm'] )
					{				
						if($data['end_time'] < $data['start_time'])
						{
							$time_validation='1';
						}
						elseif($data['end_time'] ==  $data['start_time'] && $data['start_min'] > $data['end_min'] )
						{
							$time_validation='1';
						}
					}
					else
					{
						if($data['start_ampm']!='am')
						{
							$time_validation='1';
						}
					}	
					if($time_validation=='1')
					{
						$response['status']=0;
						$response['error']=__("End Time should be greater than Start Time",'gym_mgt');
					}
					else
					{
						if($reserved_class=="")
						{
							$result=$wpdb->update( $table_reservation, $reservationdata ,$reservationid);
						}
						else
						{
							$result=array('id'=>$data['reservation_id'],'msg'=>'reserved');
						}
						if($result['msg']!='reserved')
						{
							$response['status']=1;
							$response['error_code']=200;
							$response['error']=__("Record successfully updated",'gym_mgt');
						}
						else
						{					
							if(isset($result['msg']))
							{
								$response['status']=0;
								$response['error']=__("This Date is Already Reserved.",'gym_mgt');
							}
						}
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
			else
			{
				if($menu_access_data['add'] == '1' )
				{
					if($data['start_ampm'] == $data['end_ampm'] )
					{				
						if($data['end_time'] < $data['start_time'])
						{
							$time_validation='1';	
						}
						elseif($data['end_time'] ==  $data['start_time'] && $data['start_min'] > $data['end_min'] )
						{
							$time_validation='1';
						}				
					}			
					else
					{
						if($data['start_ampm']!='am')
						{
							$time_validation='1';
						}				
					}	
					if($time_validation=='1')
					{
						$response['status']=0;
						$response['error']=__("End Time should be greater than Start Time",'gym_mgt');
					}
					else
					{	
						$getresult = $wpdb->get_results("SELECT * FROM $table_reservation where event_date ='".$reservationdata['event_date']."' and place_id ='".$reservationdata['place_id']."'");
						if(!empty($getresult))
						{
							foreach($getresult as $retrieved_data)
							{					
								$reserved_start_time_24_hours=MJgmgt_get_reservation_time_in_24_hours($retrieved_data->start_time);
								$reserved_end_time_24_hours=MJgmgt_get_reservation_time_in_24_hours($retrieved_data->end_time);
								if(($reserved_start_time_24_hours > $start_time_24_hours && $reserved_start_time_24_hours < $end_time_24_hours) || ($reserved_end_time_24_hours > $start_time_24_hours && $reserved_end_time_24_hours < $end_time_24_hours) || ($reserved_start_time_24_hours < $start_time_24_hours && $reserved_end_time_24_hours > $end_time_24_hours) || ($reserved_start_time_24_hours > $start_time_24_hours && $reserved_end_time_24_hours < $end_time_24_hours))
								{	
									$reserved_class="reserved";
								}								
							}
						}
						
						if($reserved_class == "reserved")
						{
							$result="reserved";
						}
						else
						{	
							$result=$wpdb->insert( $table_reservation, $reservationdata );				
							$titlename=get_the_title($data['event_place']);			
							//add resrvation 				
							 $gymname=get_option( 'gmgt_system_name' );
							
							$page_link=home_url().'/?dashboard=user&page=reservation&tab=reservationlist';
							 $staffdata=get_userdata($data['staff_id']);
							 $staffemail=$staffdata->user_email;
							 $staff_name=$staffdata->display_name;
							 
							$arr['[GMGT_STAFF_MEMBERNAME]']=$staff_name;	
							$arr['[GMGT_EVENT_NAME]']=MJgmgt_strip_tags_and_stripslashes($data['event_name']);	
							$arr['[GMGT_GYM_NAME]']=$gymname;
							$arr['[GMGT_EVENT_DATE]']=$reservationdata['event_date'];
							$arr['[GMGT_EVENT_PLACE]']=$titlename;
							$arr['[GMGT_START_TIME]']=$reservationdata['start_time'];
							$arr['[GMGT_END_TIME]']=$reservationdata['end_time'];
							$arr['[GMGT_PAGE_LINK]']=$page_link;
							$subject =get_option('Add_Reservation_Subject');
							$sub_arr['[GMGT_EVENT_NAME]']=MJgmgt_strip_tags_and_stripslashes($data['event_name']);
							$sub_arr['[GMGT_EVENT_PLACE]']=$titlename;
							$sub_arr['[GMGT_GYM_NAME]']=$gymname;
							$sub_arr['[GMGT_EVENT_DATE]']=$reservationdata['event_date'];
							$sub_arr['[GMGT_START_TIME]']=$reservationdata['start_time'];
							$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
							$message = get_option('Add_Reservation_Template');	
							$message_replacement = MJgmgt_string_replacemnet($arr,$message);
							$to[]=$staffemail;
							MJgmgt_send_mail($to,$subject,$message_replacement);
						}
						
						if($result!="reserved")
						{
							$response['status']=1;
							$response['error_code']=200;
							$response['error']=__("Record successfully inserted","gym_mgt");
							$response['result']='';
							return $response;
							
						}
						else
						{
							$response['status']=0;
							$response['error_code']=401;
							$response['error']=__("This Date is Already Reserved Event.",'gym_mgt');
							$response['result']='';
							return $response;
						}
					}
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("You Can't Access This Page","gym_mgt");
					$response['result']='';
					return $response;
				}
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Reservation List Function
	public function MJ_gmgt_reservation_list_data($data)
	{
		$obj_reservation=new MJgmgt_reservation;
		$user_id=$data['current_user_id'];
		$role=MJgmgt_get_roles($data['current_user_id']);
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'reservation');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				$reservationdata=$obj_reservation->MJgmgt_get_reservation_by_created_by();
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$reservationdata=$obj_reservation->MJgmgt_get_all_reservation();
			}
			else
			{
				$reservationdata="";
			}
			$response = array();
			if(!empty($reservationdata))
			{	
				$i=0;
				foreach ($reservationdata as $retrieved_data)
				{		
					$result[$i]['reservation_id'] =	$retrieved_data->id;
					$result[$i]['event_name'] =	$retrieved_data->event_name;
					$result[$i]['event_date'] = MJgmgt_getdate_in_input_box($retrieved_data->event_date );
					$result[$i]['event_place'] = get_the_title( $retrieved_data->place_id );
					$result[$i]['start_time'] = MJgmgt_timeremovecolonbefoream_pm($retrieved_data->start_time);
					$result[$i]['end_time'] = MJgmgt_timeremovecolonbefoream_pm($retrieved_data->end_time);
					$result[$i]['reserved_by'] = MJgmgt_get_display_name($retrieved_data->staff_id);
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Single Reservation Function
	public function MJ_gmgt_single_reservation_data($data)
	{
		$obj_reservation=new MJgmgt_reservation;
		$result = $obj_reservation->MJgmgt_get_single_reservation($data['reservation_id']);
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if(!empty($result))
			{			
				$reservationdata['reservation_id'] = $result->id;
				$reservationdata['event_name'] =$result->event_name;
				$reservationdata['event_date'] = MJgmgt_getdate_in_input_box($result->event_date);
				$reservationdata['event_place'] = $result->place_id;
				$reservationdata['start_time'] = $result->start_time;
				$reservationdata['end_time'] = $result->end_time;
				$reservationdata['staff_member'] = $result->staff_id;
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$reservationdata;
				return $response;
			}
			else
			{
				$response['status']=0;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=Null;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
	//Delete Reservation Function
	public function MJ_gmgt_delete_reservation_data($data)
	{
		$response=array();
		$obj_reservation=new MJgmgt_reservation;
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($data['current_user_id'],'reservation');
		$access_token = get_user_meta($data['current_user_id'] , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['delete'] == '1')
			{
				$result=$obj_reservation->MJgmgt_delete_reservation($data['reservation_id']);
				if($result)
				{
					$response['status']=1;
					$response['error_code']=200;
					$response['error']=__("Record successfully deleted",'gym_mgt');
					$response['result']='';
					return $response;
				}
				else
				{
					$response['status']=0;
					$response['error_code']=401;
					$response['error']=__("Record not found",'gym_mgt');
					$response['result']='';
					return $response;
				}
			}
			else
			{
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("You Can't Access This Page","gym_mgt");
				$response['result']='';
				return $response;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>