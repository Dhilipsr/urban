<?php 
class MJ_Gmgt_Subscription_History_API
{
	public function __construct() 
	{
		add_action('template_redirect', array($this,'redirectMethod'), 1);
	}
	public function redirectMethod()
	{
		//error_reporting(0);
		//Subscription History list
		if($_REQUEST['gmgt_json_api'] == 'subscription_history_list')
		{
			$json = file_get_contents('php://input');
		   	$data = json_decode($json, true);
			$response=$this->MJ_gmgt_subscription_history_list_data($data);
			if(is_array($response))
			{
				echo json_encode($response);
			}
			else 
			{
				header("HTTP/1.1 401 Unauthorized");
			}
			die();
		}
	}
	//Subscription History Function
	public function MJ_gmgt_subscription_history_list_data($data)
	{
		$obj_membership_payment=new MJgmgt_membership_payment;
		$user_id=$data['current_user_id'];
		//Get User Acsess //
		$menu_access_data=MJgmgt_get_userrole_wise_access_right_array_in_api($user_id,'subscription_history');
		$access_token = get_user_meta($user_id , 'access_token' , true);
		if ($data['access_token'] == $access_token)
		{
			if($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 1 )
			{
				$paymentdata=$obj_membership_payment->MJgmgt_get_member_subscription_history($user_id);
			}
			elseif($menu_access_data['view'] == '1' && $menu_access_data['own_data'] == 0 )
			{
				$paymentdata=$obj_membership_payment->MJgmgt_get_all_member_subscription_history();
			}
			else
			{
				$paymentdata="";
			}
			$response =	array();
			if(!empty($paymentdata))
			{	
				$i=0;
				foreach ($paymentdata as $retrieved_data)
				{
					$result[$i]['mp_id'] =	$retrieved_data->mp_id;
					$result[$i]['membership_name'] = MJgmgt_get_membership_name($retrieved_data->membership_id);
					$result[$i]['member_id'] =	$retrieved_data->member_id;
					$result[$i]['membership_id'] =	$retrieved_data->membership_id;
					$result[$i]['invoice_no'] =	$retrieved_data->invoice_no;
					$result[$i]['membership_amount'] =	$retrieved_data->membership_amount;
					$result[$i]['paid_amount'] =	$retrieved_data->paid_amount;
					$result[$i]['start_date'] =	$retrieved_data->start_date;
					$result[$i]['end_date'] =	$retrieved_data->end_date;
					$result[$i]['membership_status'] =	$retrieved_data->membership_status;
					$result[$i]['payment_status'] =	$retrieved_data->payment_status;
					$result[$i]['membership_fees_amount'] =	$membership_fees_amount->membership_fees_amount;
					$result[$i]['membership_signup_amount'] =	$membership_fees_amount->membership_signup_amount;
					$result[$i]['tax_amount'] =	$membership_fees_amount->tax_amount;
					$i++;
				}
				$response['status']=1;
				$response['error_code']=200;
				$response['error']=__("Record found successfully",'gym_mgt');
				$response['result']=$result;
				return $response;
			}
			else
			{
				$result=array();
				$response['status']=0;
				$response['error_code']=401;
				$response['error']=__("Record not found",'gym_mgt');
				$response['result']=$result;
			}
		}
		else
		{
			$response['status']=0;
			$response['error_code']=401;
			$response['error']=__("An Unauthorized User","gym_mgt");
			$response['result']='';
			return $response;
		}
		return $response;
	}
}
?>