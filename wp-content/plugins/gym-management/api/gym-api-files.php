<?php 
require_once GMS_PLUGIN_DIR. "/api/class/gymApi.class.php";
require_once GMS_PLUGIN_DIR. "/api/class/access_right.php";
require_once GMS_PLUGIN_DIR. "/api/class/gym-login.php";
require_once GMS_PLUGIN_DIR. "/api/class/member.php";
require_once GMS_PLUGIN_DIR. "/api/class/membership.php";
require_once GMS_PLUGIN_DIR. "/api/class/group.php";
require_once GMS_PLUGIN_DIR. "/api/class/staff_member.php";
require_once GMS_PLUGIN_DIR. "/api/class/class_schedule.php";
require_once GMS_PLUGIN_DIR. "/api/class/activity.php";
require_once GMS_PLUGIN_DIR. "/api/class/product.php";
require_once GMS_PLUGIN_DIR. "/api/class/assign_workout.php";
require_once GMS_PLUGIN_DIR. "/api/class/workout_log.php";
require_once GMS_PLUGIN_DIR. "/api/class/accountant.php";
require_once GMS_PLUGIN_DIR. "/api/class/sales.php";
require_once GMS_PLUGIN_DIR. "/api/class/reservation.php";
require_once GMS_PLUGIN_DIR. "/api/class/tax.php";
require_once GMS_PLUGIN_DIR. "/api/class/attendance.php";
require_once GMS_PLUGIN_DIR. "/api/class/membership_payment.php";
require_once GMS_PLUGIN_DIR. "/api/class/payment.php";
require_once GMS_PLUGIN_DIR. "/api/class/message.php";
require_once GMS_PLUGIN_DIR. "/api/class/notice.php";
require_once GMS_PLUGIN_DIR. "/api/class/dashboard.php";
require_once GMS_PLUGIN_DIR. "/api/class/comman.php";
require_once GMS_PLUGIN_DIR. "/api/class/nutrition.php";
require_once GMS_PLUGIN_DIR. "/api/class/subscription_history.php";

$gym= new GymApi();
$gym->load();	
?>