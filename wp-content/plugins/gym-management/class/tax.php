<?php 	
//TAX CLASS START  
class MJgmgt_tax
{		
	//ADD TAX DATA
	public function MJgmgt_add_taxes($data)
	{		
		global $wpdb;
		$table_gmgt_taxes=$wpdb->prefix .'MJgmgt_gmgt_taxes';
		$taxdata['tax_title']=MJgmgt_strip_tags_and_stripslashes(sanitize_text_field($data['tax_title']));
		$taxdata['tax_value']=sanitize_text_field($data['tax_value']);
		$taxdata['created_date']=date("Y-m-d");	
		if($data['action']=='edit')
		{	
			$taxid['tax_id']=sanitize_text_field($data['tax_id']);
			$result=$wpdb->update( $table_gmgt_taxes, $taxdata ,$taxid);		
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_gmgt_taxes,$taxdata);					
			return $result;		
		}
	}
	//get all taxes
	public function MJgmgt_get_all_taxes()
	{
		global $wpdb;
		$table_gmgt_taxes=$wpdb->prefix .'MJgmgt_gmgt_taxes';	
		$result = $wpdb->get_results("SELECT * FROM $table_gmgt_taxes");
		return $result;	
	}	
	//delete taxes
	public function MJgmgt_delete_taxes($id)
	{
		global $wpdb;
		$table_gmgt_taxes=$wpdb->prefix .'MJgmgt_gmgt_taxes';
		$result = $wpdb->query("DELETE FROM $table_gmgt_taxes where tax_id= ".$id);
		return $result;
	}
	//get single tax data
	public function MJgmgt_get_single_tax_data($tax_id)
	{
		global $wpdb;
		$table_gmgt_taxes=$wpdb->prefix .'MJgmgt_gmgt_taxes';
		$result = $wpdb->get_row("SELECT * FROM $table_gmgt_taxes where tax_id= ".$tax_id);
		return $result;
	}
}
//TAX CLASS END  
?>