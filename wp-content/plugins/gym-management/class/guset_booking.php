<?php 	
//TAX CLASS START  
class MJgmgt_guest_booking
{		
	//ADD TAX DATA
	public function MJgmgt_add_guest_booking($data)
	{		
		global $wpdb;
		$table_guest_booking=$wpdb->prefix .'gmgt_guest_booking';
		$bookingdata['first_name']=MJgmgt_strip_tags_and_stripslashes(sanitize_text_field($data['firstname']));
		$bookingdata['last_name']=MJgmgt_strip_tags_and_stripslashes(sanitize_text_field($data['lastname']));
		$bookingdata['email_id']=sanitize_text_field($data['email']);
		$bookingdata['phone_number']=sanitize_text_field($data['phonenumber']);
		$bookingdata['created_date']=date("Y-m-d");	
		
		if($data['action']=='edit')
		{	
			$guestid['guest_id']=sanitize_text_field($data['guest_id']);
			$result=$wpdb->update( $table_guest_booking, $bookingdata ,$guestid);		
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_guest_booking,$bookingdata);					
			return $result;		
		}
	}
	//get all taxes
	public function MJgmgt_get_all_guest_booking()
	{
		global $wpdb;
		$table_guest_booking=$wpdb->prefix .'gmgt_guest_booking';	
		$result = $wpdb->get_results("SELECT * FROM $table_guest_booking");
		return $result;	
	}	
	//delete taxes
	public function MJgmgt_delete_guest_booking($id)
	{
		global $wpdb;
		$table_guest_booking=$wpdb->prefix .'gmgt_guest_booking';
		$result = $wpdb->query("DELETE FROM $table_guest_booking where guest_id=".$id);
		return $result;
	}
	//get single tax data
	public function MJgmgt_get_single_guest_booking_data($guest_id)
	{
		global $wpdb;
		$table_guest_booking=$wpdb->prefix .'gmgt_guest_booking';
		$result = $wpdb->get_row("SELECT * FROM $table_guest_booking where guest_id= ".$guest_id);
		return $result;
	}
}
//TAX CLASS END  
?>