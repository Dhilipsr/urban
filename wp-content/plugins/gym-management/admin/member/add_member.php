<?php $role="member"; ?>
<script type="text/javascript">
    jQuery(document).ready(function($)
	{
		"use strict";
		$('#member_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});	
		$('#add_staff_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1})
		$('#membership_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1})
		$('#class_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});
		$("#group_form").validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});	
		$('#group_id').multiselect(
		{
			nonSelectedText :'<?php esc_html_e('Select Group','gym_mgt');?>',
			includeSelectAllOption: true,
			allSelectedText : '<?php esc_html_e('All selected','gym_mgt'); ?>',
			selectAllText : '<?php esc_html_e('Select all','gym_mgt'); ?>'
		});
		$('.classis_ids').multiselect(
		{
			nonSelectedText :'<?php esc_html_e('Select Class','gym_mgt');?>',
			includeSelectAllOption: true,
			allSelectedText : '<?php esc_html_e('All selected','gym_mgt'); ?>',
			selectAllText : '<?php esc_html_e('Select all','gym_mgt'); ?>'
		});
		$('#specialization').multiselect(
		{
			nonSelectedText :'<?php esc_html_e('Select Specialization','gym_mgt');?>',
			includeSelectAllOption: true,
			allSelectedText : '<?php esc_html_e('All selected','gym_mgt'); ?>',
			selectAllText : '<?php esc_html_e('Select all','gym_mgt'); ?>'
		});
		$('#day').multiselect(
		{
			nonSelectedText :'<?php esc_html_e('Select Day','gym_mgt');?>',
			includeSelectAllOption: true,
			allSelectedText : '<?php esc_html_e('All selected','gym_mgt'); ?>',
			selectAllText : '<?php esc_html_e('Select all','gym_mgt'); ?>'
		});
		$('#activity_id').multiselect(
		{
			nonSelectedText :'<?php esc_html_e('Select Activity','gym_mgt');?>',
			includeSelectAllOption: true,
			allSelectedText : '<?php esc_html_e('All selected','gym_mgt'); ?>',
			selectAllText : '<?php esc_html_e('Select all','gym_mgt'); ?>',
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			filterPlaceholder: '<?php esc_html_e('Search for activity...','gym_mgt');?>'
		}); 
		$('#activity_category').multiselect(
		{
			nonSelectedText :'<?php esc_html_e('Select Activity Category','gym_mgt');?>',
			includeSelectAllOption: true,
			allSelectedText : '<?php esc_html_e('All selected','gym_mgt'); ?>',
			selectAllText : '<?php esc_html_e('Select all','gym_mgt'); ?>',
			enableFiltering: true,
			allowClear: true,
			enableCaseInsensitiveFiltering: true,
			filterPlaceholder: '<?php esc_html_e('Search for activity category...','gym_mgt');?>'
		}); 
		$('#class_membership_id').multiselect(
		{
			nonSelectedText :'<?php esc_html_e('Select Membership','gym_mgt');?>',
			includeSelectAllOption: true,
			allSelectedText : '<?php esc_html_e('All selected','gym_mgt'); ?>',
			selectAllText : '<?php esc_html_e('Select all','gym_mgt'); ?>'
		});
		$('.tax_charge').multiselect({
			nonSelectedText :'<?php esc_html_e('Select Tax','gym_mgt'); ?>',
			includeSelectAllOption: true,
			allSelectedText : '<?php esc_html_e('All selected','gym_mgt'); ?>',
			selectAllText : '<?php esc_html_e('Select all','gym_mgt'); ?>'
		 });
		$(".specialization_submit").on('click',function()
		{	
			checked = $(".multiselect_validation_specialization  .dropdown-menu input:checked").length;
			if(!checked)
			{
			  	alert("<?php esc_html_e('Please select atleast one specialization','gym_mgt');?>");
			  	return false;
			}
		});
		$(".class_submit").on('click',function()
		{
			checked = $(".multiselect_validation_member .dropdown-menu input:checked").length;
			if(!checked)
			{
			  	alert("<?php esc_html_e('Please select atleast one class','gym_mgt');?>");
			  	return false;
			}
		});
		$(".day_validation_submit").on('click',function()
		{
			checked = $(".day_validation_member .dropdown-menu input:checked").length;
			if(!checked)
			{
			  	alert("<?php esc_html_e('Please select atleast One Day','gym_mgt');?>");
			  	return false;
			}
		});
		$(".day_validation_submit").on('click',function()
		{
			checked = $(".multiselect_validation_membership .dropdown-menu input:checked").length;
			if(!checked)
			{
			  	alert("<?php esc_html_e('Please select Atleast One membership.','gym_mgt');?>");
			  	return false;
			}
		});
		jQuery('#birth_date').datepicker(
		{
			dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
			maxDate : 0,
			changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+25',
			beforeShow: function (textbox, instance) 
			{
				instance.dpDiv.css(
				{
					marginTop: (-textbox.offsetHeight) + 'px'                   
				});
			},    
	        onChangeMonthYear: function(year, month, inst) 
	        {
	            jQuery(this).val(month + "/" + year);
	        }                    
		});
		jQuery('.birth_date').datepicker(
		{
			dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
			maxDate : 0,
			changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+25',
			beforeShow: function (textbox, instance) 
			{
				instance.dpDiv.css(
				{
					marginTop: (-textbox.offsetHeight) + 'px'                   
				});
			},    
	        onChangeMonthYear: function(year, month, inst) 
	        {
	            jQuery(this).val(month + "/" + year);
	        }                    
		});
		var date = new Date();
		date.setDate(date.getDate()-0);
		$('#inqiury_date').datepicker({	
			<?php
			if(get_option('gym_enable_datepicker_privious_date')=='no')
			{
			?>
				startDate: date,
				minDate:'today',
			<?php
			}
			?>
			dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
			autoclose: true
	   	});
		var date = new Date();
		date.setDate(date.getDate()-0);
		 $('#triel_date').datepicker({
			<?php
			if(get_option('gym_enable_datepicker_privious_date')=='no')
			{
			?>
				minDate:'today',
				startDate: date,
			<?php
			}
			?>	
		dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
		autoclose: true
	   	});
	   	var date = new Date();
		date.setDate(date.getDate()-0);
		$('#begin_date').datepicker({
			<?php
			if(get_option('gym_enable_datepicker_privious_date')=='no')
			{
			?>
				minDate:'today',
				startDate: date,
			<?php
			}
			?>	
		dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
		autoclose: true
	   	});
		var date = new Date();
		date.setDate(date.getDate()-0);
		$('#first_payment_date').datepicker({
		  	dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
			<?php
			if(get_option('gym_enable_datepicker_privious_date')=='no')
			{
			?>
				startDate: date,
				minDate:'today',
			<?php
			}
			?>
		autoclose: true
	   	});
		//------ADD STAFF MEMBER AJAX----------
		$('#add_staff_form').on('submit', function(e) 
		{
			e.preventDefault();
			var form = $(this).serialize();
			var valid = $('#add_staff_form').validationEngine('validate');
			if (valid == true) 
			{				
				$.ajax(
				{
					type:"POST",
					url: $(this).attr('action'),
					data:form,
					success: function(data)
					{					
						if(data!='0')
						{ 
							if(data!="")
							{ 
								$('#add_staff_form').trigger("reset");
								$('#staff_id').append(data);
								$('#reference_id').append(data);
								$('.upload_user_avatar_preview').html('<img class="image_preview_css" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">');
								$('.gmgt_user_avatar_url').val('');
							}
							$('.modal').modal('hide');
							$('.show_msg').css('display','none');
						}
						else
						{				
							$('.show_msg').css('display','block');
						}		
					},
					error: function(data){
					}
				})
			}
		});
		//------ADD GROUP AJAX----------
		$('#group_form').on('submit', function(e)
		{
			e.preventDefault();
			var form = $(this).serialize();
			var valid = $("#group_form").validationEngine('validate');
			if (valid == true)
			{
				$('.modal').modal('hide');
			}
			var categCheck_group = $('#group_id').multiselect();	
			$.ajax(
			{
				type:"POST",
				url: $(this).attr('action'),
				data:form,
				success: function(data)
				{
					if(data!="")
					{ 
						$('#group_form').trigger("reset");
						$('#group_id').append(data);
						categCheck_group.multiselect('rebuild');	
					}
				},
				error: function(data)
				{
				}
			})
		});
		//------ADD MEMBERSHIP AJAX----------
		$('#membership_form').on('submit', function(e)
		{
			e.preventDefault();
			var form = $(this).serialize();
			var valid = $('#membership_form').validationEngine('validate');
			if (valid == true)
			{				
				$.ajax(
				{
					type:"POST",
					url: $(this).attr('action'),
					data:form,
					success: function(data)
					{
						if(data!='0')
						{
							if(data!="")
							{
								$('#membership_form').trigger("reset");
								$('#membership_id').append(data);
							}
							$('.modal').modal('hide');
							$('.show_msg').css('display','none');
						}
						else
						{				
							$('.show_msg').css('display','block');
						}	
					},
					error: function(data)
					{
					}
				})
			}
		});
		//------ADD CLASS AJAX----------
		$('#class_form').on('submit',function(e)
		{
			e.preventDefault();
			var form = $(this).serialize();
			var categCheck_class = $('#classis_id').multiselect();	
			var categCheck_day = $('#day').multiselect();	
			var categCheck_class_membership = $('#class_membership_id').multiselect();	
			var valid = $('#class_form').validationEngine('validate');
			if (valid == true)
			{			
				$.ajax(
				{
					type:"POST",
					url: $(this).attr('action'),
					data:form,
					success: function(data)
					{	
						if(data=="1")
						{ 
							alert("<?php esc_html_e('End Time should be greater than Start Time','gym_mgt'); ?>");
							return false;
						}
						else
						{
							$('#class_form').trigger("reset");
							$('#classis_id').append(data);
							categCheck_class.multiselect('rebuild');	
							categCheck_day.multiselect('rebuild');	
							categCheck_class_membership.multiselect('rebuild');	
							$('.modal').modal('hide');
						}
					},
					error: function(data)
					{
					}
				})
			}
		});
    });
</script>
<?php 	
if($active_tab == 'addmember')
{
  	$member_id=0;
	$edit=0;
	if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
	{
		$member_id=esc_attr($_REQUEST['memberid']);
		$edit=1;
		$user_info = get_userdata($member_id);
		if($user_info->gmgt_hash)
		{
			$lastmember_id=MJgmgt_get_lastmember_id($role);
			$nodate=substr($lastmember_id,0,-4);
			$memberno=substr($nodate,1);
			$add="1";
			$test=(int)$memberno+(int)$add;
			//$memberno+=1;
			$newmember='M'.$test.date("my");
		}
	}
	else
	{
	    $lastmember_id=MJgmgt_get_lastmember_id($role);
		$nodate=substr($lastmember_id,0,-4);
		$memberno=substr($nodate,1);
		//$add=1;
		//$test=(int)$memberno + $add;
		$test=(int)$memberno+1;
		$newmember='M'.$test.date("my");
	}?>
    <div class="panel-body"><!-- PAGE INNNER DIV START-->
		<form name="member_form" action="" method="post" class="form-horizontal" id="member_form"><!-- MEMBER FROM START-->
			<?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
			<input type="hidden" name="action" value="<?php echo esc_attr($action);?>">
			<input type="hidden" name="role" value="<?php echo esc_attr($role);?>"  />
			<input type="hidden" name="user_id" value="<?php echo esc_attr($member_id);?>"  />
			<input type="hidden" name="gmgt_hash" value="<?php if($edit){ if($user_info->gmgt_hash){ echo esc_attr($user_info->gmgt_hash);}}?>" />
			<div class="header col-sm-12">	
				<h3><?php esc_html_e('Personal Information','gym_mgt');?></h3>
			</div>
			<div class="col-sm-6 padding_left_right_0">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="member_id"><?php esc_html_e('Member Id','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="member_id" class="form-control" type="text" value="<?php if($edit){  echo esc_attr($user_info->member_id);}else echo esc_attr($newmember);?>"  readonly name="member_id" tabindex="1">
					</div>
				</div>
				<!--nonce-->
				<?php wp_nonce_field( 'save_member_nonce' ); ?>
				<!--nonce-->
				<div class="form-group">
					<label class="col-sm-4 control-label" for="first_name"><?php esc_html_e('First Name','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-7">
						<input id="first_name" class="form-control validate[required,custom[onlyLetter_specialcharacter]] text-input" maxlength="50" type="text" value="<?php if($edit){ echo esc_attr($user_info->first_name);}elseif(isset($_POST['first_name'])) echo esc_attr($_POST['first_name']);?>" name="first_name"  tabindex="2">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="middle_name"><?php esc_html_e('Middle Name','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="middle_name" class="form-control validate[custom[onlyLetter_specialcharacter] " type="text" maxlength="50"  value="<?php if($edit){ echo esc_attr($user_info->middle_name);}elseif(isset($_POST['middle_name'])) echo esc_attr($_POST['middle_name']);?>" name="middle_name"  tabindex="3">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="last_name"><?php esc_html_e('Last Name','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-7">
						<input id="last_name" class="form-control validate[required,custom[onlyLetter_specialcharacter]] text-input" maxlength="50" type="text" value="<?php if($edit){ echo esc_attr($user_info->last_name);}elseif(isset($_POST['last_name'])) echo esc_attr($_POST['last_name']);?>" name="last_name" tabindex="4">
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">					
				<div class="form-group" >
					<label class="col-sm-4 control-label" for="birth_date"><?php esc_html_e('Date of birth','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-7">
						<input id="birth_date" class="form-control validate[required] birth_date" type="text"  name="birth_date" value="<?php if($edit){ echo esc_attr(MJgmgt_getdate_in_input_box($user_info->birth_date));}elseif(isset($_POST['birth_date'])) echo esc_attr(MJgmgt_getdate_in_input_box($_POST['birth_date']));?>" readonly  tabindex="5">
					</div>
				</div>
				<div class="form-group">	
					<label class="col-sm-4 control-label" for="gender"><?php esc_html_e('Gender','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-7">
						<?php $genderval = "male"; if($edit){ $genderval=$user_info->gender; }elseif(isset($_POST['gender'])) {$genderval=sanitize_text_field($_POST['gender']);}?>
						<label class="radio-inline">
						 	<input type="radio" value="male" class="tog validate[required]" name="gender"  <?php checked('male',$genderval); ?> tabindex="6" /><?php esc_html_e('Male','gym_mgt');?>
						</label>
						<label class="radio-inline">
						  	<input type="radio" value="female" class="tog validate[required]" name="gender" <?php checked('female',$genderval); ?>/><?php esc_html_e('Female','gym_mgt');?>
						</label>
					</div>
				</div>	
				<div class="form-group">	
					<label class="col-sm-4 control-label" for="group_id"><?php esc_html_e('Group','gym_mgt');?></label>
					<div class="col-sm-7">
						<?php 
						$joingroup_list = $obj_member->MJgmgt_get_all_joingroup($member_id);
						$groups_array = $obj_member->MJgmgt_convert_grouparray($joingroup_list);
						?>
						<?php if($edit){ $group_id=$user_info->group_id; }elseif(isset($_POST['group_id'])){$group_id=sanitize_text_field($_POST['group_id']);}else{$group_id='';}?>
						<select id="group_id"  name="group_id[]" multiple="multiple" tabindex="7">
						<?php $groupdata=$obj_group->MJgmgt_get_all_groups();
						if(!empty($groupdata))
						{
							foreach ($groupdata as $group){?>
								<option value="<?php echo esc_attr($group->id);?>" <?php if(in_array($group->id,$groups_array)) echo 'selected';  ?>><?php echo esc_html($group->group_name); ?></option>
						<?php } } ?>
						</select>
						<a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal_add_group" tabindex="8"> <?php esc_html_e('Add','gym_mgt');?></a>	
					</div>						
				</div>	
			</div>						
			<div class="header col-sm-12">	
				<hr>
				<h3><?php esc_html_e('Contact Information','gym_mgt');?></h3>
			</div>
			<div class="col-sm-6 padding_left_right_0">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="address"><?php esc_html_e('Address','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-7">
						<input id="address" class="form-control validate[required,custom[address_description_validation]]" maxlength="150" type="text"  name="address" value="<?php if($edit){ echo esc_attr($user_info->address);}elseif(isset($_POST['address'])) echo esc_attr($_POST['address']);?>" tabindex="9">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="city_name"><?php esc_html_e('City','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-7">
						<input id="city_name" class="form-control validate[required,custom[city_state_country_validation]]" maxlength="50" type="text"  name="city_name" value="<?php if($edit){ echo esc_attr($user_info->city_name);}elseif(isset($_POST['city_name'])) echo esc_attr($_POST['city_name']);?>" tabindex="10">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="state_name"><?php esc_html_e('State','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="state_name" class="form-control validate[custom[city_state_country_validation]]" maxlength="50" type="text" name="state_name" value="<?php if($edit){ echo esc_attr($user_info->state_name);}elseif(isset($_POST['state_name'])) echo esc_attr($_POST['state_name']);?>" tabindex="11">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="zip_code"><?php esc_html_e('Zip Code','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-7">
						<input id="zip_code" class="form-control  validate[required,custom[onlyLetterNumber]]" maxlength="15" type="text" name="zip_code" value="<?php if($edit){ echo esc_attr($user_info->zip_code);}elseif(isset($_POST['zip_code'])) echo esc_attr($_POST['zip_code']);?>" tabindex="12">
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">
				<div class="form-group">
					<label class="col-sm-4 col-xs-12 control-label " for="mobile"><?php esc_html_e('Mobile Number','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-2 col-xs-4">						
						<input type="text" readonly value="+<?php echo MJgmgt_get_countery_phonecode(get_option( 'gmgt_contry' ));?>"  class="form-control" name="phonecode">
					</div>
					<div class="col-sm-5 col-xs-8">
						<input id="mobile" class="form-control validate[required,custom[phone_number]] text-input phone_validation" type="text" minlength="6" name="mobile" maxlength="15" value="<?php if($edit){ echo esc_attr($user_info->mobile);}elseif(isset($_POST['mobile'])) echo esc_attr($_POST['mobile']);?>" tabindex="13">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label " for="phone"><?php esc_html_e('Phone','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="phone" class="form-control text-input phone_validation validate[custom[phone_number]]" type="text" minlength="6" maxlength="15" name="phone" value="<?php if($edit){ echo esc_attr($user_info->phone);}elseif(isset($_POST['phone'])) echo esc_attr($_POST['phone']);?>" tabindex="14">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label " for="email"><?php esc_html_e('Email','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-7">
						<input type="hidden" name="hidden_email" value="<?php if($edit){ echo esc_attr($user_info->user_email); } ?>">
						<input id="email" class="form-control validate[required,custom[email]] text-input" maxlength="100" type="text" name="email" value="<?php if($edit){ echo esc_attr($user_info->user_email);}elseif(isset($_POST['email'])) echo esc_attr($_POST['email']);?>" tabindex="15">
					</div>
				</div>
			</div>
			<div class="header col-sm-12">	
				<hr>
				<h3><?php esc_html_e('Physical Information','gym_mgt');?></h3>
			</div>
			<div class="col-sm-6 padding_left_right_0">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="weight"><?php esc_html_e('Weight','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="weight" class="form-control text-input decimal_number" type="number" min="0" onkeypress="if(this.value.length==6) return false;" step="0.01" value="<?php if($edit){ echo esc_attr($user_info->weight);}elseif(isset($_POST['weight'])) echo esc_attr($_POST['weight']);?>" name="weight" placeholder="<?php echo esc_attr(get_option( 'gmgt_weight_unit' ));?>" tabindex="16">		
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="height"><?php esc_html_e('Height','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="height" class="form-control text-input decimal_number"type="number" min="0" onkeypress="if(this.value.length==6) return false;" step="0.01" value="<?php if($edit){ echo esc_attr($user_info->height);}elseif(isset($_POST['height'])) echo esc_attr($_POST['height']);?>" name="height" placeholder="<?php echo esc_attr(get_option( 'gmgt_height_unit' ));?>" tabindex="17">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="Chest"><?php esc_html_e('Chest','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="Chest" class="form-control text-input decimal_number" type="number" min="0" onkeypress="if(this.value.length==6) return false;" step="0.01" value="<?php if($edit){ echo esc_attr($user_info->chest);}elseif(isset($_POST['chest'])) echo esc_attr($_POST['chest']);?>" name="chest" placeholder="<?php echo esc_attr(get_option( 'gmgt_chest_unit' ));?>" tabindex="18">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="Waist"><?php esc_html_e('Waist','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="waist" class="form-control text-input decimal_number" type="number" min="0" onkeypress="if(this.value.length==6) return false;" step="0.01" value="<?php if($edit){ echo esc_attr($user_info->waist);}elseif(isset($_POST['waist'])) echo esc_attr($_POST['waist']);?>" name="waist" placeholder="<?php echo esc_attr(get_option( 'gmgt_waist_unit' ));?>" tabindex="19">
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="thigh"><?php esc_html_e('Thigh','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="thigh" class="form-control text-input decimal_number" type="number" min="0" onkeypress="if(this.value.length==6) return false;" step="0.01" value="<?php if($edit){ echo esc_attr($user_info->thigh);}elseif(isset($_POST['thigh'])) echo esc_attr($_POST['thigh']);?>" name="thigh" placeholder="<?php echo esc_attr(get_option( 'gmgt_thigh_unit' ));?>" tabindex="20">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="arms"><?php esc_html_e('Arms','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="arms" class="form-control text-input decimal_number" type="number" min="0" onkeypress="if(this.value.length==6) return false;" step="0.01" value="<?php if($edit){ echo esc_attr($user_info->arms);}elseif(isset($_POST['arms'])) echo esc_attr($_POST['arms']);?>" name="arms" placeholder="<?php echo esc_attr(get_option( 'gmgt_arms_unit' ));?>" tabindex="21">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="fat"><?php esc_html_e('Fat','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="fat" class="form-control text-input decimal_number" type="number" min="0" max="100" onkeypress="if(this.value.length==6) return false;" step="0.01" value="<?php if($edit){ echo esc_attr($user_info->fat);}elseif(isset($_POST['fat'])) echo esc_attr($_POST['fat']);?>" name="fat" placeholder="<?php echo esc_attr(get_option( 'gmgt_fat_unit' ));?>" tabindex="22">
					</div>
				</div>
			</div>
			<div class="header col-sm-12">
				<hr>
				<h3><?php esc_html_e('Login Information','gym_mgt');?></h3>
			</div>
			<div class="col-sm-6 padding_left_right_0">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="username"><?php esc_html_e('User Name','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-7">
						<input id="username" class="form-control validate[required,custom[username_validation]] space_validation" type="text" maxlength="50"  name="username" value="<?php if($edit){ echo esc_attr($user_info->user_login);}elseif(isset($_POST['username'])) echo esc_attr($_POST['username']);?>" <?php if($edit) echo "readonly";?> tabindex="23">
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="password"><?php esc_html_e('Password','gym_mgt');?><?php if(!$edit) {?><span class="require-field">*</span><?php }?></label>
					<div class="col-sm-7">
						<input id="password" class="form-control space_validation <?php if(!$edit) echo esc_attr('validate[required]');?>" minlength="8" maxlength="12" type="password"  name="password" value="" tabindex="24">
					</div>
				</div>
			</div>	
			<div class="col-sm-6 padding_left_right_0">	
				<div class="form-group">
					<label class="col-sm-4 control-label" for="photo"><?php esc_html_e('Image','gym_mgt');?></label>
					<div class="col-sm-4">
						<input type="text" id="gmgt_user_avatar_url" class="form-control" name="gmgt_user_avatar" readonly value="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar );elseif(isset($_POST['gmgt_user_avatar'])) echo esc_url($_POST['gmgt_user_avatar']); ?>" />
					</div>
					<div class="col-sm-3">
						<input id="upload_user_avatar_button" type="button" class="button" value="<?php esc_html_e( 'Upload image', 'gym_mgt' ); ?>" tabindex="25" />
					</div>
					<div class="clearfix"></div>						
					<div class="col-sm-offset-4 col-sm-7">
						<div id="upload_user_avatar_preview"  >
						<?php if($edit) 
						{
							if($user_info->gmgt_user_avatar == "")
							{
								?>
								<img class="image_preview_css" src="<?php echo esc_url(get_option( 'gmgt_system_logo' )); ?>">
								<?php
								}
								else
								{
									?>
								<img class="image_preview_css"  src="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar ); ?>" />
								<?php 
								}
							}
							else 
							{
								?>
								<img class="image_preview_css" src="<?php echo esc_url(get_option( 'gmgt_system_logo' )); ?>">
								<?php 
							}
							?>
						</div>
				 	</div>
				</div>
			</div>
			<div class="header col-sm-12">	
				<hr>
				<h3><?php esc_html_e('More Information','gym_mgt');?></h3>
			</div>
			<div class="col-sm-6 padding_left_right_0">		
				<div class="form-group">
					<label class="col-sm-4 control-label" for="refered"><?php esc_html_e('Member type','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-7">
						<select name="member_type" class="form-control validate[required]" id="member_type" tabindex="26">
							<option value=""><?php esc_html_e('Select Member Type','gym_mgt');?></option>
							<?php 
							if($edit)
							{
								$mtype=$user_info->member_type;
							}
							elseif(isset($_POST['member_type']))
							{
								$mtype=sanitize_text_field($_POST['member_type']);
							}
							else
							{
								$mtype="";
							}
							$membertype_array=MJgmgt_member_type_array();
							if(!empty($membertype_array))
							{
								foreach($membertype_array as $key=>$type)
								{
									echo '<option value='.esc_attr($key).' '.selected(esc_attr($mtype),esc_attr($key)).'>'.esc_html($type).'</option>';
								}
							} ?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">	
				<div class="form-group">
					<label class="col-sm-4 control-label" for="staff_name"><?php esc_html_e('Select Staff Member','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-5">
						<?php $get_staff = array('role' => 'Staff_member');
							$staffdata=get_users($get_staff);
						?>
						<select name="staff_id" class="form-control validate[required] " id="staff_id" tabindex="27">
							<option value=""><?php  esc_html_e('Select Staff Member','gym_mgt');?></option>
							<?php 
							if($edit)
							{
								$staff_data=$user_info->staff_id;
							}
							elseif(isset($_POST['staff_id']))
							{
								$staff_data=sanitize_text_field($_POST['staff_id']);
							}
							else
							{
								$staff_data="";
							}
							if(!empty($staffdata))
							{
								foreach($staffdata as $staff)
								{
									
									echo '<option value='.esc_attr($staff->ID).' '.selected(esc_html($staff_data),$staff->ID).'>'.esc_html($staff->display_name).'</option>';
								}
							}
							?>
						</select>
					</div>
					<div class="col-sm-2 margin_top_5">					
						<a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal_add_staff_member" tabindex="28"> <?php esc_html_e('Add','gym_mgt');?></a>
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">	
				<div class="form-group">
					<label class="col-sm-4 control-label" for="intrest"><?php esc_html_e('Interest Area','gym_mgt');?></label>
					<div class="col-sm-4">
						<select class="form-control" name="intrest_area" id="intrest_area" tabindex="29">
							<option value=""><?php esc_html_e('Select Interest','gym_mgt');?></option>
							<?php
							if(isset($_REQUEST['intrest']))
							{
								$category =esc_attr($_REQUEST['intrest']);  
							}
							elseif($edit)
							{
								$category =$user_info->intrest_area;
							}
							else
							{ 
								$category = "";
							}
							$role_type=MJgmgt_get_all_category('intrest_area');
							if(!empty($role_type))
							{
								foreach ($role_type as $retrive_data)
								{
									echo '<option value="'.esc_attr($retrive_data->ID).'" '.selected(esc_attr($category),esc_attr($retrive_data->ID)).'>'.esc_html($retrive_data->post_title).'</option>';
								}
							}
						   ?>
						</select>
					</div>
					<div class="col-sm-3 add_category_padding_0">
						<button id="addremove" model="intrest_area" tabindex="30"><?php esc_html_e('Add Or Remove','gym_mgt');?></button>
					</div>
				</div>
			</div>				
			<?php 
			if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
			{
			?>
			<div class="row">	
				<div class="col-sm-6 padding_left_right_0">		
					<div class="form-group">
						<label class="col-sm-4 control-label" for="member_convert"><?php esc_html_e(' Convert into Staff Member','gym_mgt');?></label>
						<div class="col-sm-7">
							<input type="checkbox" class="member_convert" name="member_convert" value="staff_member">
						</div>
					</div>
				</div>	
			</div>	
			<?php 
			}
			?>
			<div class="col-sm-6 padding_left_right_0">	
				<div class="form-group">
					<label class="col-sm-4 control-label" for="Source"><?php esc_html_e('Referral Source','gym_mgt');?></label>
					<div class="col-sm-4">
						<select class="form-control reffer_source_font" name="source" id="source" tabindex="31">
							<option value=""><?php esc_html_e('Select Referral Source','gym_mgt');?></option>
							<?php 								
							if(isset($_REQUEST['source']))
							{
								$category =esc_attr($_REQUEST['source']);  
							}
							elseif($edit)
							{
								$category =$user_info->source;
							}
							else
							{
								$category = "";
							}
							$role_type=MJgmgt_get_all_category('source');
							if(!empty($role_type))
							{
								foreach ($role_type as $retrive_data)
								{
									echo '<option value="'.esc_attr($retrive_data->ID).'" '.selected(esc_attr($category),esc_attr($retrive_data->ID)).'>'.esc_attr($retrive_data->post_title).'</option>';
								}
							}
							?>
						</select>
					</div>
					<div class="col-sm-3 add_category_padding_0">
						<button id="addremove" model="source" tabindex="32"><?php esc_html_e('Add Or Remove','gym_mgt');?></button>
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">	
				<div class="form-group">
					<label class="col-sm-4 control-label" for="refered"><?php esc_html_e('Referred By','gym_mgt');?></label>
					<div class="col-sm-5">
						<?php 
							$staffdata=get_users([ 'role__in' => ['Staff_member', 'member']]);
						?>
						<select name="reference_id" class="form-control" id="reference_id" tabindex="33">
							<option value=""><?php esc_html_e('Select Referred Member','gym_mgt');?></option>
							<?php 
							if($edit)
							{
								$staff_data=$user_info->reference_id;
							}
							elseif(isset($_POST['reference_id']))
							{
								$staff_data=sanitize_text_field($_POST['reference_id']);
							}
							else
							{
								$staff_data="";					
							}
							if(!empty($staffdata))
							{
								foreach($staffdata as $staff)
								{						
									echo '<option value='.esc_attr($staff->ID).' '.selected(esc_attr($staff_data),esc_attr($staff->ID)).'>'.esc_html($staff->display_name).'</option>';
								}
							}
							?>
						</select>
					</div>
					<div class="col-sm-2 margin_top_5">					
						<a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal_add_staff_member" tabindex="34"> <?php esc_html_e('Add','gym_mgt');?></a>
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">	
				<div class="form-group">
					<label class="col-sm-4 control-label" for="inqiury_date"><?php esc_html_e('Inquiry Date','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="inqiury_date" class="form-control" type="text"  name="inqiury_date" value="<?php if($edit){ if($user_info->inqiury_date!=""){ echo esc_attr(MJgmgt_getdate_in_input_box($user_info->inqiury_date)); } }elseif(isset($_POST['inqiury_date'])){ echo esc_attr($_POST['inqiury_date']); }?>" tabindex="35" readonly>
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">	
				<div class="form-group">
					<label class="col-sm-4 control-label" for="triel_date"><?php esc_html_e('Trial End Date','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="triel_date" class="form-control" type="text"  name="triel_date" value="<?php if($edit){ if($user_info->triel_date!=""){ echo esc_attr(MJgmgt_getdate_in_input_box($user_info->triel_date)); } }elseif(isset($_POST['triel_date'])){ echo esc_attr($_POST['triel_date']); }?>" tabindex="36" readonly>
					</div>
				</div>
			</div>
			<div id="non_prospect_area">
				<div class="col-sm-6 padding_left_right_0">
					<div class="form-group">
						<label class="col-sm-4 control-label" for="membership"><?php esc_html_e('Membership','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-5">							
							<input type="hidden" name="membership_hidden" class="membership_hidden" value="<?php if($edit){ if(!empty($user_info->membership_id)) { echo esc_attr($user_info->membership_id); }else{ echo '0'; } }else{ echo '0';}?>">
							<?php $membershipdata=$obj_membership->MJgmgt_get_all_membership(); ?>
							<select name="membership_id" class="form-control validate[required]" id="membership_id" tabindex="37">	
								<option value=""><?php esc_html_e('Select Membership','gym_mgt');?></option>
									<?php 
									$staff_data=$user_info->membership_id;
									if(!empty($membershipdata))
									{
										foreach ($membershipdata as $membership)
										{						
											echo '<option value='.esc_attr($membership->membership_id).' '.selected(esc_attr($staff_data),esc_attr($membership->membership_id)).'>'.esc_html($membership->membership_label).'</option>';
										}
									}
									?>
							</select>
						</div>
						<div class="col-sm-2 margin_top_5">						
							<a href="#" class="btn btn-default" data-toggle="modal" id="add_membership_btn" data-target="#myModal_add_membership" tabindex="38"> <?php esc_html_e('Add','gym_mgt');?></a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 padding_left_right_0" <?php if(!$edit){ ?> class="clear_both" <?php } ?>>
					<div class="form-group">
						<label class="col-sm-4 col-xs-12 control-label" for="class_id"><?php esc_html_e('Class','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-5 col-xs-8 multiselect_validation_member">
							<select id="classis_id" class="form-control validate[required] classis_ids" multiple="multiple" name="class_id[]" tabindex="39">
									<?php 
									if($edit)					
									{	
										$obj_class=new MJgmgt_classschedule;
										$MemberShipClass = MJgmgt_get_class_id_by_membership_id($user_info->membership_id);
										$userclass  = MJgmgt_get_current_user_classis($member_id);
										foreach($MemberShipClass as $key=>$class_id)
										{
											$class_data=$obj_class->MJgmgt_get_single_class($class_id);
										?>
											<option value="<?php echo esc_attr($class_id);?>" <?php if (is_array($userclass)){ if(in_array($class_id,$userclass)){ print "Selected"; } } ?>><?php echo MJgmgt_get_class_name(esc_html($class_id)); ?> ( <?php echo MJgmgt_timeremovecolonbefoream_pm(esc_html($class_data->start_time)).' - '.MJgmgt_timeremovecolonbefoream_pm(esc_html($class_data->end_time));?>)</option>
										<?php
										}
									}
									?>
							</select>
						</div>
						<div class="col-sm-2 col-xs-4">						
							<a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal_add_class" tabindex="40"> <?php esc_html_e('Add','gym_mgt');?></a>
						</div>
					</div>
				</div>
				<?php 
				if($edit)
				{
				?>					
				<div class="col-sm-6 padding_left_right_0">
					<div class="form-group">
						<label class="col-sm-4 control-label" for="membership_status"><?php esc_html_e('Membership Status','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-7">
							<?php $membership_statusval = "Continue"; if($edit){ $membership_statusval=$user_info->membership_status; }elseif(isset($_POST['membership_status'])) {$membership_statusval=sanitize_text_field($_POST['membership_status']);}?>
							<label class="radio-inline">
							 	<input type="radio" value="Continue" class="tog validate[required]" name="membership_status" <?php checked( 'Continue', $membership_statusval); ?>/><?php esc_html_e('Continue','gym_mgt');?>
							</label>
							<label class="radio-inline">
							 	<input type="radio" value="Expired" class="tog validate[required]" name="membership_status" <?php  checked( 'Expired', $membership_statusval);  ?>/><?php esc_html_e('Expired','gym_mgt');?>
							</label>
							<label class="radio-inline">
							  	<input type="radio" value="Dropped" class="tog validate[required]" name="membership_status" <?php checked( 'Dropped', $membership_statusval); ?>/><?php esc_html_e('Dropped','gym_mgt');?> 
							</label>
						</div>
					</div>
					<input type="hidden" name="auto_renew" value="No">		
				</div>		
				<?php } ?>	
				<div class="padding_left_right_0 clear_both"> 	
					<div class="col-sm-6 padding_left_right_0">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="begin_date"><?php esc_html_e('Membership Valid From','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
								<input id="begin_date" class="form-control validate[required] begin_date" type="text" name="begin_date" value="<?php if($edit){ if($user_info->begin_date!=""){ echo esc_attr(MJgmgt_getdate_in_input_box($user_info->begin_date)); } }elseif(isset($_POST['begin_date'])) echo esc_attr($_POST['begin_date']);?>" tabindex="41" readonly>
							</div>								
						</div>
					</div>
					<div class="col-sm-6 padding_left_right_0">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="begin_date"><?php esc_html_e('Membership Valid To','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
								<input id="end_date" class="form-control validate[required]" type="text" name="end_date" value="<?php if($edit){ if($user_info->end_date!=""){ echo esc_attr(MJgmgt_getdate_in_input_box($user_info->end_date)); }
								}
								elseif(isset($_POST['end_date'])) echo esc_attr($_POST['end_date']);?>" readonly>
							</div>
						</div>
					</div>					
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="first_payment_date"><?php esc_html_e('First Payment Date','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="first_payment_date" class="form-control" type="text"  name="first_payment_date" value="<?php if($edit){ if($user_info->first_payment_date!=""){ echo esc_attr(MJgmgt_getdate_in_input_box($user_info->first_payment_date)); } }elseif(isset($_POST['first_payment_date'])){ echo esc_attr($_POST['first_payment_date']); }?>" tabindex="42" readonly>
					</div>
				</div>
			</div>
			<div id="no_of_class"></div>
			<div class="col-sm-offset-2 col-sm-8">        	
				<input type="submit" value="<?php if($edit){ esc_html_e('Save','gym_mgt'); }else{ esc_html_e('Save Member','gym_mgt');}?>" name="save_member" class="btn btn-success class_submit " tabindex="43" />
			</div>
		</form><!-- MEMBER FROM END-->
	</div><!-- PANEL BODY DIV END-->
<?php 
}
?>
<!----------ADD STAFF MEMBER POPUP------------->
<div class="modal fade overflow_scroll" id="myModal_add_staff_member" role="dialog"><!-- MODAL MAIN DIV START-->
	<div class="modal-dialog modal-lg"><!-- MODAL DIALOG DIV START-->
		<div class="modal-content float_and_width"><!-- MODAL CONTENT DIV START-->
			<div class="modal-header float_and_width">
			  	<button type="button" class="close" data-dismiss="modal">&times;</button>
			  	<h3 class="modal-title"><?php esc_html_e('Add Staff Member','gym_mgt');?></h3>
			</div>
			<div id="message" class="updated below-h2 show_msg">
				<p><?php esc_html_e('Sorry, only JPG, JPEG, PNG & GIF And BMP files are allowed.','gym_mgt');?></p>
			</div>
			<div class="modal-body float_and_width"><!-- MODAL BODY DIV START-->
				<form name="staff_form" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="form-horizontal float_and_width" id="add_staff_form" enctype="multipart/form-data"><!-- STAFF MEMBER FORM START-->
					<?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
					<input type="hidden" name="action" value="MJgmgt_add_staff_member">
					<input type="hidden" name="role" value="staff_member"  />
					<input type="hidden" name="user_id" value=""/>
					<div class="header clear_both">
						<h4><?php esc_html_e('Personal Information','gym_mgt');?></h4>
					</div>
					<div class="col-sm-6 padding_left_right_0">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="first_name"><?php esc_html_e('First Name','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
								<input id="first_name" class="form-control validate[required,custom[onlyLetter_specialcharacter]] text-input" maxlength="50" type="text" value="" name="first_name" tabindex="44">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" for="middle_name"><?php esc_html_e('Middle Name','gym_mgt');?></label>
							<div class="col-sm-7">
								<input id="middle_name" class="form-control validate[custom[onlyLetter_specialcharacter]]" maxlength="50" type="text"  value="" name="middle_name" tabindex="45">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" for="last_name"><?php esc_html_e('Last Name','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
								<input id="last_name" class="form-control validate[required,custom[onlyLetter_specialcharacter]] text-input" maxlength="50" type="text"  value="" name="last_name" tabindex="46">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" for="gender"><?php esc_html_e('Gender','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
							<?php $genderval = "male"; ?>
								<label class="radio-inline">
								 	<input type="radio" value="male" class="tog validate[required]" name="gender" <?php checked( 'male', esc_html($genderval));  ?> tabindex="47" /><?php esc_html_e('Male','gym_mgt');?>
								</label>
								<label class="radio-inline">
								  	<input type="radio" value="female" class="tog validate[required]" name="gender"  <?php  checked( 'female', $genderval); ?>/><?php esc_html_e('Female','gym_mgt');?> 
								</label>
							</div>
						</div>
					</div>
					<div class="col-sm-6 padding_left_right_0">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="birth_date"><?php esc_html_e('Date of birth','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
								<input id="staff_birth_date" class="form-control validate[required] birth_date"  type="text" name="birth_date" value="" readonly tabindex="48">
							</div>
						</div>	
						<div class="form-group">
							<label class="col-sm-4 control-label" for="role_type"><?php esc_html_e('Assign Role','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-4">
								<select class="form-control" name="role_type" id="role_type" tabindex="49">
									<option value=""><?php esc_html_e('Select Role','gym_mgt');?></option>
									<?php
									if(isset($_REQUEST['role_type']))
									{
										$category =esc_attr($_REQUEST['role_type']);  
									}
									elseif($edit)
									{
										$category =$user_info->role_type;
									}
									else
									{ 
										$category = "";
									}
									$role_type=MJgmgt_get_all_category('role_type');
									if(!empty($role_type))
									{
										foreach ($role_type as $retrive_data)
										{
											echo '<option value="'.esc_attr($retrive_data->ID).'" '.selected(esc_attr($category),esc_attr($retrive_data->ID)).'>'.esc_html($retrive_data->post_title).'</option>';
										}
									}
									?>
								</select>
							</div>
							<div class="col-sm-3 add_category_padding_0">
								<button id="addremove" model="role_type" tabindex="50"><?php esc_html_e('Add Or Remove','gym_mgt');?></button>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" for="specialization"><?php esc_html_e('Specialization','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-4 multiselect_validation_specialization specialization_css">
								<select class="form-control"  name="activity_category[]" id="specialization"  multiple="multiple" tabindex="51" >
								<?php 
									if($edit)
									{
										$category =explode(',',$user_info->activity_category);
									}
									elseif(isset($_REQUEST['activity_category']))
									{
										$category =esc_attr($_REQUEST['activity_category']);
									}
									else
									{ 
										$category = array();
									}
									$activity_category=MJgmgt_get_all_category('activity_category');
									if(!empty($activity_category))
									{
										foreach ($activity_category as $retrive_data)
										{
											$selected = "";
											if(in_array($retrive_data->ID,$category))
											
											$selected = "selected";
											echo '<option value="'.esc_attr($retrive_data->ID).'"'.esc_attr($selected).'>'.esc_html($retrive_data->post_title).'</option>';
											
										}
									}
									?>
								</select>								
							</div>	
							<div class="col-sm-3 add_category_padding_0">
								<button id="addremove" model="activity_category" tabindex="52"><?php esc_html_e('Add Or Remove','gym_mgt');?></button>
							</div>
						</div>							
					</div>							
					<div class="header clear_both">	
						<hr>
						<h4><?php esc_html_e('Contact Information','gym_mgt');?></h4>
					</div>
					<div class="col-sm-6 padding_left_right_0">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="address"><?php esc_html_e('Home Town Address','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
								<input id="address" class="form-control validate[required,custom[address_description_validation]]" maxlength="150" type="text" name="address" value="<?php if(isset($_POST['address'])) echo esc_attr($_POST['address']);?>" tabindex="53">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" for="city_name"><?php esc_html_e('City','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
								<input id="city_name" class="form-control validate[required,custom[city_state_country_validation]]" maxlength="50" type="text" name="city_name" value="" tabindex="54">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" for="state_name"><?php esc_html_e('State','gym_mgt');?></label>
							<div class="col-sm-7">
								<input id="state_name" class="form-control validate[city_state_country_validation]" type="text" maxlength="50"  name="state_name" value="" tabindex="55">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" for="zip_code"><?php esc_html_e('Zip Code','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
								<input id="zip_code" class="form-control  validate[required,custom[onlyLetterNumber]]" type="text" maxlength="15" name="zip_code" 
								value="" tabindex="56">
							</div>
						</div>
					</div>
					<div class="col-sm-6 padding_left_right_0">
						<div class="form-group">
							<label class="col-sm-4 control-label " for="mobile"><?php esc_html_e('Mobile Number','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-2">
								<input type="text" readonly value="+<?php echo MJgmgt_get_countery_phonecode(get_option( 'gmgt_contry' ));?>"  class="form-control" name="phonecode">
							</div>
							<div class="col-sm-5">
								<input id="mobile" class="form-control validate[required,custom[phone_number]] phone_validation text-input" type="text" minlength="6"  name="mobile" maxlength="15" value="" tabindex="57">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" for="phone"><?php esc_html_e('Phone','gym_mgt');?></label>
							<div class="col-sm-7">
								<input id="phone" class="form-control validate[custom[phone_number]] phone_validation text-input" minlength="6" maxlength="15" type="text"  name="phone" value="" tabindex="58">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" for="email"><?php esc_html_e('Email','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
								<input id="email" class="form-control validate[required,custom[email]] text-input" maxlength="100" type="text"  name="email" value="" tabindex="59">
							</div>
						</div>
					</div>
					<div class="header clear_both">	
						<hr>
						<h4><?php esc_html_e('Login Information','gym_mgt');?></h4>
					</div>
					<div class="col-sm-6 padding_left_right_0">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="username"><?php esc_html_e('User Name','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-7">
								<input id="username" class="form-control validate[required,custom[username_validation]] space_validation" maxlength="50" type="text"  name="username" value="" tabindex="60">
							</div>
						</div>
					</div>
					<div class="col-sm-6 padding_left_right_0">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="password"><?php esc_html_e('Password','gym_mgt');?><?php if(!$edit) {?><span class="require-field">*</span><?php }?></label>
							<div class="col-sm-7">
								<input id="password" class="form-control validate[required] space_validation" type="password" min_length="8" maxlength="12"  name="password" value="" tabindex="61">
							</div>
						</div>
					</div>
					<div class="col-sm-6 padding_left_right_0">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="photo"><?php esc_html_e('Image','gym_mgt');?></label>
							<div class="col-sm-4">
								<input type="text" id="gmgt_user_avatar_url1" class="form-control gmgt_user_avatar_url" name="gmgt_user_avatar" readonly value="" />
							</div>	
							<div class="col-sm-3">
								<input id="upload_user_avatar_button1" type="button" class="button upload_user_avatar_button" value="<?php esc_html_e( 'Upload image', 'gym_mgt' ); ?>" tabindex="62" />
							</div>
							<div class="clearfix"></div>								
							<div class="col-sm-offset-4 col-sm-7">
								<div id="upload_user_avatar_preview1" class="upload_user_avatar_preview">
									<img class="image_preview_css" src="<?php echo esc_url((get_option( 'gmgt_system_logo' ))); ?>">
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 padding_left_right_0 clear_both">
						<div class="col-sm-offset-4 col-sm-7">
							<input type="submit" value="<?php esc_html_e('Add Staff','gym_mgt');?>" name="save_staff" id="add_staff_member" class="btn btn-success specialization_submit" tabindex="63" />
						</div>
					</div>
				</form><!-- Staff MEMBER FORM END-->
			</div><!-- MODAL BODY DIV END-->
			<div class="modal-footer float_and_width">
				<div class="col-sm-12 padding_left_right_0">	
					<button type="button" class="btn btn-default" data-dismiss="modal" tabindex="64"><?php esc_html_e('Close','gym_mgt');?></button>
				</div>
			</div>
		</div><!-- MODAL ContENT DIV END-->
	</div><!-- MODAL DIALOG DIV END-->
</div><!-- MODAL MAIN DIV END-->
<!----------ADD GORUP POPUP------------->
<div class="modal fade overflow_scroll" id="myModal_add_group" role="dialog"><!-- MODAL MAIN DIV START-->
	<div class="modal-dialog modal-lg"><!-- MODAL DIALOG DIV START-->
		<div class="modal-content"><!-- MODAL CONTENT DIV START-->
			<div class="modal-header">
			  	<button type="button" class="close" data-dismiss="modal">&times;</button>
			  	<h3 class="modal-title"><?php esc_html_e('Add Group','gym_mgt');?></h3>
			</div>
			<div class="modal-body"><!-- MODAL BODY DIV START-->
				<form name="group_form" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="form-horizontal" id="group_form"><!-- GROUP FORM START-->
						<input type="hidden" name="action" value="MJgmgt_add_group">
						<input type="hidden" name="group_id" value=""  />
						<div class="form-group">
							<label class="col-sm-2 control-label" for="group_name"><?php esc_html_e('Group Name','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-sm-8">
								<input id="group_name" class="form-control validate[required,custom[popup_category_validation]] text-input" maxlength="50" type="text" value="<?php if(isset($_POST['group_name'])) echo esc_attr($_POST['group_name']);?>" name="group_name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for=""><?php esc_html_e('Group Description','gym_mgt');?></label>
							<div class="col-sm-8">
							<textarea name="group_description" class="form-control validate[custom[address_description_validation]]" maxlength="500" ></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="gmgt_membershipimage"><?php esc_html_e('Group Image','gym_mgt');?></label>
							<div class="col-sm-8">
								<input type="text" id="gmgt_gym_background_image" name="gmgt_groupimage" readonly value="<?php if(isset($_POST['gmgt_groupimage'])) echo esc_url($_POST['gmgt_groupimage']);?>" />	
								<input id="upload_image_button" type="button" class="button upload_user_cover_button" value="<?php esc_html_e( 'Upload Cover Image', 'gym_mgt' ); ?>" />
								<span class="description"><?php esc_html_e('Upload Group Image', 'gym_mgt' ); ?></span>
								<div id="upload_gym_cover_preview min_height_100">
									<img class="max_width_25 margin_top_10" src="<?php if(isset($_POST['gmgt_groupimage'])) echo esc_url($_POST['gmgt_groupimage']); else echo get_option( 'gmgt_system_logo' );?>" />
								</div>
							</div>
						</div>
						<div class="col-sm-offset-2 col-sm-8">
							<input type="submit" value="<?php if($edit){ esc_html_e('Save','gym_mgt'); }else{ esc_html_e('Save','gym_mgt');}?>" name="save_group" class="btn btn-success"/>
						</div>
				</form><!-- GROUP FORM END-->
			</div><!-- MODAL BODY DIV END-->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php esc_html_e('Close','gym_mgt');?></button>
			</div>
		</div><!-- MODAL CONTENT DIV END-->
	</div><!-- MODAL DIALOG DIV END-->
</div><!-- MODAL MAIN DIV END-->
<!----------ADD MEMBERSHIP POPUP------------->
<div class="modal fade overflow_scroll" id="myModal_add_membership" role="dialog"><!-- MODAL MAIN DIV START-->
	<div class="modal-dialog modal-lg"><!-- MODAL DIALOG DIV START-->
		<div class="modal-content"><!-- MODAL ContENT DIV START-->
			<div class="modal-header">
			  	<button type="button" class="close" data-dismiss="modal">&times;</button>
			  	<h3 class="modal-title"><?php esc_html_e('Add Membership','gym_mgt');?></h3>
			</div>
			<div id="message" class="updated below-h2 show_msg">
				<p><?php esc_html_e('Sorry, only JPG, JPEG, PNG & GIF And BMP files are allowed.','gym_mgt');?></p>
			</div>
			<div class="modal-body"><!-- MODAL BODY DIV START-->
				<form name="membership_form" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="form-horizontal" id="membership_form"><!-- MEMBERSHIP FORM START-->
					<input type="hidden" name="action" value="MJgmgt_add_ajax_membership">
					<input type="hidden" name="membership_id" class="membership_id_activity" value=""  />
					<div class="form-group">
						<label class="col-sm-2 control-label" for="membership_name"><?php esc_html_e('Membership Name','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8">
							<input id="membership_name" class="form-control validate[required,custom[popup_category_validation]] text-input" maxlength="50" type="text" value="" name="membership_name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="membership_category"><?php esc_html_e('Membership Category','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8">			
							<select class="form-control" name="membership_category" id="membership_category">
								<option value=""><?php esc_html_e('Select Membership Category','gym_mgt');?></option>
								<?php 				
								if(isset($_REQUEST['membership_category']))
								{
									$category =esc_attr($_REQUEST['membership_category']);  
								}
								else
								{
									$category = "";
								}
								$mambership_category=MJgmgt_get_all_category('membership_category');
								if(!empty($mambership_category))
								{
									foreach ($mambership_category as $retrive_data)
									{
										echo '<option value="'.esc_attr($retrive_data->ID).'" '.selected(esc_attr($category),esc_attr($retrive_data->ID)).'>'.esc_attr($retrive_data->post_title) .'</option>';
									}
								}
								?>
							</select>
						</div>
						<div class="col-sm-2 add_category_padding_0">
							<button id="addremove" model="membership_category"><?php esc_html_e('Add Or Remove','gym_mgt');?></button>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="membership_period"><?php esc_html_e('Membership Period(Days)','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8">
							<input id="membership_period" class="form-control validate[required,custom[number]] text-input" type="number" onKeyPress="if(this.value.length==3) return false;" value="<?php if(isset($_POST['membership_period'])) echo esc_attr($_POST['membership_period']);?>" name="membership_period" placeholder="<?php esc_html_e('Enter Total Number of Days','gym_mgt');?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="member_limit"><?php esc_html_e('Members Limit','gym_mgt');?></label>
						<div class="col-sm-8">
							<?php $limitval = "unlimited"; if(isset($_POST['gender'])) {$limitval=$_POST['gender'];}?>
							<label class="radio-inline">
								<input type="radio" value="limited" class="tog" name="member_limit"  <?php  checked( 'limited', esc_html($limitval)); ?>/><?php esc_html_e('limited','gym_mgt');?>
							</label>
							<label class="radio-inline">
							  	<input type="radio" value="unlimited" class="tog" name="member_limit"  <?php  checked( 'unlimited', esc_html($limitval)); ?>/><?php esc_html_e('unlimited','gym_mgt');?>
							</label>
						</div>
					</div>
					<div id="member_limit"></div>		
					<div class="form-group">
						<label class="col-sm-2 control-label" for="classis_limit"><?php esc_html_e('Classic Limit','gym_mgt');?></label>
						<div class="col-sm-8">
						<?php $limitvals = "unlimited"; if(isset($_POST['gender'])) {$limitvals=sanitize_text_field($_POST['gender']);}?>
							<label class="radio-inline">
							 <input type="radio" value="limited" class="classis_limit" name="classis_limit" <?php checked( 'limited', esc_html($limitvals)); ?>/><?php esc_html_e('limited','gym_mgt');?>
							</label>
							<label class="radio-inline">
							  	<input type="radio" value="unlimited" class="classis_limit validate[required]" name="classis_limit" <?php checked( 'unlimited', esc_html($limitvals)); ?>/><?php esc_html_e('unlimited','gym_mgt');?> 
							</label>
						</div>
					</div>
					<div id="classis_limit"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="installment_amount"><?php esc_html_e('Membership Amount','gym_mgt');?>(<?php echo MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )); ?>)<span class="require-field">*</span></label>
						<div class="col-sm-8">
							<input id="membership_amount" class="form-control validate[required] text-input" type="number" min="0" onkeypress="if(this.value.length==8) return false;" step="0.01" value="<?php if(isset($_POST['membership_amount'])) echo esc_attr($_POST['membership_amount']);?>" name="membership_amount">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="installment_plan"><?php esc_html_e('Installment Plan','gym_mgt');?>(<?php echo MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )); ?>)<span class="require-field">*</span></label>
						<div class="col-sm-2">
							<input id="installment_amount" class="form-control validate[required] text-input" min="0" onkeypress="if(this.value.length==8) return false;" step="0.01" type="number" value="<?php if(isset($_POST['installment_amount'])) echo esc_attr($_POST['installment_amount']);?>" name="installment_amount" placeholder="<?php esc_html_e('Amount','gym_mgt');?>">
						</div>
						<div class="col-sm-6">
							<select class="form-control" name="installment_plan" id="installment_plan">
								<option value=""><?php esc_html_e('Select Installment Plan','gym_mgt');?></option>
								<?php
								if(isset($_REQUEST['installment_plan']))
								{
									$category =esc_attr($_REQUEST['installment_plan']);  
								}
								else
								{
									$category = "";
								}
								$installment_plan=MJgmgt_get_all_category('installment_plan');
								if(!empty($installment_plan))
								{
									foreach ($installment_plan as $retrive_data)
									{
										echo '<option value="'.esc_attr($retrive_data->ID).'" '.selected(esc_attr($category),esc_attr($retrive_data->ID)).'>'.esc_html($retrive_data->post_title).'</option>';
									}
								}
								?>
							</select>
						</div>
						<div class="col-sm-2 add_category_padding_0">
							<button id="addremove" model="installment_plan"><?php esc_html_e('Add Or Remove','gym_mgt');?></button>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="signup_fee"><?php esc_html_e('Signup Fee','gym_mgt');?>(<?php echo MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )); ?>)</label>
						<div class="col-sm-8">
							<input id="signup_fee" class="form-control text-input" type="number" min="0" onkeypress="if(this.value.length==8) return false;" step="0.01" value="<?php if(isset($_POST['membership_name'])) echo esc_attr($_POST['membership_name']);?>" name="signup_fee">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for=""><?php esc_html_e('Tax','gym_mgt');?>(%)</label>
						<div class="col-sm-3">
							<select  class="form-control tax_charge" name="tax[]" multiple="multiple">
								<?php
								if($edit)
								{
									$tax_id=explode(',',$result->tax);
								}
								else
								{
									$tax_id[]='';
								}
								$obj_tax=new MJgmgt_tax;
								$gmgt_taxs=$obj_tax->MJgmgt_get_all_taxes();
								if(!empty($gmgt_taxs))
								{
									foreach($gmgt_taxs as $data)
									{
										$selected = "";
										if(in_array($data->tax_id,$tax_id))
											$selected = "selected";
										?>
										<option value="<?php echo esc_attr($data->tax_id); ?>"<?php echo esc_html($selected); ?> ><?php echo esc_html($data->tax_title);?>-<?php echo esc_html($data->tax_value);?></option>
									<?php 
									}
								}
								?>
							</select>		
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="activity_category"><?php esc_html_e('Select Activity Category','gym_mgt');?></label>
						<div class="col-sm-8">
								<input type="hidden" class="action_membership" value="add_membership">
								<select class="form-control activity_category_list activity_width_title" name="activity_cat_id[]" multiple="multiple" id="activity_category"><?php
								$activity_category=MJgmgt_get_all_category('activity_category');
								if($edit)
								{
									$activity_category_array=explode(',',$result->activity_cat_id);
								}
								else
								{
									$activity_category_array[]='';
								}
								if(!empty($activity_category))
								{
									foreach ($activity_category as $retrive_data)
									{		
										$selected = "";
										if(in_array($retrive_data->ID,$activity_category_array))
											$selected = "selected";
										?>
											<option value="<?php echo esc_attr($retrive_data->ID);?>" <?php echo esc_html($selected); ?>><?php echo esc_html($retrive_data->post_title);?></option>
										<?php
									}
								}
								?>
							</select>
						</div>		
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="signup_fee"><?php esc_html_e('Select Activity','gym_mgt');?></label>
						<div class="col-sm-8">
							<?php
								$activitydata=$obj_activity->MJgmgt_get_all_activity_by_activity_category($activity_category_array); ?>
							<select name="activity_id[]" id="activity_id" multiple="multiple" class="activity_list_from_category_type">	<?php 
								$activity_array = $obj_activity->MJgmgt_get_membership_activity($membership_id);
								if(!empty($activitydata))
								{
									foreach($activitydata as $activity)
									{
									?>
										<option value="<?php echo esc_attr($activity->activity_id);?>" <?php if(in_array($activity->activity_id,$activity_array)) echo "selected";?>><?php echo esc_html($activity->activity_title);?></option>
									<?php
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="signup_fee"><?php esc_html_e('Membership Description','gym_mgt');?></label>
						<div class="col-md-8">
							<?php wp_editor(isset($result->membership_description)?stripslashes($result->membership_description) : '','description'); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="gmgt_membershipimage"><?php esc_html_e('Membership Image','gym_mgt');?></label>
						<div class="col-sm-8">			
							<input type="text" id="gmgt_user_avatar_url1" class="gmgt_user_avatar_url" name="gmgt_membershipimage" readonly value="<?php if(isset($_POST['gmgt_membershipimage'])) echo esc_attr($_POST['gmgt_membershipimage']);?>" />
							 <input id="upload_image_button1" type="button" class="button upload_user_avatar_button" value="<?php esc_html_e( 'Upload Cover Image', 'gym_mgt' ); ?>" />
							 <span class="description"><?php esc_html_e('Upload Membership Image', 'gym_mgt' ); ?></span>
							<div class="upload_user_avatar_preview" id="upload_user_avatar_preview1">
								<img class="image_preview_css" src="<?php if(isset($_POST['gmgt_membershipimage'])) echo esc_url($_POST['gmgt_membershipimage']); else echo esc_url(get_option( 'gmgt_system_logo' ));?>" />
							</div>
						</div>
					</div>
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" value="<?php if($edit){ esc_html_e('Save Membership','gym_mgt'); }else{ esc_html_e('Add Membership','gym_mgt');}?>" name="save_membership" class="btn btn-success"/>
					</div>
				</form><!-- MEMBERSHIP FORM END-->
			</div><!-- MODAL BODY DIV END-->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php esc_html_e('Close','gym_mgt');?></button>
			</div>
		</div><!-- MODAL ContENT DIV END-->
	</div><!-- MODAL DIALOG DIV END-->
</div><!--MODAL MAIN DIV END-->
<!----------ADD CLASS POPUP------------->
<div class="modal fade overflow_scroll" id="myModal_add_class" role="dialog"><!--MODAL MAIN DIV START-->
	<div class="modal-dialog modal-lg"><!--MODAL DIALOG DIV START-->
		<div class="modal-content"><!--MODAL CONTENT DIV START-->
			<div class="modal-header">
			  	<button type="button" class="close" data-dismiss="modal">&times;</button>
			  	<h3 class="modal-title"><?php esc_html_e('Add Class','gym_mgt');?></h3>
			</div>
			<div class="modal-body"><!--MODAL BODY DIV START-->
				<form name="class_form" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="form-horizontal" id="class_form"><!--CLASS FORM START-->
					<input type="hidden" name="action" value="MJgmgt_add_ajax_class">
					<input type="hidden" name="class_id" value=""  />
					<div class="form-group">
						<label class="col-sm-2 control-label" for="class_name"><?php esc_html_e('Class Name','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8">
							<input id="class_name" class="form-control validate text-input validate[required,custom[popup_category_validation]]" maxlength="50" type="text" value="" name="class_name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="staff_name"><?php esc_html_e('Select Staff Member','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8">
							<?php $get_staff = array('role' => 'Staff_member');
								$staffdata=get_users($get_staff);?>
							<select name="staff_id" class="form-control validate[required] " id="staff_id">
								<option value=""><?php esc_html_e('Select Staff Member','gym_mgt');?></option>
								<?php 								
								$staff_data="";
								if(!empty($staffdata))
								{
									foreach($staffdata as $staff)
									{
										echo '<option value='.esc_attr($staff->ID).' '.selected(esc_attr($staff_data),esc_attr($staff->ID)).'>'.esc_html($staff->display_name).'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="middle_name"><?php esc_html_e('Select Assistant Staff Member','gym_mgt');?></label>
						<div class="col-sm-8">
							<?php $get_staff = array('role' => 'Staff_member');
							$staffdata=get_users($get_staff);?>
							<select name="asst_staff_id" class="form-control" id="asst_staff_id">
								<option value=""><?php esc_html_e('Select Assistant Staff Member ','gym_mgt');?></option>
								<?php
								$assi_staff_data="";
								if(!empty($staffdata))
								{
									foreach($staffdata as $staff)
									{
										echo '<option value='.esc_attr($staff->ID).' '.selected(esc_attr($assi_staff_data),esc_attr($staff->ID)).'>'.esc_attr($staff->display_name).'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="invoice_date"><?php esc_html_e('Start Date','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8">
							<input id="class_date"  class="form-control class_date validate[required]" type="text" value="<?php  echo esc_attr(MJgmgt_getdate_in_input_box(date("Y-m-d"))); ?>" name="start_date">
						</div>
					</div>
					<div class="form-group"><label class="control-label col-md-2" for="End"><?php esc_html_e('End Date','gym_mgt');?><span class="text-danger"> *</span></label>
						 <div class="col-md-8">
							<div class="radio">
								<div class="input text">
								 	<input id="end_date_class"  class="form-control class_date validate[required]" type="text" value="<?php echo esc_attr(MJgmgt_getdate_in_input_box(date("Y-m-d"))); ?>" name="end_date">
							  	</div>
						   	</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="day"><?php esc_html_e('Select Day','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8 day_validation_member">			
							<select name="day[]" class="form-control validate[required]" id="day" multiple="multiple">							
								<?php $class_days=array();
								foreach(MJgmgt_days_array() as $key=>$day)
								{
									$selected = "";
									if(in_array($key,$class_days))
										$selected = "selected";
									echo '<option value='.esc_attr($key).' '.esc_attr($selected).'>'.esc_html($day).'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="day"><?php esc_html_e('Membership','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8 multiselect_validation_membership">
							<?php
								$membersdata=array();
								$data=array();
							?>
							<?php $membershipdata=$obj_membership->MJgmgt_get_all_membership();?>
							<select name="membership_id[]" class="form-control" multiple="multiple" id="class_membership_id">	
								<?php 					
								if(!empty($membershipdata))
								{
									foreach ($membershipdata as $membership)
									{
										$selected = "";
										if(in_array($membership->membership_id,$data))
											$selected="selected";
										echo '<option value='.esc_attr($membership->membership_id) .' '.esc_attr($selected).' >'.esc_html($membership->membership_label).'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="quentity"><?php esc_html_e('Member Limit','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8">
							<input  class="form-control validate[required] text-input" min="0" onkeypress="if(this.value.length==3) return false;" type="number" value="" name="member_limit">
						</div>
					</div>	
					<div class="add_more_time_entry">
						<div class="time_entry">
							<div class="form-group">
								<label class="col-sm-2 control-label" for="starttime"><?php esc_html_e('Start Time','gym_mgt');?><span class="require-field">*</span></label>
								<div class="col-sm-2">			
								 	<select name="start_time[]" class="form-control validate[required]">
								 		<option value=""><?php esc_html_e('Select Time','gym_mgt');?></option>
										<?php 
										for($i =0 ; $i <= 12 ; $i++)
										{
										?>
											<option value="<?php echo esc_attr($i);?>"><?php echo esc_html($i);?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="col-sm-2">
								 	<select name="start_min[]" class="form-control validate[required]">
										<?php 
										foreach(MJgmgt_minute_array() as $key=>$value)
										{?>
											<option value="<?php echo esc_attr($key);?>"><?php echo esc_html($value);?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="col-sm-2">
								 	<select name="start_ampm[]" class="form-control validate[required]">
										<option value="am"><?php esc_html_e('am','gym_mgt');?></option>
										<option value="pm"><?php esc_html_e('pm','gym_mgt');?></option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="weekday"><?php esc_html_e('End Time','gym_mgt');?><span class="require-field">*</span></label>
								<div class="col-sm-2">
									<select name="end_time[]" class="form-control validate[required]">
									 	<option value=""><?php esc_html_e('Select Time','gym_mgt');?></option>
									 		<?php 
											for($i =0 ; $i <= 12 ; $i++)
											{
											?>
												<option value="<?php echo esc_attr($i);?>"><?php echo esc_html($i);?></option>
											<?php
											}
									 		?>
									</select>
								</div>
								<div class="col-sm-2">
									<select name="end_min[]" class="form-control validate[required]">
										<?php 
										foreach(MJgmgt_minute_array() as $key=>$value)
										{
										?>
											<option value="<?php echo esc_attr($key);?>"><?php echo esc_html_e($value);?></option>
										<?php
										}
									 	?>
									</select>
								</div>
								<div class="col-sm-2">				
									<select name="end_ampm[]" class="form-control validate[required]">
										<option value="am"><?php esc_html_e('am','gym_mgt');?></option>
										<option value="pm"><?php esc_html_e('pm','gym_mgt');?></option>
								   	</select>
								</div>	
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="income_entry"></label>
						<div class="col-sm-3">					
							<button id="add_new_entry" class="btn btn-default btn-sm btn-icon icon-left" type="button" name="add_new_entry" onclick="add_entry()"><?php esc_html_e('Add More Time Slots','gym_mgt'); ?>
							</button>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="quentity"><?php esc_html_e('Class Color','gym_mgt');?></label>
						<div class="col-sm-4">
							  <input type="color" value="<?php
									
							  if($edit){ echo esc_attr($result->color);}elseif(isset($_POST['class_color'])) echo esc_attr($_POST['class_color']);?>" name="class_color">
						</div>
					</div>
					<div class="col-sm-offset-2 col-sm-8">
						<input type="submit" value="<?php esc_html_e('Save','gym_mgt');?>" name="save_class" class="btn btn-success day_validation_submit"/>
					</div>
				</form><!--CLASS FORM END-->
			</div><!--MODAL BODY DIV END-->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php esc_html_e('Close','gym_mgt');?></button>
			</div>
		</div><!--MODAL CONTENT DIV END-->
	</div><!--MODAL DIALOG DIV END-->
</div>	<!--MODAL MAIN DIV END-->
<script>	
$(document).ready(function() 
{
	"use strict";
	var date = new Date();
	date.setDate(date.getDate()-0);
	$('.class_date').datepicker({
	startDate: date,
	dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
	autoclose: true
   });
  
});
//ADD ENTRY
function add_entry()
{
	$(".add_more_time_entry").append('<div class="time_entry"><div class="form-group"><label class="col-sm-2 control-label" for="starttime"><?php esc_html_e('Start Time','gym_mgt');?><span class="require-field">*</span></label><div class="col-sm-2"><select name="start_time[]" class="form-control validate[required]"><option value=""><?php esc_html_e('Select Time','gym_mgt');?></option>  <?php for($i =0 ; $i <= 12 ; $i++) { ?> <option value="<?php echo esc_attr($i);?>"><?php echo esc_html($i);?></option> <?php } ?></select></div><div class="col-sm-2"><select name="start_min[]" class="form-control validate[required]"> <?php foreach(MJgmgt_minute_array() as $key=>$value){ ?> <option value="<?php echo esc_attr($key);?>"><?php echo esc_html($value);?></option><?php }?></select></div><div class="col-sm-2"><select name="start_ampm[]" class="form-control validate[required]"><option value="am"><?php esc_html_e('am','gym_mgt');?></option><option value="pm"><?php esc_html_e('pm','gym_mgt');?></option></select></div></div><div class="form-group"><label class="col-sm-2 control-label" for="weekday"><?php esc_html_e('End Time','gym_mgt');?><span class="require-field">*</span></label><div class="col-sm-2"><select name="end_time[]" class="form-control validate[required]"><option value=""><?php esc_html_e('Select Time','gym_mgt');?></option><?php for($i =0 ; $i <= 12 ; $i++){ ?><option value="<?php echo esc_attr($i);?>"><?php echo esc_html($i);?></option><?php } ?></select></div><div class="col-sm-2"><select name="end_min[]" class="form-control validate[required]"> <?php foreach(MJgmgt_minute_array() as $key=>$value) { ?><option value="<?php echo esc_attr($key);?>"><?php echo esc_html($value);?></option><?php } ?> </select></div><div class="col-sm-2"><select name="end_ampm[]" class="form-control validate[required]"><option value="am"><?php esc_html_e('am','gym_mgt');?></option><option value="pm"><?php esc_html_e('pm','gym_mgt');?></option></select></div><div class="col-sm-2"><button type="button" class="btn btn-default" onclick="deleteParentElement(this)"><i class="entypo-trash"><?php esc_html_e('Delete','gym_mgt');?></i></button></div></div></div>');
}
// REMOVING  ENTRY
function deleteParentElement(n)
{
	alert("<?php esc_html_e('Do you really want to delete this time Slots','gym_mgt'); ?>");
	n.parentNode.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode.parentNode);
}
 </script> 