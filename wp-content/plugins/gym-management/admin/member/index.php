<?php 
$curr_user_id=get_current_user_id();
$obj_gym=new MJgmgt_Gym_management($curr_user_id);
$obj_membership=new MJgmgt_membership;
$obj_class=new MJgmgt_classschedule;
$obj_group=new MJgmgt_group;
$obj_member=new MJgmgt_member;
$obj_activity=new MJgmgt_activity;
?>
<!-- POP up code -->
<div class="popup-bg z_index_100000">
    <div class="overlay-content">
		<div class="modal-content">
			<div class="category_list"></div>     
		</div>
    </div>     
</div>
<!-- End POP-UP Code -->
<?php 
//Approve MEMBER 
if(isset($_REQUEST['action']) && $_REQUEST['action'] =='approve')
{
	if( get_user_meta(esc_attr(($_REQUEST['member_id'])), 'gmgt_hash', true))
	{
		//------------- SMS SEND -------------//
		$current_sms_service = get_option('smgt_sms_service');
		if(is_plugin_active('sms-pack/sms-pack.php'))
		{
			$mobile_number=array(); 
			$args = array();
			$userinfo=get_userdata(esc_attr($_REQUEST['member_id']));
			$mobile_number[] = "+".MJgmgt_get_countery_phonecode(get_option( 'gmgt_contry' )).$userinfo->mobile;
			$gymname=get_option( 'gmgt_system_name' );		
			$message_content ="You are successfully registered at ".esc_html($gymname);
			$args['mobile']= esc_html($mobile_number);
			$args['message_from']="MEMBER Approved";
			$args['message']=$message_content;					
			if($current_sms_service=='telerivet' || $current_sms_service ="MSG91" || $current_sms_service=='bulksmsgateway.in' || $current_sms_service=='textlocal.in' || $current_sms_service=='bulksmsnigeria' || $current_sms_service=='africastalking' || $current_sms_service == 'clickatell')
			{
				$send = send_sms($args);
			}
		}
		$obj_membership=new MJgmgt_membership;		
		$result = delete_user_meta(esc_attr($_REQUEST['member_id']), 'gmgt_hash');
		$user_info = get_userdata(esc_attr($_REQUEST['member_id']));
		$member_name=$user_info->display_name;
		$to = $user_info->user_email; 
		$login_link=home_url();
		$membership = $obj_membership->MJgmgt_get_single_membership($user_info->membership_id);
		$subject =get_option( 'Member_Approved_Template_Subject' ); 
		$gymname=get_option( 'gmgt_system_name' );
		$sub_arr['[GMGT_GYM_NAME]']=$gymname;
	    $subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
		$search=array('[GMGT_GYM_NAME]','[GMGT_LOGIN_LINK]','[GMGT_MEMBERNAME]');
		$membership_name=MJgmgt_get_membership_name($membership_id);
		$replace = array($gymname,$login_link,$member_name);
		$message_replacement = str_replace($search, $replace,get_option('Member_Approved_Template'));	
		MJgmgt_send_mail($to,$subject,$message_replacement);
		if($result)
		wp_redirect ( admin_url().'admin.php?page=gmgt_member&tab=memberlist&message=4');
	}
}
if(isset($_REQUEST['attendance']) && $_REQUEST['attendance'] == 1)
{
	$member_id=esc_attr($_REQUEST['member_id']);
	?>
	<script type="text/javascript">
	$(document).ready(function() 
	{
		"use strict";
		$('.sdate').datepicker({dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>'}); 
		$('.edate').datepicker({dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>'}); 
	});
	</script>
	<div class="page-inner min_height_1631"><!-- PAGE INNNER DIV START-->
		<div class="page-title"> 
			<h3><img src="<?php echo get_option( 'gmgt_system_logo' ) ?>" class="img-circle head_logo" width="40" height="40" /><?php echo get_option( 'gmgt_system_name' );?></h3>
		</div>
		<div id="main-wrapper"><!-- MAIN WRAPPER DIV START-->
			<div class="row"><!--ROW DIV START-->
				<div class="panel panel-white"><!-- PANEL WHITE DIV START-->
					<div class="panel-body"><!-- PANEL BODY DIV START-->
						<h2 class="nav-tab-wrapper">
							<a href="?page=gmgt_member&view_member&member_id=<?php echo esc_attr($_REQUEST['member_id']); ?>&attendance=1" class="nav-tab nav-tab-active">
							<?php echo '<span class="dashicons dashicons-menu"></span>'.esc_html__('View Attendance', 'gym_mgt'); ?></a>
						</h2>
						<form name="wcwm_report" action="" method="post">
							<input type="hidden" name="attendance" value=1> 
							<input type="hidden" name="user_id" value=<?php echo esc_attr($_REQUEST['member_id']);?>>
								<div class="form-group col-md-3">
									<label for="exam_id"><?php esc_html_e('Start Date','gym_mgt');?></label>
										<input type="text" class="form-control sdate" name="sdate"  value="<?php if(isset($_REQUEST['sdate'])) echo esc_attr($_REQUEST['sdate']);
											else echo esc_attr(MJgmgt_getdate_in_input_box(date('Y-m-d')));?>" readonly>
								</div>
								<div class="form-group col-md-3">
									<label for="exam_id"><?php esc_html_e('End Date','gym_mgt');?></label>
										<input type="text" class="form-control edate"  name="edate" value="<?php if(isset($_REQUEST['edate'])) echo esc_attr($_REQUEST['edate']); else echo esc_attr(MJgmgt_getdate_in_input_box(date('Y-m-d')));?>" readonly>
								</div>
								<div class="form-group col-md-3 button-possition">
									<label for="subject_id">&nbsp;</label>
									<input type="submit" name="view_attendance" Value="<?php esc_html_e('Go','gym_mgt');?>"  class="btn btn-info"/>
								</div>
						</form>
						<div class="clearfix"></div>
						<?php
							if(isset($_REQUEST['view_attendance']))
							{
								global $wpdb;
								$start_date = MJgmgt_get_format_for_db(esc_attr($_REQUEST['sdate']));
								$end_date = MJgmgt_get_format_for_db(esc_attr($_REQUEST['edate']));
								$user_id = esc_attr($_REQUEST['user_id']);
								$attendance = MJgmgt_view_member_attendance($start_date,$end_date,$user_id);
								$class_id = MJgmgt_get_class_id($user_id);
								if(!empty($class_id))
								{
									$class_name = MJgmgt_get_class_name_by_id($class_id);
								}
								else
								{
									$class_name="";
								}
								$curremt_date =$start_date;
								if($end_date >= $curremt_date)
								{
									$filename="Attendance Report.csv";
									$fp = fopen($filename, "w");
									// Get The Field Name
									$output="";
									$header = array();			
									$header[] = esc_html__('ID','gym_mgt');
									$header[] = esc_html__('Member Name','gym_mgt');
									$header[] = esc_html__('Class Name','gym_mgt');
									$header[] = esc_html__('Date','gym_mgt');
									$header[] = esc_html__('Day','gym_mgt');
									$header[] = esc_html__('Attendance','gym_mgt');
									fputcsv($fp, $header);
									$i=1;
									while ($end_date >= $curremt_date)
									{
										$name = MJgmgt_get_display_name($user_id);
										$row = array();
										$row[] = $i;
										$row[] = $name;
										$row[] = $class_name;
										$row[] = MJgmgt_getdate_in_input_box($curremt_date);
										$attendance_status = MJgmgt_get_attendence($user_id,$curremt_date);
										$row[] = date("D", strtotime($curremt_date));
										if(!empty($attendance_status))
										{
											$row[] = MJgmgt_get_attendence($user_id,$curremt_date);	
										}
										else
										{
											$row[] = esc_html__('Absent','gym_mgt');
										}
										$curremt_date = strtotime("+1 day", strtotime($curremt_date));
										$curremt_date = date("Y-m-d", $curremt_date);
										$i++;
									fputcsv($fp, $row);
									}
									// Download the file
									fclose($fp);
								   ?>
								<?php
								}
							}
							?>
						<?php
						//  DATA
						if(isset($_REQUEST['view_attendance']))
						{
							$start_date = MJgmgt_get_format_for_db(esc_attr($_REQUEST['sdate']));
							$end_date = MJgmgt_get_format_for_db(esc_attr($_REQUEST['edate']));
							$user_id = esc_attr($_REQUEST['user_id']);
							$attendance = MJgmgt_view_member_attendance($start_date,$end_date,$user_id);
							$curremt_date =$start_date;
							?>
							<div class="form-group col-md-4 col-xs-12 button-possition">
						   		<a class="btn btn-success" href='<?php echo $filename;?>'><?php esc_html_e('Download Report In CSV','gym_mgt');?></a>
						   </div>
							<table class="table col-md-12">
								<tr>
									<th id="width_200px"><?php esc_html_e('Date','gym_mgt');?></th>
									<th><?php esc_html_e('Day','gym_mgt');?></th>
									<th><?php esc_html_e('Attendance','gym_mgt');?></th>
								</tr>
								<?php 
								while ($end_date >= $curremt_date)
								{
									echo '<tr>';
									echo '<td>';
									echo MJgmgt_getdate_in_input_box(esc_html($curremt_date));
									echo '</td>';
									$attendance_status = MJgmgt_get_attendence($user_id,$curremt_date);
									echo '<td>';
									echo date("D", strtotime(esc_html($curremt_date)));
									echo '</td>';
									if(!empty($attendance_status))
									{
										echo '<td>';
										echo MJgmgt_get_attendence(esc_html($user_id),esc_html($curremt_date));
										echo '</td>';
									}
									else 
									{
										echo '<td>';
										echo esc_html__('Absent','gym_mgt');
										echo '</td>';
									}
									echo '</tr>';
									$curremt_date = strtotime("+1 day", strtotime($curremt_date));
									$curremt_date = date("Y-m-d", $curremt_date);
								}
								?>
							</table>
						<?php
						}
						?>
					</div><!--PANEL BODY DIV END-->
				</div><!-- PANEL WHITE DIV END-->
			</div><!--ROW DIV END-->
		</div><!-- MAIN WRAPPER DIV END-->
	</div><!-- PAGE INNNER DIV END-->
<?php 
}
else
{
	$active_tab = isset($_GET['tab'])?$_GET['tab']:'memberlist';
	?>
	<div class="page-inner min_height_1631"><!-- PAGE INNNER DIV START-->
		<div class="page-title">
			<h3><img src="<?php echo get_option( 'gmgt_system_logo' ) ?>" class="img-circle head_logo" width="40" height="40" /><?php echo get_option( 'gmgt_system_name' );?></h3>
		</div>
		<?php 	
		//SAVE MEMBER DATA
		if(isset($_POST['save_member']))
		{
			$nonce = $_POST['_wpnonce'];
			if (wp_verify_nonce( $nonce, 'save_member_nonce' ) )
			{
				if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
				{
					if(sanitize_email($_POST['email']) == sanitize_email($_POST['hidden_email']))
					{
						$txturl=esc_url_raw($_POST['gmgt_user_avatar']);
						$ext=MJgmgt_check_valid_extension($txturl);
						if(!$ext == 0)
						{	
							$result=$obj_member->MJgmgt_gmgt_add_user($_POST);
							if($result)
							{
								wp_redirect ( admin_url().'admin.php?page=gmgt_member&tab=memberlist&message=2');
							}	
						}			
						else
						{ ?>
							<div id="message" class="updated below-h2 ">
								<p><?php esc_html_e('Sorry, only JPG, JPEG, PNG & GIF And BMP files are allowed.','gym_mgt');?></p>
							</div>
						<?php 
						}
					}
					else
					{
						if( !email_exists( sanitize_email($_POST['email']) ))
						{
							$txturl=esc_url_raw($_POST['gmgt_user_avatar']);
							$ext=MJgmgt_check_valid_extension($txturl);
							if(!$ext == 0)
							{	
								$result=$obj_member->MJgmgt_gmgt_add_user($_POST);
								if($result)
								{
									wp_redirect ( admin_url().'admin.php?page=gmgt_member&tab=memberlist&message=2');
								}
							}
							else
							{
							?>
								<div id="message" class="updated below-h2 ">
									<p><?php esc_html_e('Sorry, only JPG, JPEG, PNG & GIF And BMP files are allowed.','gym_mgt');?></p>
								</div>
							<?php 
							}	
						}
						else
						{
						?>
							<div id="message" class="updated below-h2">
								<p><?php esc_html_e('Email id exists already.','gym_mgt');?></p>
							</div>
						<?php 
						}
					}
				}
				else
				{
					if( !email_exists( sanitize_email($_POST['email']) ) && !username_exists( sanitize_user($_POST['username']) ))
					{
						$txturl=esc_url_raw($_POST['gmgt_user_avatar']);
						$ext=MJgmgt_check_valid_extension($txturl);
						if(!$ext == 0)
						{	
							$result=$obj_member->MJgmgt_gmgt_add_user($_POST);		
							if($result>0)
							{
								wp_redirect ( admin_url() . 'admin.php?page=gmgt_member&tab=memberlist&message=1');
							}
						}
						else
						{ 
						?>
							<div id="message" class="updated below-h2 ">
								<p><?php esc_html_e('Sorry, only JPG, JPEG, PNG & GIF And BMP files are allowed.','gym_mgt');?></p>
							</div>
						<?php 
						}
					}
					else
					{?>
					<div id="message" class="updated below-h2">
						<p><?php esc_html_e('Username Or Email id exists already.','gym_mgt');?></p>
					</div>
			  <?php }
				}			
			}
		}
		//Delete MEMBER DATA
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
		{
			$result=$obj_member->MJgmgt_delete_usedata(esc_attr($_REQUEST['member_id']));
			if($result)
			{
				wp_redirect ( admin_url().'admin.php?page=gmgt_member&tab=memberlist&message=3');
			}
		}
		//delete selected MEMBER data//
		if(isset($_REQUEST['delete_selected']))
		{		
			if(!empty($_REQUEST['selected_id']))
			{
				foreach($_REQUEST['selected_id'] as $id)
				{
					$delete_member=$obj_member->MJgmgt_delete_usedata($id);
				}
				if($delete_member)
				{
					wp_redirect ( admin_url().'admin.php?page=gmgt_member&tab=memberlist&message=3');
				}
			}
			else
			{
				echo '<script language="javascript">';
				echo 'alert("'.esc_html__('Please select at least one record.','gym_mgt').'")';
				echo '</script>';
			}
		}
		if(isset($_REQUEST['message']))
		{
			$message =esc_attr($_REQUEST['message']);
			if($message == 1)
			{
			?>
				<div id="message" class="updated below-h2 ">
					<p><?php esc_html_e('Member added successfully.','gym_mgt');?></p>
				</div>
			<?php	
			}
			elseif($message == 2)
			{
			?>
				<div id="message" class="updated below-h2 ">
					<p><?php esc_html_e("Member updated successfully.",'gym_mgt');?></p>
				</div>
			<?php
			}
			elseif($message == 3) 
			{
			?>
				<div id="message" class="updated below-h2">
					<p><?php esc_html_e('Member deleted successfully.','gym_mgt');?></p>
				</div>
			<?php	
			}
			elseif($message == 4) 
			{?>
				<div id="message" class="updated below-h2">
					<p><?php esc_html_e('Member successfully Approved','gym_mgt');?></p>
				</div>
			<?php
			}
			elseif($message == 7) 
			{ ?>
			<div id="message" class="updated below-h2"><p>
			<?php 
				_e('Class deleted successfully','gym_mgt');
			?></div></p><?php
			}
			elseif($message == 9) 
			{ ?>
			<div id="message" class="updated below-h2"><p>
			<?php 
				_e('Class limit added successfully','gym_mgt');
			?></div></p><?php
					
			}
		}?>
		<div id="main-wrapper"><!-- MAIN WRAPPER DIV START-->
			<div class="row"><!-- ROW DIV START-->
				<div class="col-md-12"><!-- COL 12 DIV START-->
					<div class="panel panel-white"><!-- PANEL WHITE DIV START-->
						<div class="panel-body"><!-- PANEL BODY DIV START-->
							<h2 class="nav-tab-wrapper"><!-- NAV TAB WRAPPER MENU START-->
								<a href="?page=gmgt_member&tab=memberlist" class="nav-tab <?php echo $active_tab == 'memberlist' ? 'nav-tab-active' : ''; ?>">
								<?php echo '<span class="dashicons dashicons-menu"></span> '.esc_html__('Member List', 'gym_mgt'); ?></a>
								<?php  if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
								{?>
								<a href="?page=gmgt_member&tab=addmember&action=edit&member_id=<?php echo esc_attr($_REQUEST['memberid']);?>" class="nav-tab <?php echo $active_tab == 'addmember' ? 'nav-tab-active' : ''; ?>">
								<?php esc_html_e('Edit Member', 'gym_mgt'); ?></a>  
								<?php 
								}
								elseif(isset($_REQUEST['action']) && $_REQUEST['action'] == 'view')
								{ ?>
									
								<a href="?page=gmgt_member&tab=viewmember&action=view&member_id=<?php echo esc_attr($_REQUEST['member_id']);?>" class="nav-tab <?php echo $active_tab == 'viewmember' ? 'nav-tab-active' : ''; ?>">
								<?php esc_html_e('View Member', 'gym_mgt'); ?></a>  
									<?php 
								}
								else 
								{ ?>
									<a href="?page=gmgt_member&tab=addmember" class="nav-tab <?php echo $active_tab == 'addmember' ? 'nav-tab-active' : ''; ?>">
								<?php echo '<span class="dashicons dashicons-plus-alt"></span> '.esc_html__('Add Member', 'gym_mgt'); ?></a>
								<?php  
								}?>
							</h2><!-- NAV TAB WRAPPER MENU END-->
							<?php							
							if($active_tab == 'memberlist')
							{ 
								?>	
								<script type="text/javascript">
									$(document).ready(function() 
									{
										"use strict";
										jQuery('#members_list').DataTable({
											"responsive": true,
											"order": [[ 1, "asc" ]],
											"aoColumns":[
											   {"bSortable": false},
											   {"bSortable": false},
											   {"bSortable": true},
											   {"bSortable": true},
											   {"bSortable": true},
											   {"bSortable": true},
											   {"bSortable": true},
											   {"bSortable": true},
											   {"bSortable": true},
											   {"bSortable": true},
											   {"bSortable": false}],
											   language:<?php echo MJgmgt_datatable_multi_language();?>	
											});
											
										$('.select_all').on('click', function(e)
										{
											 if($(this).is(':checked',true))  
											 {
												$(".sub_chk").prop('checked', true);  
											 }  
											 else  
											 {  
												$(".sub_chk").prop('checked',false);  
											 } 
										});
									
										$('.sub_chk').on('change',function()
										{ 
											if(false == $(this).prop("checked"))
											{ 
												$(".select_all").prop('checked', false); 
											}
											if ($('.sub_chk:checked').length == $('.sub_chk').length )
											{
												$(".select_all").prop('checked', true);
											}
									  });
									  //------ Add class limit function ----------//
									$("body").on("click", ".add_classlimit_member", function(event)
									{
										var remaining_class_limit  = $(this).attr('remaining_class_limit');
										var total_class  = $(this).attr('total_class');
										var total_credit_class  = $(this).attr('total_credit_class');
										var membership_id  = $(this).attr('membership_id');
										var member_id  = $(this).attr('member_id');
									
										$(".membership_class_limit").val(total_class);
										$(".remaning_class_limit").val(remaining_class_limit);
										$(".total_credit_class").val(total_credit_class);
										$(".membership_id").val(membership_id);
										$(".member_id").val(member_id);
									
									});
									
									//------ delete class limit function ----------//
									$("body").on("click", ".del_classlimit_member", function(event)
									{
										var remaining_class_limit  = $(this).attr('remaining_class_limit');
										var total_class  = $(this).attr('total_class');
										var total_credit_class  = $(this).attr('total_credit_class');
										var membership_id  = $(this).attr('membership_id');
										var member_id  = $(this).attr('member_id');
									
										$(".membership_class_limit").val(total_class);
										$(".remaning_class_limit").val(remaining_class_limit);
										$(".total_credit_class").val(total_credit_class);
										$(".membership_id").val(membership_id);
										$(".member_id").val(member_id);
									});
									
									$('#add_class_limit_form').on('submit', function(e)
									{
										e.preventDefault();
										$('.save_add_class_btn').prop('disabled', true);
										var form = $(this).serialize();
											$.ajax(
											{
												type:"POST",
												url: $(this).attr('action'),
												data:form,
												success: function(data)
												{
													$('#add_class_limit_form').trigger("reset");
													$('.modal').modal('hide');
													window.location.href = window.location.href + "&message=9";								
												},
												error: function(data){
												}
											})
									});
									
									//delete class limit for AJAX
									$('#delete_class_limit_form').on('submit', function(e)
									{
										e.preventDefault();
										$('.delete_class_limit_btn').prop('disabled', true);
										var form = $(this).serialize();
											
										$.ajax(
										{
											type:"POST",
											url: $(this).attr('action'),
											data:form,												
											success: function(data)
											{
												if(data == 2)
												{
													alert('No Any Class Limit In This Member');
												}
												else
												{
												  $('#delete_class_limit_form').trigger("reset");
												  $('.modal').modal('hide');
												   window.location.href = window.location.href + "&message=6";	
											    }												
											},
											error: function(data){
											}
										})
									});
									
									});
								</script>
								<div class="panel-body"> <!-- PANEL BODY DIV START--> 
									<form method="post">  
										<div class="form-group col-md-3 padding_right_0">
											<label class=""><?php esc_html_e('Member type','gym_mgt');?></label>
												<select name="member_type" class="form-control validate[required]" id="member_type">
												<option value=""><?php esc_html_e('Select Member Type','gym_mgt');?></option>
													<?php
													if(isset($_POST['member_type']))
													{
														$mtype=sanitize_text_field($_POST['member_type']);
													}
													else
													{
														$mtype="";
													}
													$membertype_array=MJgmgt_member_type_array();
													if(!empty($membertype_array))
													{
														foreach($membertype_array as $key=>$type)
														{						
															echo '<option value='.esc_attr($key).' '.selected(esc_attr($mtype),esc_attr($key)).'>'.esc_html($type).'</option>';
														}
													}
													?>
												</select>			
										</div>
										<div class="form-group col-md-3 button-possition padding_right_0">
											<label for="subject_id">&nbsp;</label>
											<input type="submit" value="<?php esc_html_e('Go','gym_mgt');?>" name="filter_membertype"  class="btn btn-info"/>
										</div>
									 <?php 
										if(isset($_REQUEST['filter_membertype']) )
										{
											if(isset($_REQUEST['member_type']) && $_REQUEST['member_type'] != "")
											{
												$member_type= esc_attr($_REQUEST['member_type']);		
												$membersdata = get_users(array('meta_key' => 'member_type', 'meta_value' =>$member_type,'role'=>'member'));	
											}
											else
											{
												$membersdata =get_users( array('role' => 'member'));
											}
										}
										else 
										{					
											$membersdata =get_users( array('role' => 'member'));	
										} ?>       
									</form>
								</div>
								<form name="member_form" action="" method="post"><!-- MEMBER LIST FORM START-->
									<div class="panel-body"><!-- PANEL BODY DIV START-->
										<div class="table-responsive"><!-- TRABLE RESPONSIVE DIV START-->
											<table id="members_list" class="display" cellspacing="0" width="100%"><!-- PANEL LIST TABLE START-->
												<thead>
													<tr>
														<th><input type="checkbox" class="select_all"></th>
														<th><?php esc_html_e('Photo','gym_mgt');?></th>
														<th><?php esc_html_e('Member Name','gym_mgt');?></th>
														<th><?php esc_html_e('Member Id','gym_mgt');?></th>
														<th><?php esc_html_e('Member Type','gym_mgt');?></th>
														<th><?php esc_html_e('Joining Date','gym_mgt');?></th>
														<th><?php esc_html_e('Expire Date','gym_mgt');?></th>
														<th><?php esc_html_e('Membership Status','gym_mgt');?></th>
														<th><?php esc_html_e('Remaining Class','gym_mgt' );?></th>
														<th><?php esc_html_e('Add&Delete Class Limt','gym_mgt');?></th>
														<th><?php esc_html_e('Action','gym_mgt');?></th>
													</tr>
												</thead>
												<tfoot>
													<tr>
													   <th></th>
														<th><?php esc_html_e('Photo','gym_mgt');?></th>
														<th><?php esc_html_e('Member Name','gym_mgt');?></th>
														<th><?php esc_html_e('Member Id','gym_mgt');?></th>
														<th><?php esc_html_e('Member Type','gym_mgt');?></th>
														<th><?php esc_html_e('Joining Date','gym_mgt');?></th>
														<th><?php esc_html_e('Expire Date','gym_mgt');?></th>
														<th><?php esc_html_e('Membership Status','gym_mgt');?></th>
														<th><?php esc_html_e('Remaining Class','gym_mgt' );?></th>
														<th><?php esc_html_e('Add&Delete Class Limt','gym_mgt');?></th>
														<th><?php esc_html_e('Action','gym_mgt');?></th>
													</tr>           
												</tfoot> 
												<tbody>
												<?php 
												if(!empty($membersdata))
												{
													foreach ($membersdata as $retrieved_data)
													{
														$membership_id = get_user_meta($retrieved_data->ID,'membership_id',true);
														$membershipdata=$obj_membership->MJgmgt_get_single_membership($membership_id);
														if(!empty($membershipdata))
														{
															if($membershipdata->classis_limit=='limited')
															{
		/* $total_class=$membershipdata->on_of_classis;
		$userd_class=MJgmgt_get_user_used_membership_class($membership_id,$retrieved_data->ID);
		$remaining_class_data=$total_class-$userd_class;
		$remaining_class=$remaining_class_data." Out Of ".$total_class;
		
		
		$total_class_with_credit_limit=$membershipdata->on_of_classis + $total_member_limit_class_data  ;
		 if(empty($total_member_limit_class_data))
		 {
			 $total_member_limit_class='0';
		 }
		else
		{
			$total_member_limit_class=$total_member_limit_class_data;
		} */
		$total_class=$membershipdata->on_of_classis;
		$total_member_limit_class_data=$obj_membership->MJgmgt_get_member_credit_class($retrieved_data->ID,$membership_id);
	
		$total_class_with_credit_limit=$membershipdata->on_of_classis + $total_member_limit_class_data  ;
		 if(empty($total_member_limit_class_data))
		 {
			 $total_member_limit_class='0';
		 }
		else
		{
			$total_member_limit_class=$total_member_limit_class_data;
		}
	 
	$remaining_class_with_memberlimit=$total_class+$total_member_limit_class;
	
	$userd_class=MJgmgt_get_user_used_membership_class($membership_id,$retrieved_data->ID);
	$remaining_class_data=$total_class-$userd_class;
	$remaining_class=$remaining_class_data+$total_member_limit_class. esc_html__(' Out Of ','gym_mgt').$total_class_with_credit_limit;
	$remaining_class_limit=$remaining_class_with_memberlimit-$userd_class;
															}
															else
															{
																$remaining_class='Unlimited';
															}
														}
														?>
														<tr>
														   <td class="title"><input type="checkbox" name="selected_id[]" class="sub_chk" value="<?php echo esc_attr($retrieved_data->ID); ?>"></td>
															<td class="user_image"><?php $uid=$retrieved_data->ID;
																$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);
																if(empty($userimage))
																{
																	echo '<img src='.get_option( 'gmgt_system_logo' ).' id="width_50" class="height_50 img-circle" />';
																}
																else
																	echo '<img src='.esc_url($userimage).' id="width_50"class="height_50 img-circle"/>';
															?></td>
															<td class="name"><a href="?page=gmgt_member&tab=addmember&action=edit&memberid=<?php echo esc_attr($retrieved_data->ID);?>"><?php echo esc_html($retrieved_data->display_name);?></a></td>
															<td class="memberid"><?php echo esc_html($retrieved_data->member_id);?></td>
															<td class="memberid"><?php if(isset($retrieved_data->member_type))  echo esc_html($membertype_array[$retrieved_data->member_type]);  else echo esc_html__('Not Selected','gym_mgt');?></td>
															<td class="joining date"><?php if($retrieved_data->member_type!='Prospect'){ echo MJgmgt_getdate_in_input_box($retrieved_data->begin_date); }else{ echo "--"; }?></td>
															<td class="joining date"><?php if($retrieved_data->member_type!='Prospect'){ echo MJgmgt_getdate_in_input_box(MJgmgt_check_membership($retrieved_data->ID)); }else{ echo "--"; }?></td>
															<td class="status"><?php if($retrieved_data->member_type!='Prospect'){ esc_html_e($retrieved_data->membership_status,'gym_mgt'); }else{ esc_html_e('Prospect','gym_mgt');}?></td>
															
															<td class="remaining_class"><span class="btn-success padding_3">
															<?php 
															if($remaining_class == 'Unlimited')
															{
																echo esc_html_e('Unlimited','gym_mgt');
															}
															else
															{
															//var_dump($remaining_class);
															echo esc_html($remaining_class); 
															}
															?>
															</span></td>
															
															<td>
													<?php 
													if(!empty($membershipdata->classis_limit))
													{
														if($membershipdata->classis_limit=='limited')
														{ 
														?>
															<a href="#" class="btn btn-default add_classlimit_member" member_id="<?php echo $retrieved_data->ID;?>" membership_id="<?php echo $membership_id;?>" total_class="<?php echo $total_class; ?>"  total_credit_class="<?php echo $total_member_limit_class; ?>" remaining_class_limit="<?php echo $remaining_class_limit; ?>"  data-toggle="modal"  data-target="#myModal_add_class_limit"> <?php _e('Add','gym_mgt');?></a>	
															<a href="#" class="btn btn-danger del_classlimit_member"  data-toggle="modal"  member_id="<?php echo $retrieved_data->ID;?>" membership_id="<?php echo $membership_id;?>" total_class="<?php echo $total_class;  ?>"  total_credit_class="<?php echo $total_member_limit_class; ?>" remaining_class_limit="<?php echo $remaining_class_limit; ?>" data-target="#myModal_delete_class_limit"> <?php _e('Delete','gym_mgt');?></a>	
													
														<?php 
														}
														else
														{
															echo esc_html_e('Unlimited','gym_mgt');
														}
													}
													else
													{
														echo "-";
													}													
													?>
														
													</td>
															
															
															<td class="action"> 
																<a href="?page=gmgt_member&tab=viewmember&action=view&member_id=<?php echo esc_attr($retrieved_data->ID)?>" class="btn btn-success"> <?php esc_html_e('View', 'gym_mgt' ) ;?></a>
															<a href="?page=gmgt_member&tab=addmember&action=edit&memberid=<?php echo esc_attr($retrieved_data->ID)?>" class="btn btn-info"> <?php esc_html_e('Edit', 'gym_mgt' ) ;?></a>
															<a href="?page=gmgt_member&tab=memberlist&action=delete&member_id=<?php echo esc_attr($retrieved_data->ID);?>" class="btn btn-danger" onclick="return confirm('<?php esc_html_e('Do you really want to delete all data in this member?','gym_mgt');?>');"><?php esc_html_e( 'Delete', 'gym_mgt' ) ;?> </a>
															<a href="?page=gmgt_member&view_member&member_id=<?php echo esc_attr($retrieved_data->ID);?>&attendance=1" class="btn btn-default" 
															idtest="<?php echo esc_attr($retrieved_data->ID); ?>"><i class="fa fa-eye"></i> <?php esc_html_e('View Attendance','gym_mgt');?></a>
															<?php 
															if(get_user_meta($retrieved_data->ID, 'gmgt_hash', true)!='')
															{
																?>
																<a href="?page=gmgt_member&tab=addmember&action=approve&member_id=<?php echo esc_attr($retrieved_data->ID)?>" class="btn btn-info"> <?php esc_html_e('Approve', 'gym_mgt' ) ;?></a>
															<?php
															}
															?>
															</td>
														</tr>
														<?php 
													}
												}?>
												</tbody>
											</table><!-- MEMBER LIST TABLE END-->
											<div class="print-button pull-left">
												<input  type="submit" value="<?php esc_html_e('Delete Selected','gym_mgt');?>" name="delete_selected" class="btn btn-danger delete_selected "/>
											</div>
										</div><!-- TABLE RESPONSIVE DIV END-->
									</div><!-- PANEL BODY DIV END-->
								</form><!-- MEMBER LIST FORM END-->
								<!----------ADD Class Limit POPUP------------->
							<div class="modal fade" id="myModal_add_class_limit" role="dialog" style="overflow:scroll;">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
										  <button type="button" class="close" data-dismiss="modal">&times;</button>
										  <h3 class="modal-title"><?php _e('Add Class Limit','gym_mgt');?></h3>
										</div>
										<div class="modal-body">
										<form name="add_class_limit_form" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="form-horizontal" id="add_class_limit_form">
												<input type="hidden" name="action" value="MJgmgt_add_class_limit">
												<input type="hidden" name="member_id" class="member_id" value=""  />
												<input type="hidden" name="membership_id" class="membership_id" value=""  />
													<div class="form-group">
														<label class="col-sm-3 control-label" for="installment_amount"><?php _e('Membership Class Limit','gym_mgt');?><span class="require-field"></span></label>
														<div class="col-sm-4">
															<input  class="form-control text-input validate[required] membership_class_limit" type="text"  value="" name="membership_class_limit" disabled>
														</div>
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label" for="installment_amount"><?php _e('Total Credit Class','gym_mgt');?><span class="require-field"></span></label>
														<div class="col-sm-4">
															<input class="form-control text-input validate[required] total_credit_class" type="text"  value="" name="total_credit_class" disabled >
														</div>
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label" for="installment_amount"><?php _e('Total Remaining Class','gym_mgt');?><span class="require-field"></span></label>
														<div class="col-sm-4">
															<input class="form-control text-input validate[required] remaning_class_limit" type="text"  value="" name="remaning_class_limit" disabled >
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label" for="installment_amount"><?php _e('Add No Of Class Limit','gym_mgt');?><span class="require-field">*</span></label>
														<div class="col-sm-4">
															<input class="form-control text-input validate[required]" type="number" min="0" onkeypress="if(this.value.length==6) return false;"  value="" name="add_classlimit_member">
														</div>
													</div>												
												<div class="col-sm-offset-3 col-sm-8">
													<input type="submit" value="<?php  _e('Save','gym_mgt'); ?>" name="save_class_limit" class="btn btn-success save_add_class_btn"/>
												</div>
										</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal"><?php  _e('Close','gym_mgt'); ?></button>
										</div>
									</div>
								</div>
							</div>	
							<!----------Delete Class Limit POPUP------------->
							<div class="modal fade" id="myModal_delete_class_limit" role="dialog" style="overflow:scroll;">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
										  <button type="button" class="close" data-dismiss="modal">&times;</button>
										  <h3 class="modal-title"><?php _e('Delete Class Limit','gym_mgt');?></h3>
										</div>
										<div class="modal-body">
										<form name="delete_class_limit_form" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="form-horizontal" id="delete_class_limit_form">
												<input type="hidden" name="action" value="MJgmgt_delete_class_limit_for_member">
												<input type="hidden" name="member_id" class="member_id" value=""  />
												<input type="hidden" name="membership_id" class="membership_id" value=""  />
													<div class="form-group">
														<label class="col-sm-3 control-label" for="installment_amount"><?php _e('Membership Class Limit','gym_mgt');?><span class="require-field"></span></label>
														<div class="col-sm-4">
															<input  class="form-control text-input validate[required] membership_class_limit" type="text"  value="" name="membership_class_limit" disabled>
														</div>
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label" for="installment_amount"><?php _e('Total Credit Class','gym_mgt');?><span class="require-field"></span></label>
														<div class="col-sm-4">
															<input class="form-control text-input validate[required] total_credit_class" type="text"  value="" name="total_credit_class" disabled >
														</div>
													</div>
													
													<div class="form-group">
														<label class="col-sm-3 control-label" for="installment_amount"><?php _e('Total Remaining Class','gym_mgt');?><span class="require-field"></span></label>
														<div class="col-sm-4">
															<input class="form-control text-input validate[required] remaning_class_limit" type="text"  value="" name="remaning_class_limit" disabled >
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-3 control-label" for="installment_amount"><?php _e('Delete No Of Class Limit','gym_mgt');?><span class="require-field">*</span></label>
														<div class="col-sm-4">
															<input class="form-control text-input validate[required]" type="number" min="0" onkeypress="if(this.value.length==6) return false;"  value="" name="add_classlimit_member">
														</div>
													</div>	
                                                	<div class="col-sm-offset-3 col-sm-8">
													<input type="submit" value="<?php  _e('Delete','gym_mgt'); ?>" name="delete_class_limit" class="btn btn-success delete_class_limit_btn"/>
												</div>													
										</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Close','gym_mgt');?></button>
										</div>
									</div>
								</div>
							</div>							
							<?php 
							}
							if($active_tab == 'addmember')
							{
								require_once GMS_PLUGIN_DIR. '/admin/member/add_member.php';
							}							 
							if($active_tab == 'viewmember')
							{
								
								require_once GMS_PLUGIN_DIR. '/admin/member/view_member.php';
							}
							?>
						</div><!-- PANEL BODY DIV END-->
					</div><!-- PANEL WHITE DIV END-->
				</div><!-- COL 12 DIV END-->
			</div><!-- ROW DIV END-->
		</div><!-- MAIN WRAPPER DIV END-->
	</div><!-- PAGE INNNER DIV END-->
<?php 
} 
?> 	