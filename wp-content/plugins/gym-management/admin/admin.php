<?php 
 // This is adminside main First page of Gym management management plug in 
add_action( 'admin_head', 'MJgmgt_admin_menu_icon' );
//ADMIN MENU ICON FUNCTION
function MJgmgt_admin_menu_icon()
{
?>
	<style type="text/css">
	</style>
<?php 
}
add_action( 'admin_menu', 'MJgmgt_system_menu' );
//ADMIN SIDE MENU FUNCTION
function MJgmgt_system_menu()
{
	if (function_exists('MJgmgt_setup'))  
	{		
		add_menu_page('Gym Management',esc_html__('WPGYM','gym_mgt'),'manage_options','gmgt_system','MJgmgt_system_dashboard',plugins_url('gym-management/assets/images/gym-1.png' )); 
		if($_SESSION['gmgt_verify'] == '')
		{
			add_submenu_page('gmgt_system', esc_html__('License Settings','gym_mgt'), esc_html__( 'License Settings', 'gym_mgt' ),'manage_options','gmgt_setup','MJgmgt_options_page');
		}
		add_submenu_page('gmgt_system', 'Dashboard', esc_html__( 'Dashboard', 'gym_mgt' ), 'administrator', 'gmgt_system', 'MJgmgt_system_dashboard');
		add_submenu_page('gmgt_system', 'Membership Type', esc_html__( 'Membership Type', 'gym_mgt' ), 'administrator', 'gmgt_membership_type', 'MJgmgt_membership_manage');
		add_submenu_page('gmgt_system', 'Group', esc_html__( 'Groups', 'gym_mgt' ), 'administrator', 'gmgt_group', 'MJgmgt_group_manage');
		add_submenu_page('gmgt_system', 'Staff Members', esc_html__( 'Staff Members', 'gym_mgt' ), 'administrator', 'gmgt_staff', 'MJgmgt_staff_manage');
		add_submenu_page('gmgt_system', 'Class Schedule', esc_html__( 'Class Schedule', 'gym_mgt' ), 'administrator', 'gmgt_class', 'MJgmgt_class_manage');
		add_submenu_page('gmgt_system', 'Member', esc_html__( 'Member', 'gym_mgt' ), 'administrator', 'gmgt_member', 'MJgmgt_member_manage');
		add_submenu_page('gmgt_system', 'Activity', esc_html__( 'Activity', 'gym_mgt' ), 'administrator', 'gmgt_activity', 'MJgmgt_activity_manage');
		add_submenu_page('gmgt_system', 'Assign Workout', esc_html__( 'Assign Workout', 'gym_mgt' ), 'administrator', 'gmgt_workouttype', 'MJgmgt_workouttype_manage');
		add_submenu_page('gmgt_system', 'Nutrition Schedule', esc_html__( 'Nutrition Schedule', 'gym_mgt' ), 'administrator', 'gmgt_nutrition', 'MJgmgt_nutrition_manage');
		add_submenu_page('gmgt_system', 'Daily Workout', esc_html__( 'Daily Workout', 'gym_mgt' ), 'administrator', 'gmgt_workout', 'MJgmgt_workout_manage');
		add_submenu_page('gmgt_system', 'Product', esc_html__( 'Product', 'gym_mgt' ), 'administrator', 'gmgt_product', 'MJgmgt_product_manage');
		add_submenu_page('gmgt_system', 'Store', esc_html__( 'Store', 'gym_mgt' ), 'administrator', 'gmgt_store', 'MJgmgt_store_manage');
		add_submenu_page('gmgt_system', 'Reservation', esc_html__( 'Reservation', 'gym_mgt' ), 'administrator', 'gmgt_reservation', 'MJgmgt_reservation_manage');
		add_submenu_page('gmgt_system', 'Attendance', esc_html__( 'Attendance', 'gym_mgt' ), 'administrator', 'gmgt_attendence', 'MJgmgt_attendence_manage');
		add_submenu_page('gmgt_system', 'Accountant', esc_html__( 'Accountant', 'gym_mgt' ), 'administrator', 'gmgt_accountant', 'MJgmgt_accountant_manage');
		add_submenu_page('gmgt_system', 'Tax', esc_html__( 'Tax', 'gym_mgt' ), 'administrator', 'MJgmgt_gmgt_taxes', 'MJgmgt_gmgt_taxes');
		add_submenu_page('gmgt_system', 'Fees Payment', esc_html__( 'Membership Payment', 'gym_mgt' ), 'administrator', 'MJgmgt_fees_payment', 'MJgmgt_fees_payment');
		add_submenu_page('gmgt_system', 'Payment', esc_html__( 'Payment', 'gym_mgt' ), 'administrator', 'gmgt_payment', 'MJgmgt_payment_manage');
		add_submenu_page('gmgt_system', 'Message', esc_html__( 'Message', 'gym_mgt' ), 'administrator', 'Gmgt_message', 'MJgmgt_message_manage');
		add_submenu_page('gmgt_system', 'Newsletter', esc_html__( 'Newsletter', 'gym_mgt' ), 'administrator', 'gmgt_newsletter', 'MJgmgt_newsletter_manage');
		add_submenu_page('gmgt_system', 'Notice', esc_html__( 'Notice', 'gym_mgt' ), 'administrator', 'gmgt_notice', 'MJgmgt_notice_manage');
		add_submenu_page('gmgt_system', 'Report', esc_html__( 'Report', 'gym_mgt' ), 'administrator', 'gmgt_report', 'MJgmgt_report_manage');
		add_submenu_page('gmgt_system', 'Email Template', esc_html__( 'Email Template', 'gym_mgt' ), 'administrator', 'gmgt_mail_template', 'MJgmgt_mail_template_manage');
		add_submenu_page('gmgt_system', 'Gnrl_setting', esc_html__( 'General Settings', 'gym_mgt' ), 'administrator', 'gmgt_gnrl_settings', 'MJgmgt_gnrl_settings');
		add_submenu_page('gmgt_system', 'access_right', esc_html__( 'Access Right', 'gym_mgt' ), 'administrator', 'gmgt_access_right', 'MJgmgt_access_right');
    }
	else
	{ 		      
		die;
	}
}
//BELOW ALL PAGE call BY MENU FUNCTIONS
function MJgmgt_options_page()
{
	require_once GMS_PLUGIN_DIR. '/admin/setupform/index.php';
}
function MJgmgt_system_dashboard()
{
	require_once GMS_PLUGIN_DIR. '/admin/dasboard.php';
}	
function MJgmgt_membership_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/membership/index.php';
}
function MJgmgt_group_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/group/index.php';
}
function MJgmgt_staff_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/staff-members/index.php';
}
function MJgmgt_accountant_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/accountant/index.php';
}
function MJgmgt_class_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/class-schedule/index.php';
}
function MJgmgt_member_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/member/index.php';
}
function MJgmgt_product_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/product/index.php';
}
function MJgmgt_store_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/store/index.php';
}
function MJgmgt_nutrition_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/nutrition/index.php';
}
function MJgmgt_reservation_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/reservation/index.php';
}
function MJgmgt_attendence_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/attendence/index.php';
}
function MJgmgt_gmgt_taxes()
{
	require_once GMS_PLUGIN_DIR. '/admin/tax/index.php';
}
function MJgmgt_fees_payment()
{
	require_once GMS_PLUGIN_DIR. '/admin/membership_payment/index.php';
}
function MJgmgt_payment_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/payment/index.php';
}
function MJgmgt_message_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/message/index.php';
}
function MJgmgt_newsletter_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/news-letter/index.php';
}
function MJgmgt_activity_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/activity/index.php';
}
function MJgmgt_workouttype_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/workout-type/index.php';
}
function MJgmgt_workout_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/workout/index.php';
}
function MJgmgt_notice_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/notice/index.php';
}
function MJgmgt_report_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/report/index.php';
}
function MJgmgt_mail_template_manage()
{
	require_once GMS_PLUGIN_DIR. '/admin/email-template/index.php';
}
function MJgmgt_gnrl_settings()
{
	require_once GMS_PLUGIN_DIR. '/admin/general-settings.php';
}
function MJgmgt_access_right()
{
	require_once GMS_PLUGIN_DIR. '/admin/access_right/index.php';
}
?>