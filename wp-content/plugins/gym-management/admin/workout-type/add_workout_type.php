<?php ?>
<script type="text/javascript">
jQuery(document).ready(function($) 
{
	"use strict";
	$(".display-members").select2();
	$('#workouttype_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});
	
	
	$("#start_date").datepicker(
	{	
        dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
		minDate:0,
        onSelect: function (selected) 
        {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 0);
            $("#end_date").datepicker("option", "minDate", dt);
        }
    });
    $("#end_date").datepicker(
    {		
       dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 0);
            $("#start_date").datepicker("option", "maxDate", dt);
        }
    });
 $("#member_list").select2();
});
</script>
<?php 	
if($active_tab == 'addworkouttype')
{        	
	$workoutmember_id=0;
	$edit=0;
	if(isset($_REQUEST['workoutmember_id']))
	{
		$edit=1;
		$workoutmember_id=esc_attr($_REQUEST['workoutmember_id']);				
		$workout_logdata=MJgmgt_get_userworkout($workoutmember_id);
	}			
	if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
	{
		$edit=1;
	} 
	?>
	<div class="col-md-12"><!--COL 12 DIV STRAT-->
		<div class="panel panel-white"><!--PANEL WHITE DIV STRAT-->
            <div class="panel-body"><!--PANEL BODY DIV STRAT-->
				<form name="workouttype_form" action="" method="post" class="form-horizontal" id="workouttype_form"><!--WORKOUT TYPE FORM STRAT-->
					<?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
					<input type="hidden" name="action" value="<?php echo esc_attr($action);?>">
					<input type="hidden" name="assign_workout_id" value="" />
					<div class="form-group">
						<label class="col-sm-2 control-label" for="day"><?php esc_html_e('Member','gym_mgt');?><span class="require-field">*</span></label>	
						<div class="col-sm-8">
							<?php if($edit){ $member_id=$workoutmember_id; }elseif(isset($_POST['member_id'])){$member_id=sanitize_text_field($_POST['member_id']);}else{$member_id='';}?>
							<select id="member_list" class="display-members assigned_workout_member_id" name="member_id" required="true">
								<option value=""><?php esc_html_e('Select Member','gym_mgt');?></option>
									<?php $get_members = array('role' => 'member');
									$membersdata=get_users($get_members);
									 if(!empty($membersdata))
									 {
										foreach ($membersdata as $member)
										{
											if( $member->membership_status == "Continue")
											{
											?>
											<option value="<?php echo esc_attr($member->ID);?>" <?php selected(esc_attr($member_id),esc_attr($member->ID));?>><?php echo esc_html($member->display_name)." - ".esc_html($member->member_id); ?> </option>
										<?php
											}
										}
									 }?>
						    </select>
						</div>
						<div class="col-sm-2">
							<a href="?page=gmgt_member&tab=addmember" class="btn btn-default"> <?php esc_html_e('Add Member','gym_mgt');?></a>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="level_id"><?php esc_html_e('Level','gym_mgt');?></label>
						<div class="col-sm-8">				
							<select class="form-control" name="level_id" id="level_type">
								<option value=""><?php esc_html_e('Select Level','gym_mgt');?></option>
								<?php
								if(isset($_REQUEST['level_id']))
								{
									$category =esc_attr($_REQUEST['level_id']);  
								}
								elseif($edit)
								{
									$category =$result->level_id;
								}
								else
								{ 
									$category = "";
								}
								$measurmentdata=MJgmgt_get_all_category('level_type');
								if(!empty($measurmentdata))
								{
									foreach ($measurmentdata as $retrive_data)
									{
										echo '<option value="'.esc_attr($retrive_data->ID).'" '.selected(esc_attr($category),esc_attr($retrive_data->ID)).'>'.esc_html($retrive_data->post_title).'</option>';
									}
								}
								?>					
							</select>
						</div>
						<div class="col-sm-2 add_category_padding_0">
							<button id="addremove" model="level_type"><?php esc_html_e('Add Or Remove','gym_mgt');?></button>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="description"><?php esc_html_e('Description','gym_mgt');?></label>
						<div class="col-sm-8">
							<textarea id="description" class="form-control validate[custom[address_description_validation]]" maxlength="150"  name="description"><?php if(isset($_POST['description'])) echo esc_textarea($_POST['description']); ?> </textarea>
						</div>
					</div>
					<!--nonce-->
					<?php wp_nonce_field( 'save_workouttype_nonce' ); ?>
					<!--nonce-->					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="start_date"><?php esc_html_e('Start Date','gym_mgt');?> <span class="require-field">*</span></label>
						<div class="col-sm-3">
							<input id="start_date" class="form-control validate[required]" type="text"  name="start_date" value="<?php if(isset($_POST['start_date'])){ echo esc_attr($_POST['start_date']);}?>" readonly>
						</div>
						<label class="col-sm-2 control-label" for="end_date"><?php esc_html_e('End Date','gym_mgt');?> <span class="require-field">*</span></label>
						<div class="col-sm-3">
							<input id="end_date" class="form-control validate[required]" type="text"   name="last_date" value="<?php if(isset($_POST['end_date'])){ echo esc_attr($_POST['end_date']);}?>" readonly>
						</div>
					</div>	
					<div class="form-group">				
						<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Select Days','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8">			
							<?php
							foreach (MJgmgt_days_array() as $key=>$value)
							{
							?>
							<div class="col-md-3 padding_left_0">
								<div class="checkbox">
								  	<label><input type="checkbox" class="validate[minCheckbox[3]] checkbox" value="" name="day[]" value="<?php echo esc_attr($value);?>" id="<?php echo $key;?>" data-val="day"><?php 
									 if($key =='Sunday')
									{
										echo esc_html_e('Sunday','gym_mgt');
									} 	
									elseif($key =='Monday')
									{
										echo esc_html_e('Monday','gym_mgt');
									} 	
									elseif($key =='Tuesday')
									{
										echo esc_html_e('Tuesday','gym_mgt');
									} 	
									elseif($key =='Wednesday')
									{
										echo esc_html_e('Wednesday','gym_mgt');
									} 	
									elseif($key =='Thursday')
									{
										echo esc_html_e('Thursday','gym_mgt');
									} 	
									elseif($key =='Friday')
									{
										echo esc_html_e('Friday','gym_mgt');
									} 	
									elseif($key =='Saturday')
									{
										echo esc_html_e('Saturday','gym_mgt');
									} 	  
									//echo esc_attr($key);

									   ?> </label>
								</div>
							</div>
							<?php
							}
							?>
						</div>	
					</div>	
					<div class="form-group">					
						<div class="col-sm-12">		
							<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Workout Details','gym_mgt');?><span class="require-field">*</span></label>
							<div class="col-md-10 activity_list member_workout_activity padding_left_0">
								<?php 
								$activity_category=MJgmgt_get_all_category('activity_category');
								if(!empty($activity_category))
								{
									foreach ($activity_category as $retrive_data)
									{	
										?>		
										<label class="activity_title"><strong><?php echo esc_html($retrive_data->post_title); ?></strong></label>					
										<?php 
										$activitydata =MJgmgt_get_activity_by_category($retrive_data->ID);
										foreach($activitydata as $activity)
										{ 
										?>
										<div class="checkbox child">
											<label class="col-sm-2 padding_top_7 padding_bottom_7">
												<input type="checkbox" value="" name="avtivity_id[]" value="<?php echo esc_attr($activity->activity_id);?>" class="activity_check" id="<?php echo esc_attr($activity->activity_id);?>" data-val="activity" activity_title = "<?php echo esc_attr($activity->activity_title); ?>"><?php echo esc_html($activity->activity_title); ?> 
											</label>
											<div id="reps_sets_<?php echo esc_attr($activity->activity_id);?>" class="col-sm-10 padding_0"></div>
										</div>
										<div class="clear"></div>
										<?php 
										}
										?>
										<div class="clear"></div>
										<?php 
									}
								} ?>			
							</div>
						</div>
					</div>				
					<div class="col-sm-offset-2 col-sm-10">
						<div class="form-group">
							<div class="col-md-8">
								<input type="button" value="<?php esc_html_e('Step-1 Add Workout','gym_mgt');?>" name="sadd_workouttype" id="add_workouttype" class="btn btn-success"/>
							</div>
						</div>
					</div>	
					<div id="display_rout_list"></div>
					<div class="col-sm-offset-2 col-sm-8 schedule-save-button ">        	
						<input type="submit" value="<?php if($edit){ esc_html_e('Step-2 Save Workout','gym_mgt'); }else{ esc_html_e('Step-2 Save Workout','gym_mgt');}?>" name="save_workouttype" class="btn btn-success"/>
					</div>
				</form><!--WORKOUT TYPE FORM END-->
            </div><!--PANEL BODY DIV END-->
        </div>	<!--PANEL WHITE DIV END-->
		<?php
		if(isset($workout_logdata))
		foreach($workout_logdata as $row)
		{
			$all_logdata=MJgmgt_get_workoutdata($row->workout_id);
			$arranged_workout=MJgmgt_set_workoutarray($all_logdata);
			?>
			<div class="workout_<?php echo $row->workout_id;?> workout-block"><!--WORKOUT BLOCK DIV START-->
				<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-calendar"></i>
						<?php 
						esc_html_e('Start From ','gym_mgt');
						echo "<span class='work_date'>".MJgmgt_getdate_in_input_box($row->start_date)."</span>";
						esc_html_e(' To ','gym_mgt');
						echo "<span class='work_date'>".MJgmgt_getdate_in_input_box($row->end_date)."</span>";
						if(!empty($row->description))
						{
							esc_html_e('Description : ','gym_mgt');
							echo "<span class='work_date'>".$row->description."</span>";
						}
						?> 
						</h3>	
						<span class="removeworkout badge badge-delete pull-right" id="<?php echo $row->workout_id;?>">X</span>						
				</div>
				<div class="panel panel-white"><!--PANEL WHITE DIV START-->
					<?php
					if(!empty($arranged_workout))
					{
						?>
						<div class="work_out_datalist_header">
						<div class="col-md-2 col-sm-2">  
						<strong><?php esc_html_e('Day Name','gym_mgt');?></strong>
						</div>
						<div class="col-md-10 col-sm-10 hidden-xs">
						<span class="col-md-3"><?php esc_html_e('Activity','gym_mgt');?></span>
						<span class="col-md-3"><?php esc_html_e('Sets','gym_mgt');?></span>
						<span class="col-md-2"><?php esc_html_e('Reps','gym_mgt');?></span>
						<span class="col-md-2"><?php esc_html_e('KG','gym_mgt');?></span>
						<span class="col-md-2"><?php esc_html_e('Rest Time','gym_mgt');?></span>
						</div>
						</div>
						<?php 
						foreach($arranged_workout as $key=>$rowdata)
						{?>
							<div class="work_out_datalist">
								<div class="col-md-2 day_name">  
									<?php
									 if($key =='Sunday')
									 {
										 echo esc_html_e('Sunday','gym_mgt');
									 } 	
									 elseif($key =='Monday')
									 {
										 echo esc_html_e('Monday','gym_mgt');
									 } 	
									 elseif($key =='Tuesday')
									 {
										 echo esc_html_e('Tuesday','gym_mgt');
									 } 	
									 elseif($key =='Wednesday')
									 {
										 echo esc_html_e('Wednesday','gym_mgt');
									 } 	
									 elseif($key =='Thursday')
									 {
										 echo esc_html_e('Thursday','gym_mgt');
									 } 	
									 elseif($key =='Friday')
									 {
										 echo esc_html_e('Friday','gym_mgt');
									 } 	
									 elseif($key =='Saturday')
									 {
										 echo esc_html_e('Saturday','gym_mgt');
									 } 	  
									//echo esc_html($key);?>
								</div>
								<div class="col-md-10 col-xs-12">
									<?php 
									foreach($rowdata as $row)
									{
										echo $row."<div class='clearfix'></div> <br>";
									}
									?>
								</div>
							</div>
					 <?php
						} 
					}?>
			   </div><!--PANEL WHITE DIV END-->
		   </div><!--WORKOUT BLOCK DIV END-->
		<?php   
		}	
}
?>