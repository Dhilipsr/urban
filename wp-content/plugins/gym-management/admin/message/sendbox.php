<div class="mailbox-content"><!--MAILBOX CONTENT DIV STRAT-->
	<table class="table"><!--SENDBOX TABLE STRAT-->
		<thead>
			<tr>
				<th class="text-right" colspan="5">
					<?php 
					$max = 10;
					if(isset($_GET['pg']))
					{
						$p = $_GET['pg'];
					}
					else
					{
						$p = 1;
					}
					$limit = ($p - 1) * $max;
					$prev = $p - 1;
					$next = $p + 1;
					$limits = (int)($p - 1) * $max;
					$totlal_message = MJgmgt_count_send_item(get_current_user_id());
					$totlal_message = ceil($totlal_message / $max);
					$lpm1 = $totlal_message - 1;               	
					$offest_value = ($p-1) * $max;
					echo MJgmgt_pagination($totlal_message,$p,$lpm1,$prev,$next);
					?>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr> 			
				<th class="hidden-xs"><span><?php esc_html_e('Message For','gym_mgt');?></span></th>
				<th class="hidden-xs"><span><?php esc_html_e('Class','gym_mgt');?></span></th>
				<th><?php esc_html_e('Subject','gym_mgt');?></th>
				<th><?php esc_html_e('Description','gym_mgt');?></th>
				<th><?php esc_html_e('Attachment','gym_mgt');?></th>
			</tr>
			<?php 
			//GET SENDBOX DATA
			$offset = 0;
			if(isset($_REQUEST['pg']))
			{
				$offset = esc_attr($_REQUEST['pg']);
			}
			$message = MJgmgt_get_send_message(get_current_user_id(),$max,$offset);
			foreach($message as $msg_post)
			{
				if($msg_post->post_author==get_current_user_id())
				{
				?>
				<tr>
					<td class="hidden-xs">
						<span><?php 
						if(get_post_meta( $msg_post->ID, 'message_for',true) == 'user')
						{
							echo MJgmgt_get_display_name(get_post_meta( $msg_post->ID, 'message_gmgt_user_id',true));
						}
						else
						{							
							$rolename=get_post_meta( $msg_post->ID, 'message_for',true);
							echo MJgmgt_GetRoleName($rolename);
						}
						?>
						</span>
					</td>
					<td class="hidden-xs">
						<span><?php
						if(get_post_meta( $msg_post->ID, 'gmgt_class_id',true) !="" && get_post_meta( $msg_post->ID, 'gmgt_class_id',true) == 'all')
						{
							esc_html_e('All','gym_mgt');
						}
						elseif(get_post_meta( $msg_post->ID, 'gmgt_class_id',true) !="")
						{
							echo MJgmgt_get_class_name(get_post_meta( $msg_post->ID, 'gmgt_class_id',true)); 
						}
						?>
						</span>
					</td>
					
					<td>
					<a href="?page=Gmgt_message&tab=view_message&from=sendbox&id=<?php echo  esc_attr($msg_post->ID);?>"><?php
					$subject_char=strlen($msg_post->post_title);
	                if($subject_char <= 25)
	                {
	                    echo $msg_post->post_title;
	                }
	                else
	                {
	                    $char_limit = 25;
	                    $subject_body= substr(strip_tags($msg_post->post_title), 0, $char_limit)."...";
	                    echo $subject_body;
	                }
					//echo wordwrap($msg_post->post_title,10,"<br>\n",TRUE);
					?><?php 
					if(MJgmgt_count_reply_item($msg_post->ID)>=1){?><span class="badge badge-success pull-right"><?php echo MJgmgt_count_reply_item($msg_post->ID);?></span><?php } ?></a>
				</td>
				 <td>
					<?php 	
					$body_char=strlen($msg_post->post_content);
		            if($body_char <= 60)
		            {
		                echo $msg_post->post_content;
		            }
		            else
		            {
		                $char_limit = 60;
		                $msg_body= substr(strip_tags($msg_post->post_content), 0, $char_limit)."...";
		                echo $msg_body;
		            }				
						//echo wordwrap($msg_post->post_content,30,"<br>\n",TRUE);
					?>
				</td>
					
					
					<td>
					  	<?php
						$attchment=get_post_meta($msg_post->ID,'message_attachment',true);
						if(!empty($attchment))
						{
							$attchment_array=explode(',',$attchment);
							foreach($attchment_array as $attchment_data)
							{
							?>
								<a target="blank" href="<?php echo content_url().'/uploads/gym_assets/'.$attchment_data; ?>" class="btn btn-default"><i class="fa fa-download"></i><?php esc_html_e('View Attachment','gym_mgt');?></a></br>
							<?php
							}
						}
						else
						{
							 esc_html_e('No Attachment','gym_mgt');
						}
						?>
					</td>
				</tr>
				<?php 
				}
			}
			?>			
		</tbody>
	</table><!--SENDBOX TABLE END-->
</div><!--MAILBOX CONTENT DIV END-->