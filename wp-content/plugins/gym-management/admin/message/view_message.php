<script type="text/javascript">
jQuery(document).ready(function($) 
{
	"use strict";
	$('#message-replay').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});

});
</script>
<?php 
$obj_message= new MJgmgt_message; 
//DELETE SENDBOX DATA
if($_REQUEST['from']=='sendbox')
{
	$message = get_post($_REQUEST['id']);
	MJgmgt_change_read_status_reply($_REQUEST['id']);
	$author = $message->post_author;
	$box='sendbox';
	if(isset($_REQUEST['delete']))
	{
		echo esc_attr($_REQUEST['delete']);
		wp_delete_post($_REQUEST['id']);
		wp_safe_redirect(admin_url()."admin.php?page=Gmgt_message&tab=sentbox" );
		exit();
	}
}
//Delete INBOX DATA
if($_REQUEST['from']=='inbox')
{
	$message = MJgmgt_get_message_by_id($_REQUEST['id']);
	$message1 = get_post($message->post_id);
	$author = $message1->post_author;	
	MJgmgt_change_read_status($_REQUEST['id']);
	MJgmgt_change_read_status_reply($message1->ID);
	$box='inbox';
	if(isset($_REQUEST['delete']))
	{
		echo esc_attr($_REQUEST['delete']);
		MJgmgt_delete_message('smgt_message',esc_attr($_REQUEST['id']));
		wp_safe_redirect(admin_url()."admin.php?page=Gmgt_message&tab=inbox" );
		exit();
	}
}
//SAVE Reply MESSAGE DATA
if(isset($_POST['replay_message']))
{
	$message_id=esc_attr($_REQUEST['id']);
	$message_from=esc_attr($_REQUEST['from']);
	$result=$obj_message->MJgmgt_send_replay_message($_POST);
	if($result)
	{
		wp_safe_redirect(admin_url()."admin.php?page=Gmgt_message&tab=view_message&from=".$message_from."&id=$message_id&message=1" );
	}
}
//Delete REPLY MESSAGE DATA
if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete-reply' && isset($_REQUEST['reply_id']))
{
	$message_id=esc_attr($_REQUEST['id']);
	$message_from=esc_attr($_REQUEST['from']);
	$result=$obj_message->MJgmgt_delete_reply($_REQUEST['reply_id']);
	if($result)
	{
		wp_redirect (admin_url().'admin.php?page=Gmgt_message&tab=view_message&action=delete-reply&from='.$message_from.'&id='.$message_id.'&message=2');
	}
}
?>
<div class="mailbox-content"><!--MAILBOX CONTENT DIV STRAT-->
 	<div class="message-header">
		<h3><span><?php esc_html_e('Subject','gym_mgt')?> :</span> <?php if($box=='sendbox'){ echo esc_html($message->post_title); } else{ echo esc_html($message->subject); } ?></h3>
        <p class="message-date"><?php  if($message->date != '') echo MJgmgt_getdate_in_input_box(esc_html($message->date));?></p>
	</div>
	<div class="message-sender">                                
    	<p><?php if($box=='sendbox'){ echo MJgmgt_get_display_name(esc_html($message->post_author)); } else{ echo MJgmgt_get_display_name(esc_html($message->sender)); } ?> <span>&lt;<?php if($box=='sendbox'){ echo MJgmgt_get_emailid_byuser_id(esc_html($message->post_author)); } else{ echo MJgmgt_get_emailid_byuser_id(esc_html($message->sender)); } ?>&gt;</span></p>
    </div>
	<div class="message-content"><!--MESSAGE CONTENT DIV STRAT-->
    	<p>
		<?php 
		/* $receiver_id=0;
		if($box=='sendbox')
		{ 
			echo esc_html($message->post_content); 
			$receiver_id=(get_post_meta($_REQUEST['id'],'message_for_userid',true));
		}
		else
		{ 
			echo esc_html($message->message_body);
			$receiver_id=$message->sender;
		} */
		?>
		
		<?php
		if($box=='sendbox'){
				$message_for=get_post_meta($_REQUEST['id'],'message_for',true);
				echo "From: ".MJgmgt_get_display_name($message->post_author)."<span>&lt;".MJgmgt_get_emailid_byuser_id($message->post_author)."&gt;</span><br>";
				if($message_for == 'user'){
				echo "To: ".MJgmgt_get_display_name(get_post_meta($_REQUEST['id'],'message_for_userid',true))."<span>&lt;".MJgmgt_get_emailid_byuser_id(get_post_meta($_REQUEST['id'],'message_for_userid',true))."&gt;</span><br>";}
				else{
				echo "To: ".esc_html__('Group','gym_mgt');}?>
			<?php } 
			else
			{ 
				echo "From: ".MJgmgt_get_display_name($message->sender)."<span>&lt;".MJgmgt_get_emailid_byuser_id($message->sender)."&gt;</span><br> To: ".MJgmgt_get_display_name($message->receiver);  ?> <span>&lt;<?php echo MJgmgt_get_emailid_byuser_id($message->receiver);?>&gt;</span>
			<?php 
			}?>
		
		</p>
		<!--<div class="message-options pull-right">
			<a class="btn btn-default" href="?page=Gmgt_message&tab=view_message&id=<?php echo esc_attr($_REQUEST['id']);?>&from=<?php echo $box;?>&delete=1" onclick="return confirm('<?php esc_html_e('Are you sure you want to delete this record?','gym_mgt');?>');"><i class="fa fa-trash m-r-xs"></i><?php esc_html_e('Delete','gym_mgt')?></a> 
	   </div>-->
	   
	    <div class="message-content"><!--MESSAGE CONTENT---->
			<p><?php $receiver_id=0;
			if($box=='sendbox'){ 
			echo wordwrap($message->post_content,120,"<br>\n",TRUE);
			$receiver_id=(get_post_meta($_REQUEST['id'],'message_for_userid',true));} else{ echo wordwrap($message->message_body,120,"<br>\n",TRUE);
			$receiver_id=$message->sender;}?></p>
			<div class="message-options pull-right">
				<a class="btn btn-default" href="?page=Gmgt_message&tab=view_message&id=<?php echo $_REQUEST['id'];?>&from=<?php echo $box;?>&delete=1" onclick="return confirm('<?php esc_html_e('Are you sure you want to delete this record?','gym_mgt');?>');"><i class="fa fa-trash m-r-xs"></i><?php esc_html_e('Delete','gym_mgt')?></a> 
		   </div>
		</div>
   	</div><!--MESSAGE CONTENT DIV END-->
    <?php 
	//GET MESSAGE ALL REPLY DATA
	if(isset($_REQUEST['from']) && $_REQUEST['from']=='inbox')
	{
			$allreply_data=$obj_message->MJgmgt_get_all_replies($message->post_id);
	}
	else
	{
			$allreply_data=$obj_message->MJgmgt_get_all_replies($_REQUEST['id']);
	}
	if(!empty($allreply_data))
	{		
		foreach($allreply_data as $reply)
		{
			$receiver_name=MJgmgt_get_receiver_name_array($reply->message_id,$reply->sender_id,$reply->created_date,$reply->message_comment);
			?>
			<div class="message-content"><!--MESSAGE CONTENT DIV STRAT-->
				<p><?php echo esc_html($reply->message_comment);?><br>
					<h5>
						<?php
						_e('Reply By : ','gym_mgt'); 
						echo MJgmgt_get_display_name($reply->sender_id); 
						_e(' || ','gym_mgt'); 	
						_e('Reply To : ','gym_mgt'); 
						echo $receiver_name; 
						_e(' || ','gym_mgt'); 	
						?>
					<span class="timeago" title="<?php echo MJgmgt_gmgtConvertTime($reply->created_date);?>"></span>
					<span class="comment-delete">
					<a href="admin.php?page=Gmgt_message&tab=view_message&action=delete-reply&from=<?php echo $_REQUEST['from'];?>&id=<?php echo esc_attr($_REQUEST['id']);?>&reply_id=<?php echo esc_attr($reply->id);?>"><?php esc_html_e('Delete','gym_mgt');?></a></span>
					</h5> 
				</p>
			</div><!--MESSAGE CONTENT DIV END-->
		<?php
		}
	}
	?>
	<script type="text/javascript">
		$(document).ready(function() 
		{			
			  $('#selected_users').multiselect({ 
				 nonSelectedText :'<?php _e("Select users to reply","gym_mgt");?>',
				 includeSelectAllOption: true           
			 });
			 $("body").on("click","#check_reply_user",function()
			 {
				var checked = $(".dropdown-menu input:checked").length;

				if(!checked)
				{
					alert("<?php esc_html_e('Please select atleast one users to reply','gym_mgt');?>");
					return false;
				}		
			}); 
			$("body").on("click","#replay_message_btn",function()
			 {
				$(".replay_message_div").show();	
				$(".replay_message_btn").hide();	
			});   
		});
		</script>

	</form><!--MESSAGE REPLY FORM END-->
	<form name="message-replay" method="post" id="message-replay"><!--MESSAGE REPLAY FORM---->
			<input type="hidden" name="message_id" value="<?php if($_REQUEST['from']=='sendbox') echo esc_attr($_REQUEST['id']); else echo esc_attr($message->post_id);?>">
			<input type="hidden" name="user_id" value="<?php echo get_current_user_id();?>">
			<!-- <input type="hidden" name="receiver_id" value="<?php echo esc_attr($receiver_id);?>"> -->
			<?php
			global $wpdb;
			$tbl_name = $wpdb->prefix .'gmgt_message';
			$current_user_id=get_current_user_id();
			if((string)$current_user_id == $author)
			{		
				if($_REQUEST['from']=='sendbox')
				{
					$msg_id=$_REQUEST['id']; 
					$msg_id_integer=(int)$msg_id;
					$reply_to_users =$wpdb->get_results("SELECT *  FROM $tbl_name where post_id = $msg_id_integer");			
				}
				else
				{
					$msg_id=$message->post_id;			
					$msg_id_integer=(int)$msg_id;
					$reply_to_users =$wpdb->get_results("SELECT *  FROM $tbl_name where post_id = $msg_id_integer");			
				}		
			}
			else
			{
				$reply_to_users=array();
				$reply_to_users[]=(object)array('receiver'=>$author);
			}
			?>
			
			<div class="message-options pull-right">
				<button type="button" name="replay_message_btn" class="btn btn-default replay_message_btn" id="replay_message_btn"><i class="fa fa-reply m-r-xs"></i><?php esc_html_e('Reply','gym_mgt')?></button>
		 	</div>

			<div class="message-content float_left_width_100 replay_message_div" style="display:none;" >
				<div class="col-sm-12">
					<div class="form-group" >
						<label class="col-sm-3 control-label" ><?php _e('Select users to reply','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-9" style="margin-bottom:20px;">			
							<select name="receiver_id[]" class="form-control" id="selected_users" multiple="true">
								<?php						
								foreach($reply_to_users as $reply_to_user)
								{  	
									$user_data=get_userdata($reply_to_user->receiver);
									if(!empty($user_data))
									{								
										if($reply_to_user->receiver != get_current_user_id())
										{
											?>
											<option  value="<?php echo $reply_to_user->receiver;?>" ><?php echo MJgmgt_get_display_name($reply_to_user->receiver); ?></option>
											<?php
										}
									}							
								} 
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="photo"><?php _e('Message Comment','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-8" style="margin-bottom: 10px;">
							<textarea name="replay_message_body" maxlength="150" id="replay_message_body" class="validate[required] form-control text-input"></textarea>
						</div>
					</div>
				</div>	  
			  	<div class="message-options pull-right reply-message-btn">
					<button type="submit" name="replay_message" class="btn btn-success" id="check_reply_user"><?php esc_html_e('Send Message','gym_mgt')?></button>
				
			 	</div>
			</div>
		</form>
	
</div><!--MAILBOX CONTENT DIV END-->