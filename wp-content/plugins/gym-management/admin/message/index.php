<?php 
$obj_message= new MJgmgt_message;
//SAVE MESSAGE DATA
if(isset($_POST['save_message']))
{
	$nonce = $_POST['_wpnonce'];
	if (wp_verify_nonce( $nonce, 'save_message_nonce' ) )
	{
		$created_date = date("Y-m-d H:i:s");
		$subject = MJgmgt_strip_tags_and_stripslashes(sanitize_text_field($_POST['subject']));
		$message_body = MJgmgt_strip_tags_and_stripslashes(sanitize_text_field($_POST['message_body']));
		$created_date = date("Y-m-d H:i:s");
		$tablename="Gmgt_message";
		$role=$_POST['receiver'];
		if(isset($_REQUEST['class_id']))
		$class_id = esc_attr($_REQUEST['class_id']);
		$upload_docs_array=array();
		if(!empty($_FILES['message_attachment']['name']))
		{
			$count_array=count($_FILES['message_attachment']['name']);
			for($a=0;$a<$count_array;$a++)
			{			
				foreach($_FILES['message_attachment'] as $image_key=>$image_val)
				{		
					$document_array[$a]=array(
					'name'=>$_FILES['message_attachment']['name'][$a],
					'type'=>$_FILES['message_attachment']['type'][$a],
					'tmp_name'=>$_FILES['message_attachment']['tmp_name'][$a],
					'error'=>$_FILES['message_attachment']['error'][$a],
					'size'=>$_FILES['message_attachment']['size'][$a]
					);							
				}
			}				
			foreach($document_array as $key=>$value)		
			{	
				$get_file_name=$document_array[$key]['name'];
				$upload_docs_array[]=$obj_message->MJgmgt_load_multiple_documets($value,$value,$get_file_name);				
			} 				
		}
		$upload_docs_array_filter=array_filter($upload_docs_array);	
		if(!empty($upload_docs_array_filter))
		{
			$attachment=implode(',',$upload_docs_array_filter);
		}
		else
		{
			$attachment='';
		}
		if($role == 'member' || $role == 'staff_member' || $role == 'accountant')
		{			
			$userdata=MJgmgt_get_user_notice($role,esc_attr($_REQUEST['class_id']));
			if(!empty($userdata))
			{		
				$mail_id = array();
				$i = 0;
				foreach($userdata as $user)
				{
					if($role == 'parent' && $class_id != 'all')
					{
						$mail_id[]=$user['ID'];
					}
					else
					{ 
						$mail_id[]=$user->ID;						
					}
					$i++;
				}
				$post_id = wp_insert_post( array(
					'post_status' => 'publish',
					'post_type' => 'message',
					'post_title' => $subject,
					'post_content' =>$message_body
				) );	
				foreach($mail_id as $user_id)
				{
					$reciever_id = $user_id;
					$message_data=array('sender'=>get_current_user_id(),
						'receiver'=>$user_id,
						'subject'=>$subject,
						'message_body'=>$message_body,
						'date'=>$created_date,
						'status' =>0,
						'post_id' =>$post_id
					);
					MJgmgt_insert_record($tablename,$message_data);
					//-----MESSAGE SEND NOTIFICATION-------
					$gymname=get_option( 'gmgt_system_name' );
					$userdata = get_userdata($user_id);
					$senderuserdata = get_userdata(get_current_user_id());
					$page_link=home_url().'/?dashboard=user&page=message&tab=inbox';
					$arr['[GMGT_RECEIVER_NAME]']=$userdata->display_name;	
					$arr['[GMGT_GYM_NAME]']=$gymname;
					$arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;
					$arr['[GMGT_MESSAGE_CONTENT]']=$message_body;
					$arr['[GMGT_MESSAGE_LINK]']=$page_link;
					$subject =get_option('message_received_subject');
					$sub_arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;;
					$sub_arr['[GMGT_GYM_NAME]']=$gymname;
					$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
					$message_template = get_option('message_received_template');	
					$message_replacement = MJgmgt_string_replacemnet($arr,$message_template);
					$to=$userdata->user_email;
					MJgmgt_send_mail($to,$subject,$message_replacement);
				}
					$result=add_post_meta($post_id, 'message_for',$role);
					$result=add_post_meta($post_id, 'gmgt_class_id',$_REQUEST['class_id']);
					$result=add_post_meta($post_id, 'message_attachment',$attachment);
			}
			else
			{
				$post_id = wp_insert_post( array(
					'post_status' => 'publish',
					'post_type' => 'message',
					'post_title' => $subject,
					'post_content' =>$message_body
				
				) );
				$user_id =$_POST['receiver'];
				$message_data=array('sender'=>get_current_user_id(),
					'receiver'=>$user_id,
					'subject'=>$subject,
					'message_body'=>$message_body,
					'date'=>$created_date,
					'status' =>0,
					'post_id' =>$post_id
				);
				MJgmgt_insert_record($tablename,$message_data);
				$result=add_post_meta($post_id, 'message_for','user');
				$result=add_post_meta($post_id, 'message_gmgt_user_id',$user_id);
				$result=add_post_meta($post_id, 'message_attachment',$attachment);
				//-----MESSAGE SEND NOTIFICATION-------
					$gymname=get_option( 'gmgt_system_name' );
					$userdata = get_userdata($user_id);
					$senderuserdata = get_userdata(get_current_user_id());
					$page_link=home_url().'/?dashboard=user&page=message&tab=inbox';
					$arr['[GMGT_RECEIVER_NAME]']=$userdata->display_name;	
					$arr['[GMGT_GYM_NAME]']=$gymname;
					$arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;
					$arr['[GMGT_MESSAGE_CONTENT]']=$message_body;
					$arr['[GMGT_MESSAGE_LINK]']=$page_link;
					$subject =get_option('message_received_subject');
					$sub_arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;;
					$sub_arr['[GMGT_GYM_NAME]']=$gymname;
					$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
					$message_template = get_option('message_received_template');	
					$message_replacement = MJgmgt_string_replacemnet($arr,$message_template);
						$to=$userdata->user_email;
							MJgmgt_send_mail($to,$subject,$message_replacement);
			}
		}
		else
		{
			$user_id =sanitize_text_field($_POST['receiver']);
			$post_id = wp_insert_post( array(
				'post_status' => 'publish',
				'post_type' => 'message',
				'post_title' => $subject,
				'post_content' =>$message_body
			) );
			$message_data=array('sender'=>get_current_user_id(),
				'receiver'=>$user_id,
				'subject'=>$subject,
				'message_body'=>$message_body,
				'date'=>$created_date,
				'status' =>0,
				'post_id' =>$post_id
			);
			MJgmgt_insert_record($tablename,$message_data);
			$result=add_post_meta($post_id, 'message_for','user');
			$result=add_post_meta($post_id, 'message_gmgt_user_id',$user_id);
			$result=add_post_meta($post_id, 'message_attachment',$attachment);
			//-----MESSAGE SEND NOTIFICATION-------
			$gymname=get_option( 'gmgt_system_name' );
			$userdata = get_userdata($user_id);
			$senderuserdata = get_userdata(get_current_user_id());
			$page_link=home_url().'/?dashboard=user&page=message&tab=inbox';
			$arr['[GMGT_RECEIVER_NAME]']=$userdata->display_name;	
			$arr['[GMGT_GYM_NAME]']=$gymname;
			$arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;
			$arr['[GMGT_MESSAGE_CONTENT]']=$message_body;
			$arr['[GMGT_MESSAGE_LINK]']=$page_link;
			$subject =get_option('message_received_subject');
			$sub_arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;;
			$sub_arr['[GMGT_GYM_NAME]']=$gymname;
			$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
			$message_template = get_option('message_received_template');	
			$message_replacement = MJgmgt_string_replacemnet($arr,$message_template);
				$to=$userdata->user_email;
					MJgmgt_send_mail($to,$subject,$message_replacement);	
		}	
	}
}
$active_tab=isset($_REQUEST['tab'])?$_REQUEST['tab']:'inbox';
?>
<script type="text/javascript">
$(document).ready(function()
{
	"use strict";
	$('.sdate').datepicker({dateFormat: "yy-mm-dd"}); 
	$('.edate').datepicker({dateFormat: "yy-mm-dd"});  
} );
</script>
<div class="page-inner min_height_1631"><!--PAGE INNER DIV STRAT-->
	<div class="page-title">
		<h3>
			<img src="<?php echo get_option( 'gmgt_system_logo' ) ?>" class="img-circle head_logo" width="40" height="40" /><?php echo get_option( 'gmgt_system_name' );?>
		</h3>
	</div>
	<?php
	if(isset($result))
	{ ?>
		<div id="message" class="updated below-h2">
			<p><?php esc_html_e('Message Sent Successfully!','gym_mgt');?></p>
		</div>
	<?php
	}	
	?>
	<div id="main-wrapper"><!-- Main-wrapper START -->
		<div class="row mailbox-header">
            <div class="col-md-2 col-sm-3 col-xs-4">
                <a class="btn btn-success btn-block" href="?page=Gmgt_message&tab=compose"><?php esc_html_e('Compose','gym_mgt');?></a>
            </div>
            <div class="col-md-10 col-sm-9 col-xs-8">
                <h2><?php
					if(!isset($_REQUEST['tab']) || ($_REQUEST['tab'] == 'inbox'))
					{
                         echo esc_html__('Inbox','gym_mgt');
					}
					elseif(isset($_REQUEST['page']) && $_REQUEST['tab'] == 'sentbox')
					{
						echo esc_html__('Sent Item','gym_mgt');
					}
					elseif(isset($_REQUEST['page']) && $_REQUEST['tab'] == 'compose')
					{
						echo esc_html__('Compose','gym_mgt');
					}
					?>
				</h2>
            </div>                               
        </div>		
		<div class="col-md-2">
			<ul class="list-unstyled mailbox-nav">
				<li <?php if(!isset($_REQUEST['tab']) || ($_REQUEST['tab'] == 'inbox')){?>class="active"<?php }?>>
					<a href="?page=Gmgt_message&tab=inbox"><i class="fa fa-inbox"></i> <?php esc_html_e('Inbox','gym_mgt');?><span class="badge font_weight_700 badge-success pull-right">
					<?php 
					echo MJgmgt_count_unread_message(get_current_user_id());
					?>
					</span></a>
				</li>
				<li <?php if(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'sentbox'){?>class="active"<?php }?>><a href="?page=Gmgt_message&tab=sentbox"><i class="fa fa-sign-out"></i><?php esc_html_e('Sent','gym_mgt');?></a>
				</li>                 
			</ul>
		</div>
		<div class="col-md-10">
		 <?php  
			if(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'sentbox')
			{
				require_once GMS_PLUGIN_DIR. '/admin/message/sendbox.php';
			}
			if(!isset($_REQUEST['tab']) || ($_REQUEST['tab'] == 'inbox'))
			{
				require_once GMS_PLUGIN_DIR. '/admin/message/inbox.php';
			}
			if(isset($_REQUEST['tab']) && ($_REQUEST['tab'] == 'compose'))
			{
				require_once GMS_PLUGIN_DIR. '/admin/message/composemail.php';
			}
			if(isset($_REQUEST['tab']) && ($_REQUEST['tab'] == 'view_message'))
			{
				require_once GMS_PLUGIN_DIR. '/admin/message/view_message.php';
			}
			?>
		</div>
	</div><!-- Main-wrapper END -->
</div><!-- Page-inner END-->