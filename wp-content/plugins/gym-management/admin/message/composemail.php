<script type="text/javascript">
$(document).ready(function() 
{
	"use strict";
	$('#message_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});	
	$('[data-toggle="tooltip"]').tooltip(); 
	jQuery("body").on("change", ".input-file[type=file]", function ()
	{ 
		var file = this.files[0]; 		
		var ext = $(this).val().split('.').pop().toLowerCase(); 
		//Extension Check 
		if($.inArray(ext, [,'pdf','doc','docx','xls','xlsx','ppt','pptx','gif','png','jpg','jpeg','']) == -1)
		{
			 alert('<?php _e("Only pdf,doc,docx,xls,xlsx,ppt,pptx,gif,png,jpg,jpeg formate are allowed. '  + ext + ' formate are not allowed.","gym_mgt") ?>');
			$(this).replaceWith('<input class="btn_top input-file" name="message_attachment[]" type="file" />');
			return false; 
		} 
		//File Size Check 
		if (file.size > 20480000) 
		{
			alert("<?php esc_html_e('Too large file Size. Only file smaller than 10MB can be uploaded.','gym_mgt');?>");
			$(this).replaceWith('<input class="btn_top input-file" name="message_attachment[]" type="file" />'); 
			return false; 
		}
	}); 
	$('.onlyletter_number_space_validation').keypress(function( e ) 
	{     
		var regex = new RegExp("^[0-9a-zA-Z \b]+$");
		var key = String.fromCharCode(!event.charCode ? event.which: event.charCode);
		if (!regex.test(key)) 
		{
			event.preventDefault();
			return false;
		} 
   }); 
} );
function add_new_attachment()
{
	$(".attachment_div").append('<div class="form-group"><label class="col-sm-2 control-label" for="photo"><?php esc_html_e('Attachment','gym_mgt');?></label><div class="col-sm-4"><input  class="btn_top input-file" name="message_attachment[]" type="file" /></div><div class="col-sm-2"><input type="button" value="<?php esc_html_e('Delete','gym_mgt');?>" onclick="delete_attachment(this)" class="remove_cirtificate doc_label btn btn-danger"></div></div>');
}
function delete_attachment(n)
{
	n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);				
}
</script>
<div class="mailbox-content"><!--MAILBOX CONTENT DIV STRAT-->
	<h2>
		<?php  
		$edit=0;
		if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
		{
			 echo esc_html__( 'Edit Message', 'gym_mgt');
			 $edit=1;
			 $exam_data= get_exam_by_id($_REQUEST['exam_id']);
		}
		?>
	</h2>
	<?php
	if(isset($message))
	{
		echo '<div id="message" class="updated below-h2"><p>'.esc_html($message).'</p></div>';
	}
	?>
	<form name="class_form" action="" method="post" class="form-horizontal padding_bottom_50" id="message_form" enctype="multipart/form-data"><!--COMPOSE MAIL FORM STRAT-->
		<?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo esc_attr($action);?>">
		<div class="form-group">
			<label class="col-sm-2 control-label" for="to"><?php esc_html_e('Message To','gym_mgt');?><span class="require-field">*</span></label>
				<div class="col-sm-8">
					<select name="receiver" class="form-control validate[required] text-input receiver message_to" id="to">
						<option value="member"><?php esc_html_e('Members','gym_mgt');?></option>	
						<option value="staff_member"><?php esc_html_e('Staff Members','gym_mgt');?></option>
						<option value="accountant"><?php esc_html_e('Accountants','gym_mgt');?></option>	
						<?php echo MJgmgt_get_all_user_in_message();?>
					</select>
				</div>	
		</div>
		<!--nonce-->
		<?php wp_nonce_field( 'save_message_nonce' ); ?>
		<!--nonce-->								
		<div id="smgt_select_class" class="display_class_css">
			<div class="form-group">
				<label class="col-sm-2 control-label " for="sms_template"><?php esc_html_e('Select Class','gym_mgt');?></label>
				<div class="col-sm-8">
					 <select name="class_id"  id="class_list" class="form-control">
						<option value="all"><?php esc_html_e('All','gym_mgt');?></option>
						<?php
						  foreach(MJgmgt_get_allclass() as $classdata)
						  {  
						  ?>
						   <option  value="<?php echo esc_attr($classdata['class_id']);?>"><?php echo esc_html($classdata['class_name']);?></option>
					 <?php }?>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="subject"><?php esc_html_e('Subject','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			   	<input id="subject" class="form-control validate[required,custom[popup_category_validation]] text-input onlyletter_number_space_validation" maxlength="100" type="text" name="subject" >
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="subject"><?php esc_html_e('Message Comment','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			  	<textarea name="message_body" id="message_body" class="form-control validate[required,custom[address_description_validation]] text-input" maxlength="500"></textarea>
			</div>
		</div>
		<div id="hmsg_message_sent" class="hmsg_message_none">
			<div class="form-group">
				<label class="col-sm-2 control-label" for="sms_template"><?php esc_html_e('SMS Text','gym_mgt');?><span class="require-field">*</span></label>
				<div class="col-sm-8">
					<textarea name="sms_template" class="form-control validate[required,custom[address_description_validation]]" maxlength="160"></textarea>
					<label><?php esc_html_e('Max. 160 Character','gym_mgt');?></label>
				</div>
			</div>
		</div>	
		<div class="attachment_div">
			<div class="form-group">
				<label class="col-sm-2 control-label" for="photo"><?php esc_html_e('Attachment','gym_mgt');?></label>
				<div class="col-sm-4">
				 	<input  class="btn_top input-file" name="message_attachment[]" type="file" />
				</div>										
			</div>							
       	</div>	
		<div class="form-group">		
			<div class="col-sm-offset-2 col-sm-10">
				<input type="button" value="<?php esc_html_e('Add More Attachment','gym_mgt') ?>"  onclick="add_new_attachment()" class="btn more_attachment">
			</div>	
		</div>		
		<div class="form-group">
			<div class="col-sm-10">
				<div class="pull-right">
				<input type="submit" value="<?php if($edit){ esc_html_e('Save Message','gym_mgt'); }else{ esc_html_e('Send Message','gym_mgt');}?>" name="save_message" class="btn btn-success"/>
				</div>
			</div>
		</div>
	</form><!--COMPOSE MAIL FORM END-->
</div><!--MAILBOX CONTENT DIV END-->