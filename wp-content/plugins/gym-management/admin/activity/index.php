<?php 
$obj_membership=new MJgmgt_membership;
$obj_activity=new MJgmgt_activity;
$active_tab = isset($_GET['tab'])?$_GET['tab']:'activitylist';
?>
<!-- POP up code -->
<div class="popup-bg z_index_100000">
    <div class="overlay-content">
		<div class="modal-content">
		   <div class="category_list"> </div>
        </div>
    </div> 
</div>
<!-- End POP-UP Code -->
<div class="page-inner min_height_1631"><!-- PAGE INNER DIV START-->
    <div class="page-title">
		<h3><img src="<?php echo get_option( 'gmgt_system_logo' ) ?>" class="img-circle head_logo" width="40" height="40" /><?php echo get_option( 'gmgt_system_name' );?></h3>
	</div>
	<?php 
	//SAVE Activity DATA
	if(isset($_POST['save_activity']))
	{
		$nonce = $_POST['_wpnonce'];
		if (wp_verify_nonce($nonce, 'save_activity_nonce' ) )
		{
			if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
			{				
				$result=$obj_activity->MJgmgt_add_activity($_POST);
				if($result)
				{
					wp_redirect ( admin_url().'admin.php?page=gmgt_activity&tab=activitylist&message=2');
				}
			}
			else
			{
				$result=$obj_activity->MJgmgt_add_activity($_POST);
				if($result)
				{
					wp_redirect ( admin_url().'admin.php?page=gmgt_activity&tab=activitylist&message=1');
				}
			}
		}
	}
	//Delete ACTIVITY DATA
	if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
	{	
		$result=$obj_activity->MJgmgt_delete_activity($_REQUEST['activity_id']);
		if($result)
		{
			wp_redirect( admin_url().'admin.php?page=gmgt_activity&tab=activitylist&message=3');
		}
	}
	//selected activity delete//
	if(isset($_REQUEST['delete_selected']))
    {		
		if(!empty($_REQUEST['selected_id']))
		{
			foreach($_REQUEST['selected_id'] as $id)
			{
				$delete_activity=$obj_activity->MJgmgt_delete_activity($id);
			}
			if($delete_activity)
			{
				wp_redirect( admin_url().'admin.php?page=gmgt_activity&tab=activitylist&message=3');
			}
		}
        else
		{
			echo '<script language="javascript">';
            echo 'alert("'.esc_html__('Please select at least one record.','gym_mgt').'")';
            echo '</script>';
		}
	}
	if(isset($_REQUEST['message']))
	{
		$message =esc_attr($_REQUEST['message']);
		if($message == 1)
		{?>
			<div id="message" class="updated below-h2 ">
			<p><?php esc_html_e('Activity added successfully.','gym_mgt');?></p>
			</div>
			<?php 			
		}
		elseif($message == 2)
		{?>
			<div id="message" class="updated below-h2 ">
				<p><?php esc_html_e("Activity updated successfully.",'gym_mgt'); ?></p>
			</div>
			<?php
		}
		elseif($message == 3) 
		{?>
			<div id="message" class="updated below-h2">
				<p><?php esc_html_e('Activity deleted successfully.','gym_mgt');?></p>
			</div>
			<?php				
		}
	}
	?>
	<div id="main-wrapper"><!-- MAIN WRAPPER DIV START-->   
		<div class="row"><!-- ROW DIV START-->   
			<div class="col-md-12"><!-- COL 12 DIV START-->   
				<div class="panel panel-white"><!-- PANEL WHITE DIV START-->   
					<div class="panel-body"><!-- PANEL BODY DIV START-->   
						<h2 class="nav-tab-wrapper"><!-- NAV TAB WRAPPER MENU START-->   
							<a href="?page=gmgt_activity&tab=activitylist" class="nav-tab <?php echo $active_tab == 'activitylist' ? 'nav-tab-active' : ''; ?>"><?php echo '<span class="dashicons dashicons-menu"></span> '.esc_html__('Activity List', 'gym_mgt'); ?>
							</a>
							<?php
							if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
							{?>
								<a href="?page=gmgt_activity&tab=addactivity&&action=edit&activity_id=<?php echo esc_attr($_REQUEST['activity_id']);?>" class="nav-tab <?php echo $active_tab == 'addactivity' ? 'nav-tab-active' : ''; ?>"><?php esc_html_e('Edit Activity', 'gym_mgt'); ?></a>  
							<?php
							}
							else
							{?>
								<a href="?page=gmgt_activity&tab=addactivity" class="nav-tab <?php echo $active_tab == 'addactivity' ? 'nav-tab-active' : ''; ?>"><?php echo '<span class="dashicons dashicons-plus-alt"></span> '.esc_html__('Add Activity', 'gym_mgt'); ?></a>
							<?php
							}
							if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'view')
							{?>
								<a href="?page=gmgt_activity&tab=view_membership&&action=view&activity_id=<?php echo esc_attr($_REQUEST['activity_id']);?>" class="nav-tab <?php echo $active_tab == 'view_membership' ? 'nav-tab-active' : ''; ?>"> <?php echo '<span class="dashicons dashicons-menu"></span> '.esc_html__('View Membership', 'gym_mgt'); ?>
							</a>
							<?php 
							}
							?>
						</h2><!-- NAV TAB WRAPPER MENU END-->  
						<?php 							
						if($active_tab == 'activitylist')
						{ 
						?>	
							<script type="text/javascript">
								$(document).ready(function() 
								{
									"use strict";
									jQuery('#activity_list').DataTable({
										"responsive": true,
										"order": [[ 1, "asc" ]],
										"aoColumns":[
													  {"bSortable": false},
													  {"bSortable": true},
													  {"bSortable": true},
													  {"bSortable": true},
													  {"bSortable": false}],
											language:<?php echo MJgmgt_datatable_multi_language();?>
										});
									$('.select_all').on('click', function(e)
									{
										 if($(this).is(':checked',true))  
										 {
											$(".sub_chk").prop('checked', true);  
										 }  
										 else  
										 {  
											$(".sub_chk").prop('checked',false);  
										 } 
									});
								
									$('.sub_chk').on('change',function()
									{
										if(false == $(this).prop("checked"))
										{ 
											$(".select_all").prop('checked', false); 
										}
										if ($('.sub_chk:checked').length == $('.sub_chk').length )
										{
											$(".select_all").prop('checked', true);
										}
								  });
								});
							</script>
							<form name="wcwm_report" action="" method="post"><!-- ACTIVITY FORM START-->
								<div class="panel-body"><!-- PANEL BODY DIV START-->
									<div class="table-responsive"><!-- TABLE RESPONSIVE DIV START-->
										<table id="activity_list" class="display" cellspacing="0" width="100%"><!-- TABLE ACTIVITY LIST START-->
											<thead>
												<tr>
													<th><input type="checkbox" class="select_all"></th>
													<th><?php esc_html_e('Activity Name','gym_mgt') ;?></th>
													<th><?php esc_html_e('Activity Category','gym_mgt') ;?></th>
													<th><?php esc_html_e('Activity Trainer','gym_mgt');?></th>
													<th><?php esc_html_e('Action','gym_mgt' );?></th>
												</tr>
											</thead>
											<tfoot>
												<tr>
												   <th></th>
													<th><?php esc_html_e('Activity Name','gym_mgt');?></th>
													<th><?php esc_html_e('Activity Category','gym_mgt') ;?></th>
													<th><?php esc_html_e('Activity Trainer','gym_mgt');?></th>
													<th><?php esc_html_e('Action','gym_mgt');?></th>
												</tr>
											</tfoot>
											<tbody>
											<?php
											$activitydata=$obj_activity->MJgmet_all_activity();
											if(!empty($activitydata))
											{
												foreach ($activitydata as $retrieved_data){ ?>
												<tr>
												   	<td class="title">
												   		<input type="checkbox" name="selected_id[]" class="sub_chk" value="<?php echo esc_attr($retrieved_data->activity_id); ?>">
												   	</td>
													<td class="activityname">
														<a href="?page=gmgt_activity&tab=addactivity&action=edit&activity_id=<?php echo esc_attr($retrieved_data->activity_id);?>"><?php echo esc_html($retrieved_data->activity_title);?></a>
													</td>
													<td class="category"><?php echo get_the_title($retrieved_data->activity_cat_id);?>
													</td>
													<td class="productquentity">
													<?php 
													if(!empty($retrieved_data->activity_assigned_to))
													{
														$user=get_userdata($retrieved_data->activity_assigned_to);
														if(!empty($user->display_name))
														{
															echo esc_html($user->display_name);
														}
														else
														{
															echo "-";
														}
													}
													else
													{
														echo "-";
													}
													?>
													</td>
													<td class="action"> 
														<a href="?page=gmgt_activity&tab=view_membership&action=view&activity_id=<?php echo esc_attr($retrieved_data->activity_id)?>" class="btn btn-success"> <?php esc_html_e('View Membership', 'gym_mgt' ) ;?>
														</a>
														<a href="?page=gmgt_activity&tab=addactivity&action=edit&activity_id=<?php echo esc_attr($retrieved_data->activity_id)?>" class="btn btn-info"> <?php esc_html_e('Edit', 'gym_mgt' ) ;?>
														</a>
														<a href="?page=gmgt_activity&tab=activitylist&action=delete&activity_id=<?php echo esc_attr($retrieved_data->activity_id);?>" class="btn btn-danger" onclick="return confirm('<?php esc_html_e('Do you really want to delete this record?','gym_mgt');?>');"><?php esc_html_e( 'Delete', 'gym_mgt' ) ;?> </a>
													</td>
												</tr>
												<?php } 
											}?>
											</tbody>
										</table><!-- TABLE ACTIVITY LIST END-->
										<div class="print-button pull-left">
											 <input  type="submit" value="<?php esc_html_e('Delete Selected','gym_mgt');?>" name="delete_selected" class="btn btn-danger delete_selected "/>
										</div>
									</div><!-- TABLE RESPONSIVE DIV END-->
								</div><!-- PANEL BODY DIV END-->
							</form><!-- ACTIVITY FORM END-->
							 <?php 
							}
							if($active_tab == 'addactivity')
							{
							   require_once GMS_PLUGIN_DIR. '/admin/activity/add_activity.php';
							}
							if($active_tab == 'view_membership')
							{
							   require_once GMS_PLUGIN_DIR. '/admin/activity/view_membership.php';
							}
							?>
					</div><!-- PANEL BODY DIV END-->
				</div><!-- PANEL WHITE DIV END-->
			</div><!-- COL 12 DIV END-->
		</div><!-- ROW DIV END-->
	</div><!-- MAIN WRAPPER DIV END-->
</div><!-- PAGE INNER DIV END-->