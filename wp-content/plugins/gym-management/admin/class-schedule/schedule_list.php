<?php ?>
<script type="text/javascript">
$(document).ready(function() 
{
	"use strict";
	$('#group_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});
});
</script>
<?php
if($active_tab == 'schedulelist')
{
?>
	<div class="panel-body"><!-- PANEL BODY DIV START-->
		<table class="table table-bordered"><!-- TABLE CLASS SCHEDULE START-->
			<?php		   
			foreach(MJgmgt_days_array() as $daykey => $dayname)
			{
			?>
				<tr>
					<th width="100"><?php echo esc_html($dayname);?></th>
					<td>
						 <?php
							$period = $obj_class->MJgmgt_get_schedule_byday($daykey);
							if(!empty($period))
							{
								foreach($period as $period_data)
								{
									if(!empty($period_data))
									{
										echo '<div class="btn-group m-b-sm">';
										echo '<button class="btn btn-primary dropdown-toggle" aria-expanded="false" data-toggle="dropdown"><span class="period_box" id='.esc_attr($period_data['class_id']).'>'.MJgmgt_get_single_class_name($period_data['class_id']);
										echo '<span class="time"> ('.MJgmgt_timeremovecolonbefoream_pm($period_data['start_time']).' - '.MJgmgt_timeremovecolonbefoream_pm($period_data['end_time']).') </span>';
										echo '</span></span><span class="caret"></span></button>';
										echo '<ul role="menu" class="dropdown-menu">
											<li><a href="?page=gmgt_class&tab=addclass&action=edit&class_id='.$period_data['class_id'].'">'.esc_html__('Edit','gym_mgt').'</a></li>
											<li><a href="?page=gmgt_class&tab=schedulelist&action=delete&class_id='.$period_data['class_id'].'" onclick="return confirm(\'Are you sure, you want to delete?\')">'.esc_html__('Delete','gym_mgt').'</a></li>
										</ul>';
										echo '</div>';
									}				
								}
							}
						 ?>
					</td>
				</tr>
			<?php	
			}
			?>
        </table><!-- TABLE CLASS SCHEDULE END-->
    </div><!-- PANEL BODY DIV END-->
<?php 
}
?>