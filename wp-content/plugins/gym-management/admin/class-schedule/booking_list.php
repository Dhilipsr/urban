<?php
//Selected CLASS DATA Delete	
if(isset($_REQUEST['delete_selected_booking_list']))
{
	if(!empty(esc_attr($_REQUEST['selected_id'])))
	{
		foreach($_REQUEST['selected_id'] as $id)
		{
			$delete_class=$obj_class->MJgmgt_delete_booked_class($id);
			if($delete_class)
			{
				wp_redirect ( admin_url().'admin.php?page=gmgt_class&tab=booking_list&message=4');
			}
		}
	}
	else
	{
		echo '<script language="javascript">';
		echo 'alert("'.esc_html__('Please select at least one record.','gym_mgt').'")';
		echo '</script>';
	}
}
if($active_tab == 'booking_list')
{
	$class_cancel_booking=get_option('gym_class_cancel_booking');
?>
<!-- POP up code -->
<div class="popup-bg">
	<div class="overlay-content">
		<div class="modal-content">
			<div class="category_list"></div>	
		</div>
	</div> 
</div>
<!-- End POP-UP Code -->
<script type="text/javascript">
	$(document).ready(function() 
	{
		"use strict";
		jQuery('.booking_list').DataTable({
			"responsive": true,
			"order": [[ 2, "desc" ]],
			"aoColumns":[
						  {"bSortable": false},
						  {"bSortable": true},
						  {"bSortable": true},
						  {"bSortable": true},
						  {"bSortable": true},
						  {"bSortable": true},
						  {"bSortable": true},
						  {"bSortable": true},
						  {"bSortable": false}],
				language:<?php echo MJgmgt_datatable_multi_language();?>			  
			});
		$('.select_all').on('click', function(e)
		{
			 if($(this).is(':checked',true))  
			 {
				$(".sub_chk").prop('checked', true);  
			 }  
			 else  
			 {  
				$(".sub_chk").prop('checked',false);  
			 } 
		});
		$('.sub_chk').change(function()
		{
			if(false == $(this).prop("checked"))
			{ 
				$(".select_all").prop('checked', false); 
			}
			if ($('.sub_chk:checked').length == $('.sub_chk').length )
			{
				$(".select_all").prop('checked', true);
			}
		});
	} );
</script>
<form name="wcwm_report" action="" method="post">
<div class="panel-body"> <!-- PANEL BODY DIV START-->
	<div class="table-responsive"> <!-- TABLE RESPONSIVE DIV START-->
	    <table id="booking_list113" class="display booking_list" cellspacing="0" width="100%"> <!-- Booking LIST TABEL START-->
			<thead>
				<tr>
					<th><input type="checkbox" class="select_all"></th>
					<th><?php esc_html_e('Member Name','gym_mgt');?></th>
					<th><?php esc_html_e('Class Name','gym_mgt');?></th>
					<th><?php esc_html_e('Class Date','gym_mgt');?></th>
					<th><?php esc_html_e('Booking Date','gym_mgt');?></th>
					<th><?php esc_html_e('Day','gym_mgt');?></th>
					<th><?php esc_html_e('Starting Time','gym_mgt');?></th>
					<th><?php esc_html_e('Ending Time','gym_mgt');?></th>            
					<th><?php esc_html_e('Action','gym_mgt');?></th>            
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th></th>
					<th><?php esc_html_e('Member Name','gym_mgt');?></th>
					<th><?php esc_html_e('Class Name','gym_mgt');?></th>
					<th><?php esc_html_e('Class Date','gym_mgt');?></th>
					<th><?php esc_html_e('Booking Date','gym_mgt');?></th>
					<th><?php esc_html_e('Day','gym_mgt' );?></th>
					<th><?php esc_html_e('Starting Time','gym_mgt');?></th>
					<th><?php esc_html_e('Ending Time','gym_mgt');?></th>  
					<th><?php esc_html_e('Action','gym_mgt');?></th>       
				</tr>
			</tfoot>
			<tbody>
			<?php 
			$bookingdata=$obj_class->MJgmgt_get_all_booked_class();
			if(!empty($bookingdata))
			{
				foreach ($bookingdata as $retrieved_data)
				{
				?>
					<tr>
						<td class="title"><input type="checkbox" name="selected_id[]" class="sub_chk" value="<?php echo esc_attr($retrieved_data->id); ?>"></td>
						<td class="membername"><a href="#"><?php
						 
						 if(!empty($retrieved_data->member_id))
						 {
							echo MJgmgt_get_display_name(esc_html($retrieved_data->member_id));
						 }
						 else
						 {
							 echo "-";
						 }
						 
						 
						 ?></a></td>
						<td class="class_name"><?php  
						 if(!empty($obj_class->MJgmgt_get_class_name(esc_html($retrieved_data->class_id))))
						 {
							print $obj_class->MJgmgt_get_class_name(esc_html($retrieved_data->class_id));
						 }
						 else
						 {
							 echo "-";
						 }
						
						 ?></td>
						<td class="class_name"><?php print  str_replace('00:00:00',"",esc_html($retrieved_data->class_booking_date))?></td>
						<td class="class_name"><?php print  str_replace('00:00:00',"",esc_html($retrieved_data->booking_date))?></td>
						<td class="starttime"><?php echo esc_html($retrieved_data->booking_day);?></td>
						<?php $class_data = $obj_class->MJgmgt_get_single_class($retrieved_data->class_id); ?>
						<td class="starttime"><?php 
						if(isset($class_data->start_time))
						{
							echo MJgmgt_timeremovecolonbefoream_pm(esc_html($class_data->start_time));
						}
						else
						{
							echo "-";
						}
						?></td>
						<td class="endtime"><?php
						if(isset($class_data->start_time))
						{
							echo MJgmgt_timeremovecolonbefoream_pm(esc_html($class_data->end_time));
						}
						else
						{
							echo "-";
						}
						?></td>
						<td class="action">
							<a href="?page=gmgt_class&tab=booking_list&action=delete&class_booking_id=<?php echo esc_attr($retrieved_data->id);?>" class="btn btn-danger" onclick="return confirm('<?php esc_html_e('Do you really want to Cancel this record?','gym_mgt');?>');"><?php esc_html_e('Delete','gym_mgt');?> 
							</a>
						</td>
					</tr>
				<?php 
				} 
			}?>     
			</tbody>        
		</table><!-- Booking LIST TABEL END-->
		<div class="print-button pull-left">
			<input  type="submit" value="<?php esc_html_e('Delete Selected','gym_mgt');?>" name="delete_selected_booking_list" class="btn btn-danger delete_selected_booking_list "/>
		</div>
    </div><!-- TABLE RESPONSIVE DIV END-->
</div><!-- PANEL BODY DIV END-->
</form><!-- CLASS LIST FORM END-->
  <?php 
}
?>