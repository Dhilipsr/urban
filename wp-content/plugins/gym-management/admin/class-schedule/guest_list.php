<?php
//Selected CLASS DATA Delete
$obj_guest_booking = new MJgmgt_guest_booking;
if(isset($_REQUEST['delete_selected_booking_list']))
{
	if(!empty(esc_attr($_REQUEST['selected_id'])))
	{
		foreach($_REQUEST['selected_id'] as $id)
		{
			$delete_class=$obj_guest_booking->MJgmgt_delete_guest_booking($id);
			if($delete_class)
			{
				wp_redirect ( admin_url().'admin.php?page=gmgt_class&tab=guest_list&message=5');
			}
		}
	}
	else
	{
		echo '<script language="javascript">';
		echo 'alert("'.esc_html__('Please select at least one record.','gym_mgt').'")';
		echo '</script>';
	}
}
if($active_tab == 'guest_list')
{
?>
<!-- POP up code -->
<div class="popup-bg">
	<div class="overlay-content">
		<div class="modal-content">
			<div class="category_list"></div>	
		</div>
	</div> 
</div>
<!-- End POP-UP Code -->
<script type="text/javascript">
	$(document).ready(function() 
	{
		"use strict";
		jQuery('.booking_list').DataTable({
			"responsive": true,
			"order": [[ 2, "desc" ]],
			"aoColumns":[
						  {"bSortable": false},
						  {"bSortable": true},
						  {"bSortable": true},
						  {"bSortable": true},
						  {"bSortable": true},
						  {"bSortable": false}],
				language:<?php echo MJgmgt_datatable_multi_language();?>
			});
		$('.select_all').on('click', function(e)
		{
			 if($(this).is(':checked',true))  
			 {
				$(".sub_chk").prop('checked', true);  
			 }  
			 else  
			 {  
				$(".sub_chk").prop('checked',false);  
			 } 
		});
		$('.sub_chk').change(function()
		{
			if(false == $(this).prop("checked"))
			{ 
				$(".select_all").prop('checked', false); 
			}
			if ($('.sub_chk:checked').length == $('.sub_chk').length )
			{
				$(".select_all").prop('checked', true);
			}
		});
	} );
</script>
<form name="wcwm_report" action="" method="post">
<div class="panel-body"> <!-- PANEL BODY DIV START-->
	<div class="table-responsive"> <!-- TABLE RESPONSIVE DIV START-->
	    <table id="booking_list113" class="display booking_list" cellspacing="0" width="100%"> <!-- Booking LIST TABEL START-->
			<thead>
				<tr>
					<th><input type="checkbox" class="select_all"></th>
					<th><?php esc_html_e('Guest Name','gym_mgt');?></th>
					<th><?php esc_html_e('Email','gym_mgt');?></th>
					<th><?php esc_html_e('Phone','gym_mgt');?></th>
					<th><?php esc_html_e('Booking Date','gym_mgt');?></th>
					<th><?php esc_html_e('Action','gym_mgt');?></th>            
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th></th>
					<th><?php esc_html_e('Guest Name','gym_mgt');?></th>
					<th><?php esc_html_e('Email','gym_mgt');?></th>
					<th><?php esc_html_e('Phone','gym_mgt');?></th>
					<th><?php esc_html_e('Booking Date','gym_mgt');?></th> 
					<th><?php esc_html_e('Action','gym_mgt');?></th>       
				</tr>
			</tfoot>
			<tbody>
			<?php 
			$obj_guest_booking = new MJgmgt_guest_booking;
			$bookingdata=$obj_class->MJgmgt_get_all_booked_class();
			$guestbookingdata=$obj_guest_booking->MJgmgt_get_all_guest_booking();
			if(!empty($guestbookingdata))
			{
				foreach ($guestbookingdata as $retrieved_data)
				{
				?>
					<tr>
						<td class="title"><input type="checkbox" name="selected_id[]" class="sub_chk" value="<?php echo esc_attr($retrieved_data->guest_id); ?>"></td>
						<td class="membername"><a href="#"><?php echo $retrieved_data->first_name.' '.$retrieved_data->last_name;?></a></td>
						<td class="emailid"><?php echo $retrieved_data->email_id;  ?></td>
						<td class="phonenumber"><?php echo $retrieved_data->phone_number; ?></td>
						<td class="date"><?php echo $retrieved_data->created_date; ?></td>
						
						<td class="action">
							<a href="?page=gmgt_class&tab=guest_list&action=delete&guest_id=<?php echo esc_attr($retrieved_data->guest_id);?>" class="btn btn-danger" onclick="return confirm('<?php esc_html_e('Do you really want to delete this record?','gym_mgt');?>');"><?php esc_html_e('Delete','gym_mgt');?> 
							</a>
						</td>
					</tr>
				<?php 
				} 
			}?>     
			</tbody>        
		</table><!-- Booking LIST TABEL END-->
		<div class="print-button pull-left">
			<input  type="submit" value="<?php esc_html_e('Delete Selected','gym_mgt');?>" name="delete_selected_booking_list" class="btn btn-danger delete_selected_booking_list "/>
		</div>
    </div><!-- TABLE RESPONSIVE DIV END-->
</div><!-- PANEL BODY DIV END-->
</form><!-- CLASS LIST FORM END-->
  <?php 
}
?>