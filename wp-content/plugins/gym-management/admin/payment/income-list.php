<?php
$obj_payment= new MJgmgt_payment();
if($active_tab == 'incomelist')
{
	$invoice_id=0;
	$edit=0;
	if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
	{
		$edit=1;
		$invoice_id=$_REQUEST['income_id'];
		$result = $obj_payment->hmgt_get_invoice_data($invoice_id);
	}
	?>
	<script type="text/javascript">
	$(document).ready(function() 
	{
		"use strict";
		jQuery('#tblincome').DataTable({
			"responsive": true,
			 "order": [[ 1, "Desc" ]],
			 "aoColumns":[
						  {"bSortable": false},
						  {"bSortable": true},
						  {"bSortable": true},
						  {"bSortable": true}, 
						  {"bSortable": true}, 
						  {"bSortable": true}, 
						  {"bSortable": true}, 
						  {"bSortable": true}, 
						  {"bSortable": true}, 
						  {"bSortable": false}
					   ],
					language:<?php echo MJgmgt_datatable_multi_language();?>	   
			});
	   $('.select_all').on('click', function(e)
		{
			 if($(this).is(':checked',true))  
			 {
				$(".sub_chk").prop('checked', true);  
			 }  
			 else  
			 {  
				$(".sub_chk").prop('checked',false);  
			 } 
		});
		$('.sub_chk').change(function()
		{ 
			if(false == $(this).prop("checked"))
			{ 
				$(".select_all").prop('checked', false); 
			}
			if ($('.sub_chk:checked').length == $('.sub_chk').length )
			{
				$(".select_all").prop('checked', true);
			}
	  });
	} );
	</script>
    <div class="panel-body"><!--PANEL BODY DIV START-->
    	<div class="table-responsive"><!--TABLE RESPONSIVE DIV START-->
			<form name="wcwm_report" action="" method="post"><!--INCOME LIST FORM START-->
			<table id="tblincome" class="display" cellspacing="0" width="100%"><!--INCOME LIST TABLE START-->
				<thead>
					<tr>
					    <th><input type="checkbox" class="select_all"></th>
						<th><?php esc_html_e('Member Name','gym_mgt');?></th>
						<th><?php esc_html_e('Income Name', 'gym_mgt');?></th>
						<th><?php esc_html_e('Invoice No','gym_mgt');?></th>
						<th><?php esc_html_e('Amount','gym_mgt');?></th>
						<th><?php esc_html_e('Paid Amount','gym_mgt');?></th>
						<th><?php esc_html_e('Due Amount','gym_mgt');?></th>
						<th><?php esc_html_e('Date','gym_mgt');?></th>
						<th><?php esc_html_e('Payment Status','gym_mgt');?></th>
						<th><?php esc_html_e('Action','gym_mgt');?></th>
					</tr>
			    </thead>
				<tfoot>
					<tr>
						<th></th>
						<th><?php esc_html_e('Member Name','gym_mgt');?></th>
						<th><?php esc_html_e('Income Name', 'gym_mgt');?></th>
						<th><?php esc_html_e('Invoice No','gym_mgt');?></th>
						<th><?php esc_html_e('Amount','gym_mgt');?></th>
						<th><?php esc_html_e('Paid Amount','gym_mgt');?></th>
						<th><?php esc_html_e('Due Amount','gym_mgt');?></th>
						<th><?php esc_html_e('Date','gym_mgt');?></th>
						<th><?php esc_html_e('Payment Status','gym_mgt');?></th>
						<th><?php esc_html_e('Action','gym_mgt');?></th>
					</tr>
				</tfoot>
				<tbody>
				 <?php 
					//GET ALL INCOME DATA
					$paymentdata=$obj_payment->MJgmgt_get_all_income_data();
					foreach ($paymentdata as $retrieved_data)
					{
						if(empty($retrieved_data->invoice_no))
						{
							$invoice_no='-';
							if($retrieved_data->invoice_label=='Sell Product')
							{	
								$entry=json_decode($retrieved_data->entry);
								if(!empty($entry))
								{
									foreach($entry as $data)
									{
										 $amount=$data->amount;
									}
								}
								$total_amount=$amount;
								$paid_amount=$amount;
								$due_amount='0';
							}
							else
							{
								$entry=json_decode($retrieved_data->entry);
								$amount_value='0';
								if(!empty($entry))
								{
									foreach($entry as $data)
									{
										 $amount_value+=$data->amount;	 
									}
								}
								if($retrieved_data->payment_status=='Paid')
								{
									$total_amount=$amount_value;
									$paid_amount=$amount_value;
									$due_amount='0';
								}
								else
								{
									$total_amount=$amount_value;
									$paid_amount='0';
									$due_amount=$amount_value;
								}
							}
						}
						else
						{								
							$invoice_no=$retrieved_data->invoice_no;
							$total_amount=$retrieved_data->total_amount;
							$paid_amount=$retrieved_data->paid_amount;
							$due_amount=abs($total_amount-$paid_amount);
						}
						if($retrieved_data->total_amount == '0')
						{
							$status='Fully Paid';
						}
						else
						{
							$status=$retrieved_data->payment_status;
						}
						?>
						<tr>
						<td class="title"><input type="checkbox" name="selected_id[]" class="sub_chk" value="<?php echo esc_attr($retrieved_data->invoice_id); ?>"></td>
							<td class="member_name"><?php $user=get_userdata($retrieved_data->supplier_name);
								$memberid=get_user_meta($retrieved_data->supplier_name,'member_id',true);
								$display_label=$user->display_name;
								if($memberid)
								{
									$display_label.=" (".$memberid.")";
									echo esc_html($display_label);
								}
								?>
							</td>
							<td class="income_amount"><?php echo esc_html($retrieved_data->invoice_label);?></td>
							<td class="income_amount">							
							<?php
								echo esc_html($invoice_no);	
							?>
							</td>
							<td class="income_amount"><?php echo MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )); ?> <?php echo number_format(esc_html($total_amount),2);?></td>
							<td class="income_amount"><?php echo MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )); ?> <?php echo number_format(esc_html($paid_amount),2);?></td>
							<td class="income_amount"><?php echo MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' )); ?> <?php echo number_format(esc_html($due_amount),2);?></td>
							<td class="status"><?php echo MJgmgt_getdate_in_input_box(esc_html($retrieved_data->invoice_date));?></td>
							<td class="paymentdate">
								<?php
								echo "<span class='btn btn-success btn-xs'>";							
								echo esc_html__("$status","gym_mgt");
								echo "</span>";
								?>
							</td>								
							<?php
							if (($retrieved_data->total_amount > '0' ) && ($retrieved_data->payment_status == 'Unpaid' || $retrieved_data->payment_status == 'Partially Paid' || $retrieved_data->payment_status == 'Part Paid' || $retrieved_data->payment_status == 'Not Paid') )
							{
							?>
								<td class="action">							
									<a href="#" class="show-payment-popup btn btn-default" idtest="<?php echo esc_attr($retrieved_data->invoice_id); ?>" member_id="<?php echo esc_attr($retrieved_data->supplier_name); ?>" due_amount="<?php echo str_replace(",","",number_format($due_amount,2)); ?>"view_type="income_payment" ><?php esc_html_e('Pay','gym_mgt');?></a>
									<a href="#" class="show-invoice-popup btn btn-default" <?php if($retrieved_data->invoice_label=='Sell Product'){ ?> idtest="<?php $id=$obj_payment->MJgmgt_get_sell_id_by_income_id($retrieved_data->invoice_id); echo esc_attr($id);?>" invoice_type="sell_invoice" <?php }elseif($retrieved_data->invoice_label=='Fees Payment'){ ?> idtest="<?php $id=$obj_payment->MJgmgt_get_fees_id_by_income_id($retrieved_data->invoice_id); echo esc_attr($id);?>" invoice_type="membership_invoice" <?php }else{?> idtest="<?php echo esc_attr($retrieved_data->invoice_id); ?>" invoice_type="income" <?php } ?>>
								<i class="fa fa-eye"></i> <?php esc_html_e('View Invoice', 'gym_mgt');?></a>
								<?php
								if(!empty($retrieved_data->invoice_no))
								{
									if(!($retrieved_data->invoice_label=='Fees Payment' || $retrieved_data->invoice_label=='Sell Product'))
									{
									?>	
										<a href="?page=gmgt_payment&tab=addincome&action=edit&income_id=<?php echo esc_attr($retrieved_data->invoice_id);?>" class="btn btn-info"> <?php esc_html_e('Edit', 'gym_mgt' ) ;?></a>
									<?php
									}
								}
								?>
									<a href="?page=gmgt_payment&tab=incomelist&action=delete&income_id=<?php echo esc_attr($retrieved_data->invoice_id);?>" class="btn btn-danger" onclick="return confirm('<?php esc_html_e('Do you really want to delete this record?','gym_mgt');?>');"><?php esc_html_e( 'Delete', 'gym_mgt' ) ;?> </a>
								</td>
							<?php 
							}  
							if ($retrieved_data->total_amount == '0' || $retrieved_data->payment_status == 'Fully Paid' || $retrieved_data->payment_status == 'Paid') 
							{
							?>
							<td class="action">
								<a  href="#" class="show-invoice-popup btn btn-default" <?php if($retrieved_data->invoice_label=='Sell Product'){ ?> idtest="<?php $id=$obj_payment->MJgmgt_get_sell_id_by_income_id($retrieved_data->invoice_id); echo esc_attr($id);?>"  invoice_type="sell_invoice" <?php }elseif($retrieved_data->invoice_label=='Fees Payment'){ ?> idtest="<?php $id=$obj_payment->MJgmgt_get_fees_id_by_income_id($retrieved_data->invoice_id);  echo esc_attr($id);?>" invoice_type="membership_invoice" <?php }else{?> idtest="<?php echo esc_attr($retrieved_data->invoice_id); ?>" invoice_type="income" <?php } ?> >
								<i class="fa fa-eye"></i> <?php esc_html_e('View Invoice', 'gym_mgt');?></a>
								<?php
								if(!empty($retrieved_data->invoice_no))
								{
									if(!($retrieved_data->invoice_label=='Fees Payment' || $retrieved_data->invoice_label=='Sell Product'))
									{
									?>	
										<a href="?page=gmgt_payment&tab=addincome&action=edit&income_id=<?php echo esc_attr($retrieved_data->invoice_id);?>" class="btn btn-info"> <?php esc_html_e('Edit', 'gym_mgt' ) ;?></a>
									<?php
									}
								}
								?>
									<a href="?page=gmgt_payment&tab=incomelist&action=delete&income_id=<?php echo esc_attr($retrieved_data->invoice_id);?>" class="btn btn-danger" onclick="return confirm('<?php esc_html_e('Do you really want to delete this record?','gym_mgt');?>');"><?php esc_html_e( 'Delete', 'gym_mgt' ) ;?> </a>
							</td>
						</tr>
					<?php 
						} 
					}
				?>
				</tbody>        
			</table><!--INCOME LIST TABLE END-->
			<div class="print-button pull-left">
                <input  type="submit" value="<?php esc_html_e('Delete Selected','gym_mgt');?>" name="delete_selected_income" class="btn btn-danger delete_selected "/>
            </div>
			</form><!--INCOME LIST FORM END-->
        </div><!--TABLE RESPONSIVE DIV END-->
    </div>	<!--PANEL BODY DIV END-->	
<?php 
} 
?>