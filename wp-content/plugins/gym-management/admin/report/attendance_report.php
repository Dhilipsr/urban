<?php 
error_reporting(0);
if(isset($_REQUEST['attendance_report']))
{
    global $wpdb;
	$table_attendance = $wpdb->prefix .'gmgt_attendence';
	$table_class = $wpdb->prefix .'gmgt_class_schedule';
	$sdate =MJgmgt_get_format_for_db(esc_attr($_REQUEST['sdate']));
	$edate = MJgmgt_get_format_for_db(esc_attr($_REQUEST['edate']));
	$report_2 =$wpdb->get_results("SELECT  at.class_id, 
	SUM(case when `status` ='Present' then 1 else 0 end) as Present, 
	SUM(case when `status` ='Absent' then 1 else 0 end) as Absent 
	from $table_attendance as at,$table_class as cl where `attendence_date` BETWEEN '$sdate' AND '$edate' AND at.class_id = cl.class_id AND at.role_name = 'member' GROUP BY at.class_id") ;
		$chart_array[] = array(esc_html__('Class','gym_mgt'),esc_html__('Present','gym_mgt'),esc_html__('Absent','gym_mgt'));
	if(!empty($report_2))
		foreach($report_2 as $result)
		{
			$class_id =MJgmgt_get_class_name($result->class_id);
			$chart_array[] = array("$class_id",(int)$result->Present,(int)$result->Absent);
		}
	$options = Array(
	 		'title' => esc_html__('Member Attendance Report','gym_mgt'),
			'titleTextStyle' => Array('color' => '#66707e'),
			'legend' =>Array('position' => 'right',
					'textStyle'=> Array('color' => '#66707e')),
			'hAxis' => Array(
					'title' =>  esc_html__('Class','gym_mgt'),
					 'format' => '#',
					'titleTextStyle' => Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
					'textStyle'=> Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
					'maxAlternation' => 2),
			'vAxis' => Array(
					'title' =>  esc_html__('No of Member','gym_mgt'),
					'minValue' => 0,
					'maxValue' => 6,
					'format' => '#',
					'titleTextStyle' => Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
					'textStyle'=> Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans')),
			'colors' => array('#22BAA0','#f25656')
	);
}
require_once GMS_PLUGIN_DIR. '/lib/chart/GoogleCharts.class.php';
$GoogleCharts = new GoogleCharts;
?>
<script type="text/javascript">
$(document).ready(function() 
{
	"use strict";
	$('.sdate').datepicker({dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>'}); 
	$('.edate').datepicker({dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>'}); 
} );
</script>
    <div class="panel-body">
	    <form method="post">  
			<div class="form-group col-md-3">
				<label for="exam_id"><?php esc_html_e('Start Date','gym_mgt');?></label>	
						<input type="text"  class="form-control sdate" name="sdate" value="<?php if(isset($_REQUEST['sdate'])) echo esc_attr($_REQUEST['sdate']);else echo esc_attr(MJgmgt_getdate_in_input_box(date('Y-m-d')));?>" readonly>
			</div>
			<div class="form-group col-md-3">
				<label for="exam_id"><?php esc_html_e('End Date','gym_mgt');?></label>
					<input type="text"  class="form-control edate"  name="edate" value="<?php if(isset($_REQUEST['edate'])) echo esc_attr($_REQUEST['edate']);else echo esc_attr(MJgmgt_getdate_in_input_box(date('Y-m-d')));?>" readonly>
			</div>
			<div class="form-group col-md-3 button-possition">
				<label for="subject_id">&nbsp;</label>
				<input type="submit" name="attendance_report" Value="<?php esc_html_e('Go','gym_mgt');?>"  class="btn btn-success"/>
			</div>
		</form>
	</div>
	<?php
	if(isset($_REQUEST['attendance_report']))
	{
		global $wpdb;
		$start_date = MJgmgt_get_format_for_db(esc_attr($_REQUEST['sdate']));
		$end_date = MJgmgt_get_format_for_db(esc_attr($_REQUEST['edate']));
		$attendance = MJgmgt_get_all_member_attendence();
		$curremt_date =$start_date; 
		if($end_date >= $curremt_date)
		{					
			$filename="Attendance Report.csv";
			$fp = fopen($filename, "w");
			// Get The Field Name
			$output="";
			$header = array();			
			$header[] = esc_html__('Id','gym_mgt');
			$header[] = esc_html__('Member Name','gym_mgt');
			$header[] = esc_html__('Class Name','gym_mgt');
			$header[] = esc_html__('Date','gym_mgt');
			$header[] = esc_html__('Day','gym_mgt');
			$header[] = esc_html__('Attendance','gym_mgt');
			fputcsv($fp, $header);
			$i=1;
			foreach ($attendance as $retrive_data)
			{
				$row = array();
				$user_info = get_userdata($retrive_data->user_id);
				$row[] = $i;
				$row[] = $user_info->display_name;
				$row[] = MJgmgt_get_class_name_by_id($retrive_data->class_id);
				$row[] = $retrive_data->attendence_date;
				$attendance_status = $retrive_data->status;
				$row[] = date("D", strtotime($curremt_date));
				if(!empty($attendance_status))
				{
					$row[] = $attendance_status;	
				}
				else 
				{
					$row[] = esc_html__('Absent','gym_mgt');	
				}
				$i++;
			fputcsv($fp, $row);
			}
			// Download the file
			fclose($fp);
		   	?>
			<?php
		}
	}
	?>
    	<?php if(isset($report_2) && count($report_2) >0)
		{
    		$chart = $GoogleCharts->load( 'column' , 'chart_div' )->get( $chart_array , $options );
    	   ?>
    	   <div class="form-group col-md-4 col-xs-12 button-possition">
				<a class="btn btn-success" href='<?php echo $filename;?>'><?php esc_html_e('Download Report In CSV','gym_mgt');?></a>
			</div>
		  <div id="chart_div"  class="chart_div">
		 <?php  if(isset($report_2) && empty($report_2)) 
 {?>
    <div class="clear col-md-12"><?php esc_html_e("There is not enough data to generate report.",'gym_mgt');?></div>
  <?php 
 } ?>
  </div>
  <!-- Javascript --> 
  <script type="text/javascript" src="https://www.google.com/jsapi"></script> 
  <script type="text/javascript">
			<?php echo $chart;?>
 </script>
  <?php }
 if(isset($report_2) && empty($report_2)) 
 {?>
    <div class="clear col-md-12"><?php esc_html_e("There is not enough data to generate report.",'gym_mgt');?></div>
 <?php 
 } ?>