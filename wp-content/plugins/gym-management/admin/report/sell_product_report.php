<?php 
$month =array('1'=>esc_html__('January','gym_mgt'),'2'=>esc_html__('February','gym_mgt'),'3'=>esc_html__('March','gym_mgt'),'4'=>esc_html__('April','gym_mgt'),'5'=>esc_html__('May','gym_mgt'),'6'=>esc_html__('June','gym_mgt'),'7'=>esc_html__('July','gym_mgt'),'8'=>esc_html__('August','gym_mgt'),'9'=>esc_html__('September','gym_mgt'),'10'=>esc_html__('October','gym_mgt'),'11'=>esc_html__('November','gym_mgt'),'12'=>esc_html__('December','gym_mgt'),);
$year =isset($_POST['year'])?$_POST['year']:date('Y');
global $wpdb;
$table_name = $wpdb->prefix."gmgt_store";
$q="SELECT * FROM ".$table_name." WHERE YEAR(sell_date) =".$year." ORDER BY sell_date ASC";
$result=$wpdb->get_results($q);
$month_wise_count=array();
foreach($result as $key=>$value)
{
	$total_quantity=0;
	$all_entry=json_decode($value->entry);
	foreach($all_entry as $entry)
	{
		$total_quantity+=$entry->quentity;
	}	
	$sell_date = date_parse_from_format("Y-m-d",$value->sell_date);
	$month_wise_count[]=array('sell_date'=>$sell_date["month"],'quentity'=>$total_quantity);
}
$sumArray = array(); 
foreach ($month_wise_count as $value1) 
{ 
	$value2=(object)$value1;
	if(isset($sumArray[$value2->sell_date]))
	{
		$sumArray[$value2->sell_date] = $sumArray[$value2->sell_date] + (int)$value2->quentity;
	}
	else
	{
		$sumArray[$value2->sell_date] = (int)$value2->quentity; 
	}		
}
$chart_array = array();
$chart_array[] = array('Month','Sells Product');
foreach($sumArray as $month_value=>$quentity)
{
	$chart_array[]=array( $month[$month_value],(int)$quentity);
}
$options = Array(
			'title' => esc_html__('Sells Product Report By Month','gym_mgt'),
			'titleTextStyle' => Array('color' => '#66707e'),
			'legend' =>Array('position' => 'right',
						'textStyle'=> Array('color' => '#66707e')),
			'hAxis' => Array(
				'title' => esc_html__('Month','gym_mgt'),
				'format' => '#',
				'titleTextStyle' => Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
				'textStyle'=> Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
				'maxAlternation' => 2				
				),
			'vAxis' => Array(
				'title' => esc_html__('Sells Product','gym_mgt'),
				 'minValue' => 0,
				'maxValue' => 6,
				 'format' => '#',
				'titleTextStyle' => Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
				'textStyle'=> Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans')
				),
 		'colors' => array('#22BAA0')
			);
require_once GMS_PLUGIN_DIR. '/lib/chart/GoogleCharts.class.php';
$GoogleCharts = new GoogleCharts;
$chart = $GoogleCharts->load( 'column' , 'chart_div' )->get( $chart_array , $options );
?>
<script type="text/javascript">
$(document).ready(function() 
{
	"use strict";
	$('.sdate').datepicker({dateFormat: "yy-mm-dd"}); 
	$('.edate').datepicker({dateFormat: "yy-mm-dd"}); 
} );
</script>
<div id="chart_div" class="chart_div">
<?php 
if(empty($result)) 
{?>
    <div class="clear col-md-12"><h3><?php esc_html_e("There is not enough data to generate report.",'gym_mgt');?> </h3></div>
<?php 
} ?>
</div>
<div id="chart_div" class="chart_div"></div>
<!-- Javascript --> 
<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
<script type="text/javascript">
	<?php 
	if(!empty($result))
	{
		echo $chart;
	}
	?>
</script>