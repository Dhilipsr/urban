<?php 
 global $wpdb;
 $table_name = $wpdb->prefix."gmgt_membershiptype";
 $q="SELECT * From $table_name";
 $member_ship_array = array();
 $result=$wpdb->get_results($q); 
foreach($result as $retrive)
{
	$membership_id = $retrive->membership_id;		
	$member_ship_count =  count(get_users(array('role'=>'member','meta_key' => 'membership_id', 'meta_value' => $retrive->membership_id)));
	$member_ship_array[] = array('member_ship_id'=>$membership_id,
								 'member_ship_count'=>	$member_ship_count
								);
}
$chart_array = array();
$chart_array[] = array(esc_html__('Membership','gym_mgt'),esc_html__('Number Of Member','gym_mgt'));	
foreach($member_ship_array as $r)
{
	$chart_array[]=array( MJgmgt_get_membership_name($r['member_ship_id']),$r['member_ship_count']);
}
$options = Array(
		'title' => esc_html__('Membership Report','gym_mgt'),
		'titleTextStyle' => Array('color' => '#66707e'),
		'legend' =>Array('position' => 'right',
				'textStyle'=> Array('color' => '#66707e')),
		'hAxis' => Array(
				'title' =>  esc_html__('Membership Name','gym_mgt'),
				'format' => '#',
				'titleTextStyle' => Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
				'textStyle'=> Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
				'maxAlternation' => 2
		),
		'vAxis' => Array(
				'title' =>  esc_html__('No of Member','gym_mgt'),
				'minValue' => 0,
				'maxValue' => 6,
				'format' => '#',
				'titleTextStyle' => Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
				'textStyle'=> Array('color' => '#66707e','fontSize' => 16,'bold'=>true,'italic'=>false,'fontName' =>'open sans'),
		),
		'colors' => array('#22BAA0')
);
require_once GMS_PLUGIN_DIR. '/lib/chart/GoogleCharts.class.php';
$GoogleCharts = new GoogleCharts;
$chart = $GoogleCharts->load( 'column' , 'chart_div' )->get( $chart_array , $options );
?>
<script type="text/javascript">
$(document).ready(function() 
{
	"use strict";
	$('.sdate').datepicker({dateFormat: "yy-mm-dd"}); 
	$('.edate').datepicker({dateFormat: "yy-mm-dd"}); 
} );
</script>
<?php
	error_reporting(0);
	global $wpdb;
	$obj_membership=new MJgmgt_membership;
	$membershipdata=$obj_membership->MJgmgt_get_all_membership();
	if(!empty($membershipdata))
	{
		$filename="Membership Report.csv";
		$fp = fopen($filename, "w");
		// Get The Field Name
		$output="";
		$header = array();			
		$header[] = esc_html__('Id','gym_mgt');
		$header[] = esc_html__('Membership Name','gym_mgt');
		$header[] = esc_html__('Member Name','gym_mgt');
		$header[] = esc_html__('Start Date','gym_mgt');
		$header[] = esc_html__('End Date','gym_mgt');
		$header[] = esc_html__('Status','gym_mgt');
		fputcsv($fp, $header);
		$i=1;
		$membership_id = $membershipdata->membership_id;
		$user = get_users(array('role' => 'member'));
		foreach ($user as $user_data)
		{
			$membership = $obj_membership->MJgmgt_get_single_membership($user_data->membership_id);
			$membership_name = $membership->membership_label;
			$row = array();
			$row[] = $i;
			$row[] = $membership_name;
			$row[] = $user_data->display_name;
			$row[] = $user_data->begin_date;
			$row[] = $user_data->end_date;
			$row[] = $user_data->membership_status;
			$i++;
		fputcsv($fp, $row);
		}
		// Download the file
		fclose($fp);
	   ?>
		<?php
	}
?>
<div class="margin_top_20 form-group col-md-4 col-xs-12 button-possition">
	<a class="btn btn-success" href='<?php echo $filename;?>'><?php esc_html_e('Download Report In CSV','gym_mgt');?></a>
</div>
<div id="chart_div" class="chart_div">
<?php 
if(empty($result)) 
{?>
	<div class="clear col-md-12"><h3><?php esc_html_e("There is not enough data to generate report.",'gym_mgt');?> </h3></div>
<?php 
} ?>
</div>
<!-- Javascript --> 
<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
<script type="text/javascript">
	<?php if(!empty($result))
	{
		echo $chart;
	}
	?>
</script>