<?php 
$api->useSecure(true);
$retval = $api->lists();
?>
<script type="text/javascript">
$(document).ready(function() 
{
	"use strict";
	$('#setting_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});	
} );
</script>
<div class="panel-body"><!--PANEL BODY DIV STRAT-->
    <form name="template_form" action="" method="post" class="form-horizontal" id="setting_form"><!--Mailing LIST SYNCRONIZE USER FORM STRAT-->
	    <div class="form-group">
			<label class="col-sm-2 control-label" for="enable_quote_tab"><?php esc_html_e('Class List','gym_mgt');?></label>
			<div class="col-sm-8">
				<div class="checkbox">
					<?php 
					//GET ALL Class DATA
					$classdata=$obj_class->MJgmgt_get_all_classes();
					if(!empty($classdata))
					{
						foreach ($classdata as $retrieved_data)
						{?>								
						  <label>
								<input type="checkbox" name="syncmail[]"  value="<?php echo esc_attr($retrieved_data->class_id)?>"/><?php echo esc_html($retrieved_data->class_name);?> ( <?php echo MJgmgt_timeremovecolonbefoream_pm(esc_html($retrieved_data->start_time)).' - '.MJgmgt_timeremovecolonbefoream_pm($retrieved_data->end_time);?>)
						  </label><br/>
						<?php 
						}
					}
					?>
			    </div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="list_id"><?php esc_html_e('Mailing List','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<select name="list_id" id="list_id"  class="form-control validate[required]">
					<option value=""><?php esc_html_e('Select list','gym_mgt');?></option>
					<?php 
					//Mailing LIST DATA
					foreach ($retval['data'] as $list)
					{						
						echo '<option value="'.esc_attr($list['id']).'">'.esc_html($list['name']).'</option>';
					}
					?>
				</select>
			</div>
		</div>
		<div class="col-sm-offset-2 col-sm-8">        	
        	<input type="submit" value="<?php esc_html_e('Sync Mail', 'gym_mgt' ); ?>" name="sychroniz_email" class="btn btn-success"/>
        </div>
    </form><!--Mailing LIST SYNCRONIZE USER FORM END-->
</div><!--PANEL BODY DIV END-->