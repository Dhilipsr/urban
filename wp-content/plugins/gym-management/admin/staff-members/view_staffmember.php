<?php 
$obj_gyme = new MJgmgt_Gym_management(); 
$obj_activity = new MJgmgt_activity(); 
$staff_member_id=0;
if(isset($_REQUEST['staff_member_id']))
	$staff_member_id=esc_attr($_REQUEST['staff_member_id']);
	$edit=0;	
	$edit=1;
	$user_info = get_userdata($staff_member_id);					
?>	
<div class="panel-body"><!--PANEL BODY DIV START-->
	<div class="member_view_row1"><!--MEMBER VIEW ROW 1 DIV START-->
		<div class="col-md-8 col-sm-12 membr_left">
			<div class="col-md-6 col-sm-12 left_side">
			<?php 
			if($user_info->gmgt_user_avatar == "") { ?>
				<img alt="" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">
			<?php } 
			else { ?>
				<img class="max_width_100" src="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar ); ?>" />
			<?php }	?>
			</div>
			<div class="col-md-6 col-sm-12 right_side">
				<div class="table_row">
					<div class="col-md-5 col-sm-12 table_td">
						<i class="fa fa-user"></i> 
						<?php esc_html_e('Name','gym_mgt'); ?>	
					</div>
					<div class="col-md-7 col-sm-12 table_td">
						<span class="txt_color">
							<?php echo chunk_split(esc_html($user_info->first_name)." ".esc_html($user_info->middle_name)." ".esc_html($user_info->last_name),24,"<BR>");?> 
						</span>
					</div>
				</div>
				<div class="table_row">
					<div class="col-md-5 col-sm-12 table_td">
						<i class="fa fa-envelope"></i> 
						<?php esc_html_e('Email','gym_mgt');?> 	
					</div>
					<div class="col-md-7 col-sm-12 table_td">
						<span class="txt_color"><?php echo chunk_split(esc_html($user_info->user_email),24,"<BR>");?></span>
					</div>
				</div>
				<div class="table_row">
					<div class="col-md-5 col-sm-12 table_td">
						<i class="fa fa-phone"></i>
						<?php esc_html_e('Mobile No','gym_mgt');?> 
					</div>
					<div class="col-md-7 col-sm-12 table_td">
						<span class="txt_color">
							<span class="txt_color"><?php echo esc_html($user_info->mobile);?> </span>
						</span>
					</div>
				</div>
				<div class="table_row">
					<div class="col-md-5 col-sm-12 table_td">
						<i class="fa fa-calendar"></i>
						<?php esc_html_e('Date Of Birth','gym_mgt');?>	
					</div>
					<div class="col-md-7 col-sm-12 table_td">
						<span class="txt_color"><?php echo MJgmgt_getdate_in_input_box(esc_html($user_info->birth_date));?></span>
					</div>
				</div>
				<div class="table_row">
					<div class="col-md-5 col-sm-12 table_td">
						<i class="fa fa-mars"></i>
						<?php esc_html_e('Gender','gym_mgt');?> 
					</div>
					<div class="col-md-7 col-sm-12 table_td">
						<span class="txt_color"><?php
						
						//echo esc_html($user_info->gender);
						
							if($user_info->gender == "male")
							{
								echo esc_html_e('Male','gym_mgt');
							}
							else
							{
								echo esc_html_e('Female','gym_mgt');
							}
						?>
						
						</span>
					</div>
				</div>
				<div class="table_row">
					<div class="col-md-5 col-sm-12 table_td">
						<i class="fa fa-user"></i>
						<?php esc_html_e('User Name','gym_mgt');?>
					</div>
					<div class="col-md-7 col-sm-12 table_td">
						<span class="txt_color"><?php echo chunk_split(esc_html($user_info->user_login),25,"<BR>");?> </span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 member_right">	
			<span class="report_title">
				<span class="fa-stack cutomcircle">
					<i class="fa fa-align-left fa-stack-1x"></i>
				</span> 
				<span class="shiptitle"><?php esc_html_e('More Info','gym_mgt');?></span>		
			</span>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-map-marker padding_right_15"></i>						
					<?php esc_html_e('Address','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
					<span class="txt_color"><?php 
						if($user_info->address != '')
						{
							echo chunk_split(esc_html($user_info->address).", <BR>",15);
						}
						if($user_info->city_name != '')
						{
							echo chunk_split(esc_html($user_info->city_name).", <BR>",15);
						}
						?>
			        </span>
				</div>
			</div>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-map-marker padding_right_15"></i>						
					<?php esc_html_e('Activity','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
					<span class="txt_color">
					<?php 
						$activity_array=$obj_activity->MJgmgt_get_activity_staffmemberwise($user_info->ID);
						$class_name="-";
						$class_name_string="";
						if($activity_array)
						{												
							foreach($activity_array as $key=>$activity_id)
							{
							  echo chunk_split(esc_html($activity_id),24,", ");
							}
						}						
						else
						{
							echo chunk_split(esc_html($class_name),20,"<BR>");
						}
					?>
					</span>
				</div>
			</div>				
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-map-marker padding_right_15"></i>						
					<?php esc_html_e('Assign Role','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
					<span class="txt_color"><?php echo chunk_split(get_the_title(esc_html($user_info->role_type)),25,"<BR>");?> </span>
				</div>
			</div>
		</div>
	</div><!--MEMBER VIEW ROW 1 DIV END-->
</div><!--PANEL BODY DIV END-->