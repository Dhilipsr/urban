<?php $role='accountant'; ?>
<script type="text/javascript">
	$(document).ready(function() {
		"use strict";
		$('#staff_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});  
		jQuery('#birth_date').datepicker({
			dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
			maxDate : 0,
			changeMonth: true,
		    changeYear: true,
		    yearRange:'-65:+25',
			beforeShow: function (textbox, instance) 
			{
				instance.dpDiv.css({
					marginTop: (-textbox.offsetHeight) + 'px'                   
				});
			},    
		    onChangeMonthYear: function(year, month, inst) {
		        jQuery(this).val(month + "/" + year);
		    }                    
		});
	});
</script>
<?php 	
if($active_tab == 'add_accountant')
{
	$accountant_id=0;
	$edit=0;
	if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
	{	
		$accountant_id=esc_attr($_REQUEST['accountant_id']);
		$edit=1;
		$user_info = get_userdata($accountant_id);	
	}
	?>
	<div class="panel-body"><!-- PANLE BODY DIV START-->
		<form name="staff_form" action="" method="post" class="form-horizontal" id="staff_form"><!-- Accountant FORM START-->
			<?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
			<input type="hidden" name="action" value="<?php echo esc_attr($action);?>">
			<input type="hidden" name="role" value="<?php echo esc_attr($role);?>"  />
			<input type="hidden" name="user_id" value="<?php echo esc_attr($accountant_id);?>"  />
			<div class="header col-sm-12">
				<h3><?php esc_html_e('Personal Information','gym_mgt');?></h3>
			</div>
			<div class="col-sm-6 padding_left_right_0">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="first_name"><?php esc_html_e('First Name','gym_mgt');?><span class="require-field">*</span>
					</label>
					<div class="col-sm-7">
						<input id="first_name" class="form-control validate[required,custom[onlyLetter_specialcharacter]] text-input" maxlength="50" type="text" value="<?php if($edit){ echo esc_attr($user_info->first_name);}elseif(isset($_POST['first_name'])) echo esc_attr($_POST['first_name']);?>" name="first_name" tabindex="1">
					</div>
				</div>
				<!--nonce--->
				<?php wp_nonce_field('save_staff_nonce'); ?>
				<!--nonce--->
				<div class="form-group">
					<label class="col-sm-4 control-label" for="middle_name"><?php esc_html_e('Middle Name','gym_mgt');?>
					</label>
					<div class="col-sm-7">
						<input id="middle_name" class="form-control validate[custom[onlyLetter_specialcharacter]] " type="text" maxlength="50"  value="<?php if($edit){ echo esc_attr($user_info->middle_name);}elseif(isset($_POST['middle_name'])) echo esc_attr($_POST['middle_name']);?>" name="middle_name" tabindex="2">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="last_name"><?php esc_html_e('Last Name','gym_mgt');?><span class="require-field">*</span>
					</label>
					<div class="col-sm-7">
						<input id="last_name" class="form-control validate[required,custom[onlyLetter_specialcharacter]] text-input" maxlength="50" type="text"  value="<?php if($edit){ echo esc_attr($user_info->last_name);}elseif(isset($_POST['last_name'])) echo esc_attr($_POST['last_name']);?>" name="last_name" tabindex="3">
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">
				<div class="form-group">
					<label class="col-sm-4 control-label" for="birth_date"><?php esc_html_e('Date of birth','gym_mgt');?><span class="require-field">*</span>
					</label>
					<div class="col-sm-7">
						<input id="birth_date" class="form-control validate[required]"  type="text"  name="birth_date" 
						value="<?php if($edit){ echo MJgmgt_getdate_in_input_box($user_info->birth_date);}elseif(isset($_POST['birth_date'])) echo esc_attr($_POST['birth_date']);?>" readonly tabindex="4">
					</div>
				</div>	
				<div class="form-group">
					<label class="col-sm-4 control-label" for="gender"><?php esc_html_e('Gender','gym_mgt');?><span class="require-field">*</span>
					</label>
					<div class="col-sm-7">
					<?php $genderval = "male"; if($edit){ $genderval=$user_info->gender; }elseif(isset($_POST['gender'])) {$genderval=esc_attr($_POST['gender']);}?>
						<label class="radio-inline">
						 	<input type="radio" value="male" class="tog validate[required]" name="gender"  <?php  checked( 'male', esc_html($genderval));  ?> tabindex="5" /><?php esc_html_e('Male','gym_mgt');?>
						</label>
						<label class="radio-inline">
						  	<input type="radio" value="female" class="tog validate[required]" name="gender"  <?php  checked( 'female', esc_html($genderval));  ?>/><?php esc_html_e('Female','gym_mgt');?>
						</label>
					</div>
				</div>					
			</div>
			<div class="header col-sm-12">	
				<hr>
				<h3><?php esc_html_e('Contact Information','gym_mgt');?></h3>
			</div>		
			<div class="col-sm-6 padding_left_right_0">				
				<div class="form-group">
					<label class="col-sm-4 control-label" for="address"><?php esc_html_e('Home Town Address','gym_mgt');?><span class="require-field">*</span>
					</label>
					<div class="col-sm-7">
						<input id="address" class="form-control validate[required,custom[address_description_validation]]" maxlength="150" type="text"  name="address" value="<?php if($edit){ echo esc_attr($user_info->address);}elseif(isset($_POST['address'])) echo esc_attr($_POST['address']);?>" tabindex="6">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="city_name"><?php esc_html_e('City','gym_mgt');?><span class="require-field">*</span>
					</label>
					<div class="col-sm-7">
						<input id="city_name" class="form-control validate[required,custom[city_state_country_validation]]" maxlength="50" type="text"  name="city_name" 
						value="<?php if($edit){ echo esc_attr($user_info->city_name);}elseif(isset($_POST['city_name'])) echo esc_attr($_POST['city_name']);?>" tabindex="7">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label " for="email"><?php esc_html_e('Email','gym_mgt');?><span class="require-field">*</span>
					</label>
					<div class="col-sm-7">
						<input type="hidden"  name="hidden_email" value="<?php if($edit){ echo esc_attr($user_info->user_email); } ?>">
						<input id="email" class="form-control validate[required,custom[email]] text-input" maxlength="100" type="text" name="email" value="<?php if($edit){ echo esc_attr($user_info->user_email);}elseif(isset($_POST['email'])) echo esc_attr($_POST['email']);?>" tabindex="7">
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">		
				<div class="form-group">
					<label class="col-sm-4 col-xs-12 control-label " for="mobile"><?php esc_html_e('Mobile Number','gym_mgt');?><span class="require-field">*</span>
					</label>
					<div class="col-sm-2 col-xs-4">
						<input type="text" readonly value="+<?php echo MJgmgt_get_countery_phonecode(get_option('gmgt_contry'));?>"  class="form-control" name="phonecode">
					</div>
					<div class="col-sm-5 col-xs-8">
						<input id="mobile" class="form-control validate[required,custom[phone_number]] text-input phone_validation" type="text" name="mobile" minlength="6" maxlength="15" value="<?php if($edit){ echo esc_attr($user_info->mobile);}elseif(isset($_POST['mobile'])) echo esc_attr($_POST['mobile']);?>" tabindex="8">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label " for="phone"><?php esc_html_e('Phone','gym_mgt');?></label>
					<div class="col-sm-7">
						<input id="phone" class="form-control validate[custom[phone_number]] text-input phone_validation" type="text" minlength="6" maxlength="15"   name="phone" value="<?php if($edit){ echo esc_attr($user_info->phone);}elseif(isset($_POST['phone'])) echo esc_attr($_POST['phone']);?>" tabindex="9">
					</div>
				</div>
			</div>	
			<div class="header col-sm-12">
				<hr>
				<h3><?php esc_html_e('Login Information','gym_mgt');?></h3>
			</div>				
			<div class="col-sm-6 padding_left_right_0">						
				<div class="form-group">
					<label class="col-sm-4 control-label" for="username"><?php esc_html_e('User Name','gym_mgt');?><span class="require-field">*</span>
					</label>
					<div class="col-sm-7">
						<input id="username" class="form-control validate[required,custom[username_validation]] space_validation" type="text" maxlength="50"  name="username" value="<?php if($edit){ echo esc_attr($user_info->user_login);}elseif(isset($_POST['username'])) echo esc_attr($_POST['username']);?>" <?php if($edit) echo "readonly";?> tabindex="10">
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">		
				<div class="form-group">
					<label class="col-sm-4 control-label" for="password"><?php esc_html_e('Password','gym_mgt');?><?php if(!$edit) {?><span class="require-field">*</span><?php }?></label>
					<div class="col-sm-7">
						<input id="password" class="form-control space_validation <?php if(!$edit) echo esc_attr('validate[required]');?>" type="password" minlength="8" maxlength="12" name="password" value="" tabindex="11">
					</div>
				</div>
			</div>
			<div class="col-sm-6 padding_left_right_0">		
				<div class="form-group">
					<label class="col-sm-4 control-label" for="photo"><?php esc_html_e('Image','gym_mgt');?>
					</label>
					<div class="col-sm-4">
						<input type="text" id="gmgt_user_avatar_url" class="form-control" name="gmgt_user_avatar" readonly value="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar );elseif(isset($_POST['gmgt_user_avatar'])) echo esc_url($_POST['gmgt_user_avatar']); ?>" />
					</div>	
					<div class="col-sm-3">
						<input id="upload_user_avatar_button" type="button" class="button" value="<?php esc_html_e( 'Upload image', 'gym_mgt' ); ?>" tabindex="12" />
					</div>
					<div class="clearfix"></div>					
					<div class="col-sm-offset-4 col-sm-7">
						<div id="upload_user_avatar_preview" >
						<?php 
							if($edit) 
							{
								if($user_info->gmgt_user_avatar == "")
								{?>
									<img class="image_preview_css" src="<?php echo esc_url(get_option( 'gmgt_system_logo' )); ?>">
								<?php 
								}
								else
								{
									?>
									<img class="image_preview_css" src="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar ); ?>" />
								<?php 
								}
							}
							else 
							{
							?>
								<img class="image_preview_css" src="<?php echo esc_url(get_option( 'gmgt_system_logo' )); ?>">
							<?php 
							}?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-offset-2 col-sm-8">
				<input type="submit" value="<?php if($edit){ esc_html_e('Save','gym_mgt'); }else{ esc_html_e('Add Accountant','gym_mgt');}?>" name="save_staff" class="btn btn-success"/>
			</div>
		</form><!-- Accountant FORM END-->
	</div><!-- PANEL BODY DIV END-->
 <?php 
}
?>