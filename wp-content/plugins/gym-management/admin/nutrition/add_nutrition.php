<?php ?>
<script type="text/javascript">
jQuery(document).ready(function($) 
{
	"use strict";
	$(".display-members").select2();
	$('#nutrition_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});	       
	$(".start_date").datepicker(
	{
		<?php
		if(get_option('gym_enable_datepicker_privious_date')=='no')
		{
		?>
			minDate:'today',
		<?php
		}
		?>	
		dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
		minDate:0,
		onSelect: function (selected)
		{
    		var dt = new Date(selected);
    		dt.setDate(dt.getDate() + 0);
    		$(".end_date").datepicker("option", "minDate", dt);
    	}
    });
    $(".end_date").datepicker(
    {
		<?php
		if(get_option('gym_enable_datepicker_privious_date')=='no')
		{
		?>
			minDate:'today',
		<?php
		}
		?>	
       	dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
        onSelect: function (selected) 
        {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 0);
            $(".start_date").datepicker("option", "maxDate", dt);
        }
    });
} );
</script>
<?php 	
if($active_tab == 'addnutrition')
{
	$nutrition_id=0;
	$edit=0;
	$member_id=0;
	if(isset($_REQUEST['workoutmember_id']))
	{
		$edit=1;
		$workoutmember_id=esc_attr($_REQUEST['workoutmember_id']);			
		$nutrition_logdata=MJgmgt_get_user_nutrition($workoutmember_id);			
	}
	?>
        <div class="panel-body"><!--PANEL BODY DIV START-->
			<form name="nutrition_form" action="" method="post" class="form-horizontal" id="nutrition_form"><!--Nutrition FORM START-->
			<?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
			<input type="hidden" name="action" value="<?php echo esc_attr($action);?>">
			<input type="hidden" name="nutrition_id" value="<?php echo esc_attr($nutrition_id);?>"  />
				<div class="form-group">
					<label class="col-sm-2 control-label" for="day"><?php esc_html_e('Member','gym_mgt');?><span class="require-field">*</span></label>	
					<div class="col-sm-8">
						<?php if(isset($_POST['member_id'])){$member_id=$_POST['member_id'];}?>
						<select id="member_list" class="display-members" name="member_id" required="true">
							<option value=""><?php esc_html_e('Select Member','gym_mgt');?></option>
								<?php $get_members = array('role' => 'member');
								$membersdata=get_users($get_members);
								if(!empty($membersdata))
								{
									foreach ($membersdata as $member)
									{
										if( $member->membership_status == "Continue")
										{?>
											<option value="<?php echo esc_attr($member->ID);?>" <?php selected(esc_attr($member_id),esc_attr($member->ID));?>><?php echo esc_html($member->display_name)." - ".esc_html($member->member_id); ?> </option>
									<?php
										}
									}
								}?>
						</select>
					</div>
			    </div>
				<!--nonce-->
				<?php wp_nonce_field( 'save_nutrition_nonce' ); ?>
				<!--nonce-->
				<div class="form-group">
					<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Start Date','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-3">
						<input id="Start_date" class="start_date form-control validate[required] text-input" type="text" value="<?php if(isset($_POST['start_date'])){echo esc_attr($_POST['start_date']);}?>" name="start_date" readonly>
					</div>
					<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('End Date','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-3">
						<input id="end_date" class="datepicker form-control validate[required] text-input end_date"  type="text" value="<?php if(isset($_POST['end_date'])){echo $_POST['end_date'];}?>" name="end_date" readonly>
					</div>
				</div>	
				<div class="form-group">				
					<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Select Days','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-8">
					<?php
					foreach (MJgmgt_days_array() as $key=>$name)
					{
					?>
						<div class="col-md-3 padding_left_0">
							<div class="checkbox">
						  		<label>
						  			<input type="checkbox" value="" name="day[]" value="<?php echo esc_attr($key);?>" id="<?php echo esc_attr($key);?>" data-val="day"><?php echo esc_html($name); ?>
						  		</label>
							</div>
						</div>
					<?php
					}
					?>
					</div>	
				</div>
				<div class="form-group">					
					<div class="col-sm-12">		
						<label class="col-sm-2 control-label nutrition_details" for="notice_content"><?php esc_html_e('Nutrition Details','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-md-8 activity_list">
							<label class="activity_title checkbox">				  		
							<strong>
								<input type="checkbox" value="" name="avtivity_id[]" value="breakfast" class="nutrition_check" id="breakfast"  activity_title = "" data-val="nutrition_time"><?php esc_html_e('Break Fast','gym_mgt');?></strong></label>
								<div id="txt_breakfast"></div>
								<label class="activity_title checkbox"><strong><input type="checkbox" value="" name="avtivity_id[]" value="midmorning_snack" class="nutrition_check" id="midmorning_snack"  activity_title = "" data-val="nutrition_time"><?php esc_html_e('Mid Morning Snacks','gym_mgt');?></strong></label>
								<div id="txt_midmorning_snack"></div>
								<label class="activity_title checkbox"><strong>
								<input type="checkbox" value="" name="avtivity_id[]" value="lunch" class="nutrition_check" id="lunch" activity_title = "" data-val="nutrition_time"><?php esc_html_e('Lunch','gym_mgt');?></strong></label>
								<div id="txt_lunch"></div>
								<label class="activity_title checkbox"><strong><input type="checkbox" value="" name="avtivity_id[]" value="afternoon_snack" class="nutrition_check" 
								id="afternoon_snack"  activity_title = "" data-val="nutrition_time"><?php esc_html_e('Afternoon Snacks','gym_mgt');?></strong></label>
								<div id="txt_afternoon_snack"></div>
								<label class="activity_title checkbox"><strong><input type="checkbox" value="" name="avtivity_id[]" value="dinner" class="nutrition_check" id="dinner"  activity_title = "" data-val="nutrition_time"><?php esc_html_e('Dinner','gym_mgt');?></strong></label>
								<div id="txt_dinner"></div>
							<div class="clear"></div>
						</div>
			     	</div>
			    </div>
				<div class="col-sm-offset-2 col-sm-8">
					<div class="form-group">
						<div class="col-md-8">
							<input type="button" value="<?php esc_html_e('Step-1 Add Nutrition','gym_mgt');?>" name="save_nutrition" id="add_nutrition" class="btn btn-success"/>
						</div>
					</div>
				</div>
			    <div id="display_nutrition_list" class="clear_both"></div>
			    <div class="clear"></div>
				</hr>
				<div class="col-sm-offset-2 col-sm-8 schedule-save-button ">
					<input type="submit" value="<?php if($edit){ esc_html_e('Step 2 Save Nutrition Plan','gym_mgt'); }else{ esc_html_e('Step 2 Save Nutrition Plan','gym_mgt');}?>" name="save_nutrition" class="btn btn-success"/>
				</div>
			</form><!--Nutrition FORM END-->
        </div><!--PANEL BODY DIV END-->
<?php 
}
if(isset($nutrition_logdata))
foreach($nutrition_logdata as $row)
{
	$all_logdata=MJgmgt_get_nutritiondata($row->id); 
	$arranged_workout=MJgmgt_set_nutrition_array($all_logdata);
	?>
		<div class="workout_<?php echo $row->id;?> workout-block"><!--WORKOUT BLOCK DIV START-->
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-calendar"></i> 
				<?php
					esc_html_e('Start From ','gym_mgt');
					echo "<span class='work_date'>".MJgmgt_getdate_in_input_box(esc_html($row->start_date))."</span>";
					esc_html_e(' To ','gym_mgt');
					echo "<span class='work_date'>".MJgmgt_getdate_in_input_box(esc_html($row->expire_date)); 
				?>
				</h3>
				<span class="removenutrition badge badge-delete pull-right" id="<?php echo $row->id;?>">X</span>	 
			</div>
			<div class="panel panel-white"><!--PANEL WHITE DIV START-->
				<?php
				if(!empty($arranged_workout))
				{
					?>
					<div class="work_out_datalist_header">
						<div class="col-md-3 col-sm-3 col-xs-3">  
							<strong><?php esc_html_e('Day Name','gym_mgt');?></strong>
						</div>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<span class="col-md-3 hidden-xs"><?php esc_html_e('Time','gym_mgt');?></span>
							<span class="col-md-6"><?php esc_html_e('Description','gym_mgt');?></span>
						</div>
					</div>
					<?php 
					foreach($arranged_workout as $key=>$rowdata)
					{
						?>
						<div class="work_out_datalist">
							<div class="col-md-3 col-sm-3 col-xs-12 day_name">  
								<?php 
								//echo esc_html($key);
								if($key== 'Sunday')
								{
									echo esc_html_e('Sunday','gym_mgt');
								}
								elseif($key == 'Monday')
								{
									echo esc_html_e('Monday','gym_mgt');
								}
								elseif($key == 'Tuesday')
								{
									echo esc_html_e('Tuesday','gym_mgt');
								}
								elseif($key == 'Wednesday')
								{
									echo esc_html_e('Wednesday','gym_mgt');
								}
								elseif($key == 'Thursday')
								{
									echo esc_html_e('Thursday','gym_mgt');
								}
								elseif($key == 'Friday')
								{
									echo esc_html_e('Friday','gym_mgt');
								}
								elseif($key== 'Saturday')
								{
									echo esc_html_e('Saturday','gym_mgt');
								}
								?>
							</div>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<?php
								foreach($rowdata as $row)
								{	
									echo $row."<br>";
								}
								?>
							</div>
						</div>
				<?php } 
				}
				?>
			</div><!--PANEL WHITE DIV END-->
		</div><!--WORKOUT BLOCK DIV END-->
<?php
}	
?>