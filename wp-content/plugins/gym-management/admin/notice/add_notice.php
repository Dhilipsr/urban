<script type="text/javascript">
jQuery(document).ready(function($)
{
	"use strict";
	$('#notice_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});
	$(".start_date").datepicker({
        dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
		minDate:0,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 0);
            $(".end_date").datepicker("option", "minDate", dt);
        }
    });
    $(".end_date").datepicker({
       	dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 0);
            $(".start_date").datepicker("option", "maxDate", dt);
        }
    });	
	
} );
</script>
<?php 	
if($active_tab == 'addnotice')
{
	$notice_id=0;
	$edit=0;
	if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
	{	
		$notice_id=esc_attr($_REQUEST['notice_id']);				
		$edit=1;
		$result = get_post($notice_id);
	}
	?>
    <div class="panel-body"><!--PANEL BODY DIV START-->
		<form name="notice_form" action="" method="post" class="form-horizontal" id="notice_form"><!--NOTICE FORM START-->
			<?php 
			$action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
			<input type="hidden" name="action" value="<?php echo esc_attr($action);?>">
			<input type="hidden" name="notice_id" value="<?php echo esc_attr($notice_id);?>"  />
			<div class="form-group">
				<label class="col-sm-2 control-label" for="notice_title"><?php esc_html_e('Notice Title','gym_mgt');?><span class="require-field">*</span></label>
				<div class="col-sm-8">
					<input id="notice_title" class="form-control validate[required,custom[popup_category_validation]] text-input" maxlength="50" type="text" value="<?php if($edit){ echo esc_attr($result->post_title);}?>" name="notice_title">
					 <input type="hidden" name="notice_id" value="<?php if($edit){ echo esc_attr($result->ID);}?>"/> 
				</div>
			</div>
			<!--nonce-->
			<?php wp_nonce_field( 'save_notice_nonce' ); ?>
			<!--nonce-->
			<div class="form-group">
				<label class="col-sm-2 control-label" for="notice_for"><?php esc_html_e('Notice For','gym_mgt');?></label>
				<div class="col-sm-8">
				 	<select name="notice_for" id="notice_for" class="form-control notice_for">
				   		<option value = "all"><?php esc_html_e('All','gym_mgt');?></option>
				   		<option value="staff_member" <?php if($edit) echo selected(get_post_meta( $result->ID, 'notice_for',true),'staff_member');?>><?php esc_html_e('Staff Members','gym_mgt');?></option>
				   		<option value="member" <?php if($edit) echo selected(get_post_meta( $result->ID, 'notice_for',true),'member');?>><?php esc_html_e('Member','gym_mgt');?></option>
				   		<option value="accountant" <?php if($edit) echo selected(get_post_meta( $result->ID, 'notice_for',true),'accountant');?>><?php esc_html_e('Accountant','gym_mgt');?></option>
					</select>
				</div>
			</div>
			<div class="class_div">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="class_id"><?php esc_html_e('Class','gym_mgt');?></label>
					<div class="col-sm-8">
						<?php 
						if($edit)
						{
							$class_id=get_post_meta($result->ID,'gmgt_class_id',true);
						}
						elseif(isset($_POST['class_id']))
						{
							$class_id=sanitize_text_field($_POST['class_id']);
						}
						else
						{
							$class_id='';
						}
						?>
						<select id="class_id" class="form-control" name="class_id">
							<option value=""><?php esc_html_e('Select Class','gym_mgt');?></option>
							 <?php $classdata=$obj_class->MJgmgt_get_all_classes();
							 if(!empty($classdata))
							 {
								foreach ($classdata as $class){?>
									<option value="<?php echo esc_attr($class->class_id);?>" <?php selected($class_id,$class->class_id);  ?>><?php echo esc_html($class->class_name); ?> </option>
						<?php } } ?>
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Notice Start Date','gym_mgt');?><span class="require-field">*</span></label>
				<div class="col-sm-8">
					<input id="notice_Start_date" class="start_date form-control validate[required] text-input"  type="text" value="<?php if($edit){ echo esc_attr(MJgmgt_getdate_in_input_box(get_post_meta($result->ID,'gmgt_start_date',true)));}?>" name="start_date" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Notice End Date','gym_mgt');?><span class="require-field">*</span></label>
				<div class="col-sm-8">
					<input id="notice_end_date" class="end_date form-control validate[required] text-input"  type="text" value="<?php if($edit){ echo esc_attr(MJgmgt_getdate_in_input_box(get_post_meta($result->ID,'gmgt_end_date',true)));}?>" name="end_date" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Notice Comment','gym_mgt');?></label>
				<div class="col-sm-8">
					<textarea name="notice_content" class="form-control validate[custom[address_description_validation]]" maxlength="500" id="notice_content"><?php if($edit){ echo esc_attr($result->post_content);}?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label " for="enable"><?php esc_html_e('Send Mail','gym_mgt');?></label>
				<div class="col-sm-8">
					 <div class="checkbox">
						<label>
							<input id="chk_sms_sent_mail" type="checkbox" <?php $gym_enable_notifications = 0;if($gym_enable_notifications) echo "checked";?> value="1" name="gym_enable_notifications">
						</label>
					</div>				 
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label " for="enable"><?php esc_html_e('Send SMS','gym_mgt');?></label>
				<div class="col-sm-8">
					 <div class="checkbox">
						<label>
							<input id="chk_sms_sent" type="checkbox" <?php $gmgt_sms_service_enable = 0;if($gmgt_sms_service_enable) echo "checked";?> value="1" name="gmgt_sms_service_enable">
						</label>
					</div>				 
				</div>
			</div>
			<div id="hmsg_message_sent" class="hmsg_message_none">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="sms_template"><?php esc_html_e('SMS Text','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-8">
						<textarea name="sms_template" class="form-control validate[required]" maxlength="160"></textarea>
						<label><?php esc_html_e('Max. 160 Character','gym_mgt');?></label>
					</div>
				</div>
			</div>
			<div class="col-sm-offset-2 col-sm-8">
				<input type="submit" value="<?php if($edit){ esc_html_e('Save Notice','gym_mgt'); }else{ esc_html_e('Save Notice','gym_mgt');}?>" name="save_notice" class="btn btn-success"/>
			</div>
		</form><!--NOTICE FORM END-->
    </div><!--PANEL BODY DIV START-->
<?php 
}
?>