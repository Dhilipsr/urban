<?php 
$class_id =0;
?>
<script type="text/javascript">
$(document).ready(function() 
{
	"use strict";
	$('#product_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});
	$('.curr_date').datepicker(
	{
		dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
		endDate: '+0d',
		maxDate:'today',
		autoclose: true
	});	
$('.checkAll').on('change',function()
{
    var state = this.checked;
    state? $(':checkbox').prop('checked',true):$(':checkbox').prop('checked',false);
    state? $(this).next('b').text('Uncheck All') :$(this).next('b').text('Check All')
});
} );
</script>
<?php
if($active_tab == 'staff_attendence') 
{
	?>
	<div class="panel-body"> <!-- PANEL BODY DIV START-->
		<form method="post">          
			<div class="form-group col-md-3">
				<label class="col-sm-2 control-label" for="tcurr_date"><?php esc_html_e('Date','gym_mgt');?></label>			
					<input class="form-control curr_date" type="text"  value="<?php if(isset($_POST['tcurr_date'])) echo esc_attr($_POST['tcurr_date']);else echo MJgmgt_getdate_in_input_box(date("Y-m-d"));;?>" name="tcurr_date" readonly>			
			</div>
			<!--nonce-->
			<?php wp_nonce_field( 'staff_attendence_nonce' ); ?>
			<!--nonce-->
			 <div class="form-group col-md-3 button-possition">
				<label for="subject_id">&nbsp;</label>
				<input type="submit" value="<?php esc_html_e('Take/View  Attendance','gym_mgt');?>" name="staff_attendence"  class="btn btn-success"/>
			</div>
		 </form>
	</div> <!-- PANEL BODY DIV END-->
	<div class="clearfix"> </div>		  
	<?php 
	if(isset($_REQUEST['staff_attendence']) || isset($_REQUEST['save_staff_attendence']))
	{
	  $past_attendance=get_option('gym_enable_past_attendance'); ?>
		<div class="panel-body"> <!-- PANEL BODY DIV START--> 
			<form method="post"> 
				<input type="hidden" name="class_id" value="<?php echo esc_attr($class_id);?>" />
				<input type="hidden" name="tcurr_date" value="<?php echo esc_attr($_POST['tcurr_date']);?>" />
				 <div class="panel-heading">
					<h4 class="panel-title"><?php esc_html_e('Staff Attendance','gym_mgt');?> , 
					<?php esc_html_e('Date')?> :  <?php echo MJgmgt_getdate_in_input_box(MJgmgt_get_format_for_db(sanitize_text_field($_POST['tcurr_date'])));?></h4>
				 </div>
				 <div class="status">
				<?php 					
					$date=MJgmgt_get_format_for_db(sanitize_text_field($_POST['tcurr_date']));
					$i=1;
					$teacher = get_users(array('role'=>'staff_member'));
					foreach ($teacher as $user) 
					{
						$check_attendance = $obj_attend->MJgmgt_check_staff_attendence($user->ID,$date);
						$attendanc_status = "Present";
						if(!empty($check_attendance))
						{
							$attendanc_status = $check_attendance->status;		 
						}
						echo '<tr>';  
						echo '<tr>';
						echo '<td>['.esc_html($i).']</td>';
						echo '<td><span>' .esc_html($user->first_name).' '.esc_html($user->last_name). '</span></td>';
						?>
						<td><label class="radio-inline"><input type="radio" name = "attendanace_<?php echo esc_attr($user->ID); ?>" value ="Present" <?php checked( esc_html($attendanc_status), 'Present' );?>>
						<?php esc_html_e('Present','gym_mgt');?></label>
						<label class="radio-inline "> <input type="radio" name = "attendanace_<?php echo esc_attr($user->ID); ?>" value ="Absent" <?php checked( esc_html($attendanc_status), 'Absent' );?>>
						<?php esc_html_e('Absent','gym_mgt');?></label></td><?php
						echo '</tr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						$i++;
					}
					?>
					</div>
					<?php
					if($past_attendance == "yes")
					{ ?>
						<div class="form-group status">
							<label class="radio-inline">
								<input type="radio" name="status" value="Present" checked="checked"/> <?php esc_html_e('Present','gym_mgt');?>
							</label>
							<label class="radio-inline">
								<input type="radio" name="status" value="Absent" /> <?php esc_html_e('Absent','gym_mgt');?><br />
							</label>
						</div>
					<?php
					}
					else
					{
						if($date == date("Y-m-d"))
						{?> 
								<div class="form-group status">
								  	<label class="radio-inline">
								  		<input type="radio" name="status" value="Present" checked="checked"/> <?php esc_html_e('Present','gym_mgt');?>
								  	</label>
								  	<label class="radio-inline">
								  		<input type="radio" name="status" value="Absent" /> <?php esc_html_e('Absent','gym_mgt');?><br />
								  	</label>
								</div>
						<?php
						}
					}?>
					<div class="col-md-12">
						<table class="table">
							<tr>
								<?php
								if($past_attendance == "yes")
								{ ?>
									<th id="width_50px"><input type="checkbox" name="selectall" class="checkAll" id="selectall"/></th>
								<?php }
								else
								{
									if($date == date("Y-m-d"))
									{?> 
									   <th id="width_50px"><input type="checkbox" name="selectall" class="checkAll" id="selectall"/></th>
									  <?php
									}
									else 
									{
									?>
									   <th id="width_70px"><?php esc_html_e('Status','gym_mgt');?></th>
									<?php 
									}
								}
								?>
									<th id="width_250px"><?php esc_html_e('Staff Member Name','gym_mgt');?></th>
								<?php
								if($past_attendance == "yes")
								{ ?>
									<th id="width_70px"><?php esc_html_e('Status','gym_mgt');?>
									</th>
								<?php
								}
								else
								{
									if($date == date("Y-m-d"))
									{?> 
										<th id="width_70px"><?php esc_html_e('Status','gym_mgt');?>
										</th>
									<?php
									}
									else
									{
									?>
										<th id="width_70px"></th>
									<?php
									}	
								}?>
							</tr>
							<?php
							$teacher = get_users(array('role'=>'staff_member'));
							$date=MJgmgt_get_format_for_db(esc_attr($_REQUEST['tcurr_date']));
							foreach ( $teacher as $user )
							{
								$date=MJgmgt_get_format_for_db(esc_attr($_REQUEST['tcurr_date']));
								$check_result=$obj_attend->MJgmgt_check_staff_attendence($user->ID,$date);
								echo '<tr>';
								if($past_attendance == "yes")
								{ ?>
								<td class="checkbox_field">
								<span><input type="checkbox" class="checkbox1" name="attendence[]" value="<?php echo esc_attr($user->ID); ?>" 
								<?php 
								if($check_result=='true'){ echo "checked=\'checked\'"; } ?> /></span>
								</td>
								<?php
								}
								else
								{
									if($date== date("Y-m-d"))
									{?> 
									<td class="checkbox_field">
										<span><input type="checkbox" class="checkbox1" name="attendence[]" value="<?php echo esc_attr($user->ID); ?>" <?php if($check_result=='true'){ echo "checked=\'checked\'"; } ?> /></span>
									</td>
									<?php
									}
									else 
									{
										?>
										<td><?php if($check_result=='true') esc_html_e('Present','gym_mgt'); else esc_html_e('Absent','gym_mgt');?></td>
										<?php 
									}
								}
								echo '<td><span>' .esc_html($user->first_name).' '.esc_html($user->last_name).' </span></td>';
									if(!empty($check_result))
									{ 
										 echo '<td><span>' .esc_html($check_result->status).'</span></td>';
									}
									else 
									{
										echo '<td>&nbsp;</td>';
									}
								echo '</tr>';
							}?>
						</table>
					</div>
					<div class="cleatrfix"></div>
					<div class="col-sm-8">    
					<?php
					if($past_attendance == "yes")
					{ ?>
						<input type="submit" value="<?php esc_html_e('Save Attendance','gym_mgt');?>" name="save_staff_attendence" class="btn btn-success" />
					<?php }
					else
					{
					if($date == date("Y-m-d")){?>       	    	
					<input type="submit" value="<?php esc_html_e('Save Attendance','gym_mgt');?>" name="save_staff_attendence" class="btn btn-success" />
					<?php }
					}?>
				</div>
			</form>
		</div> <!-- PANEL BODY DIV END-->
		<?php
	}
}?>