<?php
$curr_user_id=get_current_user_id();
$obj_gym=new MJgmgt_Gym_management($curr_user_id);
$obj_class=new MJgmgt_classschedule;
$obj_workouttype=new MJgmgt_workouttype;
$obj_activity=new MJgmgt_activity;
$obj_workout=new MJgmgt_workout;
$active_tab = isset($_REQUEST['tab'])?$_REQUEST['tab']:'workoutassignlist';
//access right
$user_access=MJgmgt_get_userrole_wise_page_access_right_array();
if (isset ( $_REQUEST ['page'] ))
{	
	if($user_access['view']=='0')
	{	
		MJgmgt_access_right_page_not_access_message();
		die;
	}
	if(!empty($_REQUEST['action']))
	{
		if (isset ( $_REQUEST ['page'] ) && $_REQUEST ['page'] == $user_access['page_link'] && ($_REQUEST['action']=='delete'))
		{
			if($user_access['delete']=='0')
			{	
				MJgmgt_access_right_page_not_access_message();
				die;
			}	
		}
		if (isset ( $_REQUEST ['page'] ) && $_REQUEST ['page'] == $user_access['page_link'] && ($_REQUEST['action']=='view'))
		{
			if($user_access['add']=='0')
			{	
				MJgmgt_access_right_page_not_access_message();
				die;
			}	
		}
	}
}
?>
<script type="text/javascript">
jQuery(document).ready(function($) 
{
	"use strict";
	jQuery('#assignworkout_list').DataTable({
		"responsive": true,
		 "order": [[ 1, "asc" ]],
		 "aoColumns":[
					  {"bSortable": false},
	                  {"bSortable": true},
	                  {"bSortable": true},
					 {"bSortable": false}],
				language:<?php echo MJgmgt_datatable_multi_language();?>		 
		});
	//$(".display-members").select2();
} );
</script>
<?php 
//SAVE WORKOUT TYPE DATA
if(isset($_POST['save_workouttype']))
{
	$nonce = $_POST['_wpnonce'];
	if (wp_verify_nonce($nonce, 'save_workouttype_nonce' ))
	{
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
		{
			$result=$obj_workouttype->MJgmgt_add_workouttype($_POST);
			if($result)
			{
				wp_redirect ( home_url() .'?dashboard=user&page=assign-workout&tab=workoutassignlist&message=2');
			}
		}
		else
		{
			$result=$obj_workouttype->MJgmgt_add_workouttype($_POST);

			if($result)
			{
				wp_redirect ( home_url() .'?dashboard=user&page=assign-workout&tab=workoutassignlist&message=1');
			}
		}
	}
}
//Delete WORKOUT TYPE DATA
if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
{
	$result=$obj_workouttype->MJgmgt_delete_workouttype($_REQUEST['assign_workout_id']);
	if($result)
	{
		wp_redirect ( home_url() .'?dashboard=user&page=assign-workout&tab=workoutassignlist&message=3');
	}
}
if(isset($_REQUEST['message']))
{
	$message =esc_attr($_REQUEST['message']);
	if($message == 1)
	{?>
		<div id="message" class="updated below-h2 ">
			<p><?php esc_html_e('Workout added successfully.','gym_mgt');?></p>
		</div>
	<?php	
	}
	elseif($message == 2)
	{?>
		<div id="message" class="updated below-h2 ">
			<p><?php esc_html_e("Workout updated successfully.",'gym_mgt');?></p>
		</div>
	<?php	
	}
	elseif($message == 3) 
	{?>
		<div id="message" class="updated below-h2">
			<p><?php esc_html_e('Workout deleted successfully.','gym_mgt');?></p>
		</div>
	<?php
	}
}
?>
<!-- POP up code -->
<div class="popup-bg">
    <div class="overlay-content">
		<div class="modal-content">
		    <div class="category_list"></div>
        </div>
    </div> 
</div>
<!-- End POP-UP Code -->
<div class="panel-body panel-white"><!-- PANEL BODY DIV START -->
	<ul class="nav nav-tabs panel_tabs" role="tablist"><!-- NAV TABS MENU START -->
		<li class="margin_top_5 <?php if($active_tab == 'workoutassignlist') echo "active";?>">
			<a href="?dashboard=user&page=assign-workout&tab=workoutassignlist">
				 <i class="fa fa-align-justify"></i> <?php esc_html_e('Assigned Workout', 'gym_mgt'); ?>
			</a>
		</li>	  
		<?php
		if($user_access['add']=='1')
		{
		?>
		 <li class="margin_top_5 <?php if($active_tab == 'assignworkout') echo "active";?>">
			<a href="?dashboard=user&page=assign-workout&tab=assignworkout">
			<i class="fa fa-plus-circle"></i> <?php
			if(isset($_REQUEST['action']) && $_REQUEST['action'] =='view')
			{
				esc_html_e('View Assigned Workout', 'gym_mgt'); 
			}
			else
			{
				esc_html_e('Assign Workout', 'gym_mgt'); 
			}
			?></a> 
		  </li>	 
		<?php 
		}
		?>	
	</ul><!-- NAV TABS MENU END -->
	<div class="tab-content"><!-- TAB CONTENT DIV START -->
    	<div class="tab-pane <?php if($active_tab == 'workoutassignlist') echo "fade active in";?>" id="workoutassignlist"><!-- TAB PANE DIV START -->
			<div class="panel-body"><!-- PANEL BODY DIV START -->
			    <?php 
				if($obj_gym->role == 'member')
				{ ?>
					<div class="col-sm-4 print-button pull-right">
						<a href="?page=assign-workout&print=print" target="_blank"class="btn btn-success margin_top_5"><?php esc_html_e('Print Workout','gym_mgt');?></a>
						<a href="?page=assign-workout&workout_pdf=workout_pdf" target="_blank" class="btn btn-success workout_pdf margin_top_5"><?php esc_html_e('PDF Workout','gym_mgt');?></a>
					</div> <?php
					$current_user_id = get_current_user_id();
					$workout_logdata=MJgmgt_get_userworkout($current_user_id);
					if(!empty($workout_logdata))
					{
						foreach($workout_logdata as $row)
						{
							$all_logdata=MJgmgt_get_workoutdata($row->workout_id); 
							$arranged_workout=MJgmgt_set_workoutarray($all_logdata);
							?>
							<div class="workout_<?php echo $row->workout_id;?> workout-block"><!-- WORKOUT BLOCK DIV START -->
								<div class="panel-heading">
									<h3 class="panel-title row">
										<div class="col-xs-8">
										<i class="fa fa-calendar"></i> 
										<?php 
										esc_html_e('Start From ','gym_mgt');
										echo "<span class='work_date'>".MJgmgt_getdate_in_input_box(esc_html($row->start_date))."</span>";
										esc_html_e(' To ','gym_mgt');
										echo "<span class='work_date'>".MJgmgt_getdate_in_input_box(esc_html($row->end_date))."</span>";
										?>
										&nbsp;
										 <?php
										if(!empty($row->description))
										{
											esc_html_e('Description :','gym_mgt');
											echo "<span class='work_date'>".esc_html(esc_html($row->description))."</span>";
										}
										?>
										</div>
										<div class="col-xs-3">
										<?php
										if($user_access['delete']=='1')
										{
										?>
											<span class="removenutrition badge badge-delete pull-right" id="<?php echo esc_attr($row->workout_id);?>">X</span> 
										<?php
										}
										?>
										</div>
									</h3>
								</div>
								<div class="panel panel-white"><!-- PANEL WHITE DIV START -->
									<?php
										if(!empty($arranged_workout))
										{
										?>
											<div class="work_out_datalist_header">
												<div class="col-md-4 col-sm-4">  
													<strong><?php esc_html_e('Day Name','gym_mgt');?></strong>
												</div>
												<div class="col-md-8 col-sm-8 hidden-xs">
													<span class="col-md-3"><?php esc_html_e('Activity','gym_mgt');?></span>
													<span class="col-md-3"><?php esc_html_e('Sets','gym_mgt');?></span>
													<span class="col-md-2"><?php esc_html_e('Reps','gym_mgt');?></span>
													<span class="col-md-2"><?php esc_html_e('KG','gym_mgt');?></span>
													<span class="col-md-2"><?php esc_html_e('Rest Time','gym_mgt');?></span>
												</div>
											</div>
											<?php 
											foreach($arranged_workout as $key=>$rowdata)
											{
												?>
												<div class="work_out_datalist">
													<div class="col-md-4 day_name">
														<?php
														if($key =='Sunday')
														{
															echo esc_html_e('Sunday','gym_mgt');
														} 	
														elseif($key =='Monday')
														{
															echo esc_html_e('Monday','gym_mgt');
														} 	
														elseif($key =='Tuesday')
														{
															echo esc_html_e('Tuesday','gym_mgt');
														} 	
														elseif($key =='Wednesday')
														{
															echo esc_html_e('Wednesday','gym_mgt');
														} 	
														elseif($key =='Thursday')
														{
															echo esc_html_e('Thursday','gym_mgt');
														} 	
														elseif($key =='Friday')
														{
															echo esc_html_e('Friday','gym_mgt');
														} 	
														elseif($key =='Saturday')
														{
															echo esc_html_e('Saturday','gym_mgt');
														} 	  
														 //esc_html_e($key,'gym_mgt');
														 ?>
													</div>
													<div class="col-md-8 col-xs-12">
														<?php 
														foreach($rowdata as $row)
														{
															echo $row."<br>";
														} ?>
													</div>
												</div>
											 <?php 
											} 
										}
										?>										
								</div><!-- PANEL WHITE DIV END -->
							</div><!-- WORKOUT BLOCK DIV END -->
				  <?php }
					}
					else
					{
						esc_html_e('No Any Assigned Workout Data','gym_mgt');
					}		
			    }
				else
				{
					?>
					<div class="table-responsive"><!-- TABLE RESPONSIVE DIV START -->
						<table id="assignworkout_list" class="display" cellspacing="0" width="100%"><!-- ASSIGNED WORKOUT TABLE START -->
							<thead>
								<tr>
									<th><?php esc_html_e('Photo', 'gym_mgt' ) ;?></th>
									<th><?php esc_html_e('Member Name', 'gym_mgt' ) ;?></th>
									<th><?php esc_html_e('Member Goal', 'gym_mgt' ) ;?></th>
									<th><?php esc_html_e('Action', 'gym_mgt' ) ;?></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th><?php esc_html_e('Photo', 'gym_mgt' ) ;?></th>
									<th><?php esc_html_e('Member Name', 'gym_mgt' ) ;?></th>
									<th><?php esc_html_e('Member Goal', 'gym_mgt' ) ;?></th>
									<th><?php esc_html_e('Action', 'gym_mgt' ) ;?></th>
								</tr>
							</tfoot>
							<tbody>
								<?php 
								$get_members = array('role' => 'member');
								$membersdata=get_users($get_members);
								if(!empty($membersdata))
								{
									foreach ($membersdata as $retrieved_data){?>
									<tr>
										<td class="user_image"><?php $uid=$retrieved_data->ID;
											$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);
											if(empty($userimage))
											{
												echo '<img src='.get_option( 'gmgt_system_logo' ).' height="50px" width="50px" class="img-circle" />';
											}
											else
											{
												echo '<img src='.$userimage.' height="50px" width="50px" class="img-circle"/>';
											}
										?></td>
										<td class="member">
										<?php if($obj_gym->role == 'staff_member'){?>
										<a href="#"><?php $user=get_userdata($retrieved_data->ID);
										$display_label=$user->display_name;
										$memberid=get_user_meta($retrieved_data->ID,'member_id',true);
											if($memberid)
											{
												$display_label.=" (".$memberid.")";
												echo esc_html($display_label);
											}
										?></a>
										<?php }
										else
										{?>
										<a href=""><?php $user=get_userdata($retrieved_data->ID);
										$display_label=$user->display_name;
										$memberid=get_user_meta($retrieved_data->ID,'member_id',true);
											if($memberid)
											{
												$display_label.=" (".$memberid.")";
												echo esc_html($display_label);
											}
											?></a>
										<?php }
										?></td>
										<td class="member-goal"><?php $intrestid=get_user_meta($retrieved_data->ID,'intrest_area',true);
										if(!empty($intrestid))
										{
											echo get_the_title($intrestid);
										}
										else
										{
											echo "-";
										}
										?></td>
										<td class="action"> 
										<?php if($obj_gym->role == 'staff_member'|| ($obj_gym->role == 'member' && $retrieved_data->ID==get_current_user_id())){?>
											<a href="?dashboard=user&page=assign-workout&tab=assignworkout&action=view&workoutmember_id=<?php echo esc_attr($retrieved_data->ID);?>" class="btn btn-default"><i class="fa fa-eye"></i></i> <?php esc_html_e('View Workouts', 'gym_mgt');?></a>
										<?php 
										}
										?>
									   </td>
									</tr>
									<?php 
									} 
								}?>
							</tbody>
						</table><!-- ASSIGNED WORKOUT TABLE END -->
					</div><!-- TABLE RESPONSIVE DIV END-->
					<?php
				}
				?>
	    	</div><!-- PANEL BODY DIV END -->
	    </div><!-- TAB PANE DIV END -->
		<script type="text/javascript">
		jQuery(document).ready(function($) 
		{
			$("#member_list").select2();
			$('#workouttype_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});
			
				$("#start_date").datepicker(
				{	
					dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
					minDate:0,
					onSelect: function (selected) 
					{
						var dt = new Date(selected);
						dt.setDate(dt.getDate() + 0);
						$("#end_date").datepicker("option", "minDate", dt);
					}
				});
				$("#end_date").datepicker(
				{		
				   dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
					onSelect: function (selected) {
						var dt = new Date(selected);
						dt.setDate(dt.getDate() - 0);
						$("#start_date").datepicker("option", "maxDate", dt);
					}
				});
		} );
		</script>
		<style>
		div#ui-datepicker-div{z-index:99999 !important}
		</style>
		<?php 	
			$workoutmember_id=0;
			$edit=0;
			if(isset($_REQUEST['workouttype_id']))
				$workouttype_id=esc_attr($_REQUEST['workouttype_id']);
			if(isset($_REQUEST['workoutmember_id']))
			{
				$edit=1;
				$workoutmember_id=esc_attr($_REQUEST['workoutmember_id']);
				$workout_logdata=MJgmgt_get_userworkout($workoutmember_id);
			}
			if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'view')
			{
				$edit=1;	
			}
			?>	
			<div class="tab-pane <?php if($active_tab == 'assignworkout') echo "fade active in";?>"><!-- TAB PANE DIV START -->
			   <?php 
				if($obj_gym->role == 'staff_member' || $obj_gym->role == 'member' )
				{
					?>
					<div class="panel-body"><!-- PANEL BODY DIV START -->
						<form name="workouttype_form" action="" method="post" class="form-horizontal" id="workouttype_form"><!-- WORKOUT TYPE FORM START -->
							<?php 
							$action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
							<input type="hidden" name="action" value="<?php echo esc_attr($action);?>">
							<input type="hidden" name="assign_workout_id" value="" />
							<div class="form-group">
								<label class="col-sm-2 control-label" for="day"><?php esc_html_e('Member','gym_mgt');?><span class="require-field">*</span></label>	
								<div class="col-sm-8">
									<?php if($edit){ $member_id=$workoutmember_id; }elseif(isset($_POST['member_id'])){$member_id=$_POST['member_id'];}else{$member_id='';}?>
									<select id="member_list" class="display-members assigned_workout_member_id" name="member_id" required="true">
										<option value=""><?php esc_html_e('Select Member','gym_mgt');?></option>
										<?php $get_members = array('role' => 'member');
										$membersdata=get_users($get_members);
										 if(!empty($membersdata))
										 {
											foreach ($membersdata as $member)
											{ 
												if( $member->membership_status == "Continue")
												{?>
												<option value="<?php echo esc_attr($member->ID);?>" <?php selected(esc_attr($member_id),esc_attr($member->ID));?>><?php echo esc_html($member->display_name)." - ".esc_html($member->member_id); ?> </option>
											<?php }
											}
										 }?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="level_id"><?php esc_html_e('Level','gym_mgt');?></label>
								<div class="col-sm-8">
									<select class="form-control" name="level_id" id="level_type">
									<option value=""><?php esc_html_e('Select Level','gym_mgt');?></option>
									<?php 
									if(isset($_REQUEST['level_id']))
									{
										$category =esc_attr($_REQUEST['level_id']);  
									}
									elseif($edit)
									{
										$category =$result->level_id;
									}
									else
									{ 
										$category = "";
									}
									$measurmentdata=MJgmgt_get_all_category('level_type');
									if(!empty($measurmentdata))
									{
										foreach ($measurmentdata as $retrive_data)
										{
											echo '<option value="'.esc_attr($retrive_data->ID).'" '.selected(esc_attr($category),esc_attr($retrive_data->ID)).'>'.esc_html($retrive_data->post_title).'</option>';
										}
									}
									?>
									</select>
								</div>
								<div class="col-sm-2 add_category_padding_0"><button id="addremove" model="level_type"><?php esc_html_e('Add Or Remove','gym_mgt');?></button></div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="description"><?php esc_html_e('Description','gym_mgt');?></label>
								<div class="col-sm-8">
									<textarea id="description" class="form-control validate[custom[address_description_validation]]" maxlength="150" name="description"><?php if(isset($_POST['description'])) echo esc_html($_POST['description']); ?> </textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="start_date"><?php esc_html_e('Start Date','gym_mgt');?><span class="require-field">*</span></label>
								<div class="col-sm-3">
									<input id="start_date" class="form-control validate[required]" type="text"   name="start_date" 
									value="<?php if(isset($_POST['start_date'])){ echo esc_attr($_POST['start_date']);}?>" readonly>
								</div>
								
								<label class="col-sm-2 control-label" for="end_date"><?php esc_html_e('End Date','gym_mgt');?><span class="require-field">*</span></label>
								
								<div class="col-sm-3">
									<input id="end_date" class="form-control validate[required]" type="text"   name="last_date" 
									value="<?php if(isset($_POST['end_date'])){ echo esc_attr($_POST['end_date']);}?>" readonly>
								</div>
							</div>
							<div class="form-group">				
								<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Select Days','gym_mgt');?><span class="require-field">*</span></label>
								<div class="col-sm-8">			
								<?php
								foreach (MJgmgt_days_array() as $key=>$name)
								{
								?>
								<div class="col-md-3 padding_left_0">
									<div class="checkbox">
									  	<label><input type="checkbox" value="" name="day[]" value="<?php echo esc_attr($key);?>" id="<?php echo esc_attr($key);?>" data-val="day"><?php echo esc_html($name); ?> </label>
									</div>
								</div>
								<?php
								}
								?>
								</div>	
							</div>	
							<div class="form-group">					
								<div class="col-sm-12">		
									<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Workout Details','gym_mgt');?><span class="require-field">*</span></label>
									<div class="col-md-10 activity_list member_workout_activity padding_left_0">
										<?php 
										$activity_category=MJgmgt_get_all_category('activity_category');
										if(!empty($activity_category))
										{
											foreach ($activity_category as $retrive_data)
											{									
												?>		
												<label class="activity_title"><strong><?php echo esc_html($retrive_data->post_title); ?></strong></label>
												<?php 
												$activitydata =MJgmgt_get_activity_by_category($retrive_data->ID);
												foreach($activitydata as $activity)
												{										
													?>
													<div class="checkbox child">
													<label class="col-sm-2 padding_top_7 padding_bottom_7">
													<input type="checkbox" value="" name="avtivity_id[]" value="<?php echo esc_attr($activity->activity_id);?>" class="activity_check" 
													id="<?php echo esc_attr($activity->activity_id);?>" data-val="activity" activity_title = "<?php echo esc_attr($activity->activity_title); ?>">
													<?php echo esc_html($activity->activity_title); ?> 
													</label>
													<div id="reps_sets_<?php echo esc_attr($activity->activity_id);?>" class="col-sm-10 padding_0"></div>
												</div>
												<div class="clear"></div>
													<?php 
												}
												?>
												<div class="clear"></div>
												<?php 
											}
										}?>			
									</div>
								</div>
							</div>					
							<div class="col-sm-offset-2 col-sm-10">
								<div class="form-group">
									<div class="col-md-8">
										<input type="button" value="<?php esc_html_e('Step-1 Add Workout','gym_mgt');?>" name="sadd_workouttype" id="add_workouttype" class="btn btn-success"/>
									</div>
								</div>
							</div>
							<!--nonce-->
							<?php wp_nonce_field( 'save_workouttype_nonce' ); ?>
							<!--nonce-->
							<div id="display_rout_list"></div>
							<div class="col-sm-offset-2 col-sm-8 schedule-save-button ">
								<input type="submit" value="<?php if($edit){ esc_html_e('Step-2 Save Workout','gym_mgt'); }else{ esc_html_e('Step-2 Save Workout','gym_mgt');}?>" name="save_workouttype" class="btn btn-success"/>
							</div>
						</form><!-- WORKOUT TYPE FORM END -->
					</div><!-- PANEL BODY DIV END -->
					<?php 
				}
				if($obj_gym->role == 'staff_member'|| ($obj_gym->role == 'member' && $workoutmember_id==get_current_user_id()))
				{
					if(isset($workout_logdata))
					foreach($workout_logdata as $row)
					{
						$all_logdata=MJgmgt_get_workoutdata($row->workout_id);
						$arranged_workout=MJgmgt_set_workoutarray($all_logdata);
						?>
						<div class="workout_<?php echo $row->workout_id;?> workout-block">
							<div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-calendar"></i> 
								<?php
								esc_html_e('Start From ','gym_mgt');
								echo "<span class='work_date'>".MJgmgt_getdate_in_input_box(esc_html($row->start_date))."</span>";
								esc_html_e(' To ','gym_mgt');
								echo "<span class='work_date'>".MJgmgt_getdate_in_input_box(esc_html($row->end_date))."</span>";
								if(!empty($row->description))
								{
									esc_html_e('  Description : ','gym_mgt');
									echo "<span class='work_date'>".esc_html($row->description)."</span>";
								}
								?>
								<?php
								if($user_access['delete']=='1')
								{
								?>
									<span class="removeworkout badge badge-delete pull-right" id="<?php echo esc_attr($row->workout_id);?>">X</span>	
								<?php
								}
								?>
								</h3>						
							</div>	
							<div class="panel panel-white">
								<?php
								if(!empty($arranged_workout))
								{
								?>
									<div class="work_out_datalist_header">
										<div class="col-md-2 col-sm-2">  
											<strong><?php esc_html_e('Day Name','gym_mgt');?></strong>
										</div>
										<div class="col-md-10 col-sm-10 hidden-xs">
											<span class="col-md-3"><?php esc_html_e('Activity','gym_mgt');?></span>
											<span class="col-md-3"><?php esc_html_e('Sets','gym_mgt');?></span>
											<span class="col-md-2"><?php esc_html_e('Reps','gym_mgt');?></span>
											<span class="col-md-2"><?php esc_html_e('KG','gym_mgt');?></span>
											<span class="col-md-2"><?php esc_html_e('Rest Time','gym_mgt');?></span>
										</div>
									</div>
									<?php 
									foreach($arranged_workout as $key=>$rowdata)
									{
										?>
										<div class="work_out_datalist">
											<div class="col-md-2 day_name">  
												<?php echo esc_html($key);?>
											</div>
											<div class="col-md-10 col-xs-12">
													<?php foreach($rowdata as $row){
															echo $row."<div class='clearfix'></div><br>";
													} ?>
											</div>
										</div>
							  <?php }
								}?>
							</div>
						</div>
					<?php
					}						
				}	?>
			</div><!-- TAB PANE DIV END -->
	</div><!-- TAB CONTENT DIV END -->
</div><!-- PANEL BODY DIV END -->