<?php 
$obj_gym = new MJgmgt_Gym_management(get_current_user_id());
$obj_message= new MJgmgt_message;
$active_tab = isset($_GET['tab'])?$_GET['tab']:'inbox';
//access right
$user_access=MJgmgt_get_userrole_wise_page_access_right_array();
if (isset ( $_REQUEST ['page'] ))
{	
	if($user_access['view']=='0')
	{	
		MJgmgt_access_right_page_not_access_message();
		die;
	}
	if(!empty($_REQUEST['action']))
	{
		if (isset ( $_REQUEST ['page'] ) && $_REQUEST ['page'] == $user_access['page_link'] && ($_REQUEST['action']=='edit'))
		{
			if($user_access['edit']=='0')
			{	
				MJgmgt_access_right_page_not_access_message();
				die;
			}			
		}
		if (isset ( $_REQUEST ['page'] ) && $_REQUEST ['page'] == $user_access['page_link'] && ($_REQUEST['action']=='delete'))
		{
			if($user_access['delete']=='0')
			{	
				MJgmgt_access_right_page_not_access_message();
				die;
			}	
		}
		if (isset ( $_REQUEST ['page'] ) && $_REQUEST ['page'] == $user_access['page_link'] && ($_REQUEST['action']=='insert'))
		{
			if($user_access['add']=='0')
			{	
				MJgmgt_access_right_page_not_access_message();
				die;
			}	
		}
	}
}
?>
<div id="main-wrapper"><!-- MAIN WRAPPER DIV START -->
	<div class="row mailbox-header"><!-- ROW DIV START -->
		<div class="col-md-2 col-sm-3 col-xs-5">   
			<?php
			if($user_access['add']=='1')
			{
			?>	
				<a class="btn btn-success btn-block" href="?dashboard=user&page=message&tab=compose">
				<?php esc_html_e("Compose","gym_mgt");?></a>        	
			<?php
			}
			?>	
		</div>
		<div class="col-md-10 col-sm-9 col-xs-7">
			<h2>
				<?php
				if(!isset($_REQUEST['tab']) || ($_REQUEST['tab'] == 'inbox'))
				{
					echo esc_html__( 'Inbox', 'gym_mgt' );
				}
				elseif(isset($_REQUEST['page']) && $_REQUEST['tab'] == 'sentbox')
				{
					echo esc_html__( 'Sent Item', 'gym_mgt' );
				}
				elseif(isset($_REQUEST['page']) && $_REQUEST['tab'] == 'compose')
				{
					echo esc_html__( 'Compose', 'gym_mgt' );
				}
				elseif(isset($_REQUEST['page']) && $_REQUEST['tab'] == 'view_message')
				{
					echo esc_html__( 'View Message', 'gym_mgt' );
				}
				?>
			</h2>
		</div>
	</div><!--ROW DIV END -->
	<div class="col-md-2">
		<ul class="list-unstyled mailbox-nav">
			<li <?php if(!isset($_REQUEST['tab']) || ($_REQUEST['tab'] == 'inbox')){?>class="active"<?php }?>>
				<a href="?dashboard=user&page=message&tab=inbox">
					<i class="fa fa-inbox"></i><?php esc_html_e("Inbox","gym_mgt");?> <span class="badge badge-success font_weight_700 pull-right">
					<?php echo MJgmgt_count_unread_message(get_current_user_id());?></span>
				</a>
			</li> 							
			<li <?php if(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'sentbox'){?>class="active"<?php }?>><a href="?dashboard=user&page=message&tab=sentbox"><i class="fa fa-sign-out"></i><?php esc_html_e("Sent","gym_mgt");?></a></li>					   
		</ul>
	</div>
	 <div class="col-md-10">
	 <?php  
		if($active_tab == 'sentbox')
			require_once GMS_PLUGIN_DIR. '/template/message/sendbox.php';
		if($active_tab == 'inbox')
			require_once GMS_PLUGIN_DIR. '/template/message/inbox.php';
		if($active_tab == 'compose')
			require_once GMS_PLUGIN_DIR. '/template/message/composemail.php';
		if($active_tab == 'view_message')
			require_once GMS_PLUGIN_DIR. '/template/message/view_message.php';
		?>
	 </div>
</div><!-- Main-wrapper END-->