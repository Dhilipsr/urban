<?php
$payment_method = get_option('pm_payment_method');
$obj_membership=new MJgmgt_membership;
$obj_activity=new MJgmgt_activity;;
if(isset($_REQUEST['membership_id']))
{		
	$retrieved_data=$obj_membership->MJgmgt_get_single_membership($_REQUEST['membership_id']);
}
else
{
	esc_html_e('Membership not selected.','gym_mgt');
}
//BUY MEMBERSHIP PAYMENT
if(isset($_POST['buy_confirm_paypal']))
{	
	$obj_member=new MJgmgt_member; 
	$payment ="no";
	if (! is_user_logged_in ()) 
	{
		if(isset($_POST['member_id']))
		{
			$payment = "yes";	
		}
		else
		{		
			$page_id = get_option ( 'gmgt_login_page' );
			wp_redirect ( home_url () . "?page_id=" . $page_id);
		}
	}
	else
	{
		$payment = "yes";		
	}
	if($payment=="yes")
	{
		//Session Unset
		if(isset($_SESSION["action_frontend"]))
		{
			unset($_SESSION["action_frontend"]);
			unset($_SESSION["class_id1"]);
			unset($_SESSION["day_id1"]);
			unset($_SESSION["class_date"]);
			unset($_SESSION["Remaining_Member_limit_1"]);
			unset($_SESSION["bookedclass_membershipid"]);
		} 
		//store URL Parameter To session
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='frontend_book')
		{	
			setcookie("action_frontend", $_REQUEST['action'], time()+86400);
			setcookie("class_id1", $_REQUEST['class_id1'], time()+86400);
			setcookie("day_id1", $_REQUEST['day_id1'],time()+86400);
			setcookie("startTime_1", $_REQUEST['startTime_1'], time()+86400);
			setcookie("class_date", $_REQUEST['class_date'], time()+86400);
			setcookie("Remaining_Member_limit_1", $_REQUEST['Remaining_Member_limit_1'], time()+86400);
			setcookie("bookedclass_membershipid", $_REQUEST['bookedclass_membershipid'], time()+86400);

		}
		
		
		if($payment_method == "Skrill")
		{
			require_once PM_PLUGIN_DIR. '/lib/skrill/skrill.php';
		}
		elseif($payment_method == "Stripe")
		{
			require_once PM_PLUGIN_DIR. '/lib/stripe/index.php';
		}
		elseif($payment_method == "Instamojo")
		{
			require_once PM_PLUGIN_DIR. '/lib/instamojo/instamojo.php';
		}
		elseif($payment_method == "PayUMony")
		{
			require_once PM_PLUGIN_DIR. '/lib/OpenPayU/payuform.php';
		}
		elseif($payment_method == "2CheckOut")
		{
			require_once PM_PLUGIN_DIR. '/lib/2checkout/index.php';
		}
		elseif($payment_method == "iDeal")
		{
			require_once PM_PLUGIN_DIR. '/lib/ideal/ideal.php';
		}
		elseif($payment_method == 'Paystack')
		{
			require_once PM_PLUGIN_DIR. '/lib/paystack/paystack.php';
		}
		elseif($payment_method == 'paytm')
		{
			require_once PM_PLUGIN_DIR. '/lib/PaytmKit/index.php';
		}
		else
		{
			require_once GMS_PLUGIN_DIR. '/lib/paypal/buy_membership_process.php';
		}
	}
}
if(!empty($retrieved_data))
{ ?>
	<div class="wpgym-detail-box col-md-12"><!--WP GYM DETAIL BOX START--> 
		<div class="wpgym-border-box"><!--WP GYM BORDER BOX START--> 
			<form name="membership_buy_form" method="post" action=""><!--BUY MEMBERSHIP FORM START--> 
				<div class="wpgym-box-title">
					<span class="wpgym-membershiptitle">
						<?php echo esc_html($retrieved_data->membership_label);?>
					</span>
				</div>
				<div class="wpgym-course-lession-list">				
				</div>
				<table>
					<tbody>
					<tr>	
						<th><?php esc_html_e('Membership Period','gym_mgt');?></th>
						<td><?php echo round(esc_html($retrieved_data->membership_length_id)/30)." Months";?></td>
					</tr>					
					<tr>	
						<th><?php esc_html_e('Membership Price','gym_mgt');?></th>
						<td><?php echo MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' ))."".esc_html($retrieved_data->membership_amount);?></td>
					</tr>	
					<tr>	
						<th><?php esc_html_e('Singup Fee','gym_mgt');?></th>
						<td><?php echo MJgmgt_get_currency_symbol(get_option( 'gmgt_currency_code' ))."".esc_html($retrieved_data->signup_fee);?></td>
					</tr>				
					</tbody>
				</table>
				<?php
					$singup=($retrieved_data->signup_fee);
					$amount_member=($retrieved_data->membership_amount);
					$tax_amount=MJgmgt_get_membership_tax_amount($retrieved_data->membership_id);
					$totel_Amount= $singup + $amount_member + (float)$tax_amount;
					?>
				<input type="hidden" name="amount" value="<?php echo esc_attr($totel_Amount); ?>">
				<input type="hidden" name="member_id" value="<?php if(isset($_REQUEST['user_id'])){ echo esc_attr($_REQUEST['user_id']);} else { echo get_current_user_id(); }?>">
				<input type="hidden" name="mp_id" value="<?php echo esc_attr($retrieved_data->membership_id);?>">
				<input type="hidden" name="where_payment" value="front_end">				
				<input type="submit" name="buy_confirm_paypal" value="<?php echo esc_html__('Pay By','gym_mgt').' '.esc_attr($payment_method);?>" style="margin-top:15px;">
			</form><!--BUY MEMBERSHIP FORM END--> 
		</div>	<!--WP GYM BORDER BOX END--> 
	</div><!--WP GYM DETAIL BOX END--> 
<?php 
} 
?>