<?php 
$obj_gyme = new MJgmgt_Gym_management();
?>
<script type="text/javascript">
$(document).ready(function() 
{
	"use strict";
	$('#member_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});	
	$('#birth_date').datepicker(
	{
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }        
    }); 
} );
</script>
<?php	
	$member_id=0;
	if(isset($_REQUEST['member_id']))
		$member_id=esc_attr($_REQUEST['member_id']);
		$edit=0;					
		$edit=1;
		$user_info = get_userdata($member_id);		
?>	
<div class="panel-body"><!--PANEL BODY DIV START-->
	<div class="member_view_row1"><!--MEMBER VIEW ROW 1 DIV START-->
		<div class="col-md-8 col-sm-12 membr_left">
			<div class="col-md-6 col-sm-12 left_side">
			<?php 
			if($user_info->gmgt_user_avatar == "")
			{?>
				<img alt="" src="<?php echo get_option( 'gmgt_system_logo' ); ?>">
				<?php 
			}
			else
			{
			?>
			<img class="max_width_100" src="<?php if($edit)echo esc_url( $user_info->gmgt_user_avatar ); ?>" />
			<?php 
			}
			?>
			</div>
		        <div class="col-md-6 col-sm-12 right_side">
					<div class="table_row">
						<div class="col-md-5 col-sm-12 table_td">
							<i class="fa fa-user"></i> 
							<?php esc_html_e('Name','gym_mgt'); ?>	
						</div>
						<div class="col-md-7 col-sm-12 table_td">
							<span class="txt_color">
								<?php echo chunk_split(esc_html($user_info->first_name)." ".esc_html($user_info->middle_name)." ".esc_html($user_info->last_name),28,"<BR>");?>
							</span>
						</div>
					</div>
					<div class="table_row">
						<div class="col-md-5 col-sm-12 table_td">
							<i class="fa fa-envelope"></i> 
							<?php esc_html_e('Email','gym_mgt');?> 	
						</div>
						<div class="col-md-7 col-sm-12 table_td">
							<span class="txt_color"><?php echo chunk_split(esc_html($user_info->user_email),28,"<BR>");?></span>
						</div>
					</div>
					<div class="table_row">
						<div class="col-md-5 col-sm-12 table_td">
						<i class="fa fa-phone"></i> 
						<?php esc_html_e('Mobile No','gym_mgt');?> 
						</div>
						<div class="col-md-7 col-sm-12 table_td">
							<span class="txt_color">
								<span class="txt_color"><?php echo esc_html($user_info->mobile);?> </span>
							</span>
						</div>
					</div>
					<div class="table_row">
						<div class="col-md-5 col-sm-12 table_td">
							<i class="fa fa-calendar"></i>
							<?php esc_html_e('Date Of Birth','gym_mgt');?>	
						</div>
						<div class="col-md-7 col-sm-12 table_td">
							<span class="txt_color"><?php echo MJgmgt_getdate_in_input_box(esc_html($user_info->birth_date));?></span>
						</div>
					</div>
					<div class="table_row">
						<div class="col-md-5 col-sm-12 table_td">
							<i class="fa fa-mars"></i>
							<?php esc_html_e('Gender','gym_mgt');?> 
						</div>
						<div class="col-md-7 col-sm-12 table_td">
							<span class="txt_color"><?php 
							
							//echo esc_html($user_info->gender);
							if($user_info->gender == "male")
							{
								echo esc_html_e('Male','gym_mgt');
							}
							else
							{
								echo esc_html_e('Female','gym_mgt');
							}
							
							?></span>
						</div>
					</div>
					<div class="table_row">
						<div class="col-md-5 col-sm-12 table_td">
							<i class="fa fa-graduation-cap"></i><?php esc_html_e('Class','gym_mgt');?>
						</div>
						<div class="col-md-7 col-sm-12 table_td">
							<span class="txt_color">
							<?php 
							$ClassArr=MJgmgt_get_current_user_classis($user_info->ID);
							$class_name="-";
							$class_name_string="";
							if($ClassArr)
							{												
								foreach($ClassArr as $key=>$class_id)
								{							
									$class_name_string.=MJgmgt_get_class_name($class_id).", ";
								}	
								$class=rtrim($class_name_string,", ");
						     	echo chunk_split(esc_html($class),20,"<BR>");		
							}						
							else
							{
								echo chunk_split(esc_html($class_name),20,"<BR>");
							}
							?>
							</span>
						</div>
					</div>
					<div class="table_row">
						<div class="col-md-5 col-sm-12 table_td">
							<i class="fa fa-user"></i> 
							<?php esc_html_e('User Name','gym_mgt');?>
						</div>
						<div class="col-md-7 col-sm-12 table_td">
							<span class="txt_color"><?php echo chunk_split(esc_html($user_info->user_login),25,"<BR>");?> </span>
						</div>
					</div>
			    </div>
		</div>
	    <div class="col-md-4 col-sm-12 member_right">	
	 		<span class="report_title">
				<span class="fa-stack cutomcircle">
					<i class="fa fa-align-left fa-stack-1x"></i>
				</span> 
				<span class="shiptitle"><?php esc_html_e('More Info','gym_mgt');?></span>
			</span>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-user"></i>
					<?php esc_html_e('Staff Member','gym_mgt'); ?>	
				</div>
				<div class="col-md-6 col-sm-12 table_td">
				   <?php  $staff_member=MJgmgt_get_display_name(esc_html($user_info->staff_id));  ?>
					<span class="txt_color"><?php echo chunk_split(esc_html($staff_member),18,"<BR>");?></span>
				</div>
			</div>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-heart"></i>
					<?php esc_html_e('Interest Area','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
				    <?php $intrest_area=get_the_title($user_info->intrest_area);   ?>
					<span class="txt_color"><?php if($user_info->intrest_area!=""){ echo chunk_split(esc_html($intrest_area),18,"<BR>"); }?></span>
				</div>
			</div>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-users"></i> 
					<?php esc_html_e('MemberShip','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
				   <?php $membership_name=MJgmgt_get_membership_name($user_info->membership_id);   ?>
					<span class="txt_color"><?php echo chunk_split(esc_html($membership_name),18,"<BR>");?></span>
				</div>
			</div>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-power-off"></i> 
					<?php esc_html_e('Status','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">	
					<span class="txt_color"><?php echo esc_html_e(esc_html($user_info->membership_status),'gym_mgt');?></span>
				</div>
			</div>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-calendar"></i>
					<?php esc_html_e('Expire Date','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
					<span class="txt_color"><?php if($user_info->member_type!='Prospect'){ echo MJgmgt_getdate_in_input_box(MJgmgt_check_membership(esc_html($user_info->ID))); }else{ echo "--"; }?></span>
				</div>
			</div>
			<div class="table_row">
				<div class="col-md-6 col-sm-12 table_td">
					<i class="fa fa-map-marker padding_right_15"></i> 
					<?php esc_html_e('Address','gym_mgt');?>
				</div>
				<div class="col-md-6 col-sm-12 table_td">
					<span class="txt_color"><?php 
						if($user_info->address != '')
						 {
							echo chunk_split(esc_html($user_info->address).", <BR>",18);
						 }
						 
						if($user_info->city_name != '')
						{
							echo chunk_split(esc_html($user_info->city_name).", <BR>",18);
						}
						?> 
					</span>
				</div>
			</div>
	    </div>
	</div><!--MEMBER VIEW ROW 1 DIV END-->
</div><!--PANEL BODY DIV END-->
<div class="panel-body"><!--PANEL BODY DIV START-->
	<div class="clear"></div>
	<div class="col-md-6  col-sm-6  col-xs-12 border">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php esc_html_e('Weight','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&action=insert&&user_id=<?php echo esc_attr($_REQUEST['member_id']);?>&result_measurment=Weight" 
			class="btn btn-danger right"> <?php esc_html_e('Add Weight','gym_mgt');?></a>	
		</span>
		<?php 
		$weight_data = $obj_gyme->MJgmgt_get_weight_report('Weight',$_REQUEST['member_id']);
		$option =  $obj_gyme->MJgmgt_report_option('Weight');
		require_once GMS_PLUGIN_DIR. '/lib/chart/GoogleCharts.class.php';
		$GoogleCharts = new GoogleCharts;		
		$wait_chart = $GoogleCharts->load( 'LineChart' , 'weight_report' )->get( $weight_data , $option );		
		?>
		<div id="weight_report" class="width_100 height_250">
		<?php 
		if(empty($weight_data) || count($weight_data) == 1)
		esc_html_e('There is not enough data to generate report','gym_mgt')?>
		</div>   
		<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
		<script type="text/javascript">
			<?php 
			if(!empty($weight_data) && count($weight_data) > 1)
			echo $wait_chart;?>
		</script>
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php esc_html_e('Waist Report','gym_mgt');?></span>
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&action=insert&&user_id=<?php echo esc_attr($_REQUEST['member_id']);?>&result_measurment=Waist" 
			class="btn btn-danger right"> <?php esc_html_e('Add Waist','gym_mgt');?></a>
		</span>
		<?php 
		$waist_chart = $obj_gyme->MJgmgt_get_weight_report('Waist',$_REQUEST['member_id']);
		$option =  $obj_gyme->MJgmgt_report_option('Waist');
		$GoogleCharts = new GoogleCharts;		
		$waist_chartreport = $GoogleCharts->load( 'LineChart' , 'waist_report' )->get( $waist_chart , $option );
		?>
		<div id="waist_report" class="width_100 height_250">
		<?php 
		if(empty($waist_chart) || count($waist_chart) == 1)
		esc_html_e('There is not enough data to generate report','gym_mgt')?>
		</div>   
		<script type="text/javascript">
			<?php 
			if(!empty($waist_chart) && count($waist_chart) > 1)
			echo $waist_chartreport;?>
		</script>
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php esc_html_e('Height Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&action=insert&&user_id=<?php echo esc_attr($_REQUEST['member_id']);?>&result_measurment=Height" 
			class="btn btn-danger right"> <?php esc_html_e('Add Height','gym_mgt');?></a>	
		</span>
		<?php 
		$height_data = $obj_gyme->MJgmgt_get_weight_report('Height',$_REQUEST['member_id']);
		$option =  $obj_gyme->MJgmgt_report_option('Height');
		$GoogleCharts = new GoogleCharts;		
		$height_chart = $GoogleCharts->load( 'LineChart' , 'height_reort' )->get( $height_data , $option );		
		?>
		<div id="height_reort" class="width_100 height_250">
		<?php if(empty($height_data) || count($height_data) == 1)
		esc_html_e('There is not enough data to generate report','gym_mgt')?>
		</div>   
		<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
		<script type="text/javascript">
			<?php 
			if(!empty($height_data) && count($height_data) > 1)
			echo $height_chart;?>
		</script>
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php esc_html_e('Chest Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&action=insert&&user_id=<?php echo esc_attr($_REQUEST['member_id']);?>&result_measurment=Chest" 
			class="btn btn-danger right"> <?php esc_html_e('Add Chest','gym_mgt');?></a>	
		</span>
		<?php 
		$chest_data = $obj_gyme->MJgmgt_get_weight_report('Chest',$_REQUEST['member_id']);
		$option =  $obj_gyme->MJgmgt_report_option('Chest');
		require_once GMS_PLUGIN_DIR. '/lib/chart/GoogleCharts.class.php';
		$GoogleCharts = new GoogleCharts;		
		$chest_chart = $GoogleCharts->load( 'LineChart' , 'chest_reort' )->get( $chest_data , $option );
		?>
		<div id="chest_reort" class="width_100 height_250">
		<?php if(empty($chest_data) || count($chest_data) == 1)
		esc_html_e('There is not enough data to generate report','gym_mgt')?>
		</div>   
		<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
		<script type="text/javascript">
			<?php 
			if(!empty($chest_data) && count($chest_data) > 1)
			echo $chest_chart;?>
		</script>
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php esc_html_e('Thigh Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&action=insert&user_id=<?php echo esc_attr($_REQUEST['member_id']);?>&result_measurment=Thigh" 
			class="btn btn-danger right"> <?php esc_html_e('Add Thigh','gym_mgt');?></a>	
		</span>
		<?php 
		$thigh_data = $obj_gyme->MJgmgt_get_weight_report('Thigh',$_REQUEST['member_id']);
		$option =  $obj_gyme->MJgmgt_report_option('Thigh');
		require_once GMS_PLUGIN_DIR. '/lib/chart/GoogleCharts.class.php';
		$GoogleCharts = new GoogleCharts;		
		$thigh_chart = $GoogleCharts->load( 'LineChart' , 'thigh_reort' )->get( $thigh_data , $option );
		?>
		<div id="thigh_reort" class="width_100 height_250">
		<?php if(empty($thigh_data) || count($thigh_data) == 1)
		esc_html_e('There is not enough data to generate report','gym_mgt')?>
		</div>   
		<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
		<script type="text/javascript">
			<?php 
			if(!empty($thigh_data) && count($thigh_data) > 1)
			echo $thigh_chart;?>
		</script>
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php esc_html_e('Arms Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&action=insert&user_id=<?php echo esc_attr($_REQUEST['member_id']);?>&result_measurment=Arms" 
			class="btn btn-danger right"> <?php esc_html_e('Add Arms','gym_mgt');?></a>	
		</span>
		<?php 
		$arm_data = $obj_gyme->MJgmgt_get_weight_report('Arms',$_REQUEST['member_id']);
		$option =  $obj_gyme->MJgmgt_report_option('Arms');
		require_once GMS_PLUGIN_DIR. '/lib/chart/GoogleCharts.class.php';
		$GoogleCharts = new GoogleCharts;		
		$arm_chart = $GoogleCharts->load( 'LineChart' , 'arm_reort' )->get( $arm_data , $option );		
		?>
		<div id="arm_reort" class="width_100 height_250">
		<?php if(empty($arm_data) || count($arm_data) == 1)
		esc_html_e('There is not enough data to generate report','gym_mgt')?>
		</div>   
		<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
		<script type="text/javascript">
			<?php 
			if(!empty($arm_data) && count($arm_data) > 1)
			echo $arm_chart;?>
		</script>
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php esc_html_e('Fat Report','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&action=insert&user_id=<?php echo esc_attr($_REQUEST['member_id']);?>&result_measurment=Fat" 
			class="btn btn-danger right"> <?php esc_html_e('Add Fat','gym_mgt');?></a>	
		</span>
		<?php 
		$fat_data = $obj_gyme->MJgmgt_get_weight_report('Fat',$_REQUEST['member_id']);
		$option =  $obj_gyme->MJgmgt_report_option('Fat');
		require_once GMS_PLUGIN_DIR. '/lib/chart/GoogleCharts.class.php';
		$GoogleCharts = new GoogleCharts;		
		$fat_chart = $GoogleCharts->load( 'LineChart' , 'fat_reort' )->get( $fat_data , $option );		
		?>
		<div id="fat_reort" class="width_100 height_250">
		<?php if(empty($fat_data) || count($fat_data) == 1)
		esc_html_e('There is not enough data to generate report','gym_mgt')?>
		</div>   
		
		<script type="text/javascript">
			<?php 
			if(!empty($fat_data) && count($fat_data) > 1)
			echo $fat_chart;?>
		</script>
	</div>
	<div class="col-md-6  col-sm-6  col-xs-12 border borderleft viewmember_image_height" >
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-line-chart fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php esc_html_e('My Photos','gym_mgt');?></span>	
			<a href="?dashboard=user&page=workouts&tab=addmeasurement&&action=insert&user_id=<?php echo esc_attr($_REQUEST['member_id']);?>&result_measurment=Fat" 
			class="btn btn-danger right"> <?php esc_html_e('Add Photo','gym_mgt');?></a>
			<div id="slider1_container" class="my_photo_slider">
			<!-- Loading Screen -->
			<div u="loading" class="my_photo_slider_loding">
				<div class="my_photo_slider_loding_div">
				</div>
			</div>
			<!-- Slides Container -->
			<div u="slides" class="slides">
			<?php $obj_workout = new MJgmgt_workout();
			$measurement_data = $obj_workout->MJgmgt_get_all_measurement_by_userid($user_id);
			if(!empty($measurement_data))
			{
				foreach ($measurement_data as $retrieved_data)
				{ 
					 $userimage=$retrieved_data->gmgt_progress_image;
					 if($userimage!=""){ ?>	
					   <div>
							<img u="image" src="<?php echo $retrieved_data->gmgt_progress_image;?>"/>
						</div>
					 <?php } 
				} 
			} ?>
			</div>
			<!-- bullet navigator container -->
			<div u="navigator" class="jssorb05 bottom_16 right_6">
				<!-- bullet navigator item prototype -->
				<div u="prototype"></div>
			</div>
			<!--#endregion Bullet Navigator Skin End -->
			<!-- Arrow Left -->
			<span u="arrowleft" class="jssora11l top_123 left_8">
			</span>
			<!-- Arrow Right -->
			<span u="arrowright" class="jssora11r top_123 right_8">
			</span>
			<!--#endregion Arrow Navigator Skin End -->
			</div>
		</span>
		<?php ?>
	</div>
</div><!--PANEL BODY DIV END-->
<script type="text/javascript">
$(document).ready(function() 
{
	"use strict";
	jQuery('#subscription_list').DataTable({
		"responsive": true,
		"order": [[ 0, "asc" ]],
		"aoColumns":[
					  {"bSortable": true},
					  {"bSortable": true},
					  {"bSortable": true},
					  {"bSortable": true},
					  {"bSortable": true},
					  {"bSortable": true},
					  {"bSortable": true}],
				language:<?php echo MJgmgt_datatable_multi_language();?>		  
		});
} );
</script>	
<div class="panel-body"><!--PANEL BODY DIV START-->
	<div class="col-md-12  col-sm-12  col-xs-12 border_1">
		<span class="report_title">
			<span class="fa-stack cutomcircle">
				<i class="fa fa-align-left fa-stack-1x"></i>
			</span> 
			<span class="shiptitle"><?php esc_html_e('Subscription History','gym_mgt');?></span>	
		</span>
		<form name="wcwm_report" action="" method="post">
			<table id="subscription_list" class="display" cellspacing="0" width="100%"><!--TABLE SUBSCRIPTIOBN LIST START-->
				<thead>
					<tr>
						<th><?php esc_html_e('Title', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Amount', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Paid Amount', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Due Amount', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Membership Start Date', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Membership End Date', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Payment Status', 'gym_mgt' ) ;?></th>
					</tr>
			    </thead>
				<tfoot>
					<tr>
						<th><?php esc_html_e('Title', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Amount', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Paid Amount', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Due Amount', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Membership Start Date', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Membership End Date', 'gym_mgt' ) ;?></th>
						<th><?php esc_html_e('Payment Status', 'gym_mgt' ) ;?></th>
					</tr>
				</tfoot>
				<tbody>
				<?php 
				$obj_membership_payment=new MJgmgt_membership_payment;
				$paymentdata=$obj_membership_payment->MJgmgt_get_member_subscription_history($member_id);
				if(!empty($paymentdata))
				{
					foreach ($paymentdata as $retrieved_data)
				    { ?>
					<tr>
						<td class="productname"><?php echo MJgmgt_get_membership_name(esc_html($retrieved_data->membership_id));?></td>
						<td class="totalamount"><?php echo esc_html($retrieved_data->membership_amount);?></td>
						<td class="paid_amount"><?php echo esc_html($retrieved_data->paid_amount);?></td>
						<td class="totalamount"><?php echo esc_html($retrieved_data->membership_amount)-esc_html($retrieved_data->paid_amount);?></td>
						<td class="paymentdate"><?php echo MJgmgt_getdate_in_input_box(esc_html($retrieved_data->start_date));?></td>
						<td class="paymentdate"><?php echo MJgmgt_getdate_in_input_box(esc_html($retrieved_data->end_date));?></td>
						<td class="paymentdate">
						<?php 
						echo "<span class='btn btn-success btn-xs'>";
						echo MJgmgt_get_membership_paymentstatus($retrieved_data->mp_id);
						echo "</span>";
						?>
						</td>
					</tr>
					<?php 
					} 
				}?>
				</tbody>
			</table><!--TABLE SUBSCRIPTIOBN LIST END-->
		</form>
		</div>
</div><!--PANEL BODY DIV END-->
<script>
jQuery(document).ready(function($) 
{
	var options = {
		$AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
		$AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
		$Idle: 2000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
		$PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

		$ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
		$SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
		$SlideDuration: 800,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
		$MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
		//$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
		//$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
		$SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
		$Cols: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
		$ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
		$UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
		$PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
		$DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)

		$ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
			$Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
			$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
			$AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
			$Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
			$Scale: false                                   //Scales bullets navigator or not while slider scale
		},

		$BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
			$Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
			$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
			$AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
			$Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
			$Rows: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
			$SpacingX: 12,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
			$SpacingY: 4,                                   //[Optional] Vertical space between each item in pixel, default value is 0
			$Orientation: 1,                                //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
			$Scale: false                                   //Scales bullets navigator or not while slider scale
		}
	};

	var jssor_slider1 = new $JssorSlider$("slider1_container", options);
	//responsive code begin
	//you can remove responsive code if you don't want the slider scales while window resizing
	function ScaleSlider() 
	{
		var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
		if (parentWidth) {
			jssor_slider1.$ScaleWidth(parentWidth - 30);
		}
		else
			window.setTimeout(ScaleSlider, 30);
	}
	ScaleSlider();
	$(window).bind("load", ScaleSlider);
	$(window).bind("resize", ScaleSlider);
	$(window).bind("orientationchange", ScaleSlider);
	//responsive code end
});
</script>	