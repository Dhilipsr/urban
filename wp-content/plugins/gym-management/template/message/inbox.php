<div class="mailbox-content"><!-- MAILBOX CONTENT DIV START -->
    <div class="table-responsive"><!--TABLE RESPONSIVE DIV START -->
		<table class="table"><!-- INBOX TABLE START -->
			<thead>
				<tr>
					<th class="text-right" colspan="5">
						<?php $message = MJgmgt_count_inbox_item(get_current_user_id());
						$max = 10;
						if(isset($_GET['pg'])){
							$p = $_GET['pg'];
						}else{
							$p = 1;
						}
						$limit = ($p - 1) * $max;
						$prev = $p - 1;
						$next = $p + 1;
						$limits = (int)($p - 1) * $max;
						$totlal_message =$message;
						$totlal_message = ceil($totlal_message / $max);
						$lpm1 = $totlal_message - 1;
						$offest_value = ($p-1) * $max;
						echo MJgmgt_inbox_pagination($totlal_message,$p,$lpm1,$prev,$next);
						?>
					</th>
				</tr>
			</thead>
			<tbody>
			<tr>
				<th class="hidden-xs">
					<span><?php esc_html_e('Message For','gym_mgt');?></span>
				</th>
				<th><?php esc_html_e('Subject','gym_mgt');?></th>
				<th>
					<?php esc_html_e('Description','gym_mgt');?>
				</th>
				<th>
					<?php esc_html_e('Attachment','gym_mgt');?>
				</th>
				<th>
					<?php esc_html_e('Date','gym_mgt');?>
				</th>
			</tr>
			<?php 
			//GET INBOX MESSAGE DATA
			$message = MJgmgt_get_inbox_message(get_current_user_id(),$limit,$max);
			if(!empty($message))
			{
				foreach($message as $msg)
				{
					$attchment=get_post_meta( $msg->post_id, 'message_attachment',true);
					?>
					<tr>
						<td class="hidden-xs"><?php echo MJgmgt_get_display_name(esc_html($msg->sender));?></td>
						<!-- <td>
							<a href="?dashboard=user&page=message&tab=view_message&from=inbox&id=<?php echo esc_attr($msg->message_id);?>"> <?php echo esc_html($msg->subject);if($obj_message->MJgmgt_count_reply_item($msg->post_id)>=1){?><span class="badge badge-success pull-right"><?php echo esc_html($obj_message->MJgmgt_count_reply_item($msg->post_id));?></span><?php } ?></a>
						</td>
						<td><?php echo wp_trim_words(esc_html($msg->message_body),5)?>
						</td>-->
						
						 <td class="min_width_150_px">
				 <a href="?dashboard=user&page=message&tab=inbox&tab=view_message&from=inbox&id=<?php echo esc_attr($msg->message_id
				 );?>"> 
				<?php 
				$subject_char=strlen($msg->subject);
                if($subject_char <= 25)
                {
                    echo $msg->subject;
                }
                else
                {
                    $char_limit = 25;
                    $subject_body= substr(strip_tags($msg->subject), 0, $char_limit)."...";
                    echo $subject_body;
                }
				 ?><?php if(MJgmgt_count_reply_item($msg->post_id)>=1){?><span class="badge badge-success pull-right"><?php echo MJgmgt_count_reply_item($msg->post_id);?></span><?php } ?></a>
			</td>
			<td class="max_width_400_px">			
			<?php 
			$body_char=strlen($msg->message_body);
            if($body_char <= 60)
            {
                echo $msg->message_body;
            }
            else
            {
                $char_limit = 60;
                $msg_body= substr(strip_tags($msg->message_body), 0, $char_limit)."...";
                echo $msg_body;
            }
			//echo wordwrap($msg->message_body,30,"<br>\n",TRUE);
			?>
			</td>
						<td>	
						<?php			
						if(!empty($attchment))
						{	
							$attchment_array=explode(',',$attchment);
							foreach($attchment_array as $attchment_data)
							{
								?>
								<a target="blank" href="<?php echo content_url().'/uploads/gym_assets/'.$attchment_data; ?>" class="btn btn-default"><i class="fa fa-download"></i><?php esc_html_e('View Attachment','gym_mgt');?></a>
								<?php				
							}
						}
						else
						{
							 esc_html_e('No Attachment','gym_mgt');
						}
						?>				
						</td>
						<td>
							<?php  echo MJgmgt_getdate_in_input_box($msg->date);?>
						</td>
					</tr>
					<?php 
				}
			}
			else
			{
			?>
			<tr>
				<td colspan="4">
					<?php echo esc_html_e('No message available','gym_mgt'); ?>
				</td>
			</tr>
			<?php
			}
			?>
			</tbody>
		</table><!-- INBOX TABLE END -->
 	</div><!-- TABLE RESPONSIVE DIV END -->
</div><!-- MAILBOX CONTENT DIV END -->