<?php
$obj_message= new MJgmgt_message;
//SAVE MESSAGE DATA
if(isset($_POST['save_message']))
{
	$nonce = $_POST['_wpnonce'];
	if (wp_verify_nonce( $nonce, 'save_message_nonce' ) )
	{
		$created_date = date("Y-m-d H:i:s");
		$subject = MJgmgt_strip_tags_and_stripslashes(sanitize_text_field($_POST['subject']));
		$message_body = MJgmgt_strip_tags_and_stripslashes(sanitize_text_field($_POST['message_body']));
		$created_date = date("Y-m-d H:i:s");
		$tablename="Gmgt_message";
		$role=$_POST['receiver'];
		if(isset($_REQUEST['class_id']))
		$class_id = esc_attr($_REQUEST['class_id']);
		$upload_docs_array=array();	
		if(!empty($_FILES['message_attachment']['name']))
		{
			$count_array=count($_FILES['message_attachment']['name']);
			for($a=0;$a<$count_array;$a++)
			{			
				foreach($_FILES['message_attachment'] as $image_key=>$image_val)
				{		
					$document_array[$a]=array(
					'name'=>$_FILES['message_attachment']['name'][$a],
					'type'=>$_FILES['message_attachment']['type'][$a],
					'tmp_name'=>$_FILES['message_attachment']['tmp_name'][$a],
					'error'=>$_FILES['message_attachment']['error'][$a],
					'size'=>$_FILES['message_attachment']['size'][$a]
					);							
				}
			}				
			foreach($document_array as $key=>$value)		
			{	
				$get_file_name=$document_array[$key]['name'];
				$upload_docs_array[]=$obj_message->MJgmgt_load_multiple_documets($value,$value,$get_file_name);				
			} 				
		}
		$upload_docs_array_filter=array_filter($upload_docs_array);	
		if(!empty($upload_docs_array_filter))
		{
			$attachment=implode(',',$upload_docs_array_filter);
		}
		else
		{
			$attachment='';
		}
		if($role == 'member' || $role == 'staff_member' || $role == 'accountant' || $role == 'administrator')
		{
			$userdata=MJgmgt_get_user_notice($role,$_REQUEST['class_id']);
			if(!empty($userdata))
			{
				$mail_id = array();
				$i = 0;
					foreach($userdata as $user)
					{
						if($role == 'parent' && $class_id != 'all')
						$mail_id[]=$user['ID'];
						else 
							$mail_id[]=$user->ID;
						$i++;
					}
				$post_id = wp_insert_post( array(
						'post_status' => 'publish',
						'post_type' => 'message',
						'post_title' => $subject,
						'post_content' =>$message_body
				) );
				foreach($mail_id as $user_id)
				{
					$reciever_id = $user_id;
					$message_data=array('sender'=>get_current_user_id(),
							'receiver'=>$user_id,
							'subject'=>$subject,
							'message_body'=>$message_body,
							'date'=>$created_date,
							'status' =>0,
							'post_id' =>$post_id
					);
					MJgmgt_insert_record($tablename,$message_data);
					//-----MESSAGE SEND NOTIFICATION TEMPLATE-------
					 $userdata = get_userdata($user_id);
					 $role=$userdata->roles;
					 $reciverrole=$role[0];
					 if($reciverrole == 'administrator' ) 
					 {
						$page_link=admin_url().'admin.php?page=Gmgt_message&tab=inbox';
					 }
					 else
					 {
						$page_link=home_url().'/?dashboard=user&page=message&tab=inbox';
					 } 
					$gymname=get_option( 'gmgt_system_name' );
					$userdata = get_userdata($user_id);
					$senderuserdata = get_userdata(get_current_user_id());
					$arr['[GMGT_RECEIVER_NAME]']=$userdata->display_name;	
					$arr['[GMGT_GYM_NAME]']=$gymname;
					$arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;
					$arr['[GMGT_MESSAGE_CONTENT]']=$message_body;
					$arr['[GMGT_MESSAGE_LINK]']=$page_link;
					$subject =get_option('message_received_subject');
					$sub_arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;;
					$sub_arr['[GMGT_GYM_NAME]']=$gymname;
					$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
					$message_template = get_option('message_received_template');	
					$message_replacement = MJgmgt_string_replacemnet($arr,$message_template);
						$to=$userdata->user_email;
							MJgmgt_send_mail($to,$subject,$message_replacement);	
				}
				$result=add_post_meta($post_id, 'message_for',$role);
				$result=add_post_meta($post_id, 'gmgt_class_id',$_REQUEST['class_id']);
				$result=add_post_meta($post_id, 'message_attachment',$attachment);
			}
			else
			{
				$post_id = wp_insert_post( array(
					'post_status' => 'publish',
					'post_type' => 'message',
					'post_title' => $subject,
					'post_content' =>$message_body
			
				) );
				$user_id =$_POST['receiver'];
				$message_data=array('sender'=>get_current_user_id(),
						'receiver'=>$user_id,
						'subject'=>$subject,
						'message_body'=>$message_body,
						'date'=>$created_date,
						'status' =>0,
						'post_id' =>$post_id
				);
				MJgmgt_insert_record($tablename,$message_data);
				//-----MESSAGE SEND NOTIFICATION TEMPLATE-------
				$userdata = get_userdata($user_id);
				$role=$userdata->roles;
				$reciverrole=$role[0];
				if($reciverrole == 'administrator' ) 
				{
					$page_link=admin_url().'admin.php?page=Gmgt_message&tab=inbox';
				}
				else
				{
					$page_link=home_url().'/?dashboard=user&page=message&tab=inbox';
				} 
				$gymname=get_option( 'gmgt_system_name' );
				$userdata = get_userdata($user_id);
				$senderuserdata = get_userdata(get_current_user_id());
				$arr['[GMGT_RECEIVER_NAME]']=$userdata->display_name;	
				$arr['[GMGT_GYM_NAME]']=$gymname;
				$arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;
				$arr['[GMGT_MESSAGE_CONTENT]']=$message_body;
				$arr['[GMGT_MESSAGE_LINK]']=$page_link;
				$subject =get_option('message_received_subject');
				$sub_arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;;
				$sub_arr['[GMGT_GYM_NAME]']=$gymname;
				$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
				$message_template = get_option('message_received_template');	
				$message_replacement = MJgmgt_string_replacemnet($arr,$message_template);
					$to=$userdata->user_email;
						MJgmgt_send_mail($to,$subject,$message_replacement);
				$result=add_post_meta($post_id, 'message_for','user');
				$result=add_post_meta($post_id, 'message_gmgt_user_id',$user_id);
				$result=add_post_meta($post_id, 'message_attachment',$attachment);
			}
		}
		else
		{
		$post_id = wp_insert_post( array(
					'post_status' => 'publish',
					'post_type' => 'message',
					'post_title' => $subject,
					'post_content' =>$message_body
			) );
			$user_id =$_POST['receiver'];
			$message_data=array('sender'=>get_current_user_id(),
					'receiver'=>$user_id,
					'subject'=>$subject,
					'message_body'=>$message_body,
					'date'=>$created_date,
					'status' =>0,
					'post_id' =>$post_id
			);
			MJgmgt_insert_record($tablename,$message_data);
			//-----MESSAGE SEND NOTIFICATION-------
			$userdata = get_userdata($user_id);
			$role=$userdata->roles;
			$reciverrole=$role[0];
			if($reciverrole == 'administrator' ) 
			{
				$page_link=admin_url().'admin.php?page=Gmgt_message&tab=inbox';
			}
			else
			{
				$page_link=home_url().'/?dashboard=user&page=message&tab=inbox';
			} 
			$gymname=get_option( 'gmgt_system_name' );
			$userdata = get_userdata($user_id);
			$senderuserdata = get_userdata(get_current_user_id());
			
			$arr['[GMGT_RECEIVER_NAME]']=$userdata->display_name;	
			$arr['[GMGT_GYM_NAME]']=$gymname;
			$arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;
			$arr['[GMGT_MESSAGE_CONTENT]']=$message_body;
			$arr['[GMGT_MESSAGE_LINK]']=$page_link;
			$subject =get_option('message_received_subject');
			$sub_arr['[GMGT_SENDER_NAME]']=$senderuserdata->display_name;;
			$sub_arr['[GMGT_GYM_NAME]']=$gymname;
			$subject = MJgmgt_subject_string_replacemnet($sub_arr,$subject);
			$message_template = get_option('message_received_template');	
			$message_replacement = MJgmgt_string_replacemnet($arr,$message_template);
				$to=$userdata->user_email;
					MJgmgt_send_mail($to,$subject,$message_replacement);	
	
			$result=add_post_meta($post_id, 'message_for','user');
			$result=add_post_meta($post_id, 'message_gmgt_user_id',$user_id);
			$result=add_post_meta($post_id, 'message_attachment',$attachment);
		}
	}
}
if(isset($result))
{?>
	<div id="message" class="updated below-h2">
		<p><?php esc_html_e('Message Sent Successfully!','gym_mgt');?></p>
	</div>
<?php 
}	
?>
<script type="text/javascript">
$(document).ready(function() 
{
	"use strict";
	$('#message_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});
	jQuery("body").on("change", ".input-file[type=file]", function ()
	{ 
		"use strict";
		var file = this.files[0]; 		
		var ext = $(this).val().split('.').pop().toLowerCase(); 
		//Extension Check 
		if($.inArray(ext, [,'pdf','doc','docx','xls','xlsx','ppt','pptx','gif','png','jpg','jpeg','']) == -1)
		{
			alert('<?php _e("Only pdf,doc,docx,xls,xlsx,ppt,pptx,gif,png,jpg,jpeg formate are allowed. '  + ext + ' formate are not allowed.","gym_mgt") ?>');
			$(this).replaceWith('<input class="btn_top input-file" name="message_attachment[]" type="file" />');
			return false; 
		} 
		//File Size Check 
		if (file.size > 20480000) 
		{
			alert("<?php esc_html_e('Too large file Size. Only file smaller than 10MB can be uploaded.','gym_mgt');?>");
			$(this).replaceWith('<input class="btn_top input-file" name="message_attachment[]" type="file" />'); 
			return false; 
		}
	});	
} );
function add_new_attachment()
{
	$(".attachment_div").append('<div class="form-group"><label class="col-sm-2 control-label" for="photo"><?php esc_html_e('Attachment ','gym_mgt');?></label><div class="col-sm-3"><input  class="btn_top input-file" name="message_attachment[]" type="file" /></div><div class="col-sm-2"><input type="button" value="<?php esc_html_e('Delete','gym_mgt');?>" onclick="delete_attachment(this)" class="remove_cirtificate doc_label btn btn-danger"></div></div>');
}
function delete_attachment(n)
{
	n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);				
}
</script>
<div class="mailbox-content"><!-- MAILBOX CONTENT DIV START -->
	<h2>
		<?php  
		$edit=0;
		if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
		{
			 echo esc_html__( 'Edit Message', 'gym_mgt');
			 $edit=1;
			 $exam_data= get_exam_by_id($_REQUEST['exam_id']);
		}
		?>
	</h2>
	<?php
	if(isset($message))
		echo '<div id="message" class="updated below-h2"><p>'.$message.'</p></div>';
	?>
	<form name="class_form" action="" method="post" class="form-horizontal padding_bottom_50" id="message_form" enctype="multipart/form-data"><!-- COMPOSE MAIL FORM START -->
		<?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo esc_attr($action);?>">
		<div class="form-group">
			<label class="col-sm-2 control-label" for="to"><?php esc_html_e('Message To','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<select name="receiver" class="form-control validate[required] text-input message_to" id="to">
					<?php
					$curr_user_id=get_current_user_id();
					$obj_gym=new MJgmgt_Gym_management($curr_user_id);

					$role = $obj_gym->role;
					if($role != 'member')
					{
					?>
					<option value="member"><?php esc_html_e('Members','gym_mgt');?></option>
					<option value="staff_member"><?php esc_html_e('Staff Members','gym_mgt');?></option>
					<option value="accountant"><?php esc_html_e('Accountant','gym_mgt');?></option>
					<option value="administrator"><?php esc_html_e('Admin','gym_mgt');?></option>
					<?php 
					}
					echo MJgmgt_get_all_user_in_message();
					?>
				</select>
			</div>
		</div>
		<?php
		if($role != 'member')
		{
		?>
		<div id="smgt_select_class" class="display_class_css">
			<div class="form-group">
				<label class="col-sm-2 control-label" for="sms_template"><?php esc_html_e('Select Class','gym_mgt');?></label>
				<div class="col-sm-8">
					<select name="class_id"  id="class_list" class="form-control">
						<option value="all"><?php esc_html_e('All','gym_mgt');?></option>
						<?php
						foreach(MJgmgt_get_allclass() as $classdata)
						{  
						?>
						   	<option  value="<?php echo esc_attr($classdata['class_id']);?>" ><?php echo esc_html($classdata['class_name']);?></option>
					 <?php }?>
					</select>
				</div>
			</div>
		</div>
		<?php
		}
		?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="subject"><?php esc_html_e('Subject','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			   <input id="subject" class="form-control validate[required,custom[popup_category_validation]] onlyletter_number_space_validation" type="text" maxlength="100" name="subject" value="<?php if($edit){ echo $exam_data->exam_date;}?>">
			</div>
		</div>
		<!--nonce-->
		<?php wp_nonce_field( 'save_message_nonce' ); ?>
		<!--nonce-->
		<div class="form-group">
			<label class="col-sm-2 control-label" for="subject"><?php esc_html_e('Message Comment','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			  	<textarea name="message_body" id="message_body" class="form-control validate[required,custom[address_description_validation]]" maxlength="500"><?php if($edit){ echo esc_textarea($exam_data->exam_comment);}?></textarea>
			</div>
		</div>		
		<div class="attachment_div">
			<div class="form-group">
				<label class="col-sm-2 control-label" for="photo"><?php esc_html_e('Attachment','gym_mgt');?></label>
				<div class="col-sm-3">
				 	<input  class="btn_top input-file" name="message_attachment[]" type="file" />
				</div>										
			</div>							
       	</div>	
		<div class="form-group">		
			<div class="col-sm-offset-2 col-sm-10">
				<input type="button" value="<?php esc_attr_e('Add More Attachment','gym_mgt') ?>"  onclick="add_new_attachment()" class="btn more_attachment">
			</div>	
		</div>	   
		<div class="form-group">                      
			<div class="col-sm-10 ">
				<div class="pull-right">
					<input type="submit" value="<?php if($edit){ esc_html_e('Save Message','gym_mgt'); }else{ esc_html_e('Send Message','gym_mgt');}?>" name="save_message" class="btn btn-success"/>
				</div>
			</div>
		</div>
	</form><!-- COMPOSE MAIL FORM END -->
</div><!-- MAILBOX CONTENT DIV END -->