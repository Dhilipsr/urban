<?php 
$curr_user_id=get_current_user_id();
$obj_gym=new MJgmgt_Gym_management($curr_user_id);
$obj_nutrition=new MJgmgt_nutrition;
$active_tab = isset($_GET['tab'])?$_GET['tab']:'nutritionlist';
//access right
$user_access=MJgmgt_get_userrole_wise_page_access_right_array();
if (isset ( $_REQUEST ['page'] ))
{	
	if($user_access['view']=='0')
	{	
		MJgmgt_access_right_page_not_access_message();
		die;
	}
	if(!empty($_REQUEST['action']))
	{
		if (isset ( $_REQUEST ['page'] ) && $_REQUEST ['page'] == $user_access['page_link'] && ($_REQUEST['action']=='view'))
		{
			if($user_access['add']=='0')
			{	
				MJgmgt_access_right_page_not_access_message();
				die;
			}			
		}
		if (isset ( $_REQUEST ['page'] ) && $_REQUEST ['page'] == $user_access['page_link'] && ($_REQUEST['action']=='delete'))
		{
			if($user_access['delete']=='0')
			{	
				MJgmgt_access_right_page_not_access_message();
				die;
			}	
		}
		if (isset ( $_REQUEST ['page'] ) && $_REQUEST ['page'] == $user_access['page_link'] && ($_REQUEST['action']=='insert'))
		{
			if($user_access['add']=='0')
			{	
				MJgmgt_access_right_page_not_access_message();
				die;
			}	
		}
	}
}
//SAVE Nutrition DATA
if(isset($_POST['save_nutrition']))
{
	$nonce = $_POST['_wpnonce'];
	if (wp_verify_nonce( $nonce, 'save_nutrition_nonce' ) )
	{
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
		{		
			$result=$obj_nutrition->MJgmgt_add_nutrition($_POST);
			if($result)
			{
				wp_redirect ( home_url().'?dashboard=user&page=nutrition&tab=nutritionlist&message=2');
			}	
		}
		else
		{
			$result=$obj_nutrition->MJgmgt_add_nutrition($_POST);
			if($result)
			{
				wp_redirect ( home_url().'?dashboard=user&page=nutrition&tab=nutritionlist&message=1');
			}
		}
	}	
}
//DELETE Nutrition DATA	
if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
{	
	$result=$obj_nutrition->MJgmgt_delete_nutrition($_REQUEST['nutrition_id']);
	if($result)
	{
		wp_redirect ( home_url().'?dashboard=user&page=nutrition&tab=nutritionlist&message=3');
	}
}
if(isset($_REQUEST['message']))
{
	$message =esc_attr($_REQUEST['message']);
	if($message == 1)
	{?>
		<div id="message" class="updated below-h2 ">
			<p><?php esc_html_e('Nutrition added successfully.','gym_mgt');?></p>
		</div>
	<?php 	
	}
	elseif($message == 2)
	{?>
		<div id="message" class="updated below-h2 ">
			<p><?php esc_html_e("Nutrition updated successfully.",'gym_mgt');?></p>
		</div>
	<?php 	
	}
	elseif($message == 3) 
	{?>
		<div id="message" class="updated below-h2">
			<p><?php esc_html_e('Nutrition deleted successfully.','gym_mgt');?></p>
		</div>
	<?php		
	}
}
?>
<script type="text/javascript">
$(document).ready(function() {
	jQuery('#nutrition_list').DataTable({
		"responsive": true,
		"order": [[ 0, "asc" ]],
		"aoColumns":[
					{"bSortable": false},
					  {"bSortable": true},
					  {"bSortable": true},
					  {"bSortable": false}]
		});
		$('#nutrition_form').validationEngine({promptPosition : "bottomRight",maxErrorsPerField: 1});	
		$(".display-members").select2();
		$(".start_date").datepicker(
		{	
        	dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
			minDate:0,
        	onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 0);
            $(".end_date").datepicker("option", "minDate", dt);
        }
    });
    $(".end_date").datepicker(
    {		
       dateFormat: '<?php echo get_option('gmgt_datepicker_format');?>',
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 0);
            $(".start_date").datepicker("option", "maxDate", dt);
        }
    });
} );
</script>
<!-- POP up code -->
<div class="popup-bg">
    <div class="overlay-content">
       <div class="modal-content">
          <div class="category_list"></div>
       </div>
    </div> 
</div>
<!-- End POP-UP Code -->
<div class="panel-body panel-white"><!-- PANEL BODY DIV START -->
    <ul class="nav nav-tabs panel_tabs" role="tablist"><!-- NAV TABS MENU START -->
	  	<li class="margin_top_5 <?php if($active_tab=='nutritionlist'){?>active<?php }?>">
			<a href="?dashboard=user&page=nutrition&tab=nutritionlist" class="tab <?php echo $active_tab == 'nutritionlist' ? 'active' : ''; ?>">
             <i class="fa fa-align-justify"></i> <?php esc_html_e('Nutrition Schedule List', 'gym_mgt'); ?></a>
          </a>
        </li>
	  <?php		
		if($user_access['add']=='1')
		{
		?>
			<li class="margin_top_5 <?php if($active_tab=='addnutrition'){?>active<?php }?>">
		  	<?php  if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'view')
			{				
				?>
				<a href="?dashboard=user&page=nutrition&tab=addnutrition&&action=view&workoutmember_id=<?php echo $_REQUEST['workoutmember_id'];?>" class="nav-tab <?php echo $active_tab == 'addnutrition' ? 'nav-tab-active' : ''; ?>"><i class="fa fa-align-justify"></i> <?php esc_html_e('View  Nutrition Schedule', 'gym_mgt'); ?></a>
			 <?php
			}
			else
			{				
				?>
				<a href="?dashboard=user&page=nutrition&tab=addnutrition&&action=insert" class="tab <?php echo $active_tab == 'addnutrition' ? 'active' : ''; ?>"><i class="fa fa-plus-circle"></i> <?php esc_html_e('Add Nutrition Schedule', 'gym_mgt'); ?></a>
			<?php 
			} 
			?>	  
			</li>
		<?php 
		}
		?>
    </ul><!-- NAV TABS MENU END -->
	<div class="tab-content"><!-- TAB CONTENT DIV START -->
	<?php 
	if($active_tab == 'nutritionlist')
	{ ?>	
		<form name="wcwm_report" action="" method="post"><!-- Nutrition LIST FORM START -->
			<div class="panel-body"><!-- PANEL BODY DIV START -->
				<?php
				if($obj_gym->role=='member')
				{
					$nutrition_logdata=MJgmgt_get_user_nutrition(get_current_user_id());
				?>
					<div class="print-button pull-right">
						<a  href="?page=nutrition&print=print" target="_blank"class="btn btn-success" <?php if(empty($nutrition_logdata)){ ?> disabled <?php } ?>><?php esc_html_e('Print Nutrition','gym_mgt'); ?></a>
						<a  href="?page=nutrition&nutrition_pdf=nutrition_pdf" target="_blank" class="btn btn-success" <?php if(empty($nutrition_logdata)){ ?> disabled <?php } ?>><?php esc_html_e('PDF Nutrition','gym_mgt');?></a>
					</div>	<?php
					if(empty($nutrition_logdata)) 
					{?>
						<div class="clear col-md-12"><h3><?php esc_html_e("There is no any nutrition schedule data.",'gym_mgt');?> </h3></div>
					  <?php 
					} 
					if(isset($nutrition_logdata))
						foreach($nutrition_logdata as $row)
						{
							$all_logdata=MJgmgt_get_nutritiondata($row->id); 
							$arranged_workout=MJgmgt_set_nutrition_array($all_logdata);
							?>
							 <div class="workout_<?php echo $row->id;?> workout-block"><!-- WORKOUT BLOCK DIV START -->
								<div class="panel-heading">
									<h3 class="panel-title"><i class="fa fa-calendar"></i> 
									<?php 
									esc_html_e('Start From ','gym_mgt');
									echo "<span class='work_date'>".MJgmgt_getdate_in_input_box( $row->start_date )."</span>";
									esc_html_e(' To ','gym_mgt');
									echo "<span class='work_date'>".MJgmgt_getdate_in_input_box($row->expire_date ); 
									?></h3>						
								</div>											
								<div class="panel panel-white"><!-- PANEL WHITE DIV START -->
									<?php
										if(!empty($arranged_workout))
										{ ?>
											<div class="work_out_datalist_header">
												<div class="col-md-4 col-sm-4 col-xs-4">  
													<strong><?php esc_html_e('Day Name','gym_mgt');?></strong>
												</div>
												<div class="col-md-8 col-sm-8 col-xs-8">
													<span class="col-md-3 hidden-xs" style="margin-left: -60px;"><?php esc_html_e('Time','gym_mgt');?></span>
													<span class="col-md-3"><?php esc_html_e('Description','gym_mgt');?></span>
												</div>
											</div>
											<?php 
											foreach($arranged_workout as $key=>$rowdata)
												{
													
													?>
													<div class="work_out_datalist">
														<div class="col-md-3 col-sm-3 col-xs-12 day_name">  
															<?php 
															//echo esc_html($key);
															if($key== 'Sunday')
															{
																echo esc_html_e('Sunday','gym_mgt');
															}
															elseif($key == 'Monday')
															{
																echo esc_html_e('Monday','gym_mgt');
															}
															elseif($key == 'Tuesday')
															{
																echo esc_html_e('Tuesday','gym_mgt');
															}
															elseif($key == 'Wednesday')
															{
																echo esc_html_e('Wednesday','gym_mgt');
															}
															elseif($key == 'Thursday')
															{
																echo esc_html_e('Thursday','gym_mgt');
															}
															elseif($key == 'Friday')
															{
																echo esc_html_e('Friday','gym_mgt');
															}
															elseif($key== 'Saturday')
															{
																echo esc_html_e('Saturday','gym_mgt');
															}
															?>
														</div>
														<div class="col-md-9 col-sm-9 col-xs-12">
															<?php
															foreach($rowdata as $row)
															{	
																echo $row."<br>";			
															} 
															?>
														</div>
													</div>
										  <?php }	
										} ?>											
								</div><!-- PANEL WHITE DIV END -->
							</div><!-- WORKOUT BLOCK DIV END -->
						<?php 
						}	
				}
				else
				{
					?>
					<div class="table-responsive"><!-- TABLE RESPONSIVE DIV START -->
						<table id="nutrition_list" class="display" cellspacing="0" width="100%"><!-- TABLE Nutrition LIST START -->
							<thead>
								<tr>
									<th><?php  esc_html_e( 'Photo', 'gym_mgt' ) ;?></th>
									<th><?php  esc_html_e( 'Member Name', 'gym_mgt' ) ;?></th>
									<th><?php  esc_html_e( 'Member Goal', 'gym_mgt' ) ;?></th>
									<th><?php  esc_html_e( 'Action', 'gym_mgt' ) ;?></th>
								</tr>
							 </thead>
							<tfoot>
								<tr>
									<th><?php  esc_html_e( 'Photo', 'gym_mgt' ) ;?></th>
									<th><?php  esc_html_e( 'Member Name', 'gym_mgt' ) ;?></th>
									<th><?php  esc_html_e( 'Member Goal', 'gym_mgt' ) ;?></th>
									<th><?php  esc_html_e( 'Action', 'gym_mgt' ) ;?></th>
								</tr>
							</tfoot>
							<tbody>
							<?php
							$get_members = array('role' => 'member');
							$membersdata=get_users($get_members);
							if(!empty($membersdata))
							{
								foreach ($membersdata as $retrieved_data)
								{?>
								<tr>
									<td class="user_image"><?php $uid=$retrieved_data->ID;
										$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);
										if(empty($userimage)){
											echo '<img src='.get_option( 'gmgt_system_logo' ).' height="50px" width="50px" class="img-circle" />';
										}
										else
											echo '<img src='.$userimage.' height="50px" width="50px" class="img-circle"/>';
									?></td>
									<td class="member"><a href="#">
									<?php $user=get_userdata($retrieved_data->ID);
									$display_label=$user->display_name;
									$memberid=get_user_meta($retrieved_data->ID,'member_id',true);
									if($memberid)
									{
										$display_label.=" (".$memberid.")";
										echo esc_html($display_label);
									}
									?></a></td>
									<td class="member-goal"><?php $intrestid=get_user_meta($retrieved_data->ID,'intrest_area',true);
									if(!empty($intrestid))
									{
										echo get_the_title($intrestid);
									}
									else
									{
										echo "-";
									}
									?></td>			
									<td class="action"> 
										<a href="?dashboard=user&page=nutrition&tab=addnutrition&action=view&workoutmember_id=<?php echo esc_attr($retrieved_data->ID);?>" class="btn btn-default">
										<i class="fa fa-eye"></i> <?php esc_html_e('View Nutrition', 'gym_mgt');?></a>
									</td>               
								</tr>
								<?php
								} 			
							}?>
							</tbody>             
						</table><!-- TABLE Nutrition LIST END -->
					</div><!-- TABLE RESPONSIVE DIV END -->
				<?php 
				}
				?>
			</div><!-- PANEL BODY END-->
	    </form>	<!-- Nutrition LIST FORM END -->	
		<?php 
	}
	if($active_tab == 'addnutrition')
	{
	 	$nutrition_id=0;
	 	$edit=0;
	 	if(isset($_REQUEST['workouttype_id']))
	 		$workouttype_id=$_REQUEST['workouttype_id'];
	 	if(isset($_REQUEST['workoutmember_id']))
		{
	 		$edit=0;
	 		$workoutmember_id=$_REQUEST['workoutmember_id'];
	 		$nutrition_logdata=MJgmgt_get_user_nutrition($workoutmember_id);
	 	}?>
        <div class="panel-body"><!-- PANEL BODY START-->
			<form name="nutrition_form" action="" method="post" class="form-horizontal" id="nutrition_form"><!--Nutrition FORM START-->
				<?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
				<input type="hidden" name="action" value="<?php echo esc_attr($action);?>">
				<input type="hidden" name="nutrition_id" value="<?php echo esc_attr($nutrition_id);?>"  />
				<div class="form-group">
					<label class="col-sm-2 control-label" for="day"><?php esc_html_e('Member','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-8">
						<?php if($edit){ $member_id=$workoutmember_id; }elseif(isset($_POST['member_id'])){$member_id=$_POST['member_id'];}else{$member_id='';}?>
						<select id="member_list" class="display-members" name="member_id" required="true">
						<option value=""><?php esc_html_e('Select Member','gym_mgt');?></option>
							<?php $get_members = array('role' => 'member');
							$membersdata=get_users($get_members);
							 if(!empty($membersdata))
							 {
								foreach ($membersdata as $member){
									if( $member->membership_status == "Continue")
										{?>
									<option value="<?php echo esc_attr($member->ID);?>" <?php selected(esc_attr($member_id),esc_attr($member->ID));?>><?php echo esc_html($member->display_name)." - ".esc_html($member->member_id); ?> </option>
								<?php }
								}
							 }?>
					</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Start Date','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-3">
						<input id="Start_date" class="datepicker form-control validate[required] text-input start_date"  type="text" value="<?php if($edit){ echo esc_attr($result->start_date);}elseif(isset($_POST['start_date'])){echo esc_attr($_POST['start_date']);}?>" name="start_date" readonly>
					</div>
					<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('End Date','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-3">
						<input id="end_date" class="datepicker form-control validate[required] text-input end_date" type="text" value="<?php if($edit){ echo esc_attr($result->expire_date);}elseif(isset($_POST['end_date'])){echo esc_attr($_POST['end_date']);}?>" name="end_date" readonly>
					</div>
				</div>
				<div class="form-group">				
					<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Select Days','gym_mgt');?><span class="require-field">*</span></label>
					<div class="col-sm-8">			
					<?php
					foreach (MJgmgt_days_array() as $key=>$name)
					{
					?>
						<div class="col-md-3" style="padding-left: 0px;">
							<div class="checkbox">
							  <label><input type="checkbox" value="" name="day[]" value="<?php echo esc_attr($key);?>" id="<?php echo esc_attr($key);?>" data-val="day"><?php echo esc_html($name); ?> </label>
							</div>
						</div>
					<?php
					}
					?>
					</div>	
				</div>
				<div class="form-group">				
					<div class="col-sm-12">	
						<label class="col-sm-2 control-label" for="notice_content"><?php esc_html_e('Nutrition Details','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-md-8 activity_list">				
							<label class="activity_title checkbox">				  		
							<strong>
								<input type="checkbox" value="" name="avtivity_id[]" value="breakfast" class="nutrition_check" id="breakfast"  activity_title = "" data-val="nutrition_time"><?php esc_html_e('Break Fast','gym_mgt');?></strong></label>	
								<div id="txt_breakfast"></div>
								<label class="activity_title checkbox">				  		
								<strong>
								<input type="checkbox" value="" name="avtivity_id[]" value="midmorning_snack" class="nutrition_check" id="midmorning_snack" activity_title = "" data-val="nutrition_time"><?php esc_html_e('Mid Morning Snacks','gym_mgt');?></strong></label>
								<div id="txt_midmorning_snack"></div>
								<label class="activity_title checkbox">
								<strong>
								<input type="checkbox" value="" name="avtivity_id[]" value="lunch" class="nutrition_check" id="lunch"  activity_title = "" data-val="nutrition_time"><?php esc_html_e('Lunch','gym_mgt');?></strong></label>
								<div id="txt_lunch"></div>
								<label class="activity_title checkbox">				  		
								<strong>
								<input type="checkbox" value="" name="avtivity_id[]" value="afternoon_snack" class="nutrition_check" id="afternoon_snack"  activity_title = "" data-val="nutrition_time"><?php esc_html_e('Afternoon Snacks','gym_mgt');?></strong></label>
								<div id="txt_afternoon_snack"></div>
								<label class="activity_title checkbox"><strong>
								<input type="checkbox" value="" name="avtivity_id[]" value="dinner" class="nutrition_check" id="dinner"  activity_title = "" data-val="nutrition_time"><?php esc_html_e('Dinner','gym_mgt');?></strong></label>
								<div id="txt_dinner"></div>							
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<!--nonce-->
				<?php wp_nonce_field( 'save_nutrition_nonce' ); ?>
				<!--nonce-->
				<div class="col-sm-offset-2 col-sm-8">
					<div class="form-group">
						<div class="col-md-8">
							<input type="button" value="<?php esc_html_e('Step-1 Add Nutrition','gym_mgt');?>" name="save_nutrition" id="add_nutrition" class="btn btn-success"/>
						</div>
					</div>
				</div>
				<div id="display_nutrition_list" style="clear: both;"></div>
				<div class="clear"></div>
				<div class="col-sm-offset-2 col-sm-8">
					<input type="submit" value="<?php if($edit){ esc_html_e('Step 2 Save Nutrition Plan','gym_mgt'); }else{ esc_html_e('Step 2 Save Nutrition Plan','gym_mgt');}?>" name="save_nutrition" class="btn btn-success"/>
				</div>
		    </form><!--Nutrition FORM END-->
        </div><!--PANEL BODY DIV END-->
		<?php 
		if(isset($nutrition_logdata))
     	foreach($nutrition_logdata as $row)
		{
			$all_logdata=MJgmgt_get_nutritiondata($row->id);
			$arranged_workout=MJgmgt_set_nutrition_array($all_logdata);
			?>
			<div class="workout_<?php echo esc_attr($row->id);?> workout-block"><!--WORKOUT BLOCK DIV START-->
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-calendar"></i> 
					<?php echo "Start From <span class='work_date'>".MJgmgt_getdate_in_input_box(esc_html($row->start_date))."</span> To <span class='work_date'>".MJgmgt_getdate_in_input_box(esc_html($row->expire_date)); ?>
					<?php
					if($user_access['delete']=='1')
					{
					?>		
						<span class="removenutrition badge badge-delete pull-right" id="<?php echo esc_attr($row->id);?>">X</span>
					<?php
					}
					?>
					</h3>						
				</div>
				<div class="panel panel-white"><!--PANEL WHITE DIV START-->
					<?php
					if(!empty($arranged_workout))
					{
					?>
					<div class="work_out_datalist_header">
						<div class="col-md-3">  
						<strong><?php esc_html_e('Day Name','gym_mgt');?></strong>
						</div>
						<div class="col-md-9">
						<span class="col-md-3"><?php esc_html_e('Time','gym_mgt');?></span>
						<span class="col-md-6"><?php esc_html_e('Description','gym_mgt');?></span>
						</div>
					</div>
					<?php 
					foreach($arranged_workout as $key=>$rowdata)
					{?>
						<div class="work_out_datalist">
							<div class="col-md-3 day_name">  
								<?php echo esc_html($key);?>
							</div>
							<div class="col-md-9">
								<?php 
								foreach($rowdata as $row)
								{										
									echo $row."<br>";
								} 
								?>
							</div>
						</div>
				<?php } 
					}?>
				</div><!--PANEL WHITE DIV END-->
			</div><!--WORKOUT BLOCK DIV END-->
     	 <?php
		}	
	}
	?>
	</div><!-- TAB CONTENT DIV END -->
</div><!-- PANEL BODY DIV END -->