<?php
/*
 * WRC Pricing Tables 2.3
 * @realwebcare - https://www.realwebcare.com/
 * Auto generating pricing table from scratch.
 * Changing table design with selected template instantly.
 */
function wrcpt_activate_template() {
	$template_number = $_POST['tempcount'];
	$package_table = get_option('packageTables');
	$package_id = $id_count = 1;
	$table_lists = explode(', ', $package_table);
	$count_copy = count($table_lists) + $template_number;
	$pricing_table = 'pricing_template_' . $count_copy . rand(1,1000);
	$package_feature = $pricing_table.'_feature';
	$table_option = $pricing_table.'_option';
	$fn = 1;

	$template_features = array('Webspace', 'Monthly Bandwidth', 'Domain Name', 'Email Address', 'Online Support');
	$feature_type = array('text', 'text', 'text', 'text', 'check');
	$feature_values = array( 'fitem1' => array('5 GB', '15 GB', '30 GB', '50 GB'), 'fitem2' => array('50 GB', '150 GB', '300 GB', '500 GB'), 'fitem3' => array('25', '75', '150', '250'), 'fitem4' => array('50', '150', '300', '500'), 'fitem5' => array('cross', 'tick', 'tick', 'tick'), 'tip1' => array('', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum elit et ipsum tempus, at ultricies odio effic', '', ''), 'tip2' => array('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum elit et ipsum tempus, at ultricies odio effic', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum elit et ipsum tempus, at ultricies odio effic'), 'tip3' => array('', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum elit et ipsum tempus, at ultricies odio effic', ''), 'tip4' => array('', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum elit et ipsum tempus, at ultricies odio effic', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum elit et ipsum tempus, at ultricies odio effic'), 'tip5' => array('', '', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum elit et ipsum tempus, at ultricies odio effic') );

	if(!isset($package_table)) {
		add_option('packageTables', $pricing_table);
		add_option('packageIDs', $package_id);
		add_option('IDsCount', $id_count);
	} elseif(empty($package_table)){
		update_option('packageTables', $pricing_table);
		update_option('packageIDs', $package_id);
		update_option('IDsCount', $id_count);
	} else {
		if(in_array($pricing_table, $table_lists)) {
			$new_pricing_table = 'another_' . $pricing_table;
			$pricing_table_lists = $package_table . ', ' . $new_pricing_table;
			update_option('packageTables', $pricing_table_lists);
		} else {
			$pricing_table_lists = $package_table . ', ' . $pricing_table;
			update_option('packageTables', $pricing_table_lists);
		}
		$package_id = get_option('packageIDs');
		$id_count = get_option('IDsCount') + 1;
		$pricing_table_ids = $package_id . ', ' . $id_count;
		update_option('packageIDs', $pricing_table_ids);
		update_option('IDsCount', $id_count);
	}

	$package_options_check = array( 'cwidth' => '', 'maxcol' => '4', 'colgap' => '1', 'capwidth' => '18.73', 'ctsize' => '', 'cftsize' => '', 'tbody' => '', 'tsize' => '', 'pbody' => '', 'psbig' => '', 'pssmall' => '', 'ftbody' => '', 'ftsize' => '', 'btbody' => '', 'bwidth' => '', 'bheight' => '', 'btsize' => '', 'rtsize' => '', 'ttwidth' => '150px', 'ftdir' => 'left', 'enable' => 'yes', 'ftcap' => 'no', 'autocol' => 'yes', 'encol' => 'yes', 'colshad' => 'yes', 'dscol' => 'no', 'purgt' => 'no', 'entips' => 'yes', 'enribs' => 'yes', 'nltab' => 'no' );
	$package_details_texts = array( 'spack' => array('no', 'no', 'no', 'no'), 'pdisp' => array('show', 'show', 'show', 'show'), 'type' => array('Starter', 'Professional', 'Business', 'Premier'), 'price' => array('9', '24', '39', '54'), 'cent' => array('', '', '', ''), 'unit' => array('$', '$', '$', '$'), 'plan' => array('month', 'month', 'month', 'month'), 'btext' => array('Sign Up', 'Sign Up', 'Sign Up', 'Sign Up'), 'blink' => array('#', '#', '#', '#'), 'rtext' => array('', '', 'NEW', '') );

	if($template_number == 1) {
		$package_options_color = array( 'templ' => 'temp1', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details_color = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#64abcb', '#ecb000', '#9db74b', '#988fbb'), 'pcbig' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'btcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bthover' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bcolor' => array('#64abcb', '#ecb000', '#9db74b', '#988fbb'), 'bhover' => array('#64abcb', '#ecb000', '#9db74b', '#988fbb'), 'rtcolor' => array('#aa4518', '#aa4518', '#aa4518', '#aa4518'), 'rbcolor' => array('#faec00', '#faec00', '#faec00', '#faec00') );
		$package_options = array_merge($package_options_check, $package_options_color);
		$package_details = array_merge($package_details_texts, $package_details_color);
	} elseif($template_number == 2) {
		$package_options_color = array( 'templ' => 'temp2', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details_color = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#49ab81', '#419873', '#398564', '#317256'), 'pcbig' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'btcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bthover' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bcolor' => array('#49ab81', '#419873', '#398564', '#317256'), 'bhover' => array('#49ab81', '#419873', '#398564', '#317256'), 'rtcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'rbcolor' => array('#93291c', '#93291c', '#93291c', '#93291c') );
		$package_options = array_merge($package_options_check, $package_options_color);
		$package_details = array_merge($package_details_texts, $package_details_color);
	} elseif($template_number == 3) {
		$package_options_color = array( 'templ' => 'temp3', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details_color = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#6497b1', '#005b96', '#03396c', '#011f4b'), 'pcbig' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'btcolor' => array('#012345', '#012345', '#012345', '#012345'), 'bthover' => array('#012345', '#012345', '#012345', '#012345'), 'bcolor' => array('#b3cde0', '#b3cde0', '#b3cde0', '#b3cde0'), 'bhover' => array('#b3cde0', '#b3cde0', '#b3cde0', '#b3cde0'), 'rtcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'rbcolor' => array('#a73d30', '#a73d30', '#a73d30', '#a73d30') );
		$package_options = array_merge($package_options_check, $package_options_color);
		$package_details = array_merge($package_details_texts, $package_details_color);
	} elseif($template_number == 4) {
		$package_options_color = array( 'templ' => 'temp4', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details_color = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#FFCD00', '#FF8F45', '#9332CB', '#CC2162'), 'pcbig' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'btcolor' => array('#FFCD00', '#FF8F45', '#9332CB', '#CC2162'), 'bthover' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bhover' => array('#6239B9', '#6239B9', '#6239B9', '#6239B9'), 'rtcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'rbcolor' => array('#FFCD00', '#FF8F45', '#9332CB', '#CC2162') );
		$package_options = array_merge($package_options_check, $package_options_color);
		$package_details = array_merge($package_details_texts, $package_details_color);
	} elseif($template_number == 5) {
		$package_options_color = array( 'templ' => 'temp5', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details_color = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#2C2C2C', '#2C2C2C', '#2C2C2C', '#2C2C2C'), 'pcbig' => array('#5EC4CD', '#5EC4CD', '#5EC4CD', '#5EC4CD'), 'btcolor' => array('#2C2C2C', '#2C2C2C', '#2C2C2C', '#2C2C2C'), 'bthover' => array('#5EC4CD', '#5EC4CD', '#5EC4CD', '#5EC4CD'), 'bcolor' => array('#cccccc', '#cccccc', '#cccccc', '#cccccc'), 'bhover' => array('#eeeeee', '#eeeeee', '#eeeeee', '#eeeeee'), 'rtcolor' => array('#ff0000', '#ff0000', '#ff0000', '#ff0000'), 'rbcolor' => array('#ffc100', '#ffc100', '#ffc100', '#ffc100') );
		$package_options = array_merge($package_options_check, $package_options_color);
		$package_details = array_merge($package_details_texts, $package_details_color);
	} else {
		$package_options_color = array( 'templ' => 'temp0', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details_color = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#44A3D5', '#44A3D5', '#44A3D5', '#44A3D5'), 'pcbig' => array('#ffffff', '#ffffff', '#ffffff', '#ffffff'), 'btcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bthover' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bcolor' => array('#333333', '#333333', '#333333', '#333333'), 'bhover' => array('#333333', '#333333', '#333333', '#333333'), 'rtcolor' => array('#333333', '#333333', '#333333', '#333333'), 'rbcolor' => array('#CB0000', '#CB0000', '#CB0000', '#CB0000') );
		$package_options = array_merge($package_options_check, $package_options_color);
		$package_details = array_merge($package_details_texts, $package_details_color);
	}
	/* Generating Package Features */
	foreach($template_features as $key => $feature) {
		if($feature) {
			$feature_name['fitem'.$fn] = sanitize_text_field( $feature );
			$feature_name['ftype'.$fn] = $feature_type[$key];
			$fn++;
		} else {
			$feature_name['fitem'.$fn] = '';
			$feature_name['ftype'.$fn] = '';
			$fn++;
		}
	}
	add_option($package_feature, $feature_name);
	/* Generating Package Options */
	foreach($package_options as $key => $option) {
		$optionValue[$key] = sanitize_text_field( $option );
	}
	add_option($table_option, $optionValue);
	/* Generating Package Lists */
	for($pn = 0; $pn < 5; $pn++) {
		$package_lists = get_option($pricing_table);
		$optionName = wrcpt_update_pricing_table($pricing_table, $package_lists);
		$package_count = get_option('packageCount');
		$new_package_lists = get_option($pricing_table);
		$packageOptions = explode(', ', $new_package_lists);
		$list_count = count($packageOptions);
		foreach($package_details as $pkey => $value) {
			$packageOptions_text[$pkey] = sanitize_text_field( $value[$pn] );
		}
		foreach($feature_values as $fkey => $fvalue) {
			$featureValues_text[$fkey] = sanitize_text_field( $fvalue[$pn] );
		}
		$package_details_top = array( 'pid' => $package_count, 'order' => $list_count );
		$mergePackages = array_merge($package_details_top, $packageOptions_text, $featureValues_text);
		add_option($optionName, $mergePackages);
	}
}
add_action( 'wp_ajax_nopriv_wrcpt_activate_template', 'wrcpt_activate_template' );
add_action( 'wp_ajax_wrcpt_activate_template', 'wrcpt_activate_template' );

function wrcpt_setup_selected_template() {
	$template = $_POST['template'];
	$table_name = $_POST['packtable'];
	$option_name = $table_name.'_option';
	$table_option = get_option($option_name);
	$package_lists = get_option($table_name);
	$packageOptions = explode(', ', $package_lists);
	if($template == 'temp1') {
		$package_options = array( 'templ' => 'temp1', 'ftdir' => 'left', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#64abcb', '#ecb000', '#9db74b', '#988fbb'), 'pcbig' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'btcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bthover' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bcolor' => array('#64abcb', '#ecb000', '#9db74b', '#988fbb'), 'bhover' => array('#64abcb', '#ecb000', '#9db74b', '#988fbb'), 'rtcolor' => array('#aa4518', '#aa4518', '#aa4518', '#aa4518'), 'rbcolor' => array('#faec00', '#faec00', '#faec00', '#faec00') );
	} elseif($template == 'temp2') {
		$package_options = array( 'templ' => 'temp2', 'ftdir' => 'left', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#49ab81', '#419873', '#398564', '#317256'), 'pcbig' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'btcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bthover' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bcolor' => array('#49ab81', '#419873', '#398564', '#317256'), 'bhover' => array('#49ab81', '#419873', '#398564', '#317256'), 'rtcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'rbcolor' => array('#93291c', '#93291c', '#93291c', '#93291c') );
	} elseif($template == 'temp3') {
		$package_options = array( 'templ' => 'temp3', 'ftdir' => 'left', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#6497b1', '#005b96', '#03396c', '#011f4b'), 'pcbig' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'btcolor' => array('#012345', '#012345', '#012345', '#012345'), 'bthover' => array('#012345', '#012345', '#012345', '#012345'), 'bcolor' => array('#b3cde0', '#b3cde0', '#b3cde0', '#b3cde0'), 'bhover' => array('#b3cde0', '#b3cde0', '#b3cde0', '#b3cde0'), 'rtcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'rbcolor' => array('#a73d30', '#a73d30', '#a73d30', '#a73d30') );
	} elseif($template == 'temp4') {
		$package_options = array( 'templ' => 'temp4', 'ftdir' => 'center', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#FFCD00', '#FF8F45', '#9332CB', '#CC2162'), 'pcbig' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'btcolor' => array('#FFCD00', '#FF8F45', '#9332CB', '#CC2162'), 'bthover' => array('#ffffff', '#ffffff', '#ffffff', '#ffffff'), 'bcolor' => array('#ffffff', '#ffffff', '#ffffff', '#ffffff'), 'bhover' => array('#6239B9', '#6239B9', '#6239B9', '#6239B9'), 'rtcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'rbcolor' => array('#FFCD00', '#FF8F45', '#9332CB', '#CC2162') );
	} elseif($template == 'temp5') {
		$package_options = array( 'templ' => 'temp5', 'ftdir' => 'center', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#2C2C2C', '#2C2C2C', '#2C2C2C', '#2C2C2C'), 'pcbig' => array('#5EC4CD', '#5EC4CD', '#5EC4CD', '#5EC4CD'), 'btcolor' => array('#2C2C2C', '#2C2C2C', '#2C2C2C', '#2C2C2C'), 'bthover' => array('#5EC4CD', '#5EC4CD', '#5EC4CD', '#5EC4CD'), 'bcolor' => array('#cccccc', '#cccccc', '#cccccc', '#cccccc'), 'bhover' => array('#eeeeee', '#eeeeee', '#eeeeee', '#eeeeee'), 'rtcolor' => array('#ff0000', '#ff0000', '#ff0000', '#ff0000'), 'rbcolor' => array('#ffc100', '#ffc100', '#ffc100', '#ffc100') );
	} else {
		$package_options = array( 'templ' => 'temp0', 'ftdir' => 'left', 'cscolor' => '#cccccc', 'cshcolor' => '#333333' );
		$package_details = array( 'tcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'tbcolor' => array('#44A3D5', '#44A3D5', '#44A3D5', '#44A3D5'), 'pcbig' => array('#ffffff', '#ffffff', '#ffffff', '#ffffff'), 'btcolor' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bthover' => array('#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFFFFF'), 'bcolor' => array('#333333', '#333333', '#333333', '#333333'), 'bhover' => array('#333333', '#333333', '#333333', '#333333'), 'rtcolor' => array('#333333', '#333333', '#333333', '#333333'), 'rbcolor' => array('#CB0000', '#CB0000', '#CB0000', '#CB0000') );
	}
	foreach($packageOptions as $key => $option) {
		$package_value = get_option($option);
		foreach($package_details as $pkey => $value) {
			$packageValues_text[$pkey] = sanitize_text_field( $value[$key] );
		}
		$mergePackages = array_merge($package_value, $packageValues_text);
		update_option($option, $mergePackages);
	}
	$mergeOptions = array_merge($table_option, $package_options);
	update_option($option_name, $mergeOptions);
}
add_action( 'wp_ajax_nopriv_wrcpt_setup_selected_template', 'wrcpt_setup_selected_template' );
add_action( 'wp_ajax_wrcpt_setup_selected_template', 'wrcpt_setup_selected_template' );
?>