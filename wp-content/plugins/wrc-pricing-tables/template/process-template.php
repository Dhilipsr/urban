<?php
/*
 * WRC Pricing Tables 2.3
 * @realwebcare - https://www.realwebcare.com/
 * Processing pricing table templates.
 */
?>
<div class="wrap">
	<div class="postbox-container wrc-global" style="width:100%">
		<h2 class="main-header"><?php _e('Pricing Table Templates', 'wrcpt'); ?></h2>
		<hr>
		<div class="template-container"><?php
			for($tp = 1; $tp <= 6; $tp++) { ?>
				<div class="template-items">
					<div class="template-img">
						<img src="<?php echo plugins_url( '../images/template-'.$tp.'.png', __FILE__ ) ?>" alt="" />
					</div>
					<h2 class="template-name"><?php if($tp == 6) { ?><?php _e('Default ', 'wrcpt'); ?><?php } ?><?php _e('Template', 'wrcpt'); ?><?php if($tp != 6) { echo ' '.$tp; } ?></h2>
					<div class="template-actions">
						<span class="button button-secondary activate" onclick="wrcptactivatetemp(<?php echo $tp; ?>)"><?php _e('Create Table', 'wrcpt'); ?></span>
					</div>
				</div><?php
			} ?>
		</div>
	</div>
</div>