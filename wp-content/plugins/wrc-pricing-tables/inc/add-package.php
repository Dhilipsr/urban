<?php
/*
 * WRC Pricing Tables 2.3
 * @realwebcare - https://www.realwebcare.com/
 * Adding Pricing Table Packages
 */
function wrcpt_add_pricing_packages() {
	$pricing_table = $_POST['packtable'];
	$pricing_table_name = ucwords(str_replace('_', ' ', $pricing_table));
	$package_feature = get_option($pricing_table.'_feature');
	$featureNum = count($package_feature)/2;
	$checkValue = uniqid('yes'); ?>
	<div id="tablecolumndiv">
		<div class="tablecolumnwrap">
			<h3><?php _e('Pricing Table Columns', 'wrcpt'); ?></h3>
			<div id="addButtons"><a href="#" class="button button-large" id="addPackage"><?php _e('New Column', 'wrcpt'); ?></a></div>
			<div id="sortable_column">
				<div id="wrcpt-1" class="package_details">
					<h4 id="pcolumn1">Pricing Column 1</h4>
					<span id="hidePack1" class="column_hide inactive"><i class="dashicons dashicons-visibility"></i><input type="hidden" name="hide_show[]" value="show" /></span>
					<span id="delDisable"></span>
			  		<div id="accordion1" class="column_container">
			  			<h3 class="ptitle"><?php _e('Pricing Column Details', 'wrcpt'); ?></h3>
			  			<div class="element-input">
							<label class="input-check"><?php _e('Enlarge Column?', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('If you want to highlight the current column from some of the other columns, simply mark the checkbox.', 'wrcpt'); ?>"></a></label>
							<input type="checkbox" class="tickbox" name="special_package[0]" id="special_package1" value="yes" /><hr />
							<h4><?php _e('Package Name', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter your pricing package name here. You can also enter a short description of the package name. There are many users who choose a package based on the name, instead of features. So, a short description might help users to select the appropriate package.', 'wrcpt'); ?>"></a></h4>
			  				<label class="input-title"><?php _e('Package Name', 'wrcpt'); ?></label>
			  				<input type="text" name="package_type[]" class="medium" id="package_type" value="" placeholder="<?php _e('e.g. Enterprise', 'wrcpt'); ?>" />
    						<textarea name="package_desc[]" class="medium" id="package_desc" cols="27" rows="2" placeholder="<?php _e('Enter Short Description', 'wrcpt'); ?>"></textarea><hr />
							<h4><?php _e('Package Pricing', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter the price of the package, price currency, price plan (monthly or yearly) and a short description about pricing.', 'wrcpt'); ?>"></a></h4>
							<label class="input-title"><?php _e('Package Price', 'wrcpt'); ?></label>
			  				<input name="price_number[]" type="text" class="col_price" value="" placeholder="0" />&nbsp;.&nbsp;<input name="price_fraction[]" type="number" class="col_price" value="" min="0" max="99" placeholder="00" />
							<label class="input-title"><?php _e('Price Plan', 'wrcpt'); ?></label>
			  				<input name="package_plan[]" id="package_plan" type="text" class="medium" value="" placeholder="<?php _e('e.g. month', 'wrcpt'); ?>" />
							<label class="input-title"><?php _e('Price Unit', 'wrcpt'); ?></label>
			  				<input name="price_unit[]" id="price_unit" type="text" class="medium" value="" placeholder="<?php _e('e.g. $', 'wrcpt'); ?>" /><hr />
							<h4><?php _e('Package Features', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter feature values and feature ToolTips here. To show the tick icon mark the checkbox and to show cross icon unmark the checkbox. To display ToolTips, you should enable ToolTips in General settings under Pricing Table Settings.', 'wrcpt'); ?>"></a></h4><?php
			  				if($package_feature) {
			  					for($i = 1; $i <= $featureNum; $i++) {
									if($package_feature['ftype'.$i] == 'text') { ?>
									<label class="input-title"><?php echo $package_feature['fitem'.$i]; ?></label>
									<input type="text" class="medium" name="feature_value[]" id="feature_value" value="" placeholder="<?php _e('Feature Value', 'wrcpt'); ?>" />
									<textarea name="tooltips[]" id="tooltips" class="medium" cols="27" rows="2" placeholder="<?php _e('Enter Tooltip', 'wrcpt'); ?>"></textarea><hr /><?php
									} else { ?>
									<label class="input-check"><?php echo $package_feature['fitem'.$i]; ?></label>
									<input type="checkbox" name="feature_value[]" class="tickbox" id="feature_value" value="<?php echo $checkValue; ?>" /> 
									<textarea name="tooltips[]" id="tooltips" class="medium" cols="27" rows="2" placeholder="<?php _e('Enter Tooltip', 'wrcpt'); ?>"></textarea><hr /><?php
									}
			  					}
			  				} else { echo '<label class="input-title">' . __('Please add some features to get feature list.', 'wrcpt') . '</label>'; } ?>
							<h4><?php _e('Package Button', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter your call to action text and call to action URL here. The URL is usually either a payment link or a page where users can create an account.', 'wrcpt'); ?>"></a></h4>
							<div id="normal_button">
								<label class="input-title"><?php _e('Button Text', 'wrcpt'); ?></label>
								<input type="text" name="button_text[]" class="medium" id="button_text" value="" placeholder="<?php _e('e.g. Buy Now!', 'wrcpt'); ?>" />
								<label class="input-title"><?php _e('Button Link', 'wrcpt'); ?></label>
								<input type="text" name="button_link[]" class="medium" id="button_link" value="" placeholder="<?php _e('e.g. http://example.com', 'wrcpt'); ?>" />
							</div>
							<hr />
							<h4><?php _e('Package Ribbon', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter ribbon text to make current package more attractive to users, like \'best\', \'new\', \'hot\' etc.', 'wrcpt'); ?>"></a></h4>
			  				<label class="input-title"><?php _e('Ribbon Text', 'wrcpt'); ?></label>
			  				<input type="text" name="ribbon_text[]" class="medium" id="ribbon_text" value="" placeholder="<?php _e('e.g. Best', 'wrcpt'); ?>" />
			  			</div>
			  			<h3 class="ptitle"><?php _e('Pricing Column Colors', 'wrcpt'); ?></h3>
			  			<div class="element-input">
							<table>
								<!--Background Color -->
								<tr class="table-header">
									<td colspan="2"><?php _e('Background Colors', 'wrcpt'); ?></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Table Background Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="title_bg[]" class="title_bg1" id="title_bg" value="#44A3D5" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Ribbon Background Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="ribbon_bg[]" class="ribbon_bg1" id="ribbon_bg" value="#CB0000" /></td>
								</tr>
								<!--Font Color -->
								<tr class="table-header">
									<td colspan="2"><?php _e('Font Colors', 'wrcpt'); ?></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Title Font Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="title_color[]" class="title_color1" id="title_color" value="#FFFFFF" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Price Font Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="price_color_big[]" class="price_color_big1" id="price_color_big" value="#FFFFFF" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Ribbon Font Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="ribbon_text_color[]" class="ribbon_text_color1" id="ribbon_text_color" value="#333333" /></td>
								</tr>
								<!--Button Color -->
								<tr class="table-header">
									<td colspan="2"><?php _e('Button Colors', 'wrcpt'); ?></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Button Font Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="button_text_color[]" class="button_text_color1" id="button_text_color" value="#FFFFFF" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Button Font Hover Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="button_text_hover[]" class="button_text_hover1" id="button_text_hover" value="#FFFFFF" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Button Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="button_color[]" class="button_color1" id="button_color" value="#333333" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Button Hover Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="button_hover[]" class="button_hover1" id="button_hover" value="#333333" /></td>
								</tr>
							</table>
						</div>
			  			<input type="hidden" name="pricing_packages[]" value="" />
			  		</div>	<!-- End of column_container -->
				</div>	<!-- End of package_details -->
			</div>	<!--sortable_column -->
		</div>	<!--tablecolumnwrap -->
	</div>	<!--tablecolumndiv -->
	<div class="wrcpt-clear"></div>
	<div id="settingcolumndiv">
		<div class="settingcolumnwrap">
			<h3><?php _e('Pricing Table Settings', 'wrcpt'); ?></h3>
			<div id="accordion_advance" class="package_advance">
				<h3 class="ptitle"><?php _e('General Settings', 'wrcpt'); ?></h3>
				<div class="advance-input">
					<label class="input-check"><?php _e('Enable Pricing Table', 'wrcpt'); ?>:
					<input type="checkbox" name="wrcpt_option" class="tickbox" id="wrcpt_option" value="yes" /></label>
					<label id="modify-table" class="input-title"><?php _e('Modify Pricing Table Name', 'wrcpt'); ?></label>
					<input type="text" name="pricing_table_name" class="medium" id="pricing_table_name" value="<?php echo $pricing_table_name; ?>" />
					<div class="break-line"></div>
					<label class="input-check"><?php _e('Enable Column Shadow', 'wrcpt'); ?>:
					<input type="checkbox" name="column_shadow" class="tickbox" id="column_shadow" value="yes" /></label>
					<label class="input-check"><?php _e('Disable Shadow on Highlight', 'wrcpt'); ?>:
					<input type="checkbox" name="disable_shadow" class="tickbox" id="disable_shadow" value="yes" /></label>
					<label class="input-check"><?php _e('Align Price Unit at Right', 'wrcpt'); ?>:
					<input type="checkbox" name="unit_right" class="tickbox" id="unit_right" value="yes" /></label>
					<label class="input-check"><?php _e('Open Link in New Tab', 'wrcpt'); ?>:
					<input type="checkbox" name="new_tab" class="tickbox" id="new_tab" value="yes" /></label>
					<label class="input-check"><?php _e('Enable Feature Tooltips', 'wrcpt'); ?>:
					<input type="checkbox" name="enable_tooltip" class="tickbox" id="enable_tooltip" value="yes" /></label>
					<div class="break-line"></div>
					<label class="input-check"><?php _e('Enable Package Ribbons', 'wrcpt'); ?>:
					<input type="checkbox" name="enable_ribbon" class="tickbox" id="enable_ribbon" value="yes" /></label>
				</div>
				<h3 class="ptitle"><?php _e('Structural Settings', 'wrcpt'); ?></h3>
				<div class="advance-input">
					<label class="input-title"><?php _e('Pricing Table Container Width', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter the total width of your pricing table here.', 'wrcpt'); ?>"></a></label>
					<input type="text" name="container_width" class="medium" id="container_width" value="" placeholder="e.g. 100%" />
					<label class="input-title"><?php _e('Number of Columns per Row', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('If your pricing table has a lot of columns, then you can split the columns according to the rows by entering the number of columns in the right TextBox.', 'wrcpt'); ?>"></a></label>
					<input type="number" name="max_column" class="medium" id="max_column" value="" min="0" max="12" placeholder="e.g. 6" />
					<label class="input-title"><?php _e('Title Body Height', 'wrcpt'); ?></label>
					<input type="text" name="title_body" class="medium" id="title_body" value="" placeholder="e.g. 48px" />
					<label class="input-title"><?php _e('Price Body Height', 'wrcpt'); ?></label>
					<input type="text" name="price_body" class="medium" id="price_body" value="" placeholder="e.g. 120px" />
					<label class="input-title"><?php _e('Feature Label Body Height', 'wrcpt'); ?></label>
					<input type="text" name="feature_body" class="medium" id="feature_body" value="" placeholder="e.g. 42px" />
					<label class="input-title"><?php _e('Button Body Height', 'wrcpt'); ?></label>
					<input type="text" name="button_body" class="medium" id="button_body" value="" placeholder="e.g. 40px" />
					<label class="input-title"><?php _e('Button Width', 'wrcpt'); ?></label>
					<input type="text" name="button_width" class="medium" id="button_width" value="" placeholder="e.g. 140px" />
					<label class="input-title"><?php _e('Button Height', 'wrcpt'); ?></label>
					<input type="text" name="button_height" class="medium" id="button_height" value="" placeholder="e.g. 30px" />
					<label class="input-title"><?php _e('Tooltip Width', 'wrcpt'); ?></label>
					<input type="text" name="tooltip_width" class="medium" id="tooltip_width" value="" placeholder="e.g. 130px" /><hr />
					<label class="input-title"><?php _e('Feature Label Font Direction', 'wrcpt'); ?>:</label>
					<select name="feature_align" id="feature_align" class="font-dir">
						<option value="left" selected="selected"><?php _e('Left', 'wrcpt'); ?></option>
						<option value="right"><?php _e('Right', 'wrcpt'); ?></option>
						<option value="center"><?php _e('Center', 'wrcpt'); ?></option>
					</select>
					<hr />
					<label class="input-check"><?php _e('Disable Auto Column Width', 'wrcpt'); ?>:<a href="#" class="wrc_tooltip" rel="<?php _e('If you want to setup caption column width manually simply mark the check box. It will allow you to enter the caption column width and the space between the columns.', 'wrcpt'); ?>"></a>
					<input type="checkbox" name="auto_column" class="tickbox" id="auto_column" value="yes" /></label>
					<label class="input-title" id="margin_right"><?php _e('Space Between Columns', 'wrcpt'); ?></label>
					<input type="number" step="any" name="column_space" class="medium" id="column_space" value="" placeholder="e.g. 1" />
					<label class="input-check"><?php _e('Enable Caption Column', 'wrcpt'); ?>:<a href="#" class="wrc_tooltip" rel="<?php _e('If you would like to display features name column separately on the left of the pricing table simply mark this checkbox.', 'wrcpt'); ?>"></a>
					<input type="checkbox" name="feature_caption" class="tickbox" id="feature_caption" value="yes" /></label>
					<label class="input-title" id="cap_col_width"><?php _e('Caption Column Width', 'wrcpt'); ?></label>
					<input type="number" step="any" name="cap_column_width" class="medium" id="cap_column_width" value="" placeholder="e.g. 18 or 18.73" />
					<label class="input-check"><?php _e('Enlarge Column on Hover', 'wrcpt'); ?>:
					<input type="checkbox" name="enlarge_column" class="tickbox" id="enlarge_column" value="yes" /></label>
				</div>
				<h3 class="ptitle"><?php _e('Font Settings', 'wrcpt'); ?></h3>
				<div class="advance-input">
					<label class="input-title"><?php _e('Pricing Table Name Font Size', 'wrcpt'); ?></label>
					<input type="text" name="caption_size" class="medium" id="caption_size" value="" placeholder="e.g. 36px" />
					<label class="input-title"><?php _e('Title Font Size', 'wrcpt'); ?></label>
					<input type="text" name="title_size" class="medium" id="title_size" value="" placeholder="e.g. 24px" />
					<label class="input-title"><?php _e('Price Font Size (Big)', 'wrcpt'); ?></label>
					<input type="text" name="price_size_big" class="medium" id="price_size_big" value="" placeholder="e.g. 60px" />
					<label class="input-title"><?php _e('Price Font Size (Small)', 'wrcpt'); ?></label>
					<input type="text" name="price_size_small" class="medium" id="price_size_small" value="" placeholder="e.g. 24px" />
					<label class="input-title"><?php _e('Feature Name Font Size', 'wrcpt'); ?></label>
					<input type="text" name="cap_feat_size" class="medium" id="cap_feat_size" value="" placeholder="e.g. 12px" />
					<label class="input-title"><?php _e('Feature Values Font Size', 'wrcpt'); ?></label>
					<input type="text" name="feature_text_size" class="medium" id="feature_text_size" value="" placeholder="e.g. 12px" />
					<label class="input-title"><?php _e('Button Text Font Size', 'wrcpt'); ?></label>
					<input type="text" name="button_text_size" class="medium" id="button_text_size" value="" placeholder="e.g. 12px" />
					<label class="input-title"><?php _e('Ribbon Text Font Size', 'wrcpt'); ?></label>
					<input type="text" name="ribbon_text_size" class="medium" id="ribbon_text_size" value="" placeholder="e.g. 12px" />
				</div>
				<h3 class="ptitle"><?php _e('Advanced Color Settings', 'wrcpt'); ?></h3>
				<div class="advance-input">
					<table>
						<!--Shadow Color -->
						<tr class="table-header">
							<td colspan="2"><?php _e('Column Shadow Colors', 'wrcpt'); ?></td>
						</tr>
						<tr class="table-input">
							<th><label class="input-title"><?php _e('Column Shadow Color', 'wrcpt'); ?></label></th>
							<td><input type="text" name="col_shad_color" class="col_shad_color" id="col_shad_color" value="#ccc" /></td>
						</tr>
						<tr class="table-input">
							<th><label class="input-title"><?php _e('Column Shadow Highlight Color', 'wrcpt'); ?></label></th>
							<td><input type="text" name="col_shad_hover_color" class="col_shad_hover_color" id="col_shad_hover_color" value="#333" /></td>
						</tr>
					</table>
				</div>
			</div>	<!--package_advance -->
		</div>	<!--settingcolumnwrap -->
	</div>	<!--settingcolumndiv -->
	<div class="wrcpt-clear"></div>
	<input type="hidden" name="pricing_table" value="<?php echo $pricing_table; ?>" />
	<input type="hidden" name="checkbox_value" value="<?php echo $checkValue; ?>" />
	<input type="submit" id="wrcpt_add" name="wrcpt_add" class="button-primary" value="<?php _e('Add Package', 'wrcpt'); ?>" />
<?php
	die;
}
add_action( 'wp_ajax_nopriv_wrcpt_add_pricing_packages', 'wrcpt_add_pricing_packages' );
add_action( 'wp_ajax_wrcpt_add_pricing_packages', 'wrcpt_add_pricing_packages' );

if(isset($_POST['wrcpt_edit_process']) && $_POST['wrcpt_edit_process'] == "editprocess") {
	if( isset( $_POST['wrcpt_add'] ) ) { wrcpt_save_pricing_packages(); }
}
?>