<?php
/*
 * WRC Pricing Tables 2.3
 * @realwebcare - https://www.realwebcare.com/
 * Editing Pricing Table Packages
 */
function wrcpt_edit_pricing_packages() {
	$fv = 1; $ac = 1; $cf = 1;
	$f_value = '';
	$f_tips = '';
	$pricing_table = $_POST['packtable'];
	$pricing_table_name = ucwords(str_replace('_', ' ', $pricing_table));
	$package_feature = get_option($pricing_table.'_feature');
	$packageCombine = get_option($pricing_table.'_option');
	$package_lists = get_option($pricing_table);
	$packageOptions = explode(', ', $package_lists);
	$packageItem = get_option($packageOptions[0]);
	$featureNum = count($package_feature)/2;
	$template = isset($packageCombine['templ']) ? $packageCombine['templ'] : 'temp0';
	$checkValue = uniqid('yes'); ?>
	<input type="hidden" name="wrcpt_process" value="process" />
	<div id="tablecolumndiv">
		<div class="tablecolumnwrap">
			<h3><?php _e('Pricing Table Columns', 'wrcpt'); ?></h3>
			<div id="addButtons"><a href="#" class="button button-large" id="addPackage"><?php _e('New Column', 'wrcpt'); ?></a></div>
			<div class="accordion-expand-holder">
				<button type="button" class="expand"><?php _e('Expand all', 'wrcpt'); ?></button>
				<button type="button" class="collapse"><?php _e('Collapse all', 'wrcpt'); ?></button>
			</div>
			<div id="sortable_column">
			<?php
			if(!empty($package_lists)) {
				foreach($packageOptions as $option => $value) {
				$packageValue = get_option($value);
				if(isset($packageValue['pdisp'])) { $showpack = $packageValue['pdisp']; }
				else { $showpack = 'show'; } ?>
				<div id="wrcpt-<?php echo $ac; ?>" class="package_details">
					<h4 id="pcolumn<?php echo $ac; ?>">Pricing Column <?php echo $ac; ?></h4>
					<?php if($showpack == 'show') { ?><span id="hidePack<?php echo $ac; ?>" class="column_hide"><i class="dashicons dashicons-visibility"></i><?php } else { ?><span id="showPack<?php echo $ac; ?>" class="column_show"><i class="dashicons dashicons-hidden"></i><?php } ?><input type="hidden" name="hide_show[]" value="<?php echo $showpack; ?>" /></span>
					<span id="delPackage"></span>
					<div id="accordion<?php echo $ac; ?>" class="column_container">
						<h3 class="ptitle"><?php _e('Pricing Column Details', 'wrcpt'); ?></h3>
						<div class="element-input">
							<label class="input-check"><?php _e('Enlarge Column?', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('If you want to highlight the current column from some of the other columns, simply mark the checkbox.', 'wrcpt'); ?>"></a></label>
							<input type="checkbox" class="tickbox" name="special_package[<?php echo $ac-1; ?>]" id="special_package" value="yes"<?php if($packageValue['spack'] == 'yes') {?> checked="checked"<?php } ?> /><hr />
							<h4><?php _e('Package Name', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter your pricing package name here. You can also enter a short description of the package name. There are many users who choose a package based on the name, instead of features. So, a short description might help users to select the appropriate package.', 'wrcpt'); ?>"></a></h4>
							<label class="input-title"><?php _e('Package Name', 'wrcpt'); ?></label>
							<input type="text" name="package_type[]" class="medium" id="package_type" value="<?php echo $packageValue['type']; ?>" /><hr />
							<h4><?php _e('Package Pricing', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter the price of the package, price currency, price plan (monthly or yearly) and a short description about pricing.', 'wrcpt'); ?>"></a></h4>
							<label class="input-title"><?php _e('Package Price', 'wrcpt'); ?></label>
							<input name="price_number[]" type="text" class="col_price" value="<?php echo $packageValue['price']; ?>" />&nbsp;.&nbsp;<input name="price_fraction[]" type="number" class="col_price" value="<?php echo $packageValue['cent']; ?>" min="0" max="99" placeholder="00" />
							<label class="input-title"><?php _e('Price Plan', 'wrcpt'); ?></label>
							<input name="package_plan[]" id="package_plan" type="text" class="medium" value="<?php echo $packageValue['plan']; ?>" />
							<label class="input-title"><?php _e('Price Unit', 'wrcpt'); ?></label>
							<input name="price_unit[]" id="price_unit" type="text" class="medium" value="<?php echo $packageValue['unit']; ?>" /><hr />
							<h4><?php _e('Package Features', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter feature values and feature ToolTips here. To show the tick icon mark the checkbox and to show cross icon unmark the checkbox. To display ToolTips, you should enable ToolTips in General settings under Pricing Table Settings.', 'wrcpt'); ?>"></a></h4><?php
							if($package_feature) {
								for($i = 1; $i <= $featureNum; $i++) {
									if(isset($packageValue['fitem'.$fv])) {
										$f_value = $packageValue['fitem'.$fv];
										$f_tips = $packageValue['tip'.$fv];
									}
									if($package_feature['ftype'.$i] == 'text') { ?>
										<label class="input-title"><?php echo $package_feature['fitem'.$i]; ?></label><input type="text" class="medium" name="feature_value[]" id="feature_value" value="<?php echo $f_value; ?>" placeholder="<?php _e('Feature Value', 'wrcpt'); ?>" /><textarea name="tooltips[]" id="tooltips" class="medium" cols="27" rows="2" placeholder="<?php _e('Enter Tooltip', 'wrcpt'); ?>"><?php echo $f_tips; ?></textarea><hr /><?php
									} else { ?>
										<label class="input-check"><?php echo $package_feature['fitem'.$i]; ?></label><input type="checkbox" class="tickbox" name="feature_value[<?php echo 'ftype'.$cf; ?>]" id="feature_value" value="<?php echo $checkValue; ?>"<?php if($f_value == 'tick' || $f_value == 'yes') {?> checked="checked"<?php } ?> /><textarea name="tooltips[]" id="tooltips" class="medium" cols="27" rows="2" placeholder="<?php _e('Enter Tooltip', 'wrcpt'); ?>"><?php echo $f_tips; ?></textarea><hr /><?php
										$cf++;
									} $fv++;
								}
							} else { echo '<label class="input-title">' . __('There are no feature items!', 'wrcpt') . '</label>'; } ?>
							<h4><?php _e('Package Button', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter your call to action text and call to action URL here. The URL is usually either a payment link or a page where users can create an account.', 'wrcpt'); ?>"></a></h4>
							<div id="normal_button">
								<label class="input-title"><?php _e('Button Text', 'wrcpt'); ?></label>
								<input type="text" name="button_text[]" class="medium" id="button_text" value="<?php echo $packageValue['btext']; ?>" placeholder="<?php _e('e.g. Buy Now!', 'wrcpt'); ?>" />
								<label class="input-title"><?php _e('Button Link', 'wrcpt'); ?></label>
								<input type="text" name="button_link[]" class="medium" id="button_link" value="<?php echo $packageValue['blink']; ?>" placeholder="<?php _e('e.g. http://example.com', 'wrcpt'); ?>" />
							</div>
							<hr />
							<h4><?php _e('Package Ribbon', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter ribbon text to make current package more attractive to users, like \'best\', \'new\', \'hot\' etc.', 'wrcpt'); ?>"></a></h4>
							<label class="input-title"><?php _e('Ribbon Text', 'wrcpt'); ?></label>
							<input type="text" name="ribbon_text[]" class="medium" id="ribbon_text" value="<?php echo $packageValue['rtext']; ?>" placeholder="<?php _e('e.g. Best', 'wrcpt'); ?>" />
						</div>
						<h3 class="ptitle"><?php _e('Pricing Column Colors', 'wrcpt'); ?></h3>
						<div class="element-input">
							<table>
								<!--Background Color -->
								<tr class="table-header">
									<td colspan="2"><?php _e('Background Colors', 'wrcpt'); ?></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Table Background Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="title_bg[]" class="title_bg" id="title_bg" value="<?php echo $packageValue['tbcolor']; ?>" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Ribbon Background Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="ribbon_bg[]" class="ribbon_bg" id="ribbon_bg" value="<?php echo $packageValue['rbcolor']; ?>" /></td>
								</tr>
								<!--Font Color -->
								<tr class="table-header">
									<td colspan="2"><?php _e('Font Colors', 'wrcpt'); ?></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Title Font Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="title_color[]" class="title_color" id="title_color" value="<?php echo $packageValue['tcolor']; ?>" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Price Font Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="price_color_big[]" class="price_color_big" id="price_color_big" value="<?php echo $packageValue['pcbig']; ?>" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Ribbon Font Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="ribbon_text_color[]" class="ribbon_text_color" id="ribbon_text_color" value="<?php echo $packageValue['rtcolor']; ?>" /></td>
								</tr>
								<!--Button Color -->
								<tr class="table-header">
									<td colspan="2"><?php _e('Button Colors', 'wrcpt'); ?></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Button Font Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="button_text_color[]" class="button_text_color" id="button_text_color" value="<?php echo $packageValue['btcolor']; ?>" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Button Font Hover Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="button_text_hover[]" class="button_text_hover" id="button_text_hover" value="<?php echo $packageValue['bthover']; ?>" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Button Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="button_color[]" class="button_color" id="button_color" value="<?php echo $packageValue['bcolor']; ?>" /></td>
								</tr>
								<tr class="table-input">
									<th><label class="input-title"><?php _e('Button Hover Color', 'wrcpt'); ?></label></th>
									<td><input type="text" name="button_hover[]" class="button_hover" id="button_hover" value="<?php echo $packageValue['bhover']; ?>" /></td>
								</tr>
							</table>
						</div>
						<input type="hidden" name="pricing_packages[]" value="<?php echo $value; ?>" />
						<input type="hidden" name="package_id[]" value="<?php echo $packageValue['pid']; ?>" />
						<input type="hidden" name="order_id[]" value="<?php echo $packageValue['order']; ?>" />
					</div>	<!-- End of column_container -->
				</div>	<!-- End of package_details -->
			<?php
					$fv = 1; $ac++;
				}
			} else _e('No Packages yet!', 'wrcpt'); ?>
			</div>	<!--sortable_column -->
		</div>	<!--tablecolumnwrap -->
	</div>	<!--tablecolumndiv -->
	<div class="wrcpt-clear"></div>
	<div id="settingcolumndiv">
		<div class="settingcolumnwrap">
			<h3><?php _e('Pricing Table Settings', 'wrcpt'); ?></h3>
			<div id="accordion_advance" class="package_advance">
				<h3 class="ptitle"><?php _e('General Settings', 'wrcpt'); ?></h3>
				<div class="advance-input">
					<label class="input-check"><?php _e('Enable Pricing Table', 'wrcpt'); ?>:
					<input type="checkbox" name="wrcpt_option" class="tickbox" id="wrcpt_option" value="yes" <?php if($packageCombine['enable'] == 'yes') { ?> checked="checked"  <?php } ?> /></label>
					<label id="modify-table" class="input-title"><?php _e('Modify Pricing Table Name', 'wrcpt'); ?></label>
					<input type="text" name="pricing_table_name" class="medium" id="pricing_table_name" value="<?php echo $pricing_table_name; ?>" />
					<div class="break-line"></div>
					<label class="input-check"><?php _e('Enable Column Shadow', 'wrcpt'); ?>:
					<input type="checkbox" name="column_shadow" class="tickbox" id="column_shadow" value="yes" <?php if($packageCombine['colshad'] == 'yes') { ?> checked="checked" <?php } ?> /></label>
					<label class="input-check"><?php _e('Disable Shadow on Highlight', 'wrcpt'); ?>:
					<input type="checkbox" name="disable_shadow" class="tickbox" id="disable_shadow" value="yes" <?php if($packageCombine['dscol'] == 'yes') { ?> checked="checked" <?php } ?> /></label>
					<label class="input-check"><?php _e('Align Price Unit at Right', 'wrcpt'); ?>:
					<input type="checkbox" name="unit_right" class="tickbox" id="unit_right" value="yes" <?php if($packageCombine['purgt'] == 'yes') { ?> checked="checked" <?php } ?> /></label>
					<label class="input-check"><?php _e('Open Link in New Tab', 'wrcpt'); ?>:
					<input type="checkbox" name="new_tab" class="tickbox" id="new_tab" value="yes" <?php if($packageCombine['nltab'] == 'yes') { ?> checked="checked"  <?php } ?> /></label>
					<label class="input-check"><?php _e('Enable Feature Tooltips', 'wrcpt'); ?>:
					<input type="checkbox" name="enable_tooltip" class="tickbox" id="enable_tooltip" value="yes" <?php if($packageCombine['entips'] == 'yes') { ?> checked="checked"  <?php } ?> /></label>
					<div class="break-line"></div>
					<label class="input-check"><?php _e('Enable Package Ribbons', 'wrcpt'); ?>:
					<input type="checkbox" name="enable_ribbon" class="tickbox" id="enable_ribbon" value="yes" <?php if($packageCombine['enribs'] == 'yes') { ?> checked="checked" <?php } ?> /></label>
				</div>
				<h3 class="ptitle"><?php _e('Structural Settings', 'wrcpt'); ?></h3>
				<div class="advance-input">
					<label class="input-title"><?php _e('Pricing Table Container Width', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('Enter the total width of your pricing table here.', 'wrcpt'); ?>"></a></label>
					<input type="text" name="container_width" class="medium" id="container_width" value="<?php echo $packageCombine['cwidth']; ?>" placeholder="e.g. 100%" />
					<label class="input-title"><?php _e('Number of Columns per Row', 'wrcpt'); ?><a href="#" class="wrc_tooltip" rel="<?php _e('If your pricing table has a lot of columns, then you can split the columns according to the rows by entering the number of columns in the right TextBox.', 'wrcpt'); ?>"></a></label>
					<input type="number" name="max_column" class="medium" id="max_column" value="<?php echo $packageCombine['maxcol']; ?>" min="0" max="12" placeholder="e.g. 6" />
					<label class="input-title"><?php _e('Title Body Height', 'wrcpt'); ?></label>
					<input type="text" name="title_body" class="medium" id="title_body" value="<?php echo $packageCombine['tbody']; ?>" placeholder="e.g. 48px" />
					<label class="input-title"><?php _e('Price Body Height', 'wrcpt'); ?></label>
					<input type="text" name="price_body" class="medium" id="price_body" value="<?php echo $packageCombine['pbody']; ?>" placeholder="e.g. 120px" />
					<label class="input-title"><?php _e('Feature Label Body Height', 'wrcpt'); ?></label>
					<input type="text" name="feature_body" class="medium" id="feature_body" value="<?php echo $packageCombine['ftbody']; ?>" placeholder="e.g. 42px" />
					<label class="input-title"><?php _e('Button Body Height', 'wrcpt'); ?></label>
					<input type="text" name="button_body" class="medium" id="button_body" value="<?php echo $packageCombine['btbody']; ?>" placeholder="e.g. 40px" />
					<label class="input-title"><?php _e('Button Width', 'wrcpt'); ?></label>
					<input type="text" name="button_width" class="medium" id="button_width" value="<?php echo $packageCombine['bwidth']; ?>" placeholder="e.g. 140px" />
					<label class="input-title"><?php _e('Button Height', 'wrcpt'); ?></label>
					<input type="text" name="button_height" class="medium" id="button_height" value="<?php echo $packageCombine['bheight']; ?>" placeholder="e.g. 30px" />
					<label class="input-title"><?php _e('Tooltip Width', 'wrcpt'); ?></label>
					<input type="text" name="tooltip_width" class="medium" id="tooltip_width" value="<?php echo $packageCombine['ttwidth']; ?>" placeholder="e.g. 130px" /><hr />
					<label class="input-title"><?php _e('Feature Label Font Direction', 'wrcpt'); ?>:</label>
					<select name="feature_align" id="feature_align" class="font-dir">
						<?php if($packageCombine['ftdir'] == 'left') { ?>
						<option value="left" selected="selected"><?php _e('Left', 'wrcpt'); ?></option>
						<option value="right"><?php _e('Right', 'wrcpt'); ?></option>
						<option value="center"><?php _e('Center', 'wrcpt'); ?></option>
						<?php } elseif($packageCombine['ftdir'] == 'right') { ?>
						<option value="left"><?php _e('Left', 'wrcpt'); ?></option>
						<option value="right" selected="selected"><?php _e('Right', 'wrcpt'); ?></option>
						<option value="center"><?php _e('Center', 'wrcpt'); ?></option>
						<?php } else { ?>
						<option value="left"><?php _e('Left', 'wrcpt'); ?></option>
						<option value="right"><?php _e('Right', 'wrcpt'); ?></option>
						<option value="center" selected="selected"><?php _e('Center', 'wrcpt'); ?></option>
						<?php } ?>
					</select>
					<hr />
					<label class="input-check"><?php _e('Disable Auto Column Width', 'wrcpt'); ?>:<a href="#" class="wrc_tooltip" rel="<?php _e('If you want to setup caption column width manually simply mark the check box. It will allow you to enter the caption column width and the space between the columns.', 'wrcpt'); ?>"></a>
					<input type="checkbox" name="auto_column" class="tickbox" id="auto_column" value="yes" <?php if($packageCombine['autocol'] == 'yes') { ?> checked="checked"  <?php } ?> /></label>
					<label class="input-title" id="margin_right"><?php _e('Space Between Columns', 'wrcpt'); ?></label>
					<input type="number" step="any" name="column_space" class="medium" id="column_space" value="<?php echo $packageCombine['colgap']; ?>" placeholder="e.g. 1" />
					<label class="input-check"><?php _e('Enable Caption Column', 'wrcpt'); ?>:<a href="#" class="wrc_tooltip" rel="<?php _e('If you would like to display features name column separately on the left of the pricing table simply mark this checkbox.', 'wrcpt'); ?>"></a>
					<input type="checkbox" name="feature_caption" class="tickbox" id="feature_caption" value="yes" <?php if($packageCombine['ftcap'] == 'yes') { ?> checked="checked"  <?php } ?> /></label>
					<label class="input-title" id="cap_col_width"><?php _e('Caption Column Width', 'wrcpt'); ?></label>
					<input type="number" step="any" name="cap_column_width" class="medium" id="cap_column_width" value="<?php echo $packageCombine['capwidth']; ?>" placeholder="e.g. 18 or 18.73" />
					<label class="input-check"><?php _e('Enlarge Column on Hover', 'wrcpt'); ?>:
					<input type="checkbox" name="enlarge_column" class="tickbox" id="enlarge_column" value="yes" <?php if($packageCombine['encol'] == 'yes') { ?> checked="checked"  <?php } ?> /></label>
				</div>
				<h3 class="ptitle"><?php _e('Font Settings', 'wrcpt'); ?></h3>
				<div class="advance-input">
					<label class="input-title"><?php _e('Pricing Table Name Font Size', 'wrcpt'); ?></label>
					<input type="text" name="caption_size" class="medium" id="caption_size" value="<?php echo $packageCombine['ctsize']; ?>" placeholder="e.g. 36px" />
					<label class="input-title"><?php _e('Title Font Size', 'wrcpt'); ?></label>
					<input type="text" name="title_size" class="medium" id="title_size" value="<?php echo $packageCombine['tsize']; ?>" placeholder="e.g. 24px" />
					<label class="input-title"><?php _e('Price Font Size (Big)', 'wrcpt'); ?></label>
					<input type="text" name="price_size_big" class="medium" id="price_size_big" value="<?php echo $packageCombine['psbig']; ?>" placeholder="e.g. 60px" />
					<label class="input-title"><?php _e('Price Font Size (Small)', 'wrcpt'); ?></label>
					<input type="text" name="price_size_small" class="medium" id="price_size_small" value="<?php echo $packageCombine['pssmall']; ?>" placeholder="e.g. 24px" />
					<label class="input-title"><?php _e('Feature Name Font Size', 'wrcpt'); ?></label>
					<input type="text" name="cap_feat_size" class="medium" id="cap_feat_size" value="<?php echo $packageCombine['cftsize']; ?>" placeholder="e.g. 12px" />
					<label class="input-title"><?php _e('Feature Values Font Size', 'wrcpt'); ?></label>
					<input type="text" name="feature_text_size" class="medium" id="feature_text_size" value="<?php echo $packageCombine['ftsize']; ?>" placeholder="e.g. 12px" />
					<label class="input-title"><?php _e('Button Text Font Size', 'wrcpt'); ?></label>
					<input type="text" name="button_text_size" class="medium" id="button_text_size" value="<?php echo $packageCombine['btsize']; ?>" placeholder="e.g. 12px" />
					<label class="input-title"><?php _e('Ribbon Text Font Size', 'wrcpt'); ?></label>
					<input type="text" name="ribbon_text_size" class="medium" id="ribbon_text_size" value="<?php echo $packageCombine['rtsize']; ?>" placeholder="e.g. 12px" />
				</div>
				<h3 class="ptitle"><?php _e('Advanced Color Settings', 'wrcpt'); ?></h3>
				<div class="advance-input">
					<table>
						<!--Shadow Color -->
						<tr class="table-header">
							<td colspan="2"><?php _e('Column Shadow Colors', 'wrcpt'); ?></td>
						</tr>
						<tr class="table-input">
							<th><label class="input-title"><?php _e('Column Shadow Color', 'wrcpt'); ?></label></th>
							<td><input type="text" name="col_shad_color" class="col_shad_color" id="col_shad_color" value="<?php echo $packageCombine['cscolor']; ?>" /></td>
						</tr>
						<tr class="table-input">
							<th><label class="input-title"><?php _e('Column Shadow Highlight Color', 'wrcpt'); ?></label></th>
							<td><input type="text" name="col_shad_hover_color" class="col_shad_hover_color" id="col_shad_hover_color" value="<?php echo $packageCombine['cshcolor']; ?>" /></td>
						</tr>
					</table>
				</div>
			</div>	<!--package_advance -->
		</div>	<!--settingcolumnwrap -->
	</div>	<!--settingcolumndiv -->
	<div class="wrcpt-clear"></div>
	<input type="hidden" name="pricing_table" value="<?php echo $pricing_table; ?>" />
	<input type="hidden" name="checkbox_value" value="<?php echo $checkValue; ?>" />
	<input type="hidden" name="template" value="<?php echo $template; ?>" />
	<input type="submit" id="wrcpt_edit" name="wrcpt_edit" class="button-primary" value="<?php _e('Update Package', 'wrcpt'); ?>" />
<?php
	die;
}
add_action( 'wp_ajax_nopriv_wrcpt_edit_pricing_packages', 'wrcpt_edit_pricing_packages' );
add_action( 'wp_ajax_wrcpt_edit_pricing_packages', 'wrcpt_edit_pricing_packages' );

if(isset($_POST['wrcpt_process']) && $_POST['wrcpt_process'] == "process") {
	if( isset( $_POST['wrcpt_edit'] ) ) { wrcpt_update_pricing_package(); }
}
?>