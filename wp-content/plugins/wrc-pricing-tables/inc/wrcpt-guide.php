<?php
/*
 * WRC Pricing Tables 2.3
 * @realwebcare - https://www.realwebcare.com/
 * Plugin Guideline
 */
 ?>
<div class="wrap">
	<div class="postbox-container" style="width:75%;">
	<h2 class="main-header"><?php _e('Pricing Table Guide', 'wrcpt'); ?></h2>
		<div class="wrcusage-maincontent">
			<hr>
			<div id="poststuff">
				<div class="postbox">
					<h3><?php _e('About the plugin', 'wrcpt'); ?></h3>
					<div class="inside">
						<p><?php _e('<strong>WRC Pricing Table</strong> is a free plugin developed to display Pricing Tables in a lot more professional way. The plugin is designed with clean CSS3, and no JavaScript is used. What is more, it is responsive as well, aimed to deliver an optimum viewing experience across a wide range of devices and in all major browsers. Undoubtedly, I believe this plugin will work as a milestone for your business to represent various packages with relevant features.', 'wrcpt'); ?></p>
					</div>
				</div>
			</div><!-- End poststuff -->
			<div id="poststuff">
				<div class="postbox">
					<h3><?php _e('How does it work (Configuration)?', 'wrcpt'); ?></h3>
					<div class="inside">
						<p><?php _e('After activating the plugin, follow the guidelines presented below to create pricing tables', 'wrcpt'); ?>:</p>
						<ol>
							<li><?php _e('Navigate to <strong>Pricing Tables >> All Pricing Tables</strong> and click on <strong>Add New</strong> button to add new pricing table.', 'wrcpt'); ?></li>
							<li><?php _e('Enter the pricing table name as well as corresponding pricing column features. Set the type of each feature. Finally, click on <strong>Add Table</strong> button.', 'wrcpt'); ?></li>
							<li><?php _e('Later, you can <strong>Edit</strong>, <strong>Delete</strong> and <strong>Reorder</strong> pricing column features at any time by clicking on the <strong>Edit Features</strong> link in the pricing table lists. After naming the pricing table you will be able to add pricing columns. To add pricing column click on <strong>Add Columns</strong> link.', 'wrcpt'); ?></li>
							<li><?php _e('Now, you will be able to add pricing cloumns as many as you want. There are two sections of each column', 'wrcpt'); ?>:<br><br>
                                <ul>
                                    <li><?php _e('<b>Pricing Column Details: </b>Here you have to give the details of pricing package, like package name, price, features, button text and link, and ribbon text. To highlight the current column than others, mark the <strong>Enlarge Column</strong> option. Each feature has a tooltip box, where you can give a summary of the features.', 'wrcpt'); ?></li>
                                    <li><?php _e('<b>Pricing Column Colors: </b>Here you can select colors separately for various parts of the pricing table by the help of ColorPicker. It is divided into three parts, which will help you to select the color easily for every parts of the pricing table.', 'wrcpt'); ?></li>
                                </ul>
                            </li><br>
							<li><?php _e('There\'s a trash icon in the upper right side of each column. Click the icon to delete the column. There is a switch on-off icon next to the each trash icon. To disable / hide a column instead of deleting click on this icon. Later, the column can be activated again for re-display by clicking on that icon.', 'wrcpt'); ?></li>
							<li><?php _e('At the bottom you will get Pricing Table Settings where you can set up the table more proficiently. There are four sections here', 'wrcpt'); ?>:<br><br>
                            	<ul>
									<li><?php _e('<b>General Settings: </b>To display the pricing table in your blog posts / pages enable the <strong>Enable Pricing Table</strong> checkbox by marking it. Some other options of the pricing table need to be enable for displaying the options.', 'wrcpt'); ?></li>
                                    <li><?php _e('<b>Structural Settings: </b>In this section you have to set up the structure of the pricing table, like width, height and margin. You can set the container width that indicate the total width of pricing table. Leaving blank will set the container width at 100% by default. You can also set the number of columns in each row, maximum of six columns is recommended. By disabling the auto column width you can set space between each column, and if the caption column is enabled, then you can also set the caption column width.', 'wrcpt'); ?></li>
                                    <li><?php _e('<b>Font Settings: </b>In this section you can assign font sizes of the pricing table.', 'wrcpt'); ?></li>
                                    <li><?php _e('<b>Advanced Color Settings: </b>Column Shadow Color and Shadow Highlighted Color can be set here.', 'wrcpt'); ?></li>
                                </ul>
                            </li><br>
							<li><?php _e('After completing all the necessary fields for your pricing column click on <strong>Add Package</strong> button. Later, you can <strong>Edit</strong>, <strong>Delete</strong> and <strong>Reorder</strong> pricing columns and features at any time. For each pricing table you will get <strong>SHORTCODE</strong> with different pricing table ID, which will be generated automatically. Now, paste the shortcode in your posts or pages where you would like to show pricing table and publish. Visit the newly created post/page to see the magic!', 'wrcpt'); ?> :)</li>
						</ol>
					</div>
				</div>
			</div><!-- End poststuff -->
			<hr>
			<div class="borderTop">
				<div class="last">
					<p class="prepend-top append-1"><?php _e('Thank you so much for using this plugin. If you have any questions that are beyond the scope of this help guide, please feel free to contact with us at <a href="https://wordpress.org/support/plugin/wrc-pricing-tables" target="_blank">WordPress Support Threads</a>. We&#39;d be happy to help you. If you like our plugin, you can rate our plugin with your valubale <a target="_blank" href="https://wordpress.org/support/plugin/wrc-pricing-tables/reviews/?filter=5/#new-post">&#9733;&#9733;&#9733;&#9733;&#9733;</a> reviews.', 'wrcpt'); ?></p>
				</div>
			</div><!-- end borderTop -->

		</div><!-- End wrcusage-maincontent -->
	</div><!-- End postbox-container -->
	<?php wrcpt_sidebar(); ?>
</div><!-- End wrap -->