﻿=== WRC Pricing Tables ===
Contributors: realwebcare
Requires at least: 3.5
Tested up to: 5.7.2
Tags: responsive pricing table, pricing table, price comparison chart, price table, css3 pricing table, pricing package, price plan, pricing box, table, wordpress table, table price, pricing table wordpress
Stable Tag: 2.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Responsive CSS3 pricing tables design to present features and prices of different products. Display pricing tables or comparison table by shortcode.


== Description ==

WRC Pricing Tables is a free CSS3 pricing table plugin with modern and clean design style. Moreover, it is super responsive as well. You can present features and prices of different products in two different ways:

* By displaying the features of each product separately, or
* By comparing the features of each product through a comparison pricing tables.

You can create feature rows and package columns as many as you want. Moreover, you can rearrange row and columns by drag and drop sorting. Also, the pricing table has support for ribbon, tooltip, tick/cross icon, button and many more...
 
The plugin has also support for WordPress default Color Picker. The great thing is that we have improved default 8 color palettes to 18 palettes. As a result, you can make an impressive and more colorful price table for your business.


== Instructions after upgrading to version 2.2.7 ==

<blockquote>
In previous versions, when a user deleted a table, the ID number of the shortcode for the rest of the tables auto rearranged. As a result, users also need to change the shortcode ID(s) they were using in a page for their pricing tables. That's why we updated the shortcode system and now shortcode ID(s) will not changed on deleting a table. After updating to the new version, you only need to click on Regenerate Shortcode button to generate shortcode ID(s) for all pricing tables.
</blockquote>


== Instructions after upgrading to version 2.2.4 ==

<blockquote>
We have updated the plugin a lot in version 2.2.4 to make the pricing table more professional. Therefore, we are requesting you to update your pricing table packages and features for once from "Edit Columns" and "Edit Features" section. After that, everything will be shown as it was except few things. You may have to reset column space (in %) and for caption column, you have to set the column width (in %).
</blockquote>


== Pricing Table DEMO ==

Free Version **[DEMO](https://www.realwebcare.com/wrc-pricing-table/)**
Standard Version **[DEMO](https://www.realwebcare.com/demo?theme=CCWR-PricingTable)**
Ultimate Version **[DEMO](https://www.realwebcare.com/demo?theme=WRC-pricing-tables)**


<blockquote>
<h4>Upgrade to Premium</h4>
<p>The premium version includes lots of new css3 table designs, features, customization options with dedicated support.</p>
<p><strong>Main features of the premium WRC Pricing Tables:</strong></p>
<ul>
<li>50+ ready-made templates to create pricing table instantly (Ultimate Version)</li>
<li>Feature Categorization (Ultimate Version)</li>
<li>Option to add Star Ratings (Ultimate Version)</li>
<li>Make a copy of any existed pricing table instantly</li>
<li>Import/Export (Backup) pricing table from one website to another</li>
<li>PayPal Button Integration</li>
<li>Pricing Toggles (Switch between monthly-yearly pricing plans or between currencies or anything else...)</li>
<li>Hide empty features as well as any part of the pricing table</li>
</ul>
<p><a href="https://www.realwebcare.com/item/clean-css3-wordpress-responsive-pricing-table/">View more Premium features</a></p>
</blockquote>


== What's new in version 2.3 ==

* 6 ready-made templates to create pricing table instantly
* 18 color palettes to the default WordPress Color Picker


== Other Features ==

* Responsive For All Devices
* Easy To Use
* Unlimited Background Colors
* Unlimited Rows & Columns
* In-depth customization menus for every single option in your tables.
* Simple animations such as enlarging columns upon hovering or disabling shadows when highlighted.
* Switch between templates without losing data
* Highlight one or more columns as special by enlarging
* Hide one or more columns instead of deleting them
* Option to set column shadow and shadow color
* Option to set price unit or currency at right side
* Option to set button body height as well as button width and height
* Column width will be auto adjusted according to column space
* Sortable Columns & Feature Items
* CSS3 Tooltips & Ribbons
* Tick/Cross Icon
* Implement by Shortcode
* All Major Browser Supported


== Video Demo ==

Watch the video demo of the plugin (Pro version):

= Standard Version DEMO =
[youtube https://www.youtube.com/watch?v=LBLOFOhcNlw]

= Ultimate Version DEMO =
[Click to Watch Ultimate Version](https://www.youtube.com/watch?v=agXszKqFeOs)


== Credits ==

* Developed By: [Realwebcare](https://www.realwebcare.com/)
* [Facebook Page](https://www.facebook.com/realwebcare/)


== Changelog ==

= 2.3 (27th May 2021) =
* Added 1 more ready-made pricing table templates
* Fixed some css &amp; js issue

= 2.2.9 (5th September 2020) =
* Added 2 more ready-made pricing table templates
* Improved default 8 color palettes to 18 palettes.
* Fixed image loader jQuery issue

= 2.2.8 (12th August 2020) =
* Fixed a jQuery issue to make compatible with WP 5.5

= 2.2.7 (18 October, 2019) =
* Shortcode ID will not be reset if one of the tables is deleted.

= 2.2.6 (3 May, 2019) =
* Click to clipboard executed for shortcode.

= 2.2.5 (4 April, 2019) =
* Fixed bullet issue in li tag.
* Browser’s default tooltip has been disabled.

= 2.2.4 (1 November, 2018) =
* 15th release.
* 3 ready-made templates to create pricing table instantly
* Switch between templates without losing data
* Highlight one or more columns as special by enlarging
* Hide one or more columns instead of deleting them
* Option to set column shadow and shadow color
* Option to set price unit or currency at right side
* Option to set button body height as well as button width and height
* Column width will be auto adjusted according to column space

= 2.2.3 (16 February, 2018) =
* 14th release.
* Change H1 tag to H2 tag for SEO
* Fixed some other minor bugs.

= 2.2.2 (29 September, 2017) =
* 13th release.
* Compatibility with WordPress 4.8.2

= 2.2.1 (28 April, 2016) =
* 12th release.
* Compatibility with WordPress 4.5.2

= 2.2 (10 April, 2016) =
* 11th release.
* Fixed Ribbon Bug

= 2.1.1 (27 March, 2016) =
* 10th release.
* Fixed Feature Column Width Bug

= 2.1 (22 March, 2016) =
* 9th release.
* Text Align Option Added For Feature Items.

= 2.0.2 (20th February, 2016) =
* 8th release.

= 2.0.1 (14th February, 2016) =
* 7th release.

= 2.0 (10th February, 2016) =
* 6th release.
* Title, Price and Feature body height has been added.
* Tooltip width has been added.
* Feature value font size has been added

= 1.4 (3rd February, 2016) = 
* 5th release.
* Collapse All/Expand All option added in admin panel.
* Open link in new tab option added.

= 1.3 (15th December, 2015) = 
* 4th release.
* Release Premium Version

= 1.2 (12th June, 2015) = 
* 3rd release.

= 1.1 (8th May, 2015) = 
* 2nd release.
* Fixed space between columns issue.
* Added an option to allow setting column-width.

= 1.0 (6th May, 2015) = 
* 1st release.


== Installation ==

Installing WRC Pricing Tables Plugin is as simple as installing any other WordPress Plugin

1. Log In as an Admin on your WordPress blog.
2. In the menu displayed on the left, there is a "Plugins" tab. Click it.
3. Now click "Add New".
4. There, you have the buttons: "Upload Plugin". Click it.
5. Upload the wrc-pricing-tables.zip file and click Install Now button.
6. After the installation is finished, click Activate Plugin.
7. Go to: Pricing Tables >> Guide for the instruction.


== License ==

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


== Screenshots ==

1. Pricing Table Example with 4 columns
2. Pricing Comparison Table Example
3. Blue Pricing Table with 4 columns
4. Colorful Pricing Table with 4 columns
5. List of Pricing Tables.
6. Pricing Table Details Section.
7. Pricing Table Colors Section.
8. Pricing Table Settings.