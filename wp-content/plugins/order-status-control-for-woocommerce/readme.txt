=== Order Status Control for WooCommerce ===
Contributors: brightvesseldev, niloybrightvessel
Tags: WooCommerce, order, complete, status control, autocomplete,virtual, paypal, stripe
Requires at least: 4.8
Tested up to: 5.7.2
Requires PHP: 7.0.0
Stable Tag: 0.1
License: GPLv2 or later

Auto Complete orders for virtual-downloadable products after successful payment or predefine status.

== Description ==

Order Status control is a WooCommerce Extension which helps shop admin to complete order automatically.
By default WooCommerce is auto complete "downloadable" products but not "Virtual" products.
with help of this plugin it's auto complete virtual products as well.

it's also comes with 2 more additional cases. here is the all case details below:

* <strong>Default:</strong> Default WooCommerce Order method.
* <strong>All Orders which content only Virtual Products:</strong> Product which doesn’t have any other physical products and only have Virtual product will mark as completed.
* <strong>All Orders which have Paid Successfully:</strong> All orders (along with physical products) which successfully paid the order payment will mark as completed.
* <strong>All Orders:</strong> No matter what is the order content or has (payment complete, virtual, physical, offline payments, etc.) as soon as a customer land on the thank you page the order status will be changed to complete.

Order Status Control option will be found under WooCommerce > settings > General Tab

## 🔥 WHAT’S NEXT ##

If you like this order status control plugin, then consider checking out our other free plugins:

[Preorder for WooCommerce](https://wordpress.org/plugins/pre-orders-for-woocommerce/) – Ultimate Preorders Plugin for WooCommerce.


## 🔥 SOME OF OUR PREMIUM PLUGINS ##

[Additional Variation Images for WooCommerce](https://brightplugins.com/additional-variation-images-for-woocommerce/)
[Min/Max Quantities for WooCommerce](https://brightplugins.com/min-max-quantities-for-woocommerce-review/)
[Preorders for WooCommerce PRO](https://brightplugins.com/woocommerce-preorder-plugin-review/)

<br>
Plugin icon made by <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>

== Installation ==
#### Step-By-Step Instructions 
- Go to the WordPress Dashboard "Add New Plugin" section.
- Search For "Order Status Control".
- Install it by clicking the "Install Now" button.
- When installation finishes, click "Activate Plugin" button.

== Frequently Asked Questions ==
 


== Screenshots ==

== Changelog ==

= 0.1 =
* Beta Initial release