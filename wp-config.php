<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'a2000dem_wp35' );

/** MySQL database username */
define( 'DB_USER', 'a2000dem_wp35' );

/** MySQL database password */
define( 'DB_PASSWORD', 'H.ScF6jqrHod07nKzGT33' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'jQviAk4hOv9XsTbDBaJV9rcwyoPSQyFf9ShodXPpvNG6g71dXIG6Bn7mWyku1v25');
define('SECURE_AUTH_KEY',  'nmkMk305SWS3a0Dw08GfC9cYooNMLQgu8h6qDakqgXvCYkXW8PZmr77MSvy2ElUp');
define('LOGGED_IN_KEY',    'sNbXCEYtS329YRR8vIQNKHWhjjL5I5nFrCtbVDAzdnKpqwg20urYixdVeDjbjvqT');
define('NONCE_KEY',        'czTbpe6nwU0yU0CD3eq3GmGCld4SuP9C3I1mToV57Fj0zm3095hoHltWmBADV26F');
define('AUTH_SALT',        '5PCH5SabQKI8WV5zCgzoaKWLKdrOyZyUgXLsma2oMtBWEfyC6LPrmJSUWbUjbPC1');
define('SECURE_AUTH_SALT', 'V7c6THxEgWT3yMjRABW249759lTjkkFgTXIs7dEYwoij5nRRS5pjZ4kdCK9ekBcV');
define('LOGGED_IN_SALT',   'K1U100bZzwo6TSTVAD5oaBri6VoFdzgKrRzz2AVKSQ9fRmHPqblKtxjSzOZrCpYR');
define('NONCE_SALT',       'DzlnELbIXQfjTRogEEjURQ2LUXoYMJ6nRncnzVC1pJlhqaWDPrBgVkGkb6cS8kgg');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');
define('FS_CHMOD_DIR',0755);
define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed externally by Installatron.
 * If you remove this define() to re-enable WordPress's automatic background updating
 * then it's advised to disable auto-updating in Installatron.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
